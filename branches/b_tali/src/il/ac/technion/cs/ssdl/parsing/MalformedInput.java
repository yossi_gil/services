package il.ac.technion.cs.ssdl.parsing;

public abstract class MalformedInput extends Throwable {

	public MalformedInput() {
	    super();

    }

	public MalformedInput(String message, Throwable cause) {
	    super(message, cause);

    }

	public MalformedInput(String message) {
	    super(message);

    }

	public MalformedInput(Throwable cause) {
	    super(cause);

    }

	public void report() {
	    System.err.println(getMessage());
	    
    }

}
