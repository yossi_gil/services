package il.ac.technion.cs.ssdl.files;

import il.ac.technion.cs.ssdl.files.FileSystemVisitor.Action;
import il.ac.technion.cs.ssdl.files.FileSystemVisitor.Action.StopTraversal;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il> Mar 29, 2007
 */
public class FindClassFile {
	static final String name = "where";

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.printf("Usage: %s class [class*]\n", name);
			System.exit(1);
		}
		for (final String arg : args) {
			if (processOption(arg))
				continue;
			try {
				new FileSystemVisitor(new String[] { "C:/" }, new Searcher(arg + ".class"), ".class").go();
				new FileSystemVisitor(CLASSPATH.asArray(), new Searcher(arg + ".class"), ".class").go();
			} catch (final IOException e) {
				System.err.println(e.getMessage());
			} catch (final StopTraversal e) {
				continue;
			}
		}
	}

	static boolean reportCounts;
	static boolean findFirstOnly;

	private static boolean processOption(String arg) {
		if (arg.equals("-n")) {
			reportCounts = true;
			return true;
		}
		if (arg.equals("-f")) {
			findFirstOnly = true;
			return true;
		}
		return false;
	}

	/**
	 * An {@Action} which counts the various kinds of file system entities it
	 * encounters during the search.
	 * 
	 * @author Yossi Gil <yogi@cs.technion.ac.il> 21/05/2007
	 */
	public static class Searcher implements Action {
		private int directories = 0;
		private int files = 0;
		private int zips = 0;
		private int entries = 0;
		private final String sought;

		public Searcher(final String sought) {
			this.sought = sought;
		}

		private void report(String s) {
			if (FindClassFile.reportCounts)
				System.out.println(s + ". " + directories + " directories, " + files + " class files, " + zips + " ZIP archives, " + entries
				        + " entries.");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitDirectory(java.io.File)
		 */
		public void visitDirectory(File f) {
			directories++;
			report("Directory: " + f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitFile(java.io.File)
		 */
		public void visitFile(File f) throws StopTraversal {
			files++;
			report("File: " + f.getAbsolutePath());
			check(f.getName(), f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitZip(java.io.File)
		 */
		public void visitZip(File f) {
			zips++;
			report("Archive: " + f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitZipEntry(java.lang.String,
		 *      java.lang.String, java.io.InputStream)
		 */
		public void visitZipEntry(String zipName, String entryName, @SuppressWarnings("unused") InputStream stream) throws StopTraversal {
			entries++;
			report("Archive entry: " + entryName);
			check(entryName, zipName);
		}

		private void check(String file, String directory) throws StopTraversal {
			if (!file.endsWith(sought))
				return;
			System.out.printf("%s: %s\n", file, directory);
			if (FindClassFile.findFirstOnly)
				throw new StopTraversal();
		}

		public void visitZipDirectory(String zipName, String entryName, @SuppressWarnings("unused") InputStream stream) {
			report("Archive directory: " + entryName + " in zip " + zipName);
		}
	}
}
