package il.ac.technion.cs.ssdl.utils; import il.ac.technion.cs.ssdl.csv.CSV_Test;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;

public class Utils_Test {
	public static Test suite() {
		return new JUnit4TestAdapter(CSV_Test.class);
	}
}
