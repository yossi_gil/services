package il.ac.technion.cs.ssdl.csv;

import static il.ac.technion.cs.ssdl.utils.DBC.require;
import il.ac.technion.cs.ssdl.utils.Separator;
import java.util.Map;
import java.util.TreeMap;

public class CSVLine {
	Map<String, String> map = new TreeMap<String, String>();

	public void put(final String key) {
		require(key != null);
		map.put(key, "");
	}

	public void put(final String key, final float value) {
		require(key != null);
		put(key, "" + value);
	}

	public void put(final String key, final double value) {
		require(key != null);
		put(key, "" + value);
	}

	public void put(final String key, final int value) {
		require(key != null);
		put(key, "" + value);
	}

	public void put(final String key, final Object value) {
		require(key != null);
		if (value == null)
			put(key);
		else
			map.put(key, value.toString());
	}

	public void put(final String key, final Object a[], final int i) {
		require(key != null);
		put(key, a == null || i < 0 || i >= a.length ? null : a[i]);
	}

	public void put(final String key, final Object[] os) {
		require(key != null);
		put(key, os == null ? null : Separator.separateBy(";", os));
	}

	public String line() {
		String $ = "";
		final Separator s = new Separator(delimiter);
		for (final String k : map.keySet())
			$ += s + quote(map.get(k));
		return $;
	}

	public String header() {
		String $ = "";
		final Separator s = new Separator(delimiter);
		for (final String k : map.keySet())
			$ += s + quote(k);
		return $;
	}

	static final String quote = "\"";
	static final String delimiter = ",";

	public static String quote(final String s) {
		return quote + s.replaceAll(quote, quote + quote) + quote;
	}

	public static String makeLine(final Object... vs) {
		final StringBuffer $ = new StringBuffer(1000);
		final Separator comma = new Separator(",");
		for (final Object v : vs)
			$.append(comma).append(quote(v.toString()));
		return $.toString();
	}
}