package il.ac.technion.cs.ssdl.parsing;

public class GeneralFailure extends MalformedInput {
	public GeneralFailure(String message) {
		super(message);
	}
}
