package il.ac.technion.cs.ssdl.parsing;

import il.ac.technion.cs.ssdl.utils.IO;
import java.io.*;
import java.util.List;

/**
 * Protocol of an object that represents a resource from which textual data can
 * be retrieved. This interface provides client code with a uniform way for
 * reading data from various sources, e.g., files, the standard input stream,
 * URL connections, etc.
 * 
 * @author imaman
 * 
 */
public interface InputSource extends Comparable<InputSource> {
	/**
	 * Obtain the name of the this resource. The returned string describes the
	 * resource (e.g., file name). This name is <b>not</b> guaranteed to be
	 * unique. A resource may be unnamed if it represents an unnamed content,
	 * such as an arbitrary <code>java.io.Reader</code> object.
	 * 
	 * @return Name of the resource. Retruns an empty string if this is an
	 *         unnamed resource.
	 * @see #id()
	 */
	public String name();

	/**
	 * Obtain a unique id of this object. Note that it is possbile that two
	 * instances that refer to the same physical resource (e.g.: a file) will
	 * have different id values.
	 * <p>
	 * This id value is used primarily for sortring purposes.
	 * 
	 * @return a unqiue integer id associated with this object.
	 */
	public int id();

	/**
	 * Obtain a reader object over the contents of the underlying physical
	 * resource.
	 * 
	 * @return A Reader object
	 * @throws FileNotFoundException
	 *         If a reader could not be obtained
	 * @throws IOException 
	 */
	public Reader reader() throws FileNotFoundException, IOException;

	/**
	 * Read the contents of the resource. Successive ivnocations of this methods
	 * return the same result. This method will block if no characters are
	 * avaialble in the underyling physical resource.
	 * 
	 * @return Contents of the resource as a single String object
	 * 
	 * @throws IOException
	 */
	public String text() throws IOException;

	/**
	 * Obtain the contents of this resource as an list of lines
	 * 
	 * @return List of strings, each string is a line.
	 * @throws IOException
	 */
	public List<String> lines() throws IOException;

	/**
	 * A factory for standard {@link InputSource} objects.
	 */
	public enum Factory {
		;
		/**
		 * Create a {@link InputSource} object that obtains its contents from
		 * the specified reader.
		 * 
		 * @param r
		 *        A reader object
		 * @return A new InputResource
		 */
		public static InputSource make(Reader r) {
			return new Implementation("", r);
		}

		/**
		 * Create a {@link InputSource} object that obtains its contents from
		 * the specified file. The file name is translated into an abolute path
		 * so the resulting InputResource will be indifferent to changes in the
		 * current working directory.
		 * 
		 * @param fileName
		 *        A file name
		 * @return A new InputResource
		 */
		public static InputSource newFile(String fileName) {
			return make(new File(fileName).getAbsoluteFile());
		}

		/**
		 * Create a {@link InputSource} object that obtains its contents from
		 * the specified file. The file name is translated into an abolute path
		 * so the resulting {@Link InputResource} will be indifferent to changes
		 * in the current working directory.
		 * 
		 * @param f
		 *        A file object
		 * @return A new InputResource
		 */
		public static InputSource make(final File f) {
			return new Implementation(f.getName(), null) {
				@Override public Reader reader() throws FileNotFoundException {
					return new FileReader(f.getAbsoluteFile());
				}
			};
		}

		/**
		 * Create a {@link InputSource} object that obtains its contents from
		 * the given string.
		 * 
		 * @param s
		 *        String specifying the contents of the created InputResource.
		 * @return A new InputResource.
		 */
		public static InputSource make(String s) {
			return new Implementation("", new StringReader(s));
		}

		// /**
		// * A predefined {@link InputResource} object whose underlying physical
		// * resource is <code>System.in</code>
		// */
		// public static final InputResource stdin = new
		// StandardResourceImpl("/STDIN/", null) {
		// @Override public Reader reader() {
		// return new InputStreamReader(System.in);
		// }
		// };
		private static class Implementation implements InputSource {
			private String name;
			private String text = null;
			private final Reader reader;
			private static int counter = 0;
			private final int id;
			private List<String> lines = null;

			public Implementation(String name, Reader r) {
				id = counter++;
				this.name = name == null ? "" : name;
				reader = r;
			}

			@Override public String toString() {
				return id + ":" + name;
			}

			@Override public boolean equals(Object o) {
				if (o == null)
					return false;
				if (!this.getClass().equals(o.getClass()))
					return false;
				final Implementation that = (Implementation) o;
				return compareTo(that) == 0;
			}

			public int compareTo(InputSource that) {
				return id() - that.id();
			}

			public int id() {
				return id;
			}

			public String name() {
				return name;
			}

			public Reader reader() throws IOException{
				if (text != null)
					return new StringReader(text);
				text = IO.toString(reader);
				return new StringReader(text);
			}

			public String text() throws IOException {
				if (text != null)
					return text;
				text = IO.toString(reader());
				return text;
			}

			public List<String> lines() throws IOException {
				if (lines != null)
					return lines;
				lines = IO.lines(text());
				return lines;
			}

			public void changeName(String newName) {
	           name = newName;
	            
            }
		}
	}

	public void changeName(String newName);
}
