package il.ac.technion.cs.ssdl.utils; import static il.ac.technion.cs.ssdl.utils.DBC.require;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class Misc {
	public static <T> T[] duplicate(T[] ts) {
		final Class<?> c = ts.getClass().getComponentType();
		@SuppressWarnings("unchecked") final T[] $ = (T[]) java.lang.reflect.Array.newInstance(c, ts.length);
		System.arraycopy(ts, 0, $, 0, ts.length);
		return $;
	}

	public static boolean[] complement(boolean[] bs) {
		final boolean[] $ = new boolean[bs.length];
		for (int i = 0; i < bs.length; ++i)
			$[i] = !bs[i];
		return $;
	}

	public static boolean[] toArray(List<Boolean> bs) {
		final boolean[] $ = new boolean[bs.size()];
		for (int i = 0; i < bs.size(); ++i)
			$[i] = bs.get(i).booleanValue();
		return $;
	}

	public static <T, S extends T> void addAll(Collection<T> dest, Iterable<S> src) {
		for (final S s : src)
			dest.add(s);
	}

	public static <E, T extends Collection<E>> T add(T dest, E[] src) {
		for (final E e : src)
			dest.add(e);
		return dest;
	}

	// public static<T> T[] toArray(T... ts) { return ts; }
	@SuppressWarnings("unchecked") public static <T> T[] toArray(T t, T... ts) {
		require(t != null);
		final T[] $ = (T[]) Array.newInstance(t.getClass(), ts.length + 1);
		$[0] = t;
		for (int i = 0; i < ts.length; ++i)
			$[i + 1] = ts[i];
		return $;
	}

	public static String compaq(String s) {
		if (s == null)
			return null;
		final char chars[] = s.toCharArray();
		String $ = "";
		for (final char c : chars)
			if (" \t\r\n".indexOf(c) < 0)
				$ += c;
		return $;
	}

	public static boolean compareWithStream(String str, InputStream is) {
		final Scanner actual = new Scanner(str);
		final Scanner expected = new Scanner(is);
		while (true) {
			if (actual.hasNext() != expected.hasNext())
				return false;
			if (!actual.hasNext())
				return true;
			final String a = actual.nextLine().trim();
			final String b = expected.nextLine().trim();
			if (!a.equals(b)) {
				System.err.println("a=" + a);
				System.err.println("b=" + b);
				return false;
			}
		}
	}
}
