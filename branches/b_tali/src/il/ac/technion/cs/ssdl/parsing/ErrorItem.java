
package il.ac.technion.cs.ssdl.parsing; 
/**
 * Protocol of an object that provides the details about a single
 * compiler/parser error.
 * 
 */
public final class ErrorItem implements Comparable<ErrorItem> {
	private final Location location;
	private final String message;
	private final String sourceCode;
	
	/**
	 * Initialize a new instance with the given values.
	 * 
	 * @param location
	 *        Location where the error occured
	 * @param message
	 *        Error message
	 * @param sourceCode
	 *        Soruce code fragment
	 */
	public ErrorItem(Location location, String message, String sourceCode) {
		this.location = location;
		this.message = message;
		this.sourceCode = sourceCode;
	}




	/**
	 * Obtain the source code fragment which triggered the error
	 * 
	 * @return String
	 */

    public String getSourceCode() {
    	return sourceCode;
    }


	/**
	 * Obtain the location where the error occured
	 * 
	 * @return A {@link Location} object
	 */
    public Location getLocation() {
    	return location;
    }

	/**
	 * Obtain the error message
	 * 
	 * @return A string explaining the error
	 */
    public String getMessage() {
    	return message;
    }


    
    @Override public String toString() {
		int top = sourceCode.length();
		int len;
		if (location.end.line == location.begin.line)
			len = location.end.column - location.begin.column;
		else {
			final int nl = sourceCode.indexOf('\n');
			if (nl >= 0)
				top = nl;
			len = top - location.begin.column;
		}
		top = Math.min(LIMIT_LINE, sourceCode.length());
		len = Math.max(1, len);
		final String fragment = sourceCode.substring(0, top);
		final StringBuilder sb = new StringBuilder();
		for (int i = 1; i < location.begin.column + len + 1; ++i)
			if (i > location.begin.column)
				sb.append('^');
			else
				sb.append(' ');
		String file = location.enclosingFile();
		if (file.length() > 0)
			file += ": ";
		return file + "Line " + (location.begin.line + 1) + ": " + message + "\n" + fragment + "\n" + sb.toString();
	}

	@Override public int hashCode() {
		return location.hashCode() ^ message.hashCode();
	}

	public String where() {
		return "(" + (location.begin.line + 1) + ":" + (location.begin.column + 1) + ")..(" + (location.end.line + 1) + ":"
		        + (location.end.column + 1) + ")";
	}

	public static final int LIMIT_LINE = 60;
	

	/**
	 * {@inheritDoc}
	 */
	public int compareTo(ErrorItem that) {
		final int $ = getLocation().compareTo(that.getLocation());
		if ($ != 0)
			return $;
		return getMessage().compareTo(that.getMessage());
	}

	@Override public boolean equals(Object other) {
		if (other == null)
			return false;
		if (!other.getClass().equals(this.getClass()))
			return false;
		return compareTo((ErrorItem) other) == 0;
	}
}