
package il.ac.technion.cs.ssdl.parsing; import il.ac.technion.cs.ssdl.utils.Iterables;
import il.ac.technion.cs.ssdl.utils.Pair;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This class resprents an atbitrary annotation that may be attached to
 * definition in a program. This class is simpler than Java annotations, in
 * particular, its content is a set of pairs of key,value which can only be
 * strings.
 * <p>
 * 
 * @author imaman Aug 7, 2007
 */
public class BasicAnnotation implements Serializable {
	/**
	 * A compilation-unit wide unique id
	 */
	public final int id;
	/**
	 * A set of key,value pairs, making up the contents of this annotation
	 */
	public final Map<String, String> map = new HashMap<String, String>();
	/**
	 * The source code location where this annotation was defined
	 */
	public final Location location;

	public BasicAnnotation(int id, Location l, Iterable<Pair<String, Location>> lines) throws AnnotationError {
		this.id = id;
		location = l;
		final Map<String, StringBuilder> temp = new HashMap<String, StringBuilder>() {
			private static final long serialVersionUID = 0L;

			@Override public StringBuilder get(Object key) {
				if (!(key instanceof String))
					return null;
				String stringKey = (String) key;
				StringBuilder $ = super.get(key);
				if ($ != null)
					return $;
				$ = new StringBuilder(100);
				put(stringKey, $);
				return $;
			}
		};
		for (final Pair<String, Location> pair : lines) {
			final String curr = pair.a;
			final int index = findSplit(curr, pair.b);
			final String key = curr.substring(0, index).trim();
			final String value = curr.substring(index + 1);
			temp.get(key).append(value);
		}
		for (final Map.Entry<String, StringBuilder> e : temp.entrySet())
			map.put(e.getKey(), e.getValue().toString().trim());
	}

	private static int findSplit(String s, Location l) throws AnnotationError {
		final int p = s.indexOf(":");
		final int q = s.indexOf("=");
		if (p < 0 && q < 0)
			throw new AnnotationError(l);
		if (p < 0)
			return q;
		if (q < 0)
			return p;
		return Math.min(p, q);
	}

	@Override public int hashCode() {
		return id;
	}

	@Override public boolean equals(Object other) {
		if (other == null)
			return false;
		if (this == other)
			return true;
		if (!this.getClass().equals(other.getClass()))
			return false;
		final BasicAnnotation that = (BasicAnnotation) other;
		return id == that.id && map.equals(that.map);
	}

	@Override public String toString() {
		return id + "\n  " + Iterables.toString(map.entrySet(), "\n  ") + "\n";
	}

	public static class AnnotationError extends Exception {
		private static final long serialVersionUID = -7530057884326501280L;
		public final Location location;

		public AnnotationError(Location l) {
			super("An annotation line must have an equals sign, \"=\", which separates the key from the value");
			location = l;
		}
	}
}
