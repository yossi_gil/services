
package il.ac.technion.cs.ssdl.reflect; import java.util.HashMap;
import java.util.Map;

/**
 * @author Yossi 28/07/2007
 */
public class Primitives {
	public static Map<String, Class<?>> value = new HashMap<String, Class<?>>();
	static {
		value.put("byte", byte.class);
		value.put("short", short.class);
		value.put("int", int.class);
		value.put("long", long.class);
		value.put("float", float.class);
		value.put("double", double.class);
		value.put("char", char.class);
		value.put("boolean", boolean.class);
		value.put("void", void.class);
	}
}
