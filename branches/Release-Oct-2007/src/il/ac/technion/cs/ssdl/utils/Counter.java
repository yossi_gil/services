
package il.ac.technion.cs.ssdl.utils; 
/**
 * A simple counter class.
 * 
 * @author imaman Jul 30, 2007
 */
public class Counter {
	private int value = -1;

	public int next() {
		return value++;
	}

	@Override public String toString() {
		return "last index=" + value;
	}
}
