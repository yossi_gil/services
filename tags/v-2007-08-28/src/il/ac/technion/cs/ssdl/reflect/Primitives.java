/**
 * 
 */
package il.ac.technion.cs.ssdl.reflect;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yossi
 * @date 28/07/2007
 */
public class Primitives {
	public static Map<String, Class<?>> sample = new HashMap<String, Class<?>>();
	static {
		sample.put("byte", byte.class);
		sample.put("short", short.class);
		sample.put("int", int.class);
		sample.put("long", long.class);
		sample.put("float", float.class);
		sample.put("double", double.class);
		sample.put("char", char.class);
		sample.put("boolean", boolean.class);
		sample.put("void", void.class);
	}
}
