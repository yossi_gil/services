package il.ac.technion.cs.ssdl.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class) @SuiteClasses( { il.ac.technion.cs.ssdl.utils.Pair_Test.class, il.ac.technion.cs.ssdl.utils.Test_ClassPathInfo.class,
        il.ac.technion.cs.ssdl.utils.Test_Stringer.class, il.ac.technion.cs.ssdl.csv.CSVLineTest.class, il.ac.technion.cs.ssdl.csv.CSV_Test.class,
        il.ac.technion.cs.ssdl.files.Filename_Test.class, il.ac.technion.cs.ssdl.files.JRETest.class }) public class Services_Test {
	// Do nothing
}
