package il.ac.technion.cs.ssdl.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Set operations implemented as a statless class, with just static methods.
 */
public abstract class Sets {
	private Sets() {
		// Non instaniable
	}

	public static <E> Set<E> union(Collection<E> lhs, Collection<E> rhs) {
		final Set<E> r = new HashSet<E>(lhs);
		r.addAll(rhs);
		return r;
	}

	public static <E> Set<E> intersection(Collection<E> lhs, Collection<E> rhs) {
		final Set<E> r = new HashSet<E>(lhs);
		r.retainAll(rhs);
		return r;
	}
}
