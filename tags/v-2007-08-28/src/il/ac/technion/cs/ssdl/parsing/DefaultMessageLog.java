/**
 * 
 */
package il.ac.technion.cs.ssdl.parsing;

import il.ac.technion.cs.ssdl.utils.Function;
import il.ac.technion.cs.ssdl.utils.IO;
import il.ac.technion.cs.ssdl.utils.Iterables;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A default implementation of {@link MessageLog}
 * 
 * @author imaman
 */
public class DefaultMessageLog implements MessageLog {
	private static final long serialVersionUID = 1000629616878797828L;
	private final List<ErrorItem> errors = new ArrayList<ErrorItem>();

	@Override public String toString() {
		if (errors.isEmpty())
			return "no errors";
		return errors.size() + " errors";
	}

	/**
	 * {@inheritDoc}
	 */
	public Iterator<ErrorItem> iterator() {
		Collections.sort(errors);
		return errors.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	public void recordError(Location l, String message, String sourceCodeFragment) {
		errors.add(new DefaultErrorItem(l, message, sourceCodeFragment));
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unused") public void recordWarning(Location l, String message) {
		// Do nothing
	}
   
   

   /**
    * {@inheritDoc}
    */
	public boolean hasErrors()
   {
      return errors.size() > 0;
   }

   /**
	 * {@inheritDoc}
	 */
	public void throwIfErrors() throws ErrorBag {
		if (errors.size() == 0)
			return;
		final Iterable<String> list = Iterables.map(this, new Function<ErrorItem, String>() {
			public String eval(ErrorItem item) {
				return item.toString();
			}
		});
		throw new ErrorBag(Iterables.toArray(this, ErrorItem.class), IO.concatLines(list));
	}
}