package il.ac.technion.cs.ssdl.files; import java.util.Collection;

/**
 * A class realizing a traversal of the file system, where the traversal is
 * restricted to <code>.java</code> files only
 * <p>
 * The traversal is carried out by calling one of the class constructors,
 * {@link #JavaFilesVisitor(Collection, il.ac.technion.cs.ssdl.files.FileSystemVisitor.NonStopAction)}
 * or
 * {@link #JavaFilesVisitor(String[], il.ac.technion.cs.ssdl.files.FileSystemVisitor.NonStopAction)}
 * to set the visitation range, and the {@link Action} to be carried for each
 * visited file.
 * <p>
 * 
 * @see JavaFilesVisitor
 * @author Yossi Gil 11/07/2007
 */
public class JavaFilesVisitor extends NonStopVisitor {
	public JavaFilesVisitor(String from, Action visitor, String[] extensions) {
		super(from, visitor, extensions);
	}

	private static final String[] javaFilesExtensions = new String[] { ".java" };

	public JavaFilesVisitor(Collection<String> from, NonStopAction action) {
		super(from, action, javaFilesExtensions);
	}

	public JavaFilesVisitor(String[] from, NonStopAction action) {
		super(from, action, javaFilesExtensions);
	}
}
