package il.ac.technion.cs.ssdl.utils;

public class Stopper {
	final long begin;
	final String what;
	private long time;
	private static boolean mute = false; 

	public Stopper() {
		this(null);
	}

	/**
	 * Silence all stoppers from now on.
	 */
	public static void mute() {
		mute = true;
	}
	/**
	 * Allow stoppers to print their output from now on.
	 */
	public static void unmute() {
		mute = false;
	}
	
	private static void out(String m) {
		if (!mute)
			System.err.println(m);
	}

	public Stopper(String what) {
		this.what = what;
		if (what != null)
	
				out("Started " + what);
		begin = System.currentTimeMillis();
	}

	public void stop() {
		time = System.currentTimeMillis() - begin;
		out("Finished " + what + ": " + time + "ms");
	}

	public long time() {
		if (time != 0)
			return time;
		time = System.currentTimeMillis() - begin;
		return time;
	}
}
