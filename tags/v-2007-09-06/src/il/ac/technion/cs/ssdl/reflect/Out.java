package il.ac.technion.cs.ssdl.reflect;

import static il.ac.technion.cs.ssdl.utils.DBC.require;
import il.ac.technion.cs.ssdl.utils.Once;
import il.ac.technion.cs.ssdl.utils.Separator;
import java.util.Collection;

public class Out {
	final static int MAX_FIRST = 20;
	final static int MAX_LAST = 10;

	static public void out(String s) {
		System.out.print(s);
	}

	static public void out(String name, Object a) {
		if (a == null)
			System.out.printf("No %s\n", name);
		else
			System.out.printf("%s = %s\n", name, a);
	}

	static public void out(String name, int a) {
		System.out.printf("%s = %d\n", name, new Integer(a));
	}

	static public void out(String name, boolean v) {
		System.out.printf("%s = %b\n", name, new Boolean(v));
	}

	static public void out(String name, Object[] a) {
		require(name != null);
		if (a == null || a.length <= 0)
			System.out.printf("No %s\n", name);
		else if (a.length == 1)
			System.out.printf("Only 1 %s: %s\n", name, a[0]);
		else
			System.out.printf("Total of %d %s:\n\t%s\n", new Integer(a.length), name, Separator.separateBy("\n\t", a));
	}

	static public void out(String name, Collection<Object> a) {
		require(name != null);
		if (a == null || a.size() <= 0)
			System.out.printf("No %s\n", name);
		else if (a.size() == 1)
			System.out.printf("Only 1 %s: %s\n", name, a.iterator().next());
		else {
			System.out.printf("Total of %d %s:\n", new Integer(a.size()), name);
			int n = 0;
			final Once ellipsis = new Once("\t...\n");
			for (final Object o : a) {
				n++;
				if (n <= MAX_FIRST || n > a.size() - MAX_LAST)
					System.out.printf("\t%2d) %s\n", new Integer(n), o);
				else
					System.out.print(ellipsis);
			}
		}
	}
}
