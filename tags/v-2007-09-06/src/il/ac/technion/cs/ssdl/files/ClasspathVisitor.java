package il.ac.technion.cs.ssdl.files;

/**
 * A class realizing a file system traversal, similar to
 * {@link FileSystemVisitor}, except that the traversal is restricted to the
 * Java <em>classpath</em>,.
 * <p>
 * The traversal is carried out by calling the class constructor
 * {@link ClasspathVisitor#ClasspathVisitor(il.ac.technion.cs.ssdl.files.FileSystemVisitor.Action, String[])}
 * to set the tranversal actions, and then the invherited function {@link #go()}
 * to conduct the actual traversal.
 * <p>
 * 
 * @author Yossi Gil 11/07/2007
 */
public class ClasspathVisitor extends FileSystemVisitor {
	/**
	 * Create a new visitor object to scan the class path.
	 * 
	 * @param action
	 *        a non-<code><b>null</b></code> specifying what to do in each
	 *        visited file
	 * @param extensions
	 *        an array of non-<code><b>null</b></code> {@link String}s
	 *        specifying which file extensions the visitor shold call, e.g.,
	 *        ".class", ".csv", etc. If this parameter is
	 *        <code><b>null</b></code>, or of length 0, or contains a
	 *        {@link String} of length 0, then the visitor is invoked for all
	 *        files found in the supplied path.
	 * @see #go()
	 * 
	 */
	public ClasspathVisitor(Action action, String... extensions) {
		super(CLASSPATH.asArray(), action, extensions);
	}
}
