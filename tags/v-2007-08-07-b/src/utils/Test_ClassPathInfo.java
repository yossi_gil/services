package utils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Ignore;
import org.junit.Test;
import utils.files.Filename;
import utils.files.JRE;
import utils.testing.Assert;
import utils.testing.TesterOf;

/**
 * A class to test {@link ClassPathInfo} including path names with funny UNICODE
 * characters. If some of the text here is unreadable, you should change
 * encoding to UTF-8. In Eclipse this is done through the following menu chain:
 * Project/Properties/Info/Text File Encoding. In other versions, your may try:
 * Window -> Preferences -> General -> Workspace // - > text file encoding. Then
 * choose "Other", "UTF-8"
 * 
 * @author Itay Maman
 * @date Jul 11, 2006
 */
@TesterOf(ClassPathInfo.class)//
public class Test_ClassPathInfo {
	@Test public void testGetClassesObject() {
		ClassPathInfo cpi = ClassPathInfo.makeJREClassPath();
		Collection<String> classes = cpi.getClasses();
		assertTrue(classes.contains("java.lang.Object"));
	}

	@Test public void ensureDotSeparatedNames() {
		List<File> fs = JRE.asList();
		List<File> prg = ClassPathInfo.fromClass(Test_ClassPathInfo.class);
		fs.addAll(prg);
		ClassPathInfo cpi = new ClassPathInfo(fs);
		Set<String> set = new HashSet<String>();
		set.addAll(cpi.getClasses());
		exists(set, Object.class);
		exists(set, String.class);
		exists(set, Class.class);
		exists(set, Test_ClassPathInfo.class);
		exists(set, Assert.class);
		exists(set, Test.class);
		exists(set, TesterOf.class);
		exists(set, MyClass.class);
		exists(set, המחלקהשלי.class);
	}

	private static void exists(Set<String> set, Class<?> clazz) {
		assertTrue("class=" + clazz, set.contains(clazz.getName()));
	}

	@Test @Ignore public void testGetClassesForName() {
		int total = 0;
		int loaded = 0;
		ClassPathInfo cpi = ClassPathInfo.makeJREClassPath();
		for (String s : cpi.getClasses()) {
			total++;
			loaded += check(s) ? 1 : 0;
		}
		assertTrue("I failed to load " + (total - loaded) + " classes. This indicates that something is" + "wrong with the environment",
		        loaded > total / 2);
	}

	/**
	 * Compare the reflection-based inofrmation with the one obtained from the
	 * Filename class.
	 * 
	 * @param s
	 *            Class name
	 * @return true if the class was successfully loaded
	 */
	private static boolean check(String s) {
		try {
			Class<?> c = Class.forName(s, false, Test_ClassPathInfo.class.getClassLoader());
			assertEquals(s, c.isAnonymousClass(), Filename.isAnonymous(s));
			assertEquals(s, c.isLocalClass(), Filename.isLocal(s));
			assertEquals(s, c.getEnclosingClass() != null, Filename.isInner(s));
			assertEquals(s, c.getCanonicalName() == null, Filename.isAllInner(s));
			if (c.isMemberClass())
				assertEquals(s, c.getCanonicalName(), Filename.name2Canonical(s));
			else
				assertEquals(s, c.getCanonicalName(), s);
			return true;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}

	@Test @Ignore public void testSpecialNames() {
		check(MyClass.class.getName());
		check(המחלקהשלי.class.getName());
	}

	//
	// Sample classes (serve as input values)
	//
	public static class MyClass {
		// No body
	}

	// 
	// This class has a HEBREW name.
	// In order to see it properly, change the encoding used by your editor.
	// E.g.:
	//
	public static class המחלקהשלי {
		// No body
	}

	public static void main(String[] args) {
		new Test_ClassPathInfo().testGetClassesForName();
	}
}
