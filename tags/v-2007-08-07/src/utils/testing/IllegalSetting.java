/**
 * 
 */
package utils.testing;

/**
 * An unchecked exception that indicates an incorrect setup of the testing
 * system
 * 
 * @author Itay Maman <imaman@cs>
 * @date Jul 5, 2007
 */
public class IllegalSetting extends RuntimeException {
	private static final long serialVersionUID = 4003511375101080709L;

	public IllegalSetting() {
		// Empty
	}

	public IllegalSetting(String message) {
		super(message);
	}

	public IllegalSetting(Throwable cause) {
		super(cause);
	}
}