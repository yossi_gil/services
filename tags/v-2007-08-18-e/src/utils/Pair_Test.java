package utils;

import org.junit.Assert;
import org.junit.Test;

public class Pair_Test
{
   @SuppressWarnings("all")
   public static class NamedPair extends Pair<Integer,Integer>
   {
      public final String name;
      public NamedPair(String name, Integer a, Integer b)
      {
         super(a, b);
         this.name = name;
      }
      
      public String toString()
      {
         return name + super.toString();
      }
      
      public boolean equals(Pair<Integer, Integer> o)
      {
         if(o == this)
            return true;
         
         if(o == null)
            return false;
         
         if(!(o instanceof NamedPair))
            return false;
         
         NamedPair that = (NamedPair) o;
         return this.a.equals(that.a) && this.b.equals(that.b) 
            && this.name.equals(that.name);
      }
   }
   
   @Test public void testSymmetry()
   {
      Pair<Integer,Integer> p1 = new NamedPair("a", 10, 20);
      Pair<Integer,Integer> p2 = new Pair<Integer,Integer>(10, 20);
      
      Assert.assertEquals(p1.equals(p2), p2.equals(p1));
   }
}
