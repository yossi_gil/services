package il.ac.technion.cs.ssdl.files;

import static il.ac.technion.cs.ssdl.testing.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import il.ac.technion.cs.ssdl.utils.ClassPathInfo;
import java.io.File;
import java.util.List;
import org.junit.Test;

public class JRETest {
	@Test public final void testAsList() {
		final List<File> l = JRE.asList();
		assertNotNull(l);
		final ClassPathInfo cpi = new ClassPathInfo(l);
		assertEquals(l.size(), cpi.files.length);
		assertTrue(cpi.getClasses().contains("java.lang.Object"));
		assertFalse(cpi.getClasses().contains(this.getClass().getName()));
	}

	@Test public final void testFromClass() {
		final List<File> l = JRE.fromClass(this.getClass());
		assertNotNull(l);
		assertTrue(l.size() > 0);
	}
}
