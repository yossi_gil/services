package il.ac.technion.cs.ssdl.utils;

public class Exceptions
{
   public static RuntimeException makeUnchecked(Throwable t)
   {
      if(t instanceof RuntimeException)
         return (RuntimeException) t;
      
      RuntimeException e = new RuntimeException(t);
      e.setStackTrace(t.getStackTrace());
      return e;       
   }
}
