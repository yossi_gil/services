/**
 * 
 */
package utils.parsing;


/**
 * A default implementation of {@link ErrorItem}
 * 
 * @author imaman
 */
public class DefaultErrorItem implements ErrorItem 
{
   private final Location loc;
   private final String message;
   private final String sourceCode;
   
   /**
    * Initialize a new instance with the given values.
    * 
    * @param loc Location where the error occured
    * @param message Error message
    * @param sourceCode Soruce code fragment 
    */
   public DefaultErrorItem(Location loc, String message, String sourceCode)
   {
      this.loc = loc;
      this.message = message;
      this.sourceCode = sourceCode;
   }
   

   /**
    * {@inheritDoc}
    */
   public Location getLocation()
   {
      return loc;
   }



   /**
    * {@inheritDoc}
    */
   public String getMessage()
   {
      return message;
   }



   /**
    * {@inheritDoc}
    */
  public String getSourceCode()
   {
      return sourceCode;
   }
  
  public static final int LIMIT_LINE = 60;

   @Override
   public String toString()
   {
      int top = sourceCode.length();      

      int len;
      
      if(loc.end.line == loc.begin.line)
         len = loc.end.column - loc.begin.column;
      else
      {
         int nl = sourceCode.indexOf('\n');
         if(nl >= 0)
            top = nl;
         len = top - loc.begin.column;
      }
      
            
      top = Math.min(LIMIT_LINE, sourceCode.length());
      len = Math.max(1, len);
      String fragment = sourceCode.substring(0, top);
      
      StringBuilder sb = new StringBuilder();
      for(int i = 1; i < loc.begin.column + len; ++i)
      {
         if(i >= loc.begin.column)
            sb.append('^');
         else
            sb.append(' ');
      }
      
      String file = loc.enclosingFile();
      if(file.length() > 0)
         file += ": ";
      
      return file + "Line " + loc.begin.line + ": " + message + "\n" 
         + fragment + "\n" + sb.toString();
   }
   
   @Override
   public int hashCode()
   {
      return this.loc.hashCode() ^ this.message.hashCode();
   }
   
   /**
    * {@inheritDoc}
    */
   public int compareTo(ErrorItem that)
   {
      int comp = this.getLocation().compareTo(that.getLocation());
      if(comp != 0)
         return comp;
      return this.getMessage().compareTo(that.getMessage());
   } 
   
   @Override public boolean equals(Object other)
   {
      if(other == null)
         return false;
      
      if(!(other.getClass().equals(this.getClass())))
         return false;
      
      return compareTo((DefaultErrorItem) other) == 0;
   }
}