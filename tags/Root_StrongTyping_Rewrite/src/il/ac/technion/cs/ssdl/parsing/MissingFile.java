package il.ac.technion.cs.ssdl.parsing;

import java.io.File;

public class MissingFile extends MalformedInput {
	public MissingFile(File f) {
		super("I could not find the file " + f);
	}

	public MissingFile(String binFile) {
		this(new File(binFile));
	}
}
