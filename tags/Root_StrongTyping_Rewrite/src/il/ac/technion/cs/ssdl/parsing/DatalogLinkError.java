package il.ac.technion.cs.ssdl.parsing;


/**
 * Thrown in case linking is not possible during creation
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il> 09/05/2007
 */
@SuppressWarnings("serial") public abstract class DatalogLinkError extends DatalogError {
	public DatalogLinkError(String message) {
		super(message);
	}
}