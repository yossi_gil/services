package il.ac.technion.cs.ssdl.testing; import il.ac.technion.cs.ssdl.utils.Iterables;
import java.util.*;

/**
 * Extends {@link il.ac.technion.cs.ssdl.testing.Assert} with more assertion for
 * equality comparisons.
 * 
 * If the comparison yields a "not-equal" result, a JUnit assertion failure is
 * issued.
 * 
 * @author Itay Maman Jul 9, 2007
 */
public class Assert extends org.junit.Assert {
	public static <T> void assertCollectionsEqual(Collection<T> a, Collection<T> b) {
		assertCollectionsEqual("", a, b);
	}

	public static <T> void assertCollectionsEqual(String s, Collection<T> c, T[] a) {
		assertCollectionsEqual(s, c, Arrays.asList(a));
	}

	public static <T> void assertCollectionsEqual(Collection<T> c, T[] a) {
		assertCollectionsEqual("", c, Arrays.asList(a));
	}

	public static <T> void assertCollectionsEqual(String s, T[] a, Collection<T> c) {
		assertCollectionsEqual(s, c, a);
	}

	public static <T> void assertCollectionsEqual(String s, Collection<T> c1, Collection<T> c2) {
		assertContained(s, c1, c2);
		assertContained(s, c2, c1);
	}

	public static <T> void assertSubset(Collection<T> c1, Collection<T> c2) {
		assertContained("", c1, c2);
	}

	public static <T> void assertContained(String s, Collection<T> c1, Collection<T> c2) {
		// assertLE(s, c1.size(), c2.size());
		ArrayList<T> missing = new ArrayList<T>();
		for (final T t : c1) 
			if (!c2.contains(t))
				missing.add(t);
		switch (missing.size()) {
			case 0:
				return;
			case 1:
				fail(s + "Element '" + missing.get(0) + "' not found in " + c2.size() + " sized-\n collection " + c2);
				break;
			default:
				fail(s + "Element '" + missing.get(0) + "' and '" + missing.get(1) + "'  as well as " + (missing.size() - 2) + " other \n elements were not found in " + c2.size() + " sized-\n" +
						" collection " + c2);
				break;
		}
	}

	public static <T> void assertContains(Collection<T> c, T t) {
		assertContains("", c, t);
	}

	public static <T> void assertContains(String s, Collection<T> c, T t) {
		assertTrue(s + " t = " + t, c.contains(t));
	}

	public static <T> void assertNotContains(Collection<T> c, T t) {
		assertNotContains("", c, t);
	}

	public static <T> void assertNotContains(String s, Collection<T> c, T t) {
		assertFalse(s + " t = " + t, c.contains(t));
	}

	public static void assertLE(String s, int n, int m) {
		assertTrue(s + " n=" + n + " m=" + m, n <= m);
	}

	public static <T> void equals(String prefix, Set<T> set, Iterable<T> ts) {
		final List<T> list = Iterables.toList(ts);
		Set<T> temp = new HashSet<T>();
		temp.addAll(set);
		temp.removeAll(list);
		assertTrue(temp.toString(), temp.isEmpty());
		temp = new HashSet<T>();
		temp.addAll(list);
		temp.removeAll(set);
		assertTrue(prefix + " - " + temp.toString(), temp.isEmpty());
	}

	public static void assertEquals(int a, int b) {
		assertEquals("", a, b);
	}
	public static void assertEquals(String s, int a, int b) {
		assertEquals(s, new Integer(a), new Integer(b));
	}

	public static void assertEquals(Integer a, int b) {
		assertEquals(a, new Integer(b));
	}

	public static void assertEquals(int a, Integer b) {
		assertEquals(new Integer(a), b);
	}


	public static void assertEquals(String s, Integer a, int b) {
		assertEquals(s, a, new Integer(b));
	}

	public static void assertEquals(String s, int a, Integer b) {
		assertEquals(s, new Integer(a), b);
	}

	public static void assertEquals(boolean a, boolean b) {
		assertEquals(new Boolean(a), new Boolean(b));
	}

	public static void assertEquals(boolean a, Boolean b) {
		assertEquals(new Boolean(a), b);
	}

	public static void assertEquals(Boolean a, boolean b) {
		assertEquals(a, new Boolean(b));
	}

	public static void assertEquals(String s, boolean a, boolean b) {
		assertEquals(s, new Boolean(a), new Boolean(b));
	}

	public static void assertEquals(String s, boolean a, Boolean b) {
		assertEquals(s, new Boolean(a), b);
	}

	public static void assertEquals(String s, Boolean a, boolean b) {
		assertEquals(s, a, new Boolean(b));
	}
	public static void assertNotEquals(Object a, Object b) {
		assertNotEquals(null,a,b);
	}
	public static void assertNotEquals(String s, Object a, Object b) {
		assertFalse(s, a.equals(b));
	}
}
