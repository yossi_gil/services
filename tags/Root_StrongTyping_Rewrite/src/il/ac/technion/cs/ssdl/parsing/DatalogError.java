package il.ac.technion.cs.ssdl.parsing;

public class DatalogError extends MalformedInput {
	public DatalogError(String message) {
		super(message);
	}
}
