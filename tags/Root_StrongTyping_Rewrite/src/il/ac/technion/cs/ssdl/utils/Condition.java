package il.ac.technion.cs.ssdl.utils; 
/**
 * An interface representing a boolean function taking single argument boolean.
 * 
 * @param <Argument>
 *        Type of argumet
 * @author Yossi Gil 12/07/2007
 */
public interface Condition<Argument> {
	/**
	 * Evaluate the function for the given input
	 * 
	 * @param v
	 *        Input argument
	 * @return <code><b>true</b></code> if the predicate holds.
	 */
	public boolean holds(Argument v);
}
