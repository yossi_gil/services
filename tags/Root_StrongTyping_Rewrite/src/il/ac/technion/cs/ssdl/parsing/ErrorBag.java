package il.ac.technion.cs.ssdl.parsing;

import java.util.Arrays;
import java.util.List;

/**
 * An exception class that indicates one or more compilation errors. The
 * information about the individual erros is avaialble via the {{@link #items}
 * field.
 * 
 * @author imaman
 * 
 */
public class ErrorBag extends MalformedInput {
	/**
	 * An array of all {@link ErrorItem} objects describing all detected errors
	 */
	private final ErrorItem[] items;
	/**
	 * Textual description of all detected errors
	 */
	public final String breakout;

	/**
	 * Initialize a new instance with the given parameters
	 * 
	 * @param items
	 *        {@link ErrorItem} objects of all detected errors
	 * @param breakout
	 *        Textual description of all detected errors
	 */
	public ErrorBag(ErrorItem[] items, String breakout) {
		super("I found " + items.length + " error" + (items.length != 1 ? "s" : "") + " in your program\n" + breakout);
		this.breakout = breakout;
		this.items = items;
	}

	public ErrorBag(List<ErrorItem> errors, String breakout) {
		this(errors.toArray(new ErrorItem[0]), breakout);
	}

	/**
	 * Obtain an array of all error items. The returned array is a new copy that
	 * can be freely changed by the caller.
	 * 
	 * @return An array of all error items
	 */
	public ErrorItem[] getItems() {
		return Arrays.copyOf(items, items.length);
	}
}