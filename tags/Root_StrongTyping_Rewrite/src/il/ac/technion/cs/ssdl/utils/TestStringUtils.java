package il.ac.technion.cs.ssdl.utils; import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestStringUtils {
	@Test(expected = Throwable.class) public final void testInavlidStrip() {
		StringUtils.strip(null);
	}

	@Test(expected = Throwable.class) public final void testInavlidStrip0() {
		StringUtils.strip(".");
	}

	@Test(expected = Throwable.class) public final void testInavlidStrip1() {
		StringUtils.strip("X");
	}

	@Test public final void testValidStrip() {
		assertEquals("ab", StringUtils.strip("xaby"));
		assertEquals("", StringUtils.strip("ab"));
		assertEquals("b", StringUtils.strip("Abc"));
	}
}
