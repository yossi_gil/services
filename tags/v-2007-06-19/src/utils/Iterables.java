package utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * This class consists exclusively of static methods that operate on or return
 * Iterable objects (a-la java.util.Collections).
 */
public abstract class Iterables {
	private Iterables() {
		// Non instantiable
	}

	@SuppressWarnings("unused") public static <T, F extends T> List<T> upcast(Class<T> cls, Iterable<F> fs) {
		List<T> lst = new ArrayList<T>();
		for (F f : fs)
			lst.add(f);
		return lst;
	}

	public static <E> Iterable<E> sortAsString(Iterable<E> in) {
		return sort(in, new Comparator<E>() {
			public int compare(E o1, E o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
	}

	public static <E> List<E> sort(Iterable<E> in, Comparator<E> c) {
		List<E> lst = toList(in);
		Collections.sort(lst, c);
		return lst;
	}

	public static <E> Iterable<E> reverse(Iterable<E> in) {
		List<E> lst = toList(in);
		Collections.reverse(lst);
		return lst;
	}

	public static <E> List<E> toList(Iterable<E> in) {
		List<E> lst = new ArrayList<E>();
		for (E e : in)
			lst.add(e);
		return lst;
	}

	@SuppressWarnings("unchecked") public static <E> E[] toArray(Iterable<E> in, Class<E> cls) {
		List<E> es = toList(in);
		E[] arr = (E[]) Array.newInstance(cls, es.size());
		return es.toArray(arr);
	}

   @SuppressWarnings("unchecked") public static <E> Iterable<E> fromArray(E... es) { 
      return Arrays.asList(es);
   }
   
	public static <E> Collection<E> join(Iterable<E>... ess) {
		List<E> lst = new ArrayList<E>();
		for (Iterable<E> es : ess)
			for (E e : es)
				lst.add(e);
		return lst;
	}

	public static <E> Collection<E> set(Iterable<E>... ess) {
		Set<E> lst = new HashSet<E>();
		for (Iterable<E> es : ess)
			for (E e : es)
				lst.add(e);
		return lst;
	}

	public static <E> E first(Iterable<E> es) {
		DBC.require(es.iterator().hasNext());
		return es.iterator().next();
	}

	public static <E> E prepend(E e, Iterable<E> es) {
		return toList(es).set(0, e);
	}

	public static <F, T> Iterable<T> map(Iterable<F> fs, Func<F, T> func) {
		List<T> ts = new ArrayList<T>();
		for (F f : fs)
			ts.add(func.eval(f));
		return ts;
	}

   @SuppressWarnings("unused") 
   public static int size(Iterable<?> specs) {
      int n = 0;
      for (final Object o : specs)
         ++n;
      return n;
   }

   /**
    * Create an empty iterable
    * @param <T> Type of elements 
    * @param cls Class object of T
    * @return An empty collection of type T
    */
   public static<T> Iterable<T> empty(@SuppressWarnings("unused") Class<T> cls) {
      return new ArrayList<T>();
   }
}
