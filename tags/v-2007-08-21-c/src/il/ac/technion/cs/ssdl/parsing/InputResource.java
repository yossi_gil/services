package il.ac.technion.cs.ssdl.parsing;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * Protocol of an object that represents a resource from which textual data can
 * be retrieved. This interface provides client code with a uniform way for
 * reading data from various sources, e.g., files, the standard input stream,
 * URL connections.
 * 
 * @author imaman
 * 
 */
public interface InputResource extends Comparable<InputResource> {
	/**
	 * Obtain the name of the this resource. The returned string describes the
	 * resource (e.g., file name). This name is <b>not</b> guaranteed to be
	 * unique. A resource may be unnamed if it represents an unnamed content,
	 * such as an arbitrary <code>java.io.Reader</code> object.
	 * 
	 * @return Name of the resource. Retruns an empty string if this is an
	 *         unnamed resource.
	 * @see #id()
	 */
	public String name();

	/**
	 * Obtain a unique id of this object. Note that it is possbile that two
	 * instances that refer to the same physical resource (e.g.: a file) will
	 * have different id values. <br>
	 * <br>
	 * This id value is used primarily for sortring purposes.
	 * 
	 * @return a unqiue integer id associated with this object.
	 */
	public int id();

	/**
	 * Obtain a reader object over the contents of the underlying physical
	 * resource.
	 * 
	 * @return A Reader object
	 * @throws IOException
	 *         If a reader could not be obtained
	 */
	public Reader reader() throws IOException;

	/**
	 * Read the contents of the resource. Successive ivnocations of this methods
	 * will returns the same result. This method will block if no characters are
	 * avaialble in the underyling physical resource.
	 * 
	 * @return Contents of the resource as a single String object
	 * 
	 * @throws IOException
	 */
	public String text() throws IOException;

	/**
	 * Obtain the contents of this resource as an list of lines
	 * 
	 * @return List of strings, each string is a line.
	 * @throws IOException
	 */
	public List<String> lines() throws IOException;
}
