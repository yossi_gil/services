/**
 * 
 */
package il.ac.technion.cs.ssdl.testing;

import il.ac.technion.cs.ssdl.utils.Iterables;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Extends {@link il.ac.technion.cs.ssdl.testing.Assert} with more assertion for
 * equality comparisons.
 * 
 * If the comparison yields a "not-equal" result, a JUnit assertion failure is
 * issued.
 * 
 * @author Itay Maman
 * @date Jul 9, 2007
 */
public class Assert extends org.junit.Assert {
	public static <T> void assertCollectionsEqual(Collection<T> c, T[] a) {
		assertCollectionsEqual(c, Arrays.asList(a));
	}

	public static <T> void assertCollectionsEqual(T[] a, Collection<T> c) {
		assertCollectionsEqual(c, a);
	}

	public static <T> void assertCollectionsEqual(Collection<T> c1, Collection<T> c2) {
		assertContained(c1, c2);
		assertContained(c2, c1);
	}

	public static <T> void assertContained(Collection<T> c1, Collection<T> c2) {
		assertLE(c1.size(), c2.size());
		for (T t : c1)
			assertTrue(t.toString(), c2.contains(t));
	}

	public static void assertLE(int n, int m) {
		assertTrue("n=" + n + " m=" + m, n <= m);
	}

	public static <T> void equals(String prefix, Set<T> set, Iterable<T> ts) {
		List<T> list = Iterables.toList(ts);
		Set<T> temp = new HashSet<T>();
		temp.addAll(set);
		temp.removeAll(list);
		assertTrue(temp.toString(), temp.isEmpty());
		temp = new HashSet<T>();
		temp.addAll(list);
		temp.removeAll(set);
		assertTrue(prefix + " - " + temp.toString(), temp.isEmpty());
	}

	public static void assertEquals(int a, int b) {
		assertEquals(new Integer(a), new Integer(b));
	}

	public static void assertEquals(Integer a, int b) {
		assertEquals(a, new Integer(b));
	}

	public static void assertEquals(int a, Integer b) {
		assertEquals(new Integer(a), b);
	}

	public static void assertEquals(String s, int a, int b) {
		assertEquals(s, new Integer(a), new Integer(b));
	}

	public static void assertEquals(String s, Integer a, int b) {
		assertEquals(s, a, new Integer(b));
	}

	public static void assertEquals(String s, int a, Integer b) {
		assertEquals(s, new Integer(a), b);
	}

	public static void assertEquals(boolean a, boolean b) {
		assertEquals(new Boolean(a), new Boolean(b));
	}

	public static void assertEquals(String s, boolean a, boolean b) {
		assertEquals(s, new Boolean(a), new Boolean(b));
	}
}
