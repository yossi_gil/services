package haifa.services.utils.files;

import java.util.Collection;

/**
 * A class realizing a traversal of the file system, where the traversal is
 * restricted to <code>.class</code> files only
 * <p>
 * The traversal is carried out by calling one of the class constructors,
 * {@link #ClassFilesVisitor(Collection, haifa.services.utils.files.FileSystemVisitor.Action)}
 * or {@link #ClassFilesVisitor(String[], haifa.services.utils.files.FileSystemVisitor.Action)}
 * to set the visitation range, and the {@link Action} to be carried for each
 * visited file.
 * <p>
 * 
 * @see ClassFilesClasspathVisitor
 * @author Yossi Gil
 * @date 11/07/2007
 */
public class ClassFilesVisitor extends FileSystemVisitor {
	public ClassFilesVisitor(String from, Action visitor, String[] extensions) {
		super(from, visitor, extensions);
	}

	private static final String[] classFileExtensions = new String[] { ".class" };

	public ClassFilesVisitor(Collection<String> from, Action visitor) {
		super(from, visitor, classFileExtensions);
	}

	public ClassFilesVisitor(String[] from, Action visitor) {
		super(from, visitor, classFileExtensions);
	}

	public ClassFilesVisitor(String from, Action visitor) {
		super(from, visitor, classFileExtensions);
	}
}
