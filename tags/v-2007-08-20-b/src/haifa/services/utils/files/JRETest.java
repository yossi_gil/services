package haifa.services.utils.files;

import static haifa.services.utils.testing.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import haifa.services.utils.ClassPathInfo;

import java.io.File;
import java.util.List;
import org.junit.Test;

public class JRETest {
	@Test public final void testAsList() {
		List<File> l = JRE.asList();
		assertNotNull(l);
		ClassPathInfo cpi = new ClassPathInfo(l);
		assertEquals(l.size(), cpi.files.length);
		assertTrue(cpi.getClasses().contains("java.lang.Object"));
		assertFalse(cpi.getClasses().contains(this.getClass().getName()));
	}

	@Test public final void testFromClass() {
		List<File> l = JRE.fromClass(this.getClass());
		assertNotNull(l);
		assertTrue(l.size() > 0);
	}
}
