package haifa.services.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A 1-to-many mapping, that is: a relation where each source value is
 * associated with several "images"
 * 
 * @author Itay Maman <imaman@cs>rn *
 * @param <K>
 *        Type of source values
 * @param <V>
 *        Type of images
 */
public class MultiMap<K, V> implements Iterable<K> {
	private final Map<K, Set<V>> map = new HashMap<K, Set<V>>();

	@Override public String toString() {
		String s = "";
		for (K k : this)
			s += k + "=>" + get(k) + '\n';
		return s;
	}

	/**
	 * Add an image to the given source value
	 * 
	 * @param k
	 *        Source value
	 * @param v
	 *        Image value
	 */
	public void put(K k, V v) {
		get(k).add(v);
	}

	/**
	 * Find the set of all images of the given source value
	 * 
	 * @param k
	 *        Source value
	 * @return Set of images associated with <code>k</code>
	 */
	public Set<V> get(K k) {
		final Set<V> $ = map.get(k);
		return $ != null ? $ : clear(k);
	}

	/**
	 * Clear the set of all images of the given source value
	 * 
	 * @param k
	 *        Source value
	 * @return the newly created set object
	 */
	public Set<V> clear(K k) {
		final Set<V> $ = new HashSet<V>();
		map.put(k, $);
		return $;
	}

	/**
	 * Return an iterator over all sorce values
	 * 
	 * @return Iterator<K> object
	 */
	public Iterator<K> iterator() {
		return map.keySet().iterator();
	}
}