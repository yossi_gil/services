package haifa.services.utils.csv;

import static haifa.services.utils.DBC.sure;

import java.io.FileWriter;
import java.io.IOException;

public class CSVWriter {
	private FileWriter w;

	public String header() {
		return header;
	}

	private String header = null;

	public CSVWriter(String fileName) throws IOException {
		w = new FileWriter(fileName);
	}

	private void writeln(String s) throws IOException {
		w.write(s);
		w.write("\n");
	}

	public void write(CSVLine cl) throws IOException {
		sure(header == null || header.equals(cl.header()));
		if (header == null)
			writeln(header = cl.header());
		writeln(cl.line());
	}

	public void close() throws IOException {
		w.close();
	}
}
