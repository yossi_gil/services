package haifa.services.utils;

import static junit.framework.Assert.fail;

public enum STOP {

	;
   /**
	 * Hanlder for program exit requests. Default behavior: calls System.exit()
	 */
   private static StopHandler stopHandler;
   static {
   	stopFail();
   }
   
   public static StopHandler stopExit() {
   	return stopHandler = new StopHandler() {
   		public void stop(int exitCode) {
   			System.exit(exitCode);
   		}

		public void stop(String s) {
			System.out.println(s);
	        stop(-1);
	       
        }};
   }
   
   public static StopHandler stopFail() {
   	return stopHandler = new StopHandler() {
   		public void stop(int exitCode) {
   			fail("Design by contract failue, code = " + exitCode);
   		}

		public void stop(String s) {
			fail("Design by contract failue: " + s);
	        
        }
   	};
   }

   public static void stopRuntimeException()
   {
      stopHandler = new StopHandler() {
         public void stop(int exitCode) {
            throw new RuntimeException("Stop called, exit code=" + exitCode);
         }

		public void stop(String s) {
			  throw new RuntimeException("Stop called:" + s);
	        
        }
      };
   }
   
	/**
	 * A never-returning method to be used for dealing with assertions that
	 * should stop the program run.
	 * 
	 * @param t
	 *        The exeption to be printed.
	 */
	public static void stop(Throwable t) {
		stop(t, "Program must stop due to this error: ");
	}

	/**
	 * A never-returning method to be used for dealing with assertions that
	 * should stop the program run.
	 * 
	 * @param t
	 *        The exeption to be printed.
	 * @param s
	 *        A more detailed error description;
	 */
	public static void stop(Throwable t, String s) {
		System.err.println(s);
		t.printStackTrace();
		stop(-1);
	}
	public static void stop(int exitCode) {
		stopHandler.stop(exitCode);
	}
	public static void stop(String s) {
		stopHandler.stop(s);
	}
	/**
	 * Callback for handling program exit requests
	 */
	public static interface StopHandler {
		public void stop(int exitCode);
		public void stop(String s);
	}
}
