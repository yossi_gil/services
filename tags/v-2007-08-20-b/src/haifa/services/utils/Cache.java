/**
 * 
 */
package haifa.services.utils;

import static haifa.services.utils.DBC.require;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A generic class realizing a cached buffer of elements of an arbitrary kind,
 * but indexed by {@link String}s. If a fetched element is not in the cache,
 * then the class uses its intenral {@link Factory} object to create the given
 * element.
 * 
 * The implementation of this class uses delegation (to a {@link Map}object)
 * rather than inheritance. This design decision is due to the signatrue of
 * {@link Map#get(Object)} method, which admits any {@link Object } as its
 * parameter, which may lead to subtle runtime bugs.
 * <p>
 * By using delegation we can define the get() method to accept only
 * {@link String}s thus achieving a higher level of static type safety.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il>
 * @date 16/06/2007
 * @param <T>
 *        The type of elements stored in the cache.
 * @see Factory
 */
public class Cache<T> implements Iterable<Map.Entry<String, T>> {
	/**
	 * The {@link Factory} used for creating objects if they are not in the
	 * cache.
	 */
	private Factory<T> factory;
	/**
	 * A map that stroes cached objects, keyed by their name
	 */
	private final Map<String, T> map = new HashMap<String, T>();

	/**
	 * Initialize a cache using a given {@link Factory}.
	 * 
	 * @param factory
	 *        to be used for creating objects not in the cache.
	 */
	public Cache(Factory<T> factory) {
		this.factory = factory;
	}

	/**
	 * Obtain an iterator over the cached object
	 * 
	 * @return Iterator of Entry<String,T>
	 */
	public Iterator<Entry<String, T>> iterator() {
		return map.entrySet().iterator();
	}

	/**
	 * fetch an element from the cache, but if it is not there, try to create
	 * it, and insert it into the cache.
	 * 
	 * @param key
	 *        the key identifying the element to be fetched, empty string is
	 *        fine.
	 * @return the element identified by this key.
	 */
	public T get(String key) {
		require(key != null);
		T $ = map.get(key);
		if ($ != null)
			return $;
		if (!exhaustive) {
			$ = factory.make(key);
			map.put(key, $);
		}
		return $;
	}

	/**
	 * Store the given value in the cache, and associate it with the given key.
	 * 
	 * @param key
	 *        key with which the specified value is to be associated.
	 * @param t
	 *        value to be stored in the cache.
	 * @return previous value associated with specified key, or <tt>null</tt>.
	 */
	public T put(String key, T t) {
		return map.put(key, t);
	}

	public Collection<T> all() {
		if (!exhaustive)
			map.putAll(factory.all());
		exhaustive = true;
		return map.values();
	}

	/**
	 * <code><b>true</b></code> iff the cache holds all possible keys.
	 */
	private boolean exhaustive = false;

	/**
	 * A simple abstract factory, allowing its clients to generate objects not
	 * present in the {@link Cache}.
	 * 
	 * @author Yossi Gil <yogi@cs.technion.ac.il>
	 * @date 16/06/2007
	 * @param <T>
	 *        the type of objects that this factory generates.
	 */
	static public abstract class Factory<T> {
		/**
		 * create a new object from its {@link String} name
		 * 
		 * @param key
		 *        the identifier for the newly created object.
		 * @return the newly created object, or <code><b>null</b></code> if
		 *         no such object can be created.
		 */
		public abstract T make(String key);

		/**
		 * retrieve the universal set, i.e., the set of all possible objects,
		 * indexed by their respective keys.
		 * 
		 * @return the base implementation returns the empty set, but it is
		 *         meant to be overriden by concrete classes inheriting from
		 *         {@link Factory}
		 */
		public Map<String, T> all() {
			return new HashMap<String, T>();
		}
	}

	private static final long serialVersionUID = -3116274634413703171L;

	/**
	 * Size of cache
	 * 
	 * @return Number of elements currently stored in the cache
	 */
	public int size() {
		return map.size();
	}
}
