/**
 * 
 */
package haifa.services.utils.reflect;

import java.util.HashMap;
import java.util.Map;

/**
 * Representns a collection of a variety of reference types, including system
 * classes, local classes, inner classes, anonymous classes, locals within
 * locals, locals within inners, anonymous within locals, inners within locals
 * within anonymous, etc., plus all possible combinations of the different
 * reference types: classes, interfaces, annotations, enums, enum values, which
 * can be static or not-static, public, private, protected or default level,
 * final, abstract or none, etc.
 * 
 * @author Yossi Gil
 * @date 27/07/2007
 */
public class SomeArrayTypes {
	public static Map<String, Class<?>> sample = new HashMap<String, Class<?>>();

	static void makeSample() {
		// primitive arrays
		sample.put("byte[]", byte[].class);
		sample.put("short[]", short[].class);
		sample.put("int[]", int[].class);
		sample.put("long[[]", long[].class);
		sample.put("float[]", float[].class);
		sample.put("double[]", double[].class);
		sample.put("char[]", char[].class);
		sample.put("boolean[]", boolean[].class);
		//
		sample.put("byte[][]", byte[][].class);
		sample.put("short[][]", short[][].class);
		sample.put("int[][]", int[][].class);
		sample.put("long[][]", long[][].class);
		sample.put("float[][]", float[][].class);
		sample.put("double[][]", double[][].class);
		sample.put("char[][]", char[][].class);
		sample.put("boolean[][]", boolean[][].class);
		//
		//
		sample.put("byte[][][]", byte[][][].class);
		sample.put("short[][][]", short[][][].class);
		sample.put("int[][][]", int[][][].class);
		sample.put("long[][][]", long[][][].class);
		sample.put("float[][][]", float[][][].class);
		sample.put("double[][][]", double[][][].class);
		sample.put("char[][][]", char[][][].class);
		sample.put("boolean[][][]", boolean[][][].class);
		//
		//
		sample.put("byte[][][][][][][][][][][][]", byte[][][][][][][][][][][][].class);
		// System classes
		sample.put("java.lang.Object[]", java.lang.Object[].class);
		sample.put("java.lang.Object[][]", java.lang.Object[][].class);
		sample.put("java.lang.Object[][][]", java.lang.Object[][][].class);
		sample.put("java.lang.String[]", java.lang.String[].class);
		sample.put("java.lang.System[]", java.lang.System[].class);
		sample.put("java.util.HashSet[]", java.util.HashSet[].class);
		sample.put("java.util.Set[]", java.util.Set[].class);
		sample.put("org.junit.BeforeClass[]", org.junit.BeforeClass[].class);
		// Application classes
		sample.put("datalog.runtime.stdlib.bcel.kinds.ReferenceTypeSampler[]", haifa.services.utils.reflect.SomeArrayTypes[].class);
		sample.put("ThisGetter[].class[]", ThisGetter[].class);
		// Plain inner classes
		sample.put("Classes.Plain.StaticInner[]", Classes.Plain.StaticInner[].class);
		sample.put("Classes.Plain.NonStaticInner[]", Classes.Plain.NonStaticInner[].class);
		sample.put("Classes.Plain.PublicStaticInner[]", Classes.Plain.PublicStaticInner[].class);
		sample.put("Classes.Plain.PublicStaticInner[]", Classes.Plain.PublicStaticInner[].class);
		sample.put("Classes.Plain.PrivateNonStaticInner[]", Classes.Plain.PrivateNonStaticInner[].class);
		sample.put("Classes.Plain.PrivateStaticInner[]", Classes.Plain.PrivateStaticInner[].class);
		sample.put("Classes.Plain.ProtectedStaticInner[]", Classes.Plain.ProtectedStaticInner[].class);
		sample.put("Classes.Plain.ProtectedNonStaticInner[]", Classes.Plain.ProtectedNonStaticInner[].class);
		// Abstract inner classes
		sample.put("Classes.Abstract.StaticInner[]", Classes.Abstract.StaticInner[].class);
		sample.put("Classes.Abstract.NonStaticInner[]", Classes.Abstract.NonStaticInner[].class);
		sample.put("Classes.Abstract.PublicStaticInner[]", Classes.Abstract.PublicStaticInner[].class);
		sample.put("Classes.Abstract.PublicStaticInner[]", Classes.Abstract.PublicStaticInner[].class);
		sample.put("Classes.Abstract.PrivateNonStaticInner[]", Classes.Abstract.PrivateNonStaticInner[].class);
		sample.put("Classes.Abstract.PrivateStaticInner[]", Classes.Abstract.PrivateStaticInner[].class);
		sample.put("Classes.Abstract.ProtectedStaticInner[]", Classes.Abstract.ProtectedStaticInner[].class);
		sample.put("Classes.Abstract.ProtectedNonStaticInner[]", Classes.Abstract.ProtectedNonStaticInner[].class);
		// Final inner classes
		sample.put("Classes.Final.StaticInner[]", Classes.Final.StaticInner[].class);
		sample.put("Classes.Final.NonStaticInner[]", Classes.Final.NonStaticInner[].class);
		sample.put("Classes.Final.PublicStaticInner[]", Classes.Final.PublicStaticInner[].class);
		sample.put("Classes.Final.PublicStaticInner[]", Classes.Final.PublicStaticInner[].class);
		sample.put("Classes.Final.PrivateNonStaticInner[]", Classes.Final.PrivateNonStaticInner[].class);
		sample.put("Classes.Final.PrivateStaticInner[]", Classes.Final.PrivateStaticInner[].class);
		sample.put("Classes.Final.ProtectedStaticInner[]", Classes.Final.ProtectedStaticInner[].class);
		sample.put("Classes.Final.ProtectedNonStaticInner[]", Classes.Final.ProtectedNonStaticInner[].class);
		// Inner interface classes
		sample.put("Interfaces.StaticInner[]", Interfaces.StaticInner[].class);
		sample.put("Interfaces.NonStaticInner[]", Interfaces.NonStaticInner[].class);
		sample.put("Interfaces.PublicStaticInner[]", Interfaces.PublicStaticInner[].class);
		sample.put("Interfaces.PrivateStaticInner[]", Interfaces.PrivateStaticInner[].class);
		sample.put("Interfaces.PrivateNonStaticInner[]", Interfaces.PrivateNonStaticInner[].class);
		sample.put("Interfaces.PrivateStaticInner[]", Interfaces.PrivateStaticInner[].class);
		sample.put("Interfaces.ProtectedStaticInner[]", Interfaces.ProtectedStaticInner[].class);
		sample.put("Interfaces.ProtectedNonStaticInner[]", Interfaces.ProtectedNonStaticInner[].class);
		// Enums inner classes
		sample.put("Enums.StaticInner[]", Enums.StaticInner[].class);
		sample.put("Enums.NonStaticInner[]", Enums.NonStaticInner[].class);
		sample.put("Enums.PublicStaticInner[]", Enums.PublicStaticInner[].class);
		sample.put("Enums.PublicStaticInner[]", Enums.PublicStaticInner[].class);
		sample.put("Enums.PrivateNonStaticInner[]", Enums.PrivateNonStaticInner[].class);
		sample.put("Enums.PrivateStaticInner[]", Enums.PrivateStaticInner[].class);
		sample.put("Enums.ProtectedStaticInner[]", Enums.ProtectedStaticInner[].class);
		sample.put("Enums.ProtectedNonStaticInner[]", Enums.ProtectedNonStaticInner[].class);
		// Annotations inner classes
		sample.put("Annotations.StaticInner[]", Annotations.StaticInner[].class);
		sample.put("Annotations.NonStaticInner[]", Annotations.NonStaticInner[].class);
		sample.put("Annotations.PublicStaticInner[]", Annotations.PublicStaticInner[].class);
		sample.put("Annotations.PublicStaticInner[]", Annotations.PublicStaticInner[].class);
		sample.put("Annotations.PrivateNonStaticInner[]", Annotations.PrivateNonStaticInner[].class);
		sample.put("Annotations.PrivateStaticInner[]", Annotations.PrivateStaticInner[].class);
		sample.put("Annotations.ProtectedStaticInner[]", Annotations.ProtectedStaticInner[].class);
		sample.put("Annotations.ProtectedNonStaticInner[]", Annotations.ProtectedNonStaticInner[].class);
	}

	static {
		makeSample();
	}

	public static class Classes extends ThisGetter {
		static class Plain extends ThisGetter {
			static class StaticInner extends ThisGetter {
				// Sample input
			}

			class NonStaticInner extends ThisGetter {
				// Sample input
			}

			public static class PublicStaticInner extends ThisGetter {
				// Sample input
			}

			public class PublicNonStaticInner {
				// Sample input
			}

			private static class PrivateStaticInner extends ThisGetter {
				// Sample input
			}

			private class PrivateNonStaticInner extends ThisGetter {
				// Sample input
			}

			protected static class ProtectedStaticInner extends ThisGetter {// Sample
				// input
			}

			protected class ProtectedNonStaticInner extends ThisGetter {// Sample
				// input
			}
		}

		static class Abstract extends ThisGetter {
			static abstract class StaticInner extends ThisGetter {
				// Sample input
			}

			abstract class NonStaticInner extends ThisGetter {
				// Sample input
			}

			abstract public static class PublicStaticInner extends ThisGetter {
				// Sample input
			}

			abstract public class PublicNonStaticInner {
				// Sample input
			}

			abstract private static class PrivateStaticInner extends ThisGetter {
				// Sample input
			}

			abstract private class PrivateNonStaticInner extends ThisGetter {
				// Sample input
			}

			abstract protected static class ProtectedStaticInner extends ThisGetter {// Sample
				// input
			}

			abstract protected class ProtectedNonStaticInner extends ThisGetter {// Sample
				// input
			}
		}

		static class Final extends ThisGetter {
			static final class StaticInner extends ThisGetter {
				// Sample input
			}

			final class NonStaticInner extends ThisGetter {
				// Sample input
			}

			final public static class PublicStaticInner extends ThisGetter {
				// Sample input
			}

			final public class PublicNonStaticInner {
				// Sample input
			}

			final private static class PrivateStaticInner extends ThisGetter {
				// Sample input
			}

			final private class PrivateNonStaticInner extends ThisGetter {
				// Sample input
			}

			final protected static class ProtectedStaticInner extends ThisGetter {// Sample
				// input
			}

			final protected class ProtectedNonStaticInner extends ThisGetter {// Sample
				// input
			}
		}
	}

	static class Interfaces {
		static interface StaticInner {// Sample input
		}

		interface NonStaticInner {// Sample input
		}

		public static interface PublicStaticInner {// Sample input
		}

		public interface PublicNonStaticInner {// Sample input
		}

		private static interface PrivateStaticInner {// Sample input
		}

		private interface PrivateNonStaticInner {// Sample input
		}

		protected static interface ProtectedStaticInner {// Sample input
		}

		protected interface ProtectedNonStaticInner {// Sample input
		}
	}

	static class Enums {
		static enum StaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		enum NonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		public static enum PublicStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		public enum PublicNonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		private static enum PrivateStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		private enum PrivateNonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		protected static enum ProtectedStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		protected enum ProtectedNonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}
	}

	static class Annotations {// Sample input
		static @interface StaticInner {// Sample input
		}

		@interface NonStaticInner {// Sample input
		}

		public static @interface PublicStaticInner {// Sample input
		}

		public @interface PublicNonStaticInner {// Sample input
		}

		private static @interface PrivateStaticInner {// Sample input
		}

		private @interface PrivateNonStaticInner {// Sample input
		}

		protected static @interface ProtectedStaticInner {// Sample input
		}

		protected @interface ProtectedNonStaticInner {// Sample input
		}
	}
}