/**
 * 
 */
package haifa.services.utils.reflect;

import java.util.HashMap;
import java.util.Map;

/**
 * Representns a collection of a variety of reference types, including system
 * classes, local classes, inner classes, anonymous classes, locals within
 * locals, locals within inners, anonymous within locals, inners within locals
 * within anonymous, etc., plus all possible combinations of the different
 * reference types: classes, interfaces, annotations, enums, enum values, which
 * can be static or not-static, public, private, protected or default level,
 * final, abstract or none, etc.
 * 
 * @author Yossi Gil
 * @date 27/07/2007
 */
public class SomeReferenceTypes {
	protected static Map<String, Class<?>> typesSample = new HashMap<String, Class<?>>();

	static void makeSample() {
		// System classes
		typesSample.put("java.lang.Object", java.lang.Object.class);
		typesSample.put("java.lang.String", java.lang.String.class);
		typesSample.put("java.lang.System", java.lang.System.class);
		typesSample.put("java.util.HashSet", java.util.HashSet.class);
		typesSample.put("java.util.Set", java.util.Set.class);
		typesSample.put("org.junit.BeforeClass", org.junit.BeforeClass.class);
		// Application classes
		typesSample.put("datalog.runtime.stdlib.bcel.kinds.ReferenceTypeSampler", haifa.services.utils.reflect.SomeReferenceTypes.class);
		typesSample.put("ThisGetter.class", ThisGetter.class);
		// Plain inner classes
		typesSample.put("Classes.Plain.StaticInner", Classes.Plain.StaticInner.class);
		typesSample.put("Classes.Plain.NonStaticInner", Classes.Plain.NonStaticInner.class);
		typesSample.put("Classes.Plain.PublicStaticInner", Classes.Plain.PublicStaticInner.class);
		typesSample.put("Classes.Plain.PublicStaticInner", Classes.Plain.PublicStaticInner.class);
		typesSample.put("Classes.Plain.PrivateNonStaticInner", Classes.Plain.PrivateNonStaticInner.class);
		typesSample.put("Classes.Plain.PrivateStaticInner", Classes.Plain.PrivateStaticInner.class);
		typesSample.put("Classes.Plain.ProtectedStaticInner", Classes.Plain.ProtectedStaticInner.class);
		typesSample.put("Classes.Plain.ProtectedNonStaticInner", Classes.Plain.ProtectedNonStaticInner.class);
		// Abstract inner classes
		typesSample.put("Classes.Abstract.StaticInner", Classes.Abstract.StaticInner.class);
		typesSample.put("Classes.Abstract.NonStaticInner", Classes.Abstract.NonStaticInner.class);
		typesSample.put("Classes.Abstract.PublicStaticInner", Classes.Abstract.PublicStaticInner.class);
		typesSample.put("Classes.Abstract.PublicStaticInner", Classes.Abstract.PublicStaticInner.class);
		typesSample.put("Classes.Abstract.PrivateNonStaticInner", Classes.Abstract.PrivateNonStaticInner.class);
		typesSample.put("Classes.Abstract.PrivateStaticInner", Classes.Abstract.PrivateStaticInner.class);
		typesSample.put("Classes.Abstract.ProtectedStaticInner", Classes.Abstract.ProtectedStaticInner.class);
		typesSample.put("Classes.Abstract.ProtectedNonStaticInner", Classes.Abstract.ProtectedNonStaticInner.class);
		// Final inner classes
		typesSample.put("Classes.Final.StaticInner", Classes.Final.StaticInner.class);
		typesSample.put("Classes.Final.NonStaticInner", Classes.Final.NonStaticInner.class);
		typesSample.put("Classes.Final.PublicStaticInner", Classes.Final.PublicStaticInner.class);
		typesSample.put("Classes.Final.PublicStaticInner", Classes.Final.PublicStaticInner.class);
		typesSample.put("Classes.Final.PrivateNonStaticInner", Classes.Final.PrivateNonStaticInner.class);
		typesSample.put("Classes.Final.PrivateStaticInner", Classes.Final.PrivateStaticInner.class);
		typesSample.put("Classes.Final.ProtectedStaticInner", Classes.Final.ProtectedStaticInner.class);
		typesSample.put("Classes.Final.ProtectedNonStaticInner", Classes.Final.ProtectedNonStaticInner.class);
		// Inner interface classes
		typesSample.put("Interfaces.StaticInner", Interfaces.StaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.NonStaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.PublicStaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.PrivateStaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.PrivateNonStaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.PrivateStaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.ProtectedStaticInner.class);
		typesSample.put("Interfaces.StaticInner", Interfaces.ProtectedNonStaticInner.class);
		// Enums inner classes
		typesSample.put("Enums.StaticInner", Enums.StaticInner.class);
		typesSample.put("Enums.NonStaticInner", Enums.NonStaticInner.class);
		typesSample.put("Enums.PublicStaticInner", Enums.PublicStaticInner.class);
		typesSample.put("Enums.PublicStaticInner", Enums.PublicStaticInner.class);
		typesSample.put("Enums.PrivateNonStaticInner", Enums.PrivateNonStaticInner.class);
		typesSample.put("Enums.PrivateStaticInner", Enums.PrivateStaticInner.class);
		typesSample.put("Enums.ProtectedStaticInner", Enums.ProtectedStaticInner.class);
		typesSample.put("Enums.ProtectedNonStaticInner", Enums.ProtectedNonStaticInner.class);
		// Values of enums inner classes
		typesSample.put("Enums.StaticInner.Value", Enums.StaticInner.Value.get());
		typesSample.put("Enums.NonStaticInner.Value", Enums.NonStaticInner.Value.get());
		typesSample.put("Enums.PublicStaticInner.Value", Enums.PublicStaticInner.Value.get());
		typesSample.put("Enums.PublicStaticInner.Value", Enums.PublicStaticInner.Value.get());
		typesSample.put("Enums.PrivateNonStaticInner.Value", Enums.PrivateNonStaticInner.Value.get());
		typesSample.put("Enums.PrivateStaticInner.Value", Enums.PrivateStaticInner.Value.get());
		typesSample.put("Enums.ProtectedStaticInner.Value", Enums.ProtectedStaticInner.Value.get());
		typesSample.put("Enums.ProtectedNonStaticInner.Value", Enums.ProtectedNonStaticInner.Value.get());
		// Annotations inner classes
		typesSample.put("Annotations.StaticInner", Annotations.StaticInner.class);
		typesSample.put("Annotations.NonStaticInner", Annotations.NonStaticInner.class);
		typesSample.put("Annotations.PublicStaticInner", Annotations.PublicStaticInner.class);
		typesSample.put("Annotations.PublicStaticInner", Annotations.PublicStaticInner.class);
		typesSample.put("Annotations.PrivateNonStaticInner", Annotations.PrivateNonStaticInner.class);
		typesSample.put("Annotations.PrivateStaticInner", Annotations.PrivateStaticInner.class);
		typesSample.put("Annotations.ProtectedStaticInner", Annotations.ProtectedStaticInner.class);
		typesSample.put("Annotations.ProtectedNonStaticInner", Annotations.ProtectedNonStaticInner.class);
	}

	static {
		makeSample();
	}

	static class Classes extends ThisGetter {
		static class Plain extends ThisGetter {
			static class StaticInner extends ThisGetter {
				// Sample input
			}

			class NonStaticInner extends ThisGetter {
				// Sample input
			}

			public static class PublicStaticInner extends ThisGetter {
				// Sample input
			}

			public class PublicNonStaticInner {
				// Sample input
			}

			private static class PrivateStaticInner extends ThisGetter {
				// Sample input
			}

			private class PrivateNonStaticInner extends ThisGetter {
				// Sample input
			}

			protected static class ProtectedStaticInner extends ThisGetter {// Sample
				// input
			}

			protected class ProtectedNonStaticInner extends ThisGetter {// Sample
				// input
			}
		}

		static class Abstract extends ThisGetter {
			static abstract class StaticInner extends ThisGetter {
				// Sample input
			}

			abstract class NonStaticInner extends ThisGetter {
				// Sample input
			}

			abstract public static class PublicStaticInner extends ThisGetter {
				// Sample input
			}

			abstract public class PublicNonStaticInner {
				// Sample input
			}

			abstract private static class PrivateStaticInner extends ThisGetter {
				// Sample input
			}

			abstract private class PrivateNonStaticInner extends ThisGetter {
				// Sample input
			}

			abstract protected static class ProtectedStaticInner extends ThisGetter {// Sample
				// input
			}

			abstract protected class ProtectedNonStaticInner extends ThisGetter {// Sample
				// input
			}
		}

		static class Final extends ThisGetter {
			static final class StaticInner extends ThisGetter {
				// Sample input
			}

			final class NonStaticInner extends ThisGetter {
				// Sample input
			}

			final public static class PublicStaticInner extends ThisGetter {
				// Sample input
			}

			final public class PublicNonStaticInner {
				// Sample input
			}

			final private static class PrivateStaticInner extends ThisGetter {
				// Sample input
			}

			final private class PrivateNonStaticInner extends ThisGetter {
				// Sample input
			}

			final protected static class ProtectedStaticInner extends ThisGetter {// Sample
				// input
			}

			final protected class ProtectedNonStaticInner extends ThisGetter {// Sample
				// input
			}
		}
	}

	static class Interfaces {
		static interface StaticInner {// Sample input
		}

		interface NonStaticInner {// Sample input
		}

		public static interface PublicStaticInner {// Sample input
		}

		public interface PublicNonStaticInner {// Sample input
		}

		private static interface PrivateStaticInner {// Sample input
		}

		private interface PrivateNonStaticInner {// Sample input
		}

		protected static interface ProtectedStaticInner {// Sample input
		}

		protected interface ProtectedNonStaticInner {// Sample input
		}
	}

	static class Enums {
		static enum StaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		enum NonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		public static enum PublicStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		public enum PublicNonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		private static enum PrivateStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		private enum PrivateNonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		protected static enum ProtectedStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}

		protected enum ProtectedNonStaticInner {
			Value {
				@Override public Class<?> get() {
					return this.getClass();
				}
			};
			public abstract Class<?> get();
		}
	}

	static class Annotations {// Sample input
		static @interface StaticInner {// Sample input
		}

		@interface NonStaticInner {// Sample input
		}

		public static @interface PublicStaticInner {// Sample input
		}

		public @interface PublicNonStaticInner {// Sample input
		}

		private static @interface PrivateStaticInner {// Sample input
		}

		private @interface PrivateNonStaticInner {// Sample input
		}

		protected static @interface ProtectedStaticInner {// Sample input
		}

		protected @interface ProtectedNonStaticInner {// Sample input
		}
	}
}

abstract class ThisGetter {
	public Class<?> get() {
		return this.getClass();
	}

	byte byteMethod() {
		return 0;
	}

	short shortMethod() {
		return 0;
	}

	int intMethod() {
		return 0;
	}

	long longMethod() {
		return 0;
	}

	double doubleMethod() {
		return '0';
	}

	float floatMethod() {
		return 0;
	}

	char charMethod() {
		return '0';
	}

	boolean booleanMethod() {
		return false;
	}

	int[] arrayMethod() {
		return null;
	}

	Object[] ObjectArrayMethod() {
		return null;
	}

	strictfp float strictfpMethod() {
		return 0;
	}
}
