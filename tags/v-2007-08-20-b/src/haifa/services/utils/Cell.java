package haifa.services.utils;

@Deprecated public class Cell<T> {
	private T value;

	public T get() {
		return value;
	}

	public void set(T t) {
		value = t;
	}
}
