/**
 * 
 */
package haifa.services.utils.testing;

import haifa.services.utils.Procedure;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import org.junit.Test;
import org.junit.internal.runners.TestMethodRunner;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.Parameterized.Parameters;

/**
 * This is JUnit runner class specialized for test classes that implement the
 * Proc<Object> interface (That is, the class declares a
 * <code>public void eval(Object o)</code> method). The tested class must be
 * annotated with <code> @RunWith(ProcRunner.class)</code> in order to be tested by ProcRunner.
 * 
 * In addition, the tested class is expected to provide test data by declaring a
 * no-arg, public, static method annotated with <code> @Parameters</code>.
 * 
 * The runner will invoke the eval() method with each of the values from the
 * test data.
 * 
 * @see org.junit.runner.RunWith
 * @see org.junit.runner.Runner
 * @see haifa.services.utils.Procedure
 * @see org.junit.runners.Parameterized.Parameters
 * 
 * @author Itay Maman <imaman@cs>
 * @date Jul 5, 2007
 */
public class ProcRunner extends Runner {
	private final Class<? extends Procedure<Object>> testee;
	private final Collection<Object> values;

	@SuppressWarnings("unchecked") public ProcRunner(Class<? extends Procedure<Object>> testee) {
		this.testee = testee;
		if (!Procedure.class.isAssignableFrom(testee))
			throw new IllegalSetting("class " + testee.getName() + " does not implement Proc");
		Method m = getParametersMethod();
		try {
			values = (Collection<Object>) m.invoke(null);
      } catch(IllegalSetting e) {
         throw e;      
		} catch (Exception e) {         
			throw new IllegalSetting(e);
		}
	}

	private Method getParametersMethod() {
		for (Method each : testee.getMethods()) {
			if (!Modifier.isStatic(each.getModifiers()))
				continue;
			if (!Modifier.isPublic(each.getModifiers()))
				continue;
			if (each.isAnnotationPresent(Parameters.class))
				return each;
		}
		throw new IllegalSetting("No public static @Parameters method on class " + testee.getName());
	}

	@Override public Description getDescription() {
		Description $ = Description.createSuiteDescription(testee.getSimpleName());
		for (Object o : values)
			$.addChild(Description.createTestDescription(testee, "arg=" + o));
		return $;
	}

	@Override public void run(RunNotifier notifier) {
		Procedure<Object> inst;
		try {
			inst = testee.newInstance();
		} catch (Throwable t) {
			IllegalSetting il = new IllegalSetting(t);
			il.setStackTrace(t.getStackTrace());
			throw il;
		}
		Description suite = getDescription();
		int i = -1;
		for (Object each : values) {
			++i;
			Description d = suite.getChildren().get(i);
			TestMethodRunner tmr = new TestMethodRunner(new MiddleMan(inst, each), MiddleMan.TEST_METHOD, notifier, d);
			try {
				tmr.run();
			} catch (Throwable t) {
				t.printStackTrace(System.out);
				notifier.fireTestFailure(new Failure(d, t));
			}
		}
	}

	/**
	 * This class is an implementation detail of ProcRunner. Translates JUnit's
	 * invocation of a no-arg method into an invocation of a single-argument
	 * method on the actuall subject.
	 * 
	 * @author Itay Maman <imaman@cs>
	 */
	public static class MiddleMan {
		private final Object arg;
		private final Procedure<Object> lab;

		public MiddleMan(Procedure<Object> l, Object arg) {
			lab = l;
			this.arg = arg;
		}

		@Test public void test() {
			lab.eval(arg);
		}

		public static final Method TEST_METHOD;
		static {
			try {
				TEST_METHOD = MiddleMan.class.getMethod("test");
			} catch (NoSuchMethodException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
