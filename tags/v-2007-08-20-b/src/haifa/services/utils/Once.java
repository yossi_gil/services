package haifa.services.utils;

public class Once {
	private String value;

	public Once(String value) {
		this.value = value;
	}

	@Override public String toString() {
		String $ = value != null ? value : "";
		value = null;
		return $;
	}
}
