package utils;

/**
 * An interface representing a class with an invariant.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il>
 * @date 11/04/2006
 */
public interface Checkable {
	/**
	 * This function represents the invariant of the implementing class. It
	 * returns nothing. If the invariant is violated, a runtime exeption aborts
	 * execution.
	 * 
	 */
	public void invariant();
}
