package utils;

/**
 * An interface representing a typed function with a single argument.
 * 
 * @param <Argument>
 *        Type of argumet
 * @param <Result>
 *        Type of result
 * @author Yossi Gil
 * @date 12/07/2007
 */
public interface Function<Argument, Result> {
	/**
	 * Evaluate the function for the given input
	 * 
	 * @param f
	 *        Input argument
	 * @return Value of the function for the given argument
	 */
	public Result eval(Argument f);
}
