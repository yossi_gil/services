package il.ac.technion.cs.ssdl.files;

import il.ac.technion.cs.ssdl.utils.Separator;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * A class representing the location of the <em>Java Runtime Enivonment</em>
 * (JRE), that is, the standard Java library, on the file system.
 * <p>
 * Since Java does not yet have an API that provides this information the JRE
 * location is computed using two heuristics:
 * <ul>
 * <li>If the default class loader is an instance of {@link URLClassLoader},
 * then its {@link URLClassLoader#getURLs()} method is invoked to find the JRE.
 * <li>If the above failes, the system property "sun.boot.class.path" is
 * fetched, giving the JRE location in JVMs by Sun.
 * </ul>
 * 
 * 
 * 
 * 
 * @author Yossi Gil
 * @date 12/07/2007
 */
public enum JRE {
	;
	/**
	 * retrieve the system's CLASSPATH
	 * 
	 * @return the content of the classpath, broken into array entries
	 */
	public static List<File> asList() {
		try {
			return fromClass(Object.class);
		} catch (Throwable t) {
			// Abosrb, let's try the other option...
			final List<File> $ = new ArrayList<File>();
			final String cp = System.getProperty("sun.boot.class.path");
			for (StringTokenizer st = new StringTokenizer(cp, File.pathSeparator); st.hasMoreTokens();)
				$.add(new File(st.nextToken()));
			return $;
		}
	}

	/**
	 * Obtain the class path location used by the class loader of a given
	 * classes.
	 * 
	 * @param cs
	 *        An array of classes
	 * @return A list of files
	 * @throws IllegalArgumentException
	 *         If the class loader of <code>c</code> is not a URLClassLoader
	 */
	public static List<File> fromClass(Class<?>... cs) throws IllegalArgumentException {
		final List<File> $ = new ArrayList<File>();
		for (Class<?> c : cs) {
			final ClassLoader cl = c.getClassLoader();
			if (!(cl instanceof URLClassLoader))
				throw new IllegalArgumentException("Class loader is not a URLClassLoader. class=" + c.getName());
			for (final URL url : ((URLClassLoader) cl).getURLs())
				try {
					$.add(new File(url.toURI()));
				} catch (URISyntaxException e) {
					throw new IllegalArgumentException("I cannot obtain a file from url " + url);
				}
		}
		return $;
	}

	public static void main(String[] args) {
		System.out.println(ClassLoader.getSystemResource("java/lang/Object.class"));
		System.out.println(Separator.separateBy(asList(), "\n"));
	}
}
