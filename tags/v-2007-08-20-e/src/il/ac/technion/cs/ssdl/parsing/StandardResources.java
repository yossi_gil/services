package il.ac.technion.cs.ssdl.parsing;

import il.ac.technion.cs.ssdl.utils.IO;
import java.io.*;
import java.util.List;

/**
 * A factory for standard {@link InputResource} objects.
 */
public class StandardResources {
	private StandardResources() {
		// Non instantiable
	}

	/**
	 * Create a {@link InputResource} object that obtains its contents from the
	 * specified reader.
	 * 
	 * @param r
	 *        A reader object
	 * @return A new InputResource
	 */
	public static InputResource newReader(Reader r) {
		return new StandardResourceImpl("", r);
	}

	/**
	 * Create a {@link InputResource} object that obtains its contents from the
	 * specified file. The file name is translated into an abolute path so the
	 * resulting InputResource will be indifferent to changes in the current
	 * working directory.
	 * 
	 * @param fileName
	 *        A file name
	 * @return A new InputResource
	 */
	public static InputResource newFile(String fileName) {
		return newFile(new File(fileName).getAbsoluteFile());
	}

	/**
	 * Create a {@link InputResource} object that obtains its contents from the
	 * specified file. The file name is translated into an abolute path so the
	 * resulting InputResource will be indifferent to changes in the current
	 * working directory.
	 * 
	 * @param f
	 *        A file object
	 * @return A new InputResource
	 */
	public static InputResource newFile(File f) {
		final File abs = f.getAbsoluteFile();
		return new StandardResourceImpl(f.getName(), null) {
			@Override public Reader reader() throws FileNotFoundException {
				return new FileReader(abs);
			}
		};
	}

	/**
	 * Create a {@link InputResource} object that obtains its contents from the
	 * given string.
	 * 
	 * @param s
	 *        String specifying the contents of the created InputResource.
	 * @return A new InputResource.
	 */
	public static InputResource newString(String s) {
		return new StandardResourceImpl("", new StringReader(s));
	}

//	/**
//	 * A predefined {@link InputResource} object whose underlying physical
//	 * resource is <code>System.in</code>
//	 */
//	public static final InputResource stdin = new StandardResourceImpl("/STDIN/", null) {
//		@Override public Reader reader() {
//			return new InputStreamReader(System.in);
//		}
//	};

	private static class StandardResourceImpl implements InputResource {
		private final String name;
		private String text = null;
		private final Reader reader;
		private static int counter = -1;
		private final int id;
		private List<String> lines = null;

		public StandardResourceImpl(String name, Reader r) {
			this.id = ++counter;
			this.name = name == null ? "" : name;
			this.reader = r;
		}

		@Override public String toString() {
			return id + ":" + name;
		}

		@Override public boolean equals(Object o) {
			if (o == null)
				return false;
			if (!this.getClass().equals(o.getClass()))
				return false;
			StandardResourceImpl that = (StandardResourceImpl) o;
			return this.compareTo(that) == 0;
		}

		public int compareTo(InputResource that) {
			return this.id() - that.id();
		}

		public int id() {
			return id;
		}

		public String name() {
			return name;
		}

		public Reader reader() throws IOException {
			if (text != null)
				return new StringReader(text);
			text = IO.toString(reader);
			return new StringReader(text);
		}

		public String text() throws IOException {
			if (text != null)
				return text;
			text = IO.toString(reader());
			return text;
		}

		public List<String> lines() throws IOException {
			if(lines != null)
				return lines;
			
			String temp = text();
			lines = IO.lines(temp);
			return lines;
		}
	}
}
