package il.ac.technion.cs.ssdl.utils;

import static il.ac.technion.cs.ssdl.utils.DBC.ensure;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassFinder {
	private static final String CLASS_SUFFIX = ".class";
	static ClassLoader classLoader = ClassLoader.getSystemClassLoader();

	public static ArrayList<Class<?>> getClasses(ClassFilter classFilter) {
		String classpath = System.getProperty("java.class.path", null);
		return getClasses(classFilter, classpath);
	}

	/**
	 * @param classFilter
	 *        filter
	 * @param classpath
	 * @return all classes in classpath which are accepted by the classFilter
	 */
	public static ArrayList<Class<?>> getClasses(ClassFilter classFilter, String classpath) {
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		try {
			ensure(classpath == null, "classpath == null");
			String[] directories = classpath.split(File.pathSeparator);
			for (String dir : directories) {
				File classpathDir = new File(dir);
				if (classpathDir.isDirectory())
					classes.addAll(getClassesFromDir(classpathDir, classFilter));
				// else
				// classes.addAll( getClassesFromJar(classpathDir, classFilter)
				// );
			}
		} catch (Exception e) {
			// halt program?!?
			e.printStackTrace();
		}
		return classes;
	}

	private static List<Class<?>> getClassesFromDir(File classpathElement, ClassFilter classFilter) {
		List<Class<?>> allEntries = getClassesFromDir(classpathElement, "", classFilter);
		return allEntries;
	}

	/**
	 * 
	 * @param dir
	 *        file which represents the directory within we are searching
	 * @param prefix
	 *        directories in which we traversed to reach this directory
	 * @return retreives all classes' names which are in the classpath
	 */
	private static ArrayList<Class<?>> getClassesFromDir(File dir, String prefix, ClassFilter classFilter) {
		ArrayList<Class<?>> $ = new ArrayList<Class<?>>();
		// list all classes in current directory
		List<String> classesInCurrentDir = Arrays.asList(dir.list(new IsClassFilter()));
		// add the prefix to all entries
		for (String className : classesInCurrentDir) {
			String fullname = prefix + className;
			Class<?> loadedClass;
			try {
				loadedClass = getClass(fullname);
				if (fullname.contains("Extends.class"))
					System.out.println(fullname);
				if (classFilter.accept(loadedClass))
					$.add(loadedClass);
			} catch (ClassNotFoundException e) {
				System.out.println("couldn't load class " + fullname); // TODO
				// remove
				// after
				// debugging
			}
		}
		// call recursively to all subdirectories
		List<File> allSubdirectories = Arrays.asList(dir.listFiles(new IsDirectoryFilter()));
		for (File subDir : allSubdirectories)
			$.addAll(getClassesFromDir(subDir, prefix + subDir.getName() + ".", classFilter));
		return $;
	}

	private static Class<?> getClass(String fullFilename) throws ClassNotFoundException {
		String classname = fullFilename.substring(0, fullFilename.length() - CLASS_SUFFIX.length()); // remove
		// the
		// .class
		// suffix
		return classLoader.loadClass(classname);
		// return Class.forName(fullname);
	}

	protected static ArrayList<Class<?>> getClassesFromJar(File jarFile, ClassFilter classFilter) {
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		try {
			if (jarFile.getName().endsWith(".jar")) {
				System.out.println(jarFile + " is being scanned"); // TODO
				// remove
				// after
				// debugging
				Enumeration<JarEntry> fileNames;
				fileNames = new JarFile(jarFile).entries();
				JarEntry entry = null;
				while (fileNames.hasMoreElements()) {
					entry = fileNames.nextElement();
					if (!entry.isDirectory() && entry.getName().endsWith(CLASS_SUFFIX))
						try {
							Class<?> loadedClass = getClass(entry.getName());
							if (classFilter.accept(loadedClass))
								classes.add(loadedClass);
						} catch (ClassNotFoundException e) {
							System.out.println("couldn't load jar class " + entry.getName()); // TODO
							// remove
							// after
							// debugging
						}
				}
			}
		} catch (IOException e) {
			// TODO halt program?!?
			e.printStackTrace();
		}
		return classes;
	}

	static class IsClassFilter implements FilenameFilter {
		public boolean accept(@SuppressWarnings("unused") File dir, String name) {
			return name.endsWith(".class");
		}
	}

	static class IsDirectoryFilter implements FileFilter {
		public boolean accept(File dir) {
			return dir.isDirectory();
		}
	}

	static public interface ClassFilter {
		public boolean accept(Class<?> loadedClass);
	}
}
