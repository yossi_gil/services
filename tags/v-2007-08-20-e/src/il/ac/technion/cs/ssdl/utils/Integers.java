package il.ac.technion.cs.ssdl.utils;

import java.util.HashSet;

/**
 * A typedef for a set of integers.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il>
 * @date 18/05/2006
 */
public class Integers extends HashSet<Integer> {
	// A simple typedef, with no added fields or methods.
}
