/**
 * 
 */
package il.ac.technion.cs.ssdl.parsing;


/**
 * An exception that indicates an error, in a JTL program, that is detected at
 * compilation/linking time.
 * 
 * @author imaman
 */
public class StaticError extends RuntimeException {
	private static final long serialVersionUID = 2152061099759747608L;

	/**
	 * Location where the error occured. May be <code>null</code>.
	 */
	public final Location location;


	/**
	 * Initialize a new instance by wrapping another exception.
	 * 
	 * @param e
	 *        Exception to warp
	 * @param loc
	 *        Location where the error occured
	 */
	protected StaticError(Exception e, Location loc) {
		super(e);
		location = loc;
	}

	/**
	 * Initialzie a new instance with the given parameters. Typically either
	 * <code>pos</code> or <code>loc</code> is <code>null</code>.
	 * 
	 * @param message
	 *        Error message
	 * @param loc
	 *        Location where the error occured.
	 */
	public StaticError(String message, Location loc) {
		super(message);
		this.location = loc;
	}
}