/**
 * 
 */
package utils.testing;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import utils.Iterables;

/**
 * Extends {@link junit.framework.Assert} with more assertion for equality
 * comparisons.
 * 
 * If the comparison yields a "not-equal" result, a JUnit assertion failure is
 * issued.
 * 
 * @author Itay Maman
 * @date Jul 9, 2007
 */
public class Assert extends junit.framework.Assert {
	public static <T> void assertCollectionsEqual(Collection<T> c, T[] a) {
		assertCollectionsEqual(c, Arrays.asList(a));
	}

	public static <T> void assertCollectionsEqual(T[] a, Collection<T> c) {
		assertCollectionsEqual(c, a);
	}

	public static <T> void assertCollectionsEqual(Collection<T> c1, Collection<T> c2) {
		assertContained(c1, c2);
		assertContained(c2, c1);
	}

	public static <T> void assertContained(Collection<T> c1, Collection<T> c2) {
		for (T t : c1)
			assertTrue(t.toString(), c2.contains(t));
	}

	public static <T> void equals(String prefix, Set<T> set, Iterable<T> ts) {
		List<T> list = Iterables.toList(ts);
		Set<T> temp = new HashSet<T>();
		temp.addAll(set);
		temp.removeAll(list);
		assertTrue(temp.toString(), temp.isEmpty());
		temp = new HashSet<T>();
		temp.addAll(list);
		temp.removeAll(set);
		assertTrue(prefix + " - " + temp.toString(), temp.isEmpty());
	}

	public static void assertEquals(int a, int b) {
		assertEquals(new Integer(a), new Integer(b));
	}

	public static void assertEquals(Integer a, int b) {
		assertEquals(a, new Integer(b));
	}

	public static void assertEquals(int a, Integer b) {
		assertEquals(new Integer(a), b);
	}

	public static void assertEquals(String s, int a, int b) {
		assertEquals(s, new Integer(a), new Integer(b));
	}

	public static void assertEquals(String s, Integer a, int b) {
		assertEquals(s, a, new Integer(b));
	}

	public static void assertEquals(String s, int a, Integer b) {
		assertEquals(s, new Integer(a), b);
	}
}
