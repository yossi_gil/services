package utils;

public class Pair<First, Second> {
	public final First a;
	public final Second b;

	public Pair(First a, Second b) {
		this.a = a;
		this.b = b;
	}

	@Override public String toString() {
		return "<" + a + "," + b + ">";
	}

	@Override public int hashCode() {
		return a.hashCode() >>> 1 ^ b.hashCode();
	}

	@Override public final boolean equals(Object o) {
		if (o == this)
			return true;
		if (o == null || !getClass().equals(o.getClass()))
			return false;
		@SuppressWarnings("unchecked") final Pair<First, Second> p = (Pair<First, Second>) o;
		return equals(p);
	}

	public boolean equals(Pair<First, Second> o) {
		return a.equals(o.a) && b.equals(o.b);
	}
}
