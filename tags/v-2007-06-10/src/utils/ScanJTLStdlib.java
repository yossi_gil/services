package utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import utils.FileSystemVisitor.Action;
import utils.FileSystemVisitor.Action.StopTraversal;

/**
 * 
 * 
 * @author Yossi Gil
 * @date Mar 29, 2007
 */
public class ScanJTLStdlib {
	static final String name = "jtldoc_scan";

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.printf("Usage: %s\n", name);
			System.exit(1);
		}
		try {
			new FileSystemVisitor(args, new Searcher(".class"), ".class").go();
		} catch (IOException e) {
			System.err.println(e);
		} catch (StopTraversal e) {
			System.err.println(e);
		}
	}

	static boolean reportCounts;
	static boolean findFirstOnly;

	/**
	 * @author Yossi Gil
	 * @date 21/05/2007
	 */
	public static class Searcher implements Action {
		private int directories = 0;
		private int files = 0;
		private int zips = 0;
		private int entries = 0;
		private final String sought;

		public Searcher(final String sought) {
			this.sought = sought;
		}

		private void report(String s) {
			if (reportCounts)
				System.out.println(s + ". " + directories + " directories, " + files + " class files, " + zips + " ZIP archives, " + entries
				        + " entries.");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitDirectory(java.io.File)
		 */
		public void visitDirectory(File f) {
			directories++;
			report("Directory: " + f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitFile(java.io.File)
		 */
		public void visitFile(File f) throws StopTraversal {
			files++;
			report("File: " + f.getAbsolutePath());
			check(f.getName(), f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitZip(java.io.File)
		 */
		public void visitZip(File f) {
			zips++;
			report("Archive: " + f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitZipEntry(java.lang.String,
		 *      java.lang.String, java.io.InputStream)
		 */
		public void visitZipEntry(String zipName, String entryName, @SuppressWarnings("unused")
      InputStream stream) throws StopTraversal {
			entries++;
			report("Archive entry: " + entryName);
			check(entryName, zipName);
		}

		private void check(String file, String directory) throws StopTraversal {
			if (!file.endsWith(sought))
				return;
			System.out.printf("%s: %s\n", file, directory);
			if (findFirstOnly)
				throw new StopTraversal();
		}
	}
}
