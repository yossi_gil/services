package utils;

import java.io.*;

/**
 * Static methods for I/O related operations
 */
public class IO {
	/**
	 * Read the contents of the given reader and return it as a String
	 * 
	 * @param r
	 *        Reader
	 * @return the entire content of <code>r</code>
	 * @throws IOException
	 */
	public static String toString(Reader r) throws IOException {
		final StringBuilder sb = new StringBuilder();
		while (true) {
			final int c = r.read();
			if (c < 0)
				return sb.toString();
			sb.append((char) c);
		}
	}

	/**
	 * Read the contents of the given stream and return it as a String
	 * 
	 * @param is
	 *        Input stream
	 * @return the entire content of <code>is</code>
	 * @throws IOException
	 */
	public static String toString(InputStream is) throws IOException {
		return toString(new InputStreamReader(is));
	}

	/**
	 * Read the contents of the given class-path file.
	 * 
	 * @param clazz
	 *        Class - Specifies a location in the class-path tree
	 * @param path
	 *        Relative path to the file from the given class
	 * @return Contents of the file
	 * @throws IOException
	 */
	public static String toString(Class<?> clazz, String path) throws IOException {
		return toString(clazz.getResourceAsStream(path));
	}

	/**
	 * Write a string to a file
	 * 
	 * @param outputFile
	 *        File to be written
	 * @param contents
	 *        String to write
	 * @throws IOException
	 */
	public static void write(File outputFile, String contents) throws IOException {
		FileWriter fw = new FileWriter(outputFile);
		try {
			fw.append(contents);
		} finally {
			fw.close();
		}
	}
}
