package utils.parsing;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;


/**
 * The protocol of an object that records all errors detected during 
 * compilation/parsing of a program.
 */
public interface MessageLog extends Serializable, Iterable<ErrorItem>
{
   /**
    * Record a warning
    * @param loc Location where the warning occured
    * @param message Text explaining the warning
    */
   public void recordWarning(Location loc, String message);

   /**
    * Record an error
    * @param loc Location where the error occured
    * @param message Text explaining the error
    * @param sourceFragment Source code fragment the triggered the error
    */
   public void recordError(Location loc, String message, String sourceFragment);
   
         
   /**
    * Request the message log to throw an exception if at least one error
    * was recorded.  
    * @exception ErrorBag
    */
   public void throwIfErrors() throws ErrorBag;
   
   
   /**
    * A predefined instance with a "do-nothing" behavior
    */
   public static final MessageLog NOP = new MessageLog()
   {
      private static final long serialVersionUID = 8235303558533463190L;

      @SuppressWarnings("unused")
      public void recordWarning(Location loc, String message)
      {
         // Do nothing
      }

      @SuppressWarnings("unused")
      public void recordError(Location loc, String message, String src)
      {
         // Do nothing
      }
      
      public void throwIfErrors() 
      {
         // Do nothing         
      }

      @SuppressWarnings("unchecked")
      public Iterator<ErrorItem> iterator()
      {
         return Collections.EMPTY_LIST.iterator();
      }
      
      
   };



}
