package il.ac.technion.cs.ssdl.testing;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation allows the programmer to associate a class with the class
 * that realized its test case.
 * 
 * @see TesterOf
 */
@Retention(RetentionPolicy.RUNTIME) public @interface TestedBy {
	public Class<?> value();
}
