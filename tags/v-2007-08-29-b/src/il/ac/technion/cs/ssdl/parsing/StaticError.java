/**
 * 
 */
package il.ac.technion.cs.ssdl.parsing;

/**
 * An exception that indicates an error, in a JTL program, that is detected at
 * compilation/linking time.
 * 
 * @author imaman
 */
public class StaticError extends RuntimeException {
	private static final long serialVersionUID = 2152061099759747608L;
	/**
	 * Location where the error occured. May be <code>null</code>.
	 */
	public final Location location;

	/**
	 * Initialize a new instance by wrapping another exception.
	 * 
	 * @param e
	 *        Exception to warp
	 * @param location
	 *        Location where the error occured
	 */
	protected StaticError(Exception e, Location location) {
		super(e);
		this.location = location;
	}

	/**
	 * Initialzie a new instance with the given parameters. Typically either
	 * <code>pos</code> or <code>l</code> is <code>null</code>.
	 * 
	 * @param message
	 *        Error message
	 * @param l
	 *        Location where the error occured.
	 */
	public StaticError(String message, Location l) {
		super(message);
		location = l;
	}
}