package utils;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {
	private T[] _ts;
	private int _index;
	private int _end;

	public static <E> Iterator<E> make(E[] es) {
		return make(es, 0);
	}

	public static <E> Iterator<E> make(E[] es, int begin) {
		return make(es, begin, es.length);
	}

	public static <E> Iterator<E> make(E[] es, int begin, int end) {
		return new ArrayIterator<E>(es, begin, end);
	}

	private ArrayIterator(T[] ts, int begin, int end) {
		_ts = ts;
		_index = begin;
		_end = end;
	}

	public boolean hasNext() {
		return _index < _end;
	}

	public T next() {
		return _ts[_index++];
	}

	public void remove() {
		throw new UnsupportedOperationException("ArrayIterator cannot remove " + "elements");
	}
}
