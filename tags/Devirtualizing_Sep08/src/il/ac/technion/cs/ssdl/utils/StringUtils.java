package il.ac.technion.cs.ssdl.utils;

import static il.ac.technion.cs.ssdl.utils.DBC.require;
import static org.junit.Assert.assertEquals;
import java.util.Collection;
import org.junit.Test;

/**
 * A bunch of STRING functions.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il> 05/06/2007
 */
public enum StringUtils {
	// No elements in this name space
	;
	public static String getTrailer(final String s) {
		return s.substring(s.lastIndexOf('.') + 1);
	}
	/**
	 * Strip the first and last character of a string.
	 * 
	 * @param s
	 *        a non null string of length at least two to strip
	 * @return <code>s</code> but without its first and last character.
	 */
	public static String strip(String s) {
		require(s != null);
		require(s.length() >= 2);
		return s.substring(1, s.length() - 1);
	}
	/**
	 * Chop the first character of a string.
	 * 
	 * @param s
	 *        a non null string of length at least two to strip
	 * @return <code>s</code> but without its first and last character.
	 */
	public static String chop(String s) {
		require(s != null);
		require(s.length() >= 1);
		return s.substring(1, s.length());
	}

	/**
	 * Quote a string
	 * 
	 * @param s
	 *        a non-null string for quoting
	 * @return the string <code>s</code> wrapped with single quotes
	 */
	public static String quote(String s) {
		return "'" + s + "'";
	}

	/**
	 * Concatenate any number of strings.
	 * 
	 * @param ss
	 *        a variable number of strtings
	 * @return the concatentation of the strings in <code>ss</code>
	 */
	public static final String cat(String... ss) {
		final StringBuilder b = new StringBuilder("");
		for (final String s : ss)
			b.append(s);
		return b.toString();
	}

	public static final String cat(String[]... ss) {
		final StringBuilder b = new StringBuilder("");
		for (final String[] s : ss)
			b.append(cat(s));
		return b.toString();
	}

	public static String fill(int n, char c) {
		return fill(n, Character.toString(c));
	}

	public static String fill(int n, String s) {
		String $ = "";
		for (int i = 0; i < n; ++i)
			$ += s;
		return $;
	}

	public final static boolean startsWith(String s, String... prefixes) {
		for (final String prefix : prefixes)
			if (s.startsWith(prefix))
				return true;
		return false;
	}

	/**
	 * Repeat a string a fixed number of times
	 * 
	 * @param n
	 *        a non-negative integer
	 * @param s
	 *        a string to repreat
	 * @return a {@link String} containing <code>s</code> concatentated <coden</code>
	 *         times
	 */
	public static String repeat(int n, String s) {
		final StringBuffer $ = new StringBuffer();
		for (int i = 0; i < n; i++)
			$.append(s);
		return $.toString();
	}
	
	public static String pluralize(final int n, String singular) {
		return pluralize(n, singular, singular + "s");
	}

	public static String pluralize(final int n, String singular, String plural) {
		switch (n) {
			case 0:
				return "no " + plural;
			case 1:
				return "" + singular;
			case 2: 
				return "two " + plural;
			case 3:
				return "three " + plural;
			case 4:
				return "four " + plural;
			case 5:
				return "five " + plural;
			case 6:
				return "six " + plural;
			case  7:
				return "seven " + plural;
			case 8:
				return "eight " + plural;
			case 9:
				return "nine " + plural;
			default:
				return n + " " + plural;
		}
	}
	
	final static int MAX_FIRST = 20;
	final static int MAX_LAST = 10;
	static public String pretty(String singular, Collection<? extends Object> a) {
		return pretty(singular, singular + "s", a);
	}
		
	static public String pretty(String singular, String plural, Collection<? extends Object> a) {
		if (a == null || a.size() <= 0)
			return "";
		if (a.size() == 1)
			return "1 " + singular + ": " + a.iterator().next() + "\n";
		String $ = a.size() + " " + plural + ":\n";
		int n = 0;
		final Once ellipsis = new Once("\t...\n");
		for (final Object o : a) {
			n++;
			if (n <= MAX_FIRST || n > a.size() - MAX_LAST)
				$ += "\t" + n+ ") " + o + "\n";
			else
				$ += ellipsis;
		}
		return $;
	}
	
	/**
	 * Compute the string equivalent ordinal of a positive integer, e.g., for 1
	 * return "1st", for 22, the "22nd", etc.
	 * 
	 * @param n
	 *        a non-negative integer to convert
	 * @return the ordinal string representation of
	 *         <code>n</code>
	 * 
	 */
	static public String ordinal(int n) {
		require(n >= 0);
		final String th = "th";
		switch (n % 10) {
				case 1:
					return n + ((n != 11) ? "st" : th);
				case 2:
					return n + ((n != 12) ? "nd" : th);
				case 3:
					return n + ((n != 13) ? "th" : th);
				default: 
					return n + th;
		}
	}
	@Test(expected = Throwable.class) public final void testInavlidStrip() {
		StringUtils.strip(null);
	}

	@Test(expected = Throwable.class) public final void testInavlidStrip0() {
		StringUtils.strip(".");
	}

	@Test(expected = Throwable.class) public final void testInavlidStrip1() {
		StringUtils.strip("X");
	}

	@Test public final void testValidStrip() {
		assertEquals("ab", StringUtils.strip("xaby"));
		assertEquals("", StringUtils.strip("ab"));
		assertEquals("b", StringUtils.strip("Abc"));
	}
}

