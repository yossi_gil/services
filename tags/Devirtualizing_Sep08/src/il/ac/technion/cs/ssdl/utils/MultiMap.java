package il.ac.technion.cs.ssdl.utils; import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A 1-to-many mapping, that is: a relation where each source value is
 * associated with several "images"
 * 
 * @author Itay Maman <imaman@cs>
 * @param <K>
 *        Type of source values
 * @param <V>
 *        Type of images
 */
public final class MultiMap<K, V> implements Iterable<K> {
	private final HashMap<K, Set<V>> map;

	public MultiMap() {
		map = new HashMap<K, Set<V>>();
	}

	public MultiMap(int initialCapacity) {
		this(initialCapacity, 0.75f);
	}

	public MultiMap(int initialCapacity, float loadFactor) {
		map = new HashMap<K, Set<V>>(initialCapacity, loadFactor);
	}

	@Override public String toString() {
		final StringBuilder sb = new StringBuilder();
		for (final K k : this)
			sb.append(k + "=>" + get(k) + '\n');
		return sb.toString();
	}

	/**
	 * Obtain all images
	 * 
	 * @return Set of V objects
	 */
	public Set<V> values() {
		final Set<V> $ = new HashSet<V>();
		for (Set<V> curr : map.values())
			$.addAll(curr);
		return $;
	}

	/**
	 * Add an image to the given source value
	 * 
	 * @param k
	 *        Source value
	 * @param v
	 *        Image value
	 */
	public void put(K k, V v) {
		get(k).add(v);
	}

	/**
	 * Find the set of all images of the given source value
	 * 
	 * @param k
	 *        Source value
	 * @return Set of images associated with <code>k</code>
	 */
	public Set<V> get(K k) {
		final Set<V> $ = map.get(k);
		return $ != null ? $ : clear(k);
	}

	/**
	 * Clear the set of all images of the given source value
	 * 
	 * @param k
	 *        Source value
	 * @return the newly created set object
	 */
	public Set<V> clear(K k) {
		final Set<V> $ = new HashSet<V>();
		map.put(k, $);
		return $;
	}

	/**
	 * Return an iterator over all sorce values
	 * 
	 * @return Iterator<K> object
	 */
	public Iterator<K> iterator() {
		return map.keySet().iterator();
	}

	/**
	 * How many keys are stored in this map?
	 * 
	 * @return Number of keys
	 */
	public int numKeys() {
		return map.keySet().size();
	}

	public int size() {
		return map.size();
	}
}