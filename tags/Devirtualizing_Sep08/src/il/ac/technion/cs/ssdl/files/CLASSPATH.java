package il.ac.technion.cs.ssdl.files; import il.ac.technion.cs.ssdl.reflect.Explore;
import java.io.File;

/**
 * A represenation of the system global CLASSPATH.
 * 
 * @author Yossi Gil 12/07/2007
 */
public enum CLASSPATH {
	;
	/**
	 * retrieve the system's CLASSPATH
	 * 
	 * @return the content of the classpath, broken into array entries
	 */
	public static String[] asArray() {
		return System.getProperty("java.class.path", null).split(File.pathSeparator);
	}
	public static void main(String[] args) {
		Explore.go(System.getProperty("java.class.path", null));
	   Explore.go(asArray());
    }
}
