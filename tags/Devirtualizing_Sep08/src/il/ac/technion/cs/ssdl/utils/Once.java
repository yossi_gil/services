package il.ac.technion.cs.ssdl.utils; 
/**
 * A class to manage printing a string exactly once. In the first invocation of
 * {@link #toString()}, the initial value is returned. In all subsequent
 * invocations, the empty string is returned.
 * 
 * @author Yossi Gil 21/08/2007
 */
public class Once {
	private String value;

	public Once(String value) {
		this.value = value;
	}

	@Override public String toString() {
		final String $ = value != null ? value : "";
		value = null;
		return $;
	}
}
