package il.ac.technion.cs.ssdl.csv; import static il.ac.technion.cs.ssdl.utils.DBC.require;
import java.io.FileWriter;
import java.io.IOException;

public class CSVWriter {
	private FileWriter w;

	public String header() {
		return header;
	}

	private String header = null;

	public CSVWriter(String fileName) throws IOException {
		w = new FileWriter(fileName);
	}

	private void writeln(String s) throws IOException {
		w.write(s);
		w.write("\n");
	}

	public void write(CSVLine cl) throws IOException {
		require(header == null || header.equals(cl.header()));
		if (header == null)
			writeln(header = cl.header());
		writeln(cl.line());
	}

	public void close() throws IOException {
		w.close();
	}
}
