package il.ac.technion.cs.ssdl.parsing;

import static il.ac.technion.cs.ssdl.utils.StringUtils.pluralize;
import il.ac.technion.cs.ssdl.utils.Separator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * The protocol of an object that records all errors detected during
 * compilation/parsing of a program.
 */
public interface ErrorLog extends Serializable, Iterable<ErrorItem> {
	/**
	 * Record a warning
	 * 
	 * @param l
	 *        Location where the warning occured
	 * @param message
	 *        Text explaining the warning
	 */
	public void recordWarning(Location l, String message);

	/**
	 * Record an error
	 * 
	 * @param l
	 *        Location where the error occured
	 * @param message
	 *        Text explaining the error
	 * @param sourceFragment
	 *        Source code fragment the triggered the error
	 */
	public void recordError(Location l, String message, String sourceFragment);

	public void recordError(String message);

	/**
	 * Request the message log to throw an exception if at least one error was
	 * recorded.
	 * 
	 * @exception ErrorBag
	 */
	public void throwIfErrors() throws ErrorBag;

	/**
	 * Does this log contain any errors?
	 * 
	 * @return <code>true</code> if yes.
	 */
	public boolean hasErrors();

	public int count();

	/**
	 * A predefined instance with a "do-nothing" behavior
	 */
	public static final ErrorLog SILENT = new ErrorLog() {
		private int count = 0;
		private static final long serialVersionUID = 8235303558533463190L;

		@SuppressWarnings("unused") public void recordWarning(Location l, String message) {
			count++;
		}

		@SuppressWarnings("unused") public void recordError(Location l, String message, String src) {
			count++;
		}

		public void recordError(@SuppressWarnings("unused") String message) {
			count++;
		}

		public void throwIfErrors() {
			// Do nothing
		}

		public boolean hasErrors() {
			return false;
		}

		@SuppressWarnings("unchecked") public Iterator<ErrorItem> iterator() {
			return Collections.EMPTY_LIST.iterator();
		}

		public int count() {
			return count;
		}
	};

	public static class DEFAULT implements ErrorLog {
		private static final long serialVersionUID = 1000629616878797828L;
		private final List<ErrorItem> errors = new ArrayList<ErrorItem>();

		@Override public String toString() {
			return pluralize(errors.size(), "error", "errors");
		}

		/**
		 * {@inheritDoc}
		 */
		public Iterator<ErrorItem> iterator() {
			Collections.sort(errors);
			return errors.iterator();
		}

		/**
		 * {@inheritDoc}
		 */
		public void recordError(Location l, String message, String sourceCodeFragment) {
			errors.add(new ErrorItem(l, message, sourceCodeFragment));
		}

		/**
		 * {@inheritDoc}
		 */
		public void recordError(String message) {
			errors.add(new ErrorItem(null, message, null));
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unused")//
		public void recordWarning(Location l, String message) {
			// Do nothing
		}

		/**
		 * {@inheritDoc}
		 */
		public boolean hasErrors() {
			return errors.size() > 0;
		}

		/**
		 * {@inheritDoc}
		 */
		public void throwIfErrors() throws ErrorBag {
			if (hasErrors())
				throw new ErrorBag(errors, Separator.separateBy(errors, "\n"));
		}

		public int count() {
			return errors.size();
		}
	}
}
