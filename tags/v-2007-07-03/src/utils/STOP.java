package utils;



public enum STOP {
	// A namespace
	;
/**
 * Hanlder for program exit requests. Default behavior: calls System.exit()
 */
public static StopHandler stopHandler = new StopHandler() {
	public void stop(int exitCode) {
		System.exit(exitCode);
	}
};

	/**
	 * A never-returning method to be used for dealing with assertions that
	 * should stop the program run.
	 * 
	 * @param t
	 *            The exeption to be printed.
	 */
	public static void stop(Throwable t) {
		stop(t, "Program must stop due to this error: ");
	}

	/**
	 * A never-returning method to be used for dealing with assertions that
	 * should stop the program run.
	 * 
	 * @param t
	 *            The exeption to be printed.  
	 * @param s
	 *            A more detailed error description;
	 */
	public static void stop(Throwable t, String s) {
		System.err.println(s);
		t.printStackTrace();
		stop(-1);
	}
	public static void stop(int exitCode) {
		stopHandler.stop(exitCode);
	}
	/**
	 * Callback for handling program exit requests
	 */
	public interface StopHandler {
		public void stop(int exitCode);
	}
}
