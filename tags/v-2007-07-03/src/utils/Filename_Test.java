package utils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;

@TesterOf(Filename.class) public class Filename_Test {
	@Test public void testHead() {
		assertEquals("", Filename.head_part(""));
		assertEquals("", Filename.head_part("a"));
		assertEquals("", Filename.head_part("$"));
		assertEquals("", Filename.head_part("$a"));
		assertEquals("", Filename.head_part("a$"));
		assertEquals("a.b", Filename.head_part("a.b.c"));
		assertEquals("a.b", Filename.head_part("a.b.c$1"));
		assertEquals("a.b", Filename.head_part("a.b.c$d"));
		assertEquals("a.b", Filename.head_part("a.b.$d"));
		assertEquals("a.b$1.c$d", Filename.head_part("a.b$1.c$d.X"));
		assertEquals("a.b$a", Filename.head_part("a.b$a.c$d"));
		assertEquals("a.b", Filename.head_part("a.b.c"));
		assertEquals("a.b.c.$", Filename.head_part("a.b.c.$.XXX"));
		assertEquals("a.b$c", Filename.head_part("a.b$c.x$d"));
		assertEquals("com.sun.corba.se.impl.io", Filename.head_part("com.sun.corba.se.impl.io.ValueUtility$IdentityKeyValueStack$KeyValuePair"));
		assertEquals("mypackage", Filename.head_part("mypackage.Myclass$1"));
		assertEquals("", Filename.head_part("a$1"));
		assertEquals("b", Filename.head_part("b.a$1"));
		assertEquals("b", Filename.head_part("b.a$c"));
		assertEquals("com.sun.corba.se.impl.encoding", Filename.head_part("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertEquals("com.sun.security.auth.callback", Filename.head_part("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertEquals("javax.swing", Filename.head_part("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
		assertEquals("", Filename.head_part("$1"));
		assertEquals("", Filename.head_part("a"));
		assertEquals("mypackage", Filename.head_part("mypackage.Myclass$A"));
		assertEquals("", Filename.head_part("a$1"));
		assertEquals("", Filename.head_part("a$b"));
		assertEquals("b", Filename.head_part("b.a$1"));
		assertEquals("b", Filename.head_part("b.a$b"));
		assertEquals("b", Filename.head_part("b.a$c"));
		assertEquals("com.sun.corba.se.impl.encoding", Filename.head_part("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertEquals("com.sun.security.auth.callback", Filename.head_part("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertEquals("com.sun.corba.se.impl.encoding", Filename.head_part("com.sun.corba.se.impl.encoding.CodeSetConversion$UTF16BTCConverter"));
		assertEquals("com.sun.corba.se.impl.encoding", Filename
		        .head_part("com.sun.corba.se.impl.encoding.IDLJavaSerializationInputStream$_ByteArrayInputStream"));
		assertEquals("javax.swing", Filename.head_part("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}

	@Test public void testTail() {
		assertEquals("", Filename.tail_part(""));
		assertEquals("a", Filename.tail_part("a"));
		assertEquals("$", Filename.tail_part("$"));
		assertEquals("$a", Filename.tail_part("$a"));
		assertEquals("a$", Filename.tail_part("a$"));
		assertEquals("c", Filename.tail_part("a.b.c"));
		assertEquals("c$1", Filename.tail_part("a.b.c$1"));
		assertEquals("c$d", Filename.tail_part("a.b.c$d"));
		assertEquals("$d", Filename.tail_part("a.b.$d"));
		assertEquals("X", Filename.tail_part("a.b$1.c$d.X"));
		assertEquals("c$d", Filename.tail_part("a.b$a.c$d"));
		assertEquals("c", Filename.tail_part("a.b.c"));
		assertEquals("XXX", Filename.tail_part("a.b.c.$.XXX"));
		assertEquals("x$d", Filename.tail_part("a.b$c.x$d"));
		assertEquals("ValueUtility$IdentityKeyValueStack$KeyValuePair", Filename
		        .tail_part("com.sun.corba.se.impl.io.ValueUtility$IdentityKeyValueStack$KeyValuePair"));
		assertEquals("Myclass$1", Filename.tail_part("mypackage.Myclass$1"));
		assertEquals("a$1", Filename.tail_part("a$1"));
		assertEquals("a$1", Filename.tail_part("b.a$1"));
		assertEquals("a$c", Filename.tail_part("b.a$c"));
		assertEquals("BufferManagerWriteCollect$1", Filename.tail_part("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertEquals("DialogCallbackHandler$2", Filename.tail_part("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertEquals("JSlider$1SmartHashtable$LabelUIResource", Filename.tail_part("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
		assertEquals("$1", Filename.tail_part("$1"));
		assertEquals("a", Filename.tail_part("a"));
		assertEquals("Myclass$A", Filename.tail_part("mypackage.Myclass$A"));
		assertEquals("a$1", Filename.tail_part("a$1"));
		assertEquals("a$b", Filename.tail_part("a$b"));
		assertEquals("a$1", Filename.tail_part("b.a$1"));
		assertEquals("a$b", Filename.tail_part("b.a$b"));
		assertEquals("a$c", Filename.tail_part("b.a$c"));
		assertEquals("CodeSetConversion$UTF16BTCConverter", Filename.tail_part("com.sun.corba.se.impl.encoding.CodeSetConversion$UTF16BTCConverter"));
		assertEquals("IDLJavaSerializationInputStream$_ByteArrayInputStream", Filename
		        .tail_part("com.sun.corba.se.impl.encoding.IDLJavaSerializationInputStream$_ByteArrayInputStream"));
		assertEquals("JSlider$1SmartHashtable$LabelUIResource", Filename.tail_part("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}

	@Test public void testTrailer() {
		assertEquals("", Filename.trailer_part(""));
		assertEquals("", Filename.trailer_part("a"));
		assertEquals("", Filename.trailer_part("$"));
		assertEquals("1", Filename.trailer_part("a$1"));
		assertEquals("", Filename.trailer_part("$1"));
		assertEquals("XYZ", Filename.trailer_part("abc$XYZ"));
		assertEquals("XYZ", Filename.trailer_part("ab$XYZ"));
		assertEquals("XYZ", Filename.trailer_part("a$XYZ"));
		assertEquals("1", Filename.trailer_part("abc$XYZ$1"));
		assertEquals("1", Filename.trailer_part("ab$XYZ$1"));
		assertEquals("1", Filename.trailer_part("a$XYZ$1"));
		assertEquals("", Filename.trailer_part("$XYZ"));
		assertEquals("", Filename.trailer_part("$abc"));
		assertEquals("", Filename.trailer_part("a$"));
		assertEquals("", Filename.trailer_part("a.b.c"));
		assertEquals("1", Filename.trailer_part("a.b.c$1"));
		assertEquals("d", Filename.trailer_part("a.b.c$d"));
		assertEquals("", Filename.trailer_part("a.b.$d"));
		assertEquals("", Filename.trailer_part("a.b$1.c$d.X"));
		assertEquals("d", Filename.trailer_part("a.b$a.c$d"));
		assertEquals("", Filename.trailer_part("a.b.c"));
		assertEquals("", Filename.trailer_part("a.b.c.$.XXX"));
		assertEquals("d", Filename.trailer_part("a.b$c.x$d"));
		assertEquals("KeyValuePair", Filename.trailer_part("com.sun.corba.se.impl.io.ValueUtility$IdentityKeyValueStack$KeyValuePair"));
		assertEquals("1", Filename.trailer_part("mypackage.Myclass$1"));
		assertEquals("1", Filename.trailer_part("a$1"));
		assertEquals("1", Filename.trailer_part("b.a$1"));
		assertEquals("c", Filename.trailer_part("b.a$c"));
		assertEquals("1", Filename.trailer_part("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertEquals("2", Filename.trailer_part("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertEquals("LabelUIResource", Filename.trailer_part("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
		assertEquals("", Filename.trailer_part("$1"));
		assertEquals("", Filename.trailer_part("a"));
		assertEquals("A", Filename.trailer_part("mypackage.Myclass$A"));
		assertEquals("1", Filename.trailer_part("a$1"));
		assertEquals("b", Filename.trailer_part("a$b"));
		assertEquals("1", Filename.trailer_part("b.a$1"));
		assertEquals("b", Filename.trailer_part("b.a$b"));
		assertEquals("c", Filename.trailer_part("b.a$c"));
		assertEquals("UTF16BTCConverter", Filename.trailer_part("com.sun.corba.se.impl.encoding.CodeSetConversion$UTF16BTCConverter"));
		assertEquals("_ByteArrayInputStream", Filename
		        .trailer_part("com.sun.corba.se.impl.encoding.IDLJavaSerializationInputStream$_ByteArrayInputStream"));
		assertEquals("LabelUIResource", Filename.trailer_part("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}

	@Test public void testClass2Path() {
		assertEquals("a.b.c", Filename.path2class("a.b.c"));
		assertEquals("a.b.c", Filename.path2class("a/b/c"));
		assertEquals("a.b.c", Filename.path2class("a/b/c.class"));
		assertEquals("a.b.c$1", Filename.path2class("a/b/c$1.class"));
		assertEquals("a.b.c$d", Filename.path2class("a/b/c$d.class"));
		assertEquals("a.b$1.c$d", Filename.path2class("a/b$1/c$d.class"));
		assertEquals("a.b$a.c$d", Filename.path2class("a/b$a/c$d.class"));
	}

	@Test public void testName2Canonical() {
		assertEquals("", Filename.name2Canonical(""));
		assertEquals("a", Filename.name2Canonical("a"));
		assertEquals("a.b.c", Filename.name2Canonical("a.b.c"));
		assertEquals("a.b.c.d", Filename.name2Canonical("a.b.c$d"));
		assertEquals("a.b$c.x.d", Filename.name2Canonical("a.b$c.x$d"));
		assertEquals("com.sun.corba.se.impl.io.ValueUtility.IdentityKeyValueStack.KeyValuePair", Filename
		        .name2Canonical("com.sun.corba.se.impl.io.ValueUtility$IdentityKeyValueStack$KeyValuePair"));
	}

	@Test public void testIsAnonymous() {
		assertTrue(Filename.isAnonymous("A$1"));
		assertFalse(Filename.isAnonymous("a"));
		assertTrue(Filename.isAnonymous("mypackage.Myclass$1"));
		assertTrue(Filename.isAnonymous("a$1"));
		assertTrue(Filename.isAnonymous("b.a$1"));
		assertFalse(Filename.isAnonymous("b.a$c"));
		assertTrue(Filename.isAnonymous("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertTrue(Filename.isAnonymous("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertFalse(Filename.isAnonymous("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}

	@Test public void testIsInner() {
		assertFalse(Filename.isInner("$1"));
		assertFalse(Filename.isInner("a"));
		assertTrue(Filename.isInner("mypackage.Myclass$A"));
		assertFalse(Filename.isInner("a$1"));
		assertTrue(Filename.isInner("a$b"));
		assertFalse(Filename.isInner("b.a$1"));
		assertTrue(Filename.isInner("b.a$b"));
		assertTrue(Filename.isInner("b.a$c"));
		assertFalse(Filename.isInner("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertFalse(Filename.isInner("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertTrue(Filename.isInner("com.sun.corba.se.impl.encoding.CodeSetConversion$UTF16BTCConverter"));
		assertTrue(Filename.isInner("com.sun.corba.se.impl.encoding.IDLJavaSerializationInputStream$_ByteArrayInputStream"));
		assertTrue(Filename.isInner("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}

	@Test public void testIsAllInner() {
		assertFalse(Filename.isAllInner("$1"));
		assertFalse(Filename.isAllInner("a"));
		assertTrue(Filename.isAllInner("mypackage.Myclass$A"));
		assertFalse(Filename.isAllInner("a$1"));
		assertTrue(Filename.isAllInner("a$b"));
		assertFalse(Filename.isAllInner("b.a$1"));
		assertTrue(Filename.isAllInner("b.a$b"));
		assertTrue(Filename.isAllInner("b.a$c"));
		assertFalse(Filename.isAllInner("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertFalse(Filename.isAllInner("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertTrue(Filename.isAllInner("com.sun.corba.se.impl.encoding.CodeSetConversion$UTF16BTCConverter"));
		assertTrue(Filename.isAllInner("com.sun.corba.se.impl.encoding.IDLJavaSerializationInputStream$_ByteArrayInputStream"));
		assertFalse(Filename.isAllInner("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}

	@Test public void testIsLocal() {
		assertFalse(Filename.isLocal("$1"));
		assertFalse(Filename.isLocal("a"));
		assertTrue(Filename.isLocal("mypackage.Myclass$2A"));
		assertFalse(Filename.isLocal("a$1"));
		assertFalse(Filename.isLocal("a$b"));
		assertFalse(Filename.isLocal("b.a$1"));
		assertFalse(Filename.isLocal("b.a$b"));
		assertTrue(Filename.isLocal("b.a$2c"));
		assertFalse(Filename.isLocal("com.sun.corba.se.impl.encoding.BufferManagerWriteCollect$1"));
		assertFalse(Filename.isLocal("com.sun.security.auth.callback.DialogCallbackHandler$2"));
		assertFalse(Filename.isLocal("com.sun.corba.se.impl.activation.RepositoryImpl$DBServerDef"));
		assertFalse(Filename.isLocal("javax.swing.JSlider$1SmartHashtable$LabelUIResource"));
	}
}
