package utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import utils.FileSystemVisitor.Action.StopTraversal;
import static utils.DBC.*;

/**
 * A class realizing a file system traversal algorithm, including devling into
 * archives such as ZIP and JAR files.
 * <p>
 * The traversal is carried out by calling the class constructor
 * {@link FileSystemVisitor#FileSystemVisitor(String[], utils.FileSystemVisitor.Action, String[])}
 * to set up the traversal parameters, and then function {@link #go()} to
 * conduct the actual traversal.
 * <p>
 * 
 * @author Yossi Gil
 * @date 21/05/2007
 * @see #go()
 * @see FileSystemVisitor#FileSystemVisitor(String[],
 *      utils.FileSystemVisitor.Action, String[])
 * @see Action
 */
public class FileSystemVisitor {
	/**
	 * Create a new visitor object to scan an array of {@link String}s naming
	 * the search locations.
	 * 
	 * @param from
	 *            a non-<code><b>null</b></code> specifying where the
	 *            traversal should begin. If this array is of length 0, then no
	 *            traversal shall be carried out. Also, if the supplied
	 *            directories are not disjoint, then the same file may be
	 *            visited more than once.
	 * @param visitor
	 *            a non-<code><b>null</b></code> specifying what to do in
	 *            each visited file
	 * @param extensions
	 *            an array of non-<code><b>null</b></code> {@link String}s
	 *            specifying which file extensions the visitor shold call, e.g.,
	 *            ".class", ".csv", etc. If this parameter is
	 *            <code><b>null</b></code>, or of length 0, or contains a
	 *            {@link String} of length 0, then the visitor is invoked for
	 *            all files found in the supplied path.
	 * @see #go
	 * @see #FileSystemVisitor(Collection, utils.FileSystemVisitor.Action,
	 *      String[])
	 * 
	 */
	public FileSystemVisitor(String[] from, Action visitor, String... extensions) {
		require(from != null);
		require(visitor != null);
		require(extensions != null);
		require(All.notNull(from));
		require(All.notNull(extensions));
		this.from = from;
		this.visitor = visitor;
		this.extensions = extensions;
	}

	/**
	 * Create a new visitor object to scan an array of {@link String}s naming
	 * the search locations.
	 * 
	 * @param from
	 *            a non-<code><b>null</b></code> specifying where the
	 *            traversal should begin. If this collection is empty, then no
	 *            traversal shall be carried out. Also, if the supplied
	 *            directories are not disjoint, then the same file may be
	 *            visited more than once.
	 * @param visitor
	 *            a non-<code><b>null</b></code> specifying what to do in
	 *            each visited file
	 * @param extensions
	 *            an array of non-<code><b>null</b></code> {@link String}s
	 *            specifying which file extensions the visitor shold call, e.g.,
	 *            ".class", ".csv", etc. If this parameter is
	 *            <code><b>null</b></code>, or of length 0, or contains a
	 *            {@link String} of length 0, then the visitor is invoked for
	 *            all files found in the supplied path.
	 * @see #go
	 * @see #FileSystemVisitor(String[], utils.FileSystemVisitor.Action,
	 *      String[])
	 * 
	 * 
	 */
	public FileSystemVisitor(Collection<String> from, Action visitor, String... extensions) {
		this(from.toArray(new String[0]), visitor, extensions);
	}

	/**
	 * Conduct the traversal. For each file encountered during the traversal,
	 * the {@link FileSystemVisitor} invokes one of
	 * <ol>
	 * <li>{@link Action#visitFile(File)},
	 * <li>{@link Action#visitDirectory(File)},
	 * <li> {@ink #visitZipEntry(String, String, InputStreamReader)}, or
	 * <li> {@link Action#visitZip(File)}
	 * </ol>
	 * functions, depending on the file type. If this function throws
	 * {@link Action.StopTraversal} exception, then the traversal stops:
	 * completely if the current file was a plain file, i.e.,
	 * {@link Action#visitFile(File)} or
	 * {@ink #visitZipEntry(String, String, InputStreamReader)} was called, or
	 * just of the contained files, if this was an arhive or a directory file.
	 * 
	 * @throws IOException
	 *             if the file system could not traversed for some reason
	 * @throws StopTraversal
	 *             if the visitor object requested to stop the visitation.
	 */
	public void go() throws IOException, StopTraversal {
		for (String s : from)
			recurse(new File(s));
	}

	/**
	 * Conduct recursive traversal starting at a given file
	 * 
	 * @param f
	 *            a file, which may be a directory, a zip, or a plain file, at
	 *            which the traversal begins
	 * @throws IOException
	 *             if the file system could not traversed for some reason
	 * @throws StopTraversal
	 *             if the visitor object requested to stop the visitation.
	 */
	private void recurse(final File f) throws IOException, StopTraversal {
		if (f.isDirectory()) {
			recurseDirecotry(f);
			return;
		}
		if (isZipFile(f)) {
			scanZip(f);
			return;
		}
		if (endsWith(f, extensions))
			visitor.visitFile(f);
	}

	/**
	 * conduct recursive traversal of a directory
	 * 
	 * @param f
	 *            a directory
	 * @throws IOException
	 *             if the file system could not traversed for some reason
	 */
	private void recurseDirecotry(File f) throws IOException {
		try {
			visitor.visitDirectory(f);
			for (final String name : f.list())
				recurse(new File(f, name));
		} catch (Action.StopTraversal e) {
			// do not visit children of this directory
		}
	}

	/**
	 * scan entries of a zip file
	 * 
	 * @param f
	 *            a zip or other archive file
	 * @throws IOException
	 *             if the file system could not traversed for some reason
	 * @throws StopTraversal
	 *             if the visitor object requested to stop the visitation.
	 *             However, if the visitor requrested to stop the visitation of
	 *             the zip file itself, the scanning of this zip file will stop,
	 *             but the no exception is thrown, and the entire traversal
	 *             contintue.
	 */
	private void scanZip(File f) throws IOException, StopTraversal {
		try {
			visitor.visitZip(f);
		} catch (Action.StopTraversal e) {
			return; // do not visit any elements of this zip file, but continue
			// traversal.
		}
		final ZipFile Z = new ZipFile(f.getAbsoluteFile());
		final Enumeration<? extends ZipEntry> es = Z.entries();
		while (es.hasMoreElements()) {
			final ZipEntry e = es.nextElement();
			try {
				if (e.isDirectory())
					visitor.visitZipDirectory(Z.getName(), e.getName(), Z.getInputStream(e));
				else if (endsWith(e.getName(), extensions))
					visitor.visitZipEntry(Z.getName(), e.getName(), Z.getInputStream(e));
			} catch (StopTraversal x) {
				System.out.printf("Found at zip\n!!!");
				throw x;
			}
		}
	}

	/**
	 * A list of all recognized extension archive file names
	 */
	private static final String[] ZIP_FILE_TYPES = { ".zip", ".jar", ".war", ".ear" };

	/**
	 * determine if a file name ends with any one of the supplied extensions
	 * 
	 * @param file
	 *            a file to examine
	 * @param extensions
	 *            a list of potential extensions
	 * @return <code><b>true</b></code> iff the file name ends with any one
	 *         of the supplied extensions.
	 */
	private static boolean endsWith(final File file, String[] extensions) {
		return endsWith(file.getName(), extensions);
	}

	/**
	 * determine if a string ends with any one of the supplied extensions
	 * 
	 * @param s
	 *            a string to examine
	 * @param suffixes
	 *            a list of potential extensions
	 * @return <code><b>true</b></code> iff the file name ends with any one
	 *         of the supplied extensions.
	 */
	private static boolean endsWith(final String s, String[] ends) {
		for (final String end : ends)
			if (s.endsWith(end))
				return true;
		return false;
	}

	/**
	 * @param file
	 *            a file to examine
	 * @return <code><b>true</b></code> iff the file is appears to be a zip
	 *         file.
	 */
	private static boolean isZipFile(final File file) {
		return endsWith(file, ZIP_FILE_TYPES);
	}

	/**
	 * @author Yossi Gil
	 * @date 21/05/2007
	 */
	public static interface Action {
		/**
		 * action to conduct for each ordinary, i.e., non-zip and non-directory,
		 * file.
		 * 
		 * @param f
		 *            the file to visit
		 * @throws StopTraversal
		 *             in case the visitor wishes to <i>completely</i> stop the
		 *             traversal
		 * @see #visitDirectory(File)
		 * @see #visitZip(File)
		 */
		void visitFile(File f) throws StopTraversal;

		/**
		 * action to conduct for each entry found in a zip file, encountered
		 * throughout the traversal
		 * 
		 * @param zipName
		 *            the name of the Zip file from which this entry was taken
		 * @param entryName
		 *            the name of the visited entry in the Zip file
		 * @param stream
		 *            an open stream into the content of this entry
		 * @throws StopTraversal
		 *             in case the visitor wishes to terminate the entire
		 *             traversal process
		 */
		void visitZipEntry(String zipName, String entryName, InputStream stream) throws StopTraversal;

		/**
		 * action to conduct for each directory encountered throught the
		 * traversal.
		 * 
		 * @param f
		 *            the directory file object
		 * @throws StopTraversal
		 *             in case the visitor wishes to stop the traversal of this
		 *             directory.
		 * @see #visitFile(File)
		 * @see #visitZip(File)
		 * @see #visitZipEntry(String, String, InputStream)
		 */
		void visitDirectory(File f) throws StopTraversal;

		/**
		 * action to conduct for each zip and other archive files encountered
		 * throught the traversal.
		 * 
		 * @param f
		 *            the archive file object
		 * @throws StopTraversal
		 *             in case the visitor wishes to stop the traversal of this
		 *             archive file.
		 * @see #visitFile(File)
		 * @see #visitDirectory(File)
		 */
		void visitZip(File f) throws StopTraversal;

		/**
		 * action to conduct for each directory encountered in an archival file
		 * scanned throught the traversal.
		 * 
		 * @param zipName
		 *            the name of the Zip file from which this entry was taken
		 * @param entryName
		 *            the name of the visited entry in the Zip file
		 * @param stream
		 *            an open stream into the content of this entry
		 * @throws StopTraversal
		 *             in case the visitor wishes to terminate the entire
		 *             traversal process
		 * @see #visitFile(File)
		 * @see #visitDirectory(File)
		 */
		void visitZipDirectory(String zipName, String entryName, InputStream stream) throws StopTraversal;

		/**
		 * @author Yossi Gil
		 * @date 21/05/2007
		 */
		public static class StopTraversal extends Exception {
			/**
			 * Create a new {@link StopTraversal} object
			 */
			public StopTraversal() {
				super();
			}

			/**
			 * Create a new {@link StopTraversal} object with a specific message
			 * 
			 * @param message
			 *            a message to record
			 * @see Exception
			 */
			public StopTraversal(String message) {
				super(message);
			}
		}
	}

	/**
	 * A simplified {@link Action} with no exceptions thrown whose partial
	 * implementation does nothing for directories and archives, and leaves it
	 * to the extending class to implement concrete a concrete action for files
	 * and zip entries, moreover, in visiting archive entries it uses a visiting
	 * function that ignores the the archive name.
	 * 
	 * @author Yossi Gil
	 * @date 18/06/2007
	 */
	public abstract static class FileOnlyAction implements Action {
		/**
		 * A do-nothing function, ignoring its arguments
		 * 
		 * @param _
		 *            ignored
		 * 
		 * @see utils.FileSystemVisitor.Action#visitDirectory(java.io.File)
		 */
		public final void visitDirectory(@SuppressWarnings("unused") File _) {
			// do nothing
		}

		/**
		 * A do-nothing function, ignoring its arguments
		 * 
		 * @param _
		 *            ignored
		 * @see utils.FileSystemVisitor.Action#visitZip(java.io.File)
		 */
		public final void visitZip(@SuppressWarnings("unused") File _) {
			// Do nothing
		}

		public final void visitZipDirectory(@SuppressWarnings("unused") String zipName, @SuppressWarnings("unused") String entryName,
		        @SuppressWarnings("unused") InputStream stream) {
			// Do nothing
		}

		public final void visitZipEntry(@SuppressWarnings("unused") String zipName, String entryName, InputStream stream) {
			visitZipEntry(entryName, stream);
		}

		public abstract void visitFile(File f);

		public abstract void visitZipEntry(String entryName, InputStream stream);
	}

	/**
	 * A simplified {@link Action} with no exceptions thrown whose partial
	 * implementation does nothing for files, archives and ordinary archive
	 * entries, and leaves it to the extending class to implement concrete a
	 * concrete action for file system directories and direcotries found within
	 * zip entries, moreover, in visiting directories found in archive entries
	 * it uses a visiting function that ignores the the archive name.
	 * 
	 * @author Yossi Gil
	 * @date 18/06/2007
	 */
	public abstract static class DirectoryOnlyAction implements Action {
		/**
		 * A do-nothing function, ignoring its parameters.
		 * 
		 * @param _
		 *            ignored
		 * @see utils.FileSystemVisitor.Action#visitZip(java.io.File)
		 */
		@SuppressWarnings("unused")//
		public final void visitZip(File _) {
			// Do nothing
		}

		/**
		 * A do-nothing function, ignoring its parameters.
		 * 
		 * @param _
		 *            ignored
		 * @see utils.FileSystemVisitor.Action#visitFile(java.io.File)
		 */
		@SuppressWarnings("unused")//
		public final void visitFile(File _) {
			// Do nothing
		}

		/**
		 * A do-nothing function, ignoring its parameters.
		 * 
		 * @param _
		 *            ignored
		 * @param __
		 *            ignored
		 * @param ___
		 *            ignored
		 * @see utils.FileSystemVisitor.Action#visitZipEntry(java.lang.String,
		 *      java.lang.String, java.io.InputStream)
		 */
		@SuppressWarnings("unused")//
		public final void visitZipEntry(String _, String __, InputStream ___) {
			// Do nothing
		}

		public final void visitZipDirectory(@SuppressWarnings("unused") String zipName, String entryName, InputStream stream) {
			visitZipDirectory(entryName, stream);
		}

		/**
		 * Visit a directory entry contained in an archive.
		 * 
		 * @param entryName
		 *            the name of the discovered directory
		 * @param stream
		 *            to be used for opening it if necessary
		 */
		protected abstract void visitZipDirectory(String entryName, InputStream stream);

		/**
		 * @param f
		 */
		public abstract void visitDirectory(File f) throws StopTraversal;
	}

	/**
	 * at which directories in the file system should the traversal start
	 */
	private final String[] from;
	/**
	 * what should be done for each file traversed
	 */
	private final Action visitor;
	/**
	 * for which extensions should the {@link Action} object be invoked.
	 */
	private final String[] extensions;
}
