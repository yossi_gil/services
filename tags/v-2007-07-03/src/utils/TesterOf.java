package utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation allows the programmer to associate a test-case class with the
 * class that it tests.
 * 
 * @see TestedBy
 */
@Retention(RetentionPolicy.RUNTIME) public @interface TesterOf {
	public Class<?>[] value();
}
