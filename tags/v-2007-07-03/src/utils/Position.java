package utils;

import java.io.Serializable;

/**
 * Represents a position in a file, including a column and line number.
 * 
 * @author Yossi Gil
 * @date 13/06/2007
 */
public final class Position implements Comparable<Position>, Serializable {
	private static final long serialVersionUID = -9094620074260625651L;
	public final int line;
	public final int column;

	@Override public String toString() {
		return "(" + line + ":" + column + ")";
	}

	/**
	 * @param line
	 *            the line of this position
	 * @param column
	 *            the column of this position
	 */
	public Position(final int line, final int column) {
		this.line = line;
		this.column = column;
	}

	public boolean before(Position p) {
		return compareTo(p) < 0;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		final Position other = (Position) o;
		return column == other.column && line == other.line;
	}

	public int compareTo(Position p) {
		return line != p.line ? line - p.line : column - p.column;
	}
}
