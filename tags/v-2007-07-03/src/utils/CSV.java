package utils;

import static utils.DBC.require;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class realize the CSV specification, by comprising methods for
 * manipulating CSV files. e.g. 1, 2, 3 4, 5, 6
 * 
 * The class supports string arrays as data. e.g. 1, "{9; 10}", 3 4, "{8; 11}",
 * 6
 * 
 * This class also supports converting strings to enums instances. e.g. 1,
 * EXTENDS, 3 4, IMPLEMENTS, 6
 * 
 * This is a simplified version of the CSV specification, each record must be a
 * single line. Within are some other useful auxilary functions for string
 * manipulations.
 * 
 * @author Oren Rubin
 */
public class CSV {
	public static void save(File file, String[][] data) throws IOException {
		PrintWriter pw = new PrintWriter(new FileWriter(file));
		pw.print(toCsv(data));
		pw.close();
	}

	public static String toCsv(String[][] data) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		for (String[] line : data) {
			boolean isFirst = true;
			for (String s : line) {
				pw.print(isFirst ? "" : ",");
				isFirst = false;
				pw.print(escape(s));
			}
			pw.println();
		}
		pw.flush();
		return sw.toString();
	}

	/**
	 * Read a CSV file.
	 * 
	 * @param file
	 *            Input file
	 * @return A two dimensional array of strings
	 * @throws IOException
	 *             some problem with file 'filename'
	 */
	public static String[][] load(File file) throws IOException {
		return load(new FileReader(file));
	}

	/**
	 * Read a CSV file from the given Reader object.
	 * 
	 * @param r
	 *            Input reader
	 * @return A two dimensional array of strings
	 */
	public static String[][] load(Reader r) {
		ArrayList<String[]> result = new ArrayList<String[]>(20);
		for (Scanner s = new Scanner(r); s.hasNext();)
			result.add(split(s.nextLine()));
		return result.toArray(new String[result.size()][]);
	}

	private static final String NULL = "\\0";

	/**
	 * Escape the given input
	 * 
	 * @param in
	 *            Input string
	 * @return Escaped form of the input
	 */
	public static String escape(String in) {
		if (in == null)
			return NULL;
		int len = in.length();
		StringBuilder out = new StringBuilder(len);
		for (int i = 0; i < len; ++i) {
			char c = in.charAt(i);
			if (c == '\\')
				out.append("\\\\");
			else if (c == '\n')
				out.append("\\n");
			else if (c == '\r')
				out.append("\\r");
			else if (c == '\t')
				out.append("\\t");
			else if (c == ',')
				out.append("\\.");
			else
				out.append(c);
		}
		return out.toString();
	}

	/**
	 * Unescape the given input
	 * 
	 * @param in
	 *            Input string
	 * @return Unescaped string
	 */
	public static String unescape(String in) {
		if (NULL.equals(in))
			return null;
		boolean esc = false;
		int len = in.length();
		StringBuilder out = new StringBuilder(len);
		for (int i = 0; i < len; ++i) {
			char c = in.charAt(i);
			if (c == 'n' && esc) {
				out.append("\n");
				esc = false;
			} else if (c == 'r' && esc) {
				out.append("\r");
				esc = false;
			} else if (c == 't' && esc) {
				out.append("\t");
				esc = false;
			} else if (c == '.' && esc) {
				out.append(",");
				esc = false;
			} else if (c == '\\' && esc) {
				out.append("\\");
				esc = false;
			} else if (c == '\\' && !esc) {
				esc = true;
			} else {
				out.append(c);
				esc = false;
			}
		}
		return out.toString();
	}

	/**
	 * Split a comma separated string into its sub parts
	 * 
	 * @param s
	 *            Input string
	 * @return Array of sub parts, in their original order
	 */
	public static String[] split(String s) {
		if (s.length() == 0)
			return new String[0];
		List<String> lst = new ArrayList<String>();
		int begin = 0;
		while (true) {
			int pos = s.indexOf(',', begin);
			if (pos < 0) {
				pos = s.length();
				String part = s.substring(begin, s.length());
				String t = unescape(part);
				lst.add(t);
				break;
			}
			lst.add(unescape(s.substring(begin, pos)));
			begin = pos + 1;
		}
		return lst.toArray(new String[lst.size()]);
	}

	/**
	 * Split a comma separated string into an array of enum values.
	 * 
	 * @param <T>
	 *            Type of enum class
	 * @param cls
	 *            Class object of T
	 * @param s
	 *            Input string
	 * @return Array of T
	 */
	@SuppressWarnings("unchecked") public static <T extends Enum<T>> T[] split(Class<T> cls, String s) {
		String[] arr = split(s);
		T[] result = (T[]) Array.newInstance(cls, arr.length);
		for (int i = 0; i < result.length; ++i)
			result[i] = arr[i] == null ? null : Enum.valueOf(cls, arr[i]);
		return result;
	}

	/**
	 * Split a comma separated string into an array of classes.
	 * 
	 * @param s
	 *            Input string
	 * @return Array of T
	 */
	@SuppressWarnings("unchecked") public static Class<?>[] splitToClasses(String s) {
		String[] arr = split(s);
		Class<?>[] result = new Class<?>[arr.length]; // (T[])
		// Array.newInstance(cls,
		// arr.length);
		for (int i = 0; i < result.length; ++i) {
			try {
				Class<?> c = arr[i] == null ? null : Class.forName(arr[i]);
				result[i] = c;
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		return result;
	}

	/**
	 * Combine the given array into a comma separated string. Each element is
	 * escapifieid, so commas inside the elements cannot do not collide with the
	 * separating commas.
	 * 
	 * @param <T>
	 *            type of array elements
	 * @param parts
	 *            Input array
	 * @return Combined string
	 * @see CSV#escape(String)
	 */
	public static <T> String combine(T[] parts) {
		require(parts != null);
		StringBuilder result = new StringBuilder(parts.length * 10);
		boolean isFirst = true;
		for (T t : parts) {
			result.append(isFirst ? "" : ",");
			isFirst = false;
			result.append(escape(t == null ? null : t.toString()));
		}
		return result.toString();
	}

	/**
	 * Combine the given array of enum values into a comma separated string.
	 * Each array element is first converted into a string using its name()
	 * method and then is escapifieid.
	 * 
	 * @param <T>
	 *            type of array elements
	 * @param parts
	 *            Input array
	 * @return Combined string
	 * @see CSV#escape(String)
	 */
	public static <T extends Enum<T>> String combine(T[] parts) {
		String[] arr = new String[parts.length];
		for (int i = 0; i < arr.length; ++i)
			arr[i] = parts[i] == null ? null : parts[i].name();
		return combine(arr);
	}

	/**
	 * Combine the given array of Class objects values into a comma separated
	 * string.
	 * 
	 * @param classes
	 *            Input array
	 * @return Combined string
	 * @see #splitToClasses(String)
	 */
	public static String combine(Class<?>[] classes) {
		String[] arr = new String[classes.length];
		for (int i = 0; i < arr.length; ++i)
			arr[i] = classes[i] == null ? null : classes[i].getName();
		return combine(arr);
	}
}
