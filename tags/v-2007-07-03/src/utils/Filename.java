package utils;

import java.io.File;

@TestedBy(Filename_Test.class)//
public enum Filename {,;
	/**
	 * Convert a file name as found in the file system, to a class name.
	 * 
	 * @param path
	 *            a file name to be converted
	 * @param root
	 *            the root of the packages directory
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2class(String path, String root) {
		return path2class(removeRoot(path, root));
	}

	/**
	 * Convert a relative file name into a class name.
	 * 
	 * @param path
	 *            a relative path name, with respect to JAVA packages directory
	 *            system
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2class(String path) {
		return path.replaceAll("\\.class$", "").replaceAll("[\\/]", ".");
	}

	/**
	 * Convert an absoulte directory name as found in the file system, to a
	 * class name.
	 * 
	 * @param path
	 *            a file name to be converted
	 * @param root
	 *            the root of the packages directory
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2package(String path, String root) {
		return path2package(removeRoot(path, root));
	}

	/**
	 * Convert a relative directory name into a pakcage name.
	 * 
	 * @param path
	 *            a relative path name, with respect to JAVA packages directory
	 *            system
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2package(String path) {
		return path.replaceAll("[\\/]", ".");
	}

	/**
	 * Trim the root portion of an absolute path
	 * 
	 * @param path
	 *            the absolute path
	 * @param root
	 *            the root portion to be trimmed the path
	 * @return path, but without the root portion
	 */
	public static String removeRoot(String path, String root) {
		return path.replace(root + File.separator, "");
	}

	public static String head_part(String name) {
		int index = name.lastIndexOf('.');
		return index < 0 ? "" : name.substring(0, index);
	}

	public static String tail_part(String name) {
		int index = name.lastIndexOf('.');
		return index < 0 ? name : name.substring(index + 1);
	}

	public static boolean isAnonymous(String name) {
		return trailer_part(name).matches("[0-9]+");
	}

	public static boolean isLocal(String name) {
		return trailer_part(name).matches("[0-9][A-Za-z_$].*");
	}

	public static boolean isInner(String name) {
		return trailer_part(name).matches("[A-Za-z_$].*");
	}

	public static boolean isAllInner(String name) {
		return isInner(name) && !tail_part(name).matches(".*[$][0-9].*");
	}

	public static String name2Canonical(String name) {
		for (String before = name, after;; before = after) {
			after = before.replaceFirst("\\$([a-zA-Z_][a-zA-Z0-9_$]*)$", ".$1");
			if (after.equals(before))
				return after;
		}
	}

	public static String trailer_part(String name) {
		String tail = tail_part(name);
		int index = tail.lastIndexOf('$');
		return index < 1 ? "" : tail.substring(index + 1);
	}
}
