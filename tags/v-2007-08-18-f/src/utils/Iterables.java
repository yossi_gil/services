package utils;

import static utils.DBC.require;
import java.lang.reflect.Array;
import java.util.*;

/**
 * This class consists exclusively of static methods that operate on or return
 * Iterable objects (a-la java.util.Collections).
 */
public abstract class Iterables {
	private Iterables() {
		// Non instantiable
	}

	@SuppressWarnings("unused") public static <T, F extends T> List<T> upcast(Class<T> cls, Iterable<F> fs) {
		List<T> lst = new ArrayList<T>();
		for (F f : fs)
			lst.add(f);
		return lst;
	}

	public static <E> Iterable<E> sortAsString(Iterable<E> in) {
		return sort(in, new Comparator<E>() {
			public int compare(E o1, E o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
	}

	public static <E> List<E> sort(Iterable<E> in, Comparator<E> c) {
		List<E> lst = toList(in);
		Collections.sort(lst, c);
		return lst;
	}

	public static <E> Iterable<E> reverse(Iterable<E> in) {
		List<E> lst = toList(in);
		Collections.reverse(lst);
		return lst;
	}

	public static <E> List<E> toList(Iterable<E> in) {
		List<E> lst = new ArrayList<E>();
		for (E e : in)
			lst.add(e);
		return lst;
	}

	@SuppressWarnings("unchecked") public static <E> E[] toArray(Iterable<E> in, Class<E> cls) {
		List<E> es = toList(in);
		E[] arr = (E[]) Array.newInstance(cls, es.size());
		return es.toArray(arr);
	}

	@SuppressWarnings("unchecked") public static <E> Iterable<E> fromArray(E... es) {
		return Arrays.asList(es);
	}

	public static <E> Collection<E> join(Iterable<E>... ess) {
		List<E> $ = new ArrayList<E>();
		for (Iterable<E> es : ess)
			for (E e : es)
				$.add(e);
		return $;
	}

	public static <E> Collection<E> set(Iterable<E>... ess) {
		Set<E> $ = new HashSet<E>();
		for (Iterable<E> es : ess)
			for (E e : es)
				$.add(e);
		return $;
	}

	public static <E> E first(Iterable<E> es) {
		require(es.iterator().hasNext());
		return es.iterator().next();
	}

	public static <E> E prepend(E e, Iterable<E> es) {
		return toList(es).set(0, e);
	}

	public static <F, T> Iterable<T> map(Iterable<? extends F> fs, Function<F, T> func) {
		List<T> ts = new ArrayList<T>();
		for (F f : fs)
			ts.add(func.eval(f));
		return ts;
	}

	@SuppressWarnings("unused") public static int size(Iterable<?> specs) {
		int n = 0;
		for (final Object o : specs)
			++n;
		return n;
	}

	/**
	 * Create an empty iterable
	 * 
	 * @param <T>
	 *        Type of elements
	 * @param cls
	 *        Class object of T
	 * @return An empty collection of type T
	 */
	public static <T> Iterable<T> empty(@SuppressWarnings("unused") Class<T> cls) {
		return new ArrayList<T>();
	}

	/**
	 * Sort a given Iterable object
	 * 
	 * @param <T>
	 *        Type of elements
	 * @param ts
	 *        Iterable of T objects
	 * @return A new Iterable object, with all elements of ts, sorted
	 */
	public static <T extends Comparable<T>> Iterable<T> sort(Iterable<T> ts) {
		List<T> lst = toList(ts);
		Collections.sort(lst);
		return lst;
	}

	/**
	 * Add every element of the given Iterable object to an existing list
	 * 
	 * @param <T>
	 *        Type of elements
	 * @param trg
	 *        List to which elements are added
	 * @param src
	 *        Elements to add
	 */
	public static <T> void addAll(List<T> trg, Iterable<T> src) {
		for (T a : src)
			trg.add(a);
	}

	/**
	 * Obtain a string representation of an iterable object
	 * 
	 * @param src
	 *        An iterable object
	 * @param sep
	 *        String to separate two consecutive elements
	 * @return String representation
	 */
	public static String toString(Iterable<?> src, String sep) {
		boolean isFirst = true;
		StringBuilder sb = new StringBuilder(100);
		for (Object o : src) {
			if (!isFirst)
				sb.append(sep);
			isFirst = false;
			sb.append(o);
		}
		return sb.toString();
	}

	/**
	 * Obtain a string representation of a map
	 * 
	 * @param <K>
	 *        Type of keys
	 * @param <V>
	 *        Type of values
	 * 
	 * @param src
	 *        A map
	 * @param eq
	 *        String to separatre the key from the data
	 * @param sep
	 *        String to separate two consecutive entries
	 * @return String representation
	 */
	public static <K, V> String toString(Map<K, V> src, String eq, String sep) {
		boolean isFirst = true;
		StringBuilder sb = new StringBuilder(100);
		for (K k : Iterables.sortAsString(src.keySet())) {
			if (!isFirst)
				sb.append(sep);
			isFirst = false;
			sb.append(k + eq + src.get(k));
		}
		return sb.toString();
	}

	/**
	 * Pair-wise comparison of two collections
	 * 
	 * @param <T>
	 *        type of elements in both collections
	 * 
	 * @param a
	 *        First collection
	 * @param b
	 *        Second collection
	 * @return 0 if equal, -1 if a is before b, 1 otherwise
	 */
	public static <T extends Comparable<T>> int compare(Iterable<T> a, Iterable<T> b) {
		Iterator<T> ia = a.iterator();
		Iterator<T> ib = b.iterator();
		while (ia.hasNext() && ib.hasNext()) {
			T ta = ia.next();
			T tb = ib.next();
			int comp = ta.compareTo(tb);
			if (comp != 0)
				return comp;
		}
		if (!ia.hasNext() && !ib.hasNext())
			return 0;
		if (ia.hasNext())
			return 1;
		return -1;
	}
}
