package utils.files;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static utils.testing.Assert.assertEquals;
import java.io.File;
import java.util.List;
import org.junit.Test;
import utils.ClassPathInfo;

public class JRETest {
	@Test public final void testAsList() {
		List<File> l = JRE.asList();
		assertNotNull(l);
		ClassPathInfo cpi = new ClassPathInfo(l);
		assertEquals(l.size(), cpi.files.length);
		assertTrue(cpi.getClasses().contains("java.lang.Object"));
		assertFalse(cpi.getClasses().contains(this.getClass().getName()));
	}

	@Test public final void testFromClass() {
		List<File> l = JRE.fromClass(this.getClass());
		assertNotNull(l);
		assertTrue(l.size() > 0);
	}
}
