package utils;

import static utils.DBC.require;

/**
 * A bunch of STRING functions.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il>
 * @date 05/06/2007
 */
public enum STRING {
	// No elements in this name space
	/**
	 * Strip the first and last character of a string.
	 * 
	 * @param s
	 *        a string to strip
	 * @return <code>s</code> but without its first and last character.
	 */
	;
	public static String strip(String s) {
		require(s != null);
		require(s.length() >= 2);
		return s.substring(1, s.length() - 1);
	}

	/**
	 * Concatenate any number of strings.
	 * 
	 * @param ss
	 *        a variable number of strtings
	 * @return the concatentation of the strings in <code>ss</code>
	 */
	public static String cat(String... ss) {
		StringBuilder b = new StringBuilder("");
		for (String s : ss)
			b.append(s);
		return b.toString();
	}

	public static String cat(String[]... ss) {
		StringBuilder b = new StringBuilder("");
		for (String[] s : ss)
			b.append(cat(s));
		return b.toString();
	}

	public static String fill(int n, char c) {
		return fill(n, Character.toString(c));
	}

	public static String fill(int n, String s) {
		String $ = "";
		for (int i = 0; i < n; ++i)
			$ += s;
		return $;
	}
}
