package il.ac.technion.cs.ssdl.utils;

import static il.ac.technion.cs.ssdl.utils.DBC.require;
import java.lang.reflect.Array;
import java.util.*;

/**
 * This class consists exclusively of static methods that operate on or return
 * Iterable objects (a-la java.util.Collections).
 */
public abstract class Iterables {
	private Iterables() {
		// Non instantiable
	}

	public static <T, F extends T> List<T> upcast(@SuppressWarnings("unused") Class<T> cls, Iterable<F> fs) {
		final List<T> $ = new ArrayList<T>();
		for (final F f : fs)
			$.add(f);
		return $;
	}

	public static <E> Iterable<E> sortAsString(Iterable<E> in) {
		return sort(in, new Comparator<E>() {
			public int compare(E o1, E o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
	}

	public static <E> List<E> sort(Iterable<E> in, Comparator<E> c) {
		final List<E> $ = toList(in);
		Collections.sort($, c);
		return $;
	}

	public static <E> Iterable<E> reverse(Iterable<E> in) {
		final List<E> $ = toList(in);
		Collections.reverse($);
		return $;
	}

	public static <E> List<E> toList(Iterable<E> in) {
		final List<E> $ = new ArrayList<E>();
		for (final E e : in)
			$.add(e);
		return $;
	}

	public static <E> E[] toArray(Iterable<E> in, Class<E> cls) {
		final List<E> es = toList(in);
		@SuppressWarnings("unchecked") final E[] arr = (E[]) Array.newInstance(cls, es.size());
		return es.toArray(arr);
	}

	@SuppressWarnings("unchecked") public static <E> Iterable<E> fromArray(E... es) {
		return Arrays.asList(es);
	}

	public static <E> Collection<E> join(Iterable<E>... ess) {
		final List<E> $ = new ArrayList<E>();
		for (final Iterable<E> es : ess)
			for (final E e : es)
				$.add(e);
		return $;
	}

	public static <E> Collection<E> set(Iterable<E>... ess) {
		final Set<E> $ = new HashSet<E>();
		for (final Iterable<E> es : ess)
			for (final E e : es)
				$.add(e);
		return $;
	}

	public static <E> E first(Iterable<E> es) {
		require(es.iterator().hasNext());
		return es.iterator().next();
	}

	public static <E> E prepend(E e, Iterable<E> es) {
		return toList(es).set(0, e);
	}

	public static <F, T> Iterable<T> map(Iterable<? extends F> fs, Function<F, T> func) {
		final List<T> ts = new ArrayList<T>();
		for (final F f : fs)
			ts.add(func.eval(f));
		return ts;
	}

	@SuppressWarnings("unused") public static int size(Iterable<?> specs) {
		int n = 0;
		for (final Object o : specs)
			++n;
		return n;
	}

	/**
	 * Create an empty iterable
	 * 
	 * @param <T>
	 *        Type of elements
	 * @param cls
	 *        Class object of T
	 * @return An empty collection of type T
	 */
	public static <T> Iterable<T> empty(@SuppressWarnings("unused") Class<T> cls) {
		return new ArrayList<T>();
	}

	/**
	 * Sort a given Iterable object
	 * 
	 * @param <T>
	 *        Type of elements
	 * @param ts
	 *        Iterable of T objects
	 * @return A new Iterable object, with all elements of ts, sorted
	 */
	public static <T extends Comparable<T>> Iterable<T> sort(Iterable<T> ts) {
		final List<T> $ = toList(ts);
		Collections.sort($);
		return $;
	}

	/**
	 * Add every element of the given Iterable object to an existing list.
    * This static metod compensates for the lack of an instance method
    * <code>addAll(Iterable&lt;T^gt;> ts)</code> in <code>java.util.List</code>
    * and other classes.
	 * 
	 * @param <T>
	 *        Type of elements
	 * @param target
	 *        List to which elements are added
	 * @param src
	 *        Elements to add
	 */
	public static <T> void addAll(List<T> target, Iterable<T> src) {
		for (final T a : src)
			target.add(a);
	}

	/**
	 * Obtain a string representation of an iterable object
	 * 
	 * @param src
	 *        An iterable object
	 * @param between
	 *        String to separate two consecutive elements
	 * @return String representation
	 */
	public static String toString(Iterable<?> src, String between) {
		final Separator s = new Separator(between);
		final StringBuilder sb = new StringBuilder(100);
		for (final Object o : src)
			sb.append(s + o.toString());
		return sb.toString();
	}

	/**
	 * Obtain a string representation of a map
	 * 
	 * @param <K>
	 *        Type of keys
	 * @param <V>
	 *        Type of values
	 * 
	 * @param src
	 *        A map
	 * @param eq
	 *        String to separatre the key from the data
	 * @param between
	 *        String to separate two consecutive entries
	 * @return String representation
	 */
	public static <K, V> String toString(Map<K, V> src, String eq, String between) {
		final StringBuilder sb = new StringBuilder(100);
		final Separator s = new Separator(between);
		for (final K k : Iterables.sortAsString(src.keySet()))
			sb.append(s + k.toString() + eq + src.get(k));
		return sb.toString();
	}

	/**
	 * Pair-wise comparison of two collections
	 * 
	 * @param <T>
	 *        type of elements in both collections
	 * 
	 * @param a
	 *        First collection
	 * @param b
	 *        Second collection
	 * @return 0 if equal, -1 if a is before b, 1 otherwise
	 */
	public static <T extends Comparable<T>> int compare(Iterable<T> a, Iterable<T> b) {
		final Iterator<T> ia = a.iterator();
		final Iterator<T> ib = b.iterator();
		while (ia.hasNext() && ib.hasNext()) {
			final int comp = ia.next().compareTo(ib.next());
			if (comp != 0)
				return comp;
		}
		if (!ia.hasNext() && !ib.hasNext())
			return 0;
		if (ia.hasNext())
			return 1;
		return -1;
	}
}
