package il.ac.technion.cs.ssdl.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * This cache class allows a value to be retrieved by either by a String key or
 * by another value. If the key object was used in the past, the returned value
 * is the previously created object.
 * 
 * @param <V>
 *        The Value type
 */
public abstract class BasicFactory<V> implements FactoryConcept<V> {
	private final Map<V, V> map = new HashMap<V, V>();

	protected abstract V stringToValue(String s);

	public V fromString(String s) {
		return from(stringToValue(s));
	}

	public V from(V k) {
		final V v = map.get(k);
		if (v != null)
			return v;
		map.put(k, k);
		return k;
	}
}
