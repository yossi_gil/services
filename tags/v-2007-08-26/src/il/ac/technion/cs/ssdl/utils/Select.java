package il.ac.technion.cs.ssdl.utils;

public enum Select {
	;
	public static <T> T select(T v, T defaultValue) {
		return v != null ? v : defaultValue;
	}

	public static int select(Integer v, Integer defaultValue) {
		return v != null ? v.intValue() : defaultValue.intValue();
	}

	public static int select(Integer v, int defaultValue) {
		return v != null ? v.intValue() : defaultValue;
	}
}
