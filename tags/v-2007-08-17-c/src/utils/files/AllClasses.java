package utils.files;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import utils.files.FileSystemVisitor.Action.StopTraversal;

public class AllClasses {
	public static void main(String[] args) throws IOException, StopTraversal {
		for (final String root : CLASSPATH.asArray())
			new ClassFilesVisitor(root, new FileSystemVisitor.FileOnlyAction() {
				@Override public void visitFile(File f) {
					System.out.println(Filename.path2class(f.getAbsolutePath(), root));
				}

				@Override public void visitZipEntry(String entryName, @SuppressWarnings("unused") InputStream _) {
					System.out.println(Filename.path2class(entryName, root));
				}
			}).go();
	}
}
