package utils;

/**
 * An interface representing the factory concept. Every factory class must have
 * a method that given a string, i.e., a literla, returns an actual value.
 * 
 * @param <V>
 *            The super type of all run-time values
 */
public interface FactoryConcept<V> {
	V fromString(String s);
}
