package utils;

import java.util.Formatter;

/**
 * A simple implementation of design by contract. Violations are reported to
 * <code>System.err</code.>. Error descriptions are passed by a <code>printf</code> 
 * like argument syntax. Often used with static import.
 * 
 * @author Yossi Gil (11/01/2006)
 */
public final class DBC {
	/**
	 * Hanlder for program exit requests. Default behavior: calls System.exit()
	 */
	public static StopHandler stopHandler = new StopHandler() {
		public void stop(int exitCode) {
			System.exit(exitCode);
		}
	};

	private DBC() {
		// Prevent accidental instantiations of this class.
	}

	public static void xinvariants(Checkable c) {
		try {
			c.toString();
		} catch (Throwable t) {
			// Absorb
		}
		c.invariant();
	}

	/**
	 * A possibly non-returning method to be used for checking pre-conditions.
	 * 
	 * @param cond
	 *            if false, program will halt.
	 * @param args
	 *            a list of strings in a <code>printf</code> like format
	 *            describing the violation.
	 */
	public static void require(boolean cond, String... args) {
		error(cond, "Requirement not met", args);
	}

	/**
	 * A possibly non-returning method to be used for checking post-conditions.
	 * 
	 * @param cond
	 *            if false, program will halt.
	 * @param args
	 *            a list of strings in a <code>printf</code> like format
	 *            describing the violation.
	 */
	public static void ensure(boolean cond, String... args) {
		error(cond, "Promise not kept", args);
	}

	/**
	 * A never-returning method to be used in points of code which should never
	 * be reached.
	 * 
	 * @param args
	 *            a list of strings in a <code>printf</code> like format
	 *            describing the violation.
	 */
	public static void unreachable(String... args) {
		error("This point in the code should never be reached", args);
	}

	/**
	 * A possibly non-returning method to be used for checking assertions.
	 * 
	 * @param cond
	 *            If false, then the condition is violated and execution
	 *            terminates with an error report.
	 * @param args
	 *            a list of strings in a <code>printf</code> like format
	 *            describing the violation.
	 */
	public static void sure(boolean cond, String... args) {
		error(cond, "Assumption failed", args);
	}

	/**
	 * A never-returning method indicating code sites with missing functionality
	 * 
	 * @param args
	 *            a list of strings in a <code>printf</code> like format
	 *            describing the task to be done.
	 */
	public static void todo(String... args) {
		error("Feature unsupported. ", args);
	}

	/**
	 * A never-returning method to be used for dealing with assertions that
	 * should stop the program run.
	 * 
	 * @param t
	 *            The exeption to be printed.
	 */
	public static void stop(Throwable t) {
		stop(t, "Program must stop due to this error: ");
	}

	/**
	 * A never-returning method to be used for dealing with assertions that
	 * should stop the program run.
	 * 
	 * @param t
	 *            The exeption to be printed. *
	 * @param s
	 *            A more detailed error description;
	 */
	public static void stop(Throwable t, String s) {
		System.err.println(s);
		t.printStackTrace();
		stop(-1);
	}

	/**
	 * A possibly non returning method used in class implementation.
	 * 
	 * @param cond
	 *            If false, method will not return and print error message.
	 * @param kind
	 *            A string describing the error kind, e.g., pre-condition
	 *            failure
	 * @param args
	 *            Additional stings describing the error kind in a
	 *            <code>printf</code> format.
	 */
	private static void error(boolean cond, String kind, String... args) {
		if (cond)
			return;
		error(kind, args);
	}

	private static void error(String kind, String... args) {
		String s = buildMessage(kind, args);
		System.out.flush();
		System.err.flush();
		System.err.println(s);
		System.err.flush();
		Log.printStackTrace();
		stop(-1);
	}

	private static void stop(int exitCode) {
		stopHandler.stop(exitCode);
	}

	public static String sprintf(String format, String... args) {
		Formatter f = new Formatter();
		Object os[] = args;
		f.format(format, os);
		return f.toString();
	}

	public static String buildMessage(String kind, String... args) {
		String result = kind + " ";
		switch (args.length) {
			case 0:
				break;
			case 1:
				result += args[0];
				break;
			default:
				Object os[] = new Object[args.length - 1];
				for (int i = 1; i < args.length; i++)
					os[i - 1] = args[i];
				Formatter f = new Formatter();
				f.format(args[0], os);
				result += f.out();
		}
		return result;
	}

	/**
	 * Callback for handling program exit requests
	 */
	public interface StopHandler {
		public void stop(int exitCode);
	}
}
