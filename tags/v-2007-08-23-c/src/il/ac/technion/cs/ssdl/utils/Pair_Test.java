package il.ac.technion.cs.ssdl.utils;

import static il.ac.technion.cs.ssdl.testing.Assert.assertEquals;
import org.junit.Test;

public class Pair_Test {
	@SuppressWarnings("all") public static class NamedPair extends Pair<Integer, Integer> {
		public final String name;

		public NamedPair(String name, Integer a, Integer b) {
			super(a, b);
			this.name = name;
		}

		public String toString() {
			return name + super.toString();
		}

		public boolean equals(Pair<Integer, Integer> o) {
			if (o == this)
				return true;
			if (o == null)
				return false;
			if (!(o instanceof NamedPair))
				return false;
			final NamedPair that = (NamedPair) o;
			return a.equals(that.a) && b.equals(that.b) && name.equals(that.name);
		}
	}

	@Test public void testSymmetry() {
		final Pair<Integer, Integer> p1 = new NamedPair("a", new Integer(10), new Integer(20));
		final Pair<Integer, Integer> p2 = new Pair<Integer, Integer>(new Integer(10), new Integer(20));
		assertEquals(p1.equals(p2), p2.equals(p1));
	}
}
