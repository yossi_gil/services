package il.ac.technion.cs.ssdl.utils;

public class Exceptions
{
   /**
    * Translate any exception to an unchecked exception
    * 
    * @param t Exception to translate
    * @return An unchecked exception
    */
   public static RuntimeException makeUnchecked(Throwable t)
   {
      if(t instanceof RuntimeException)
         return (RuntimeException) t;
      
      RuntimeException e = new RuntimeException(t);
      e.setStackTrace(t.getStackTrace());
      return e;       
   }
}
