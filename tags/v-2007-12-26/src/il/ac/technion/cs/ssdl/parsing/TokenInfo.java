/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 ******************************************************************************/
package il.ac.technion.cs.ssdl.parsing; 
/**
 * This class represents a token that was recognized by a lexical scanner.
 */
public class TokenInfo {
	public final int kind;
	public final int beginLine, beginColumn, endLine, endColumn;
	public final String image;

	public TokenInfo(int kind, int beginLine, int beginColumn, int endLine, int endColumn, String image) {
		this.kind = kind;
		this.beginLine = beginLine;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
		this.image = image;
	}

	@Override public String toString() {
		return image;
	}
}
