package il.ac.technion.cs.ssdl.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Bottle
{
   static
   {
      write("\n\n\n===========================\nStarting at " 
         + new java.util.Date());
   }
   
   public static PrintWriter get() throws IOException
   {
      File f = new File(System.getProperty("user.home"), "services.log.txt");
      PrintWriter pw = new PrintWriter(new FileWriter(f, true));
      return pw;
   }
   
   public static void write(String s)
   {
      try
      {
         PrintWriter pw = get();
         pw.println(s);
         pw.close();
      }
      catch(IOException e)
      {
         //
      }      
   }
   
   public static void write(Throwable t)
   {
      try
      {
         PrintWriter pw = get();
         t.printStackTrace(pw);
         pw.close();
      }
      catch(IOException e)
      {
//         JDialog.
      }
   }
}
