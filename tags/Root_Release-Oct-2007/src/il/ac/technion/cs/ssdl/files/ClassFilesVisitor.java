package il.ac.technion.cs.ssdl.files; import java.io.File;
import java.util.Collection;

/**
 * A class realizing a traversal of the file system, where the traversal is
 * restricted to <code>.class</code> files only
 * <p>
 * The traversal is carried out by calling one of the class constructors,
 * {@link #ClassFilesVisitor(Collection, il.ac.technion.cs.ssdl.files.FileSystemVisitor.FileOnlyAction)
 * or
 * {@link #ClassFilesVisitor(String[], il.ac.technion.cs.ssdl.files.FileSystemVisitor.Action)}
 * to set the visitation range, and the {@link Action} to be carried for each
 * visited file.
 * <p>
 * 
 * @see ClassFilesClasspathVisitor
 * @author Yossi Gil 11/07/2007
 */
public class ClassFilesVisitor extends FileSystemVisitor {
	public ClassFilesVisitor(String from, Action visitor, String[] extensions) {
		super(from, visitor, extensions);
	}

	private static final String[] classFileExtensions = new String[] { ".class" };

	public ClassFilesVisitor(Collection<String> from, FileOnlyAction visitor) {
		super(from, visitor, classFileExtensions);
	}
	
	public ClassFilesVisitor(File[] from, FileOnlyAction visitor) {
		super(from, visitor, classFileExtensions);
	}

	public ClassFilesVisitor(String[] from, FileOnlyAction visitor) {
		super(from, visitor, classFileExtensions);
	}

	public ClassFilesVisitor(String from, FileOnlyAction visitor) {
		super(from, visitor, classFileExtensions);
	}


}
