package il.ac.technion.cs.ssdl.utils; import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class Reflection {
	public static Class<?> typeOf(Member m) {
		if (m instanceof Method)
			return ((Method) m).getReturnType();
		if (m instanceof Field)
			return ((Field) m).getType();
		return null;
	}
}
