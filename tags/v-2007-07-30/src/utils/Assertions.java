/**
 * 
 */
package utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import junit.framework.Assert;

/**
 * This class consists exclusively of static methods that pertain to comparisons
 * of values in unit tests.
 * 
 * If the comparison yields a "not-equal" result, a JUnit assertion failure is
 * issued.
 * 
 * @author Itay Maman
 * @date Jul 9, 2007
 */
public enum Assertions {
	;
	public static <T> void equals(Set<T> set, T[] arr) {
		equals("", set, Arrays.asList(arr));
	}

	public static <T> void equals(String prefix, Set<T> set, Iterable<T> ts) {
		List<T> list = Iterables.toList(ts);
		Set<T> temp = new HashSet<T>();
		temp.addAll(set);
		temp.removeAll(list);
		Assert.assertTrue(temp.toString(), temp.isEmpty());
		temp = new HashSet<T>();
		temp.addAll(list);
		temp.removeAll(set);
		Assert.assertTrue(prefix + " - " + temp.toString(), temp.isEmpty());
	}

	public static void equals(int a, int b) {
		Assert.assertEquals(new Integer(a), new Integer(b));
	}
}
