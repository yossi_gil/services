package utils.reflect;

import static utils.reflect.Out.out;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import utils.ClassPathInfo;

/**
 * A class realizing heuristics for finding the file system path of the
 * class file from which a certain {@link Class} object was loaded.
 * 
 * @author Yossi Gil
 * @date 24/07/2007
 */
public enum ClassFileFinder {
	;
	/**
	 * Find the {@link File} of a given class file from a {@link Class} object.
	 * 
	 * @param c
	 *            a  non-null {@link Class} object
	 * @return the path leading to the class file from which this object was
	 *         instantiated, or <code><b>null</b></code> if <code>c</code> represents a type which 
	 *         was not loaded from a class file, i.e., <code>c</code> is a pritimitive type or 
	 *         an array type.
	 * @throws ClassNotFoundException
	 *             in case the class file was not found.
	 * @see #findURI(Class)
	 * @see #findURL(Class)
	 */
	public static File findFile(Class<?> c) throws ClassNotFoundException {
		final String name = findFileName(c);
		if (name == null)
			return null;
		return new File(name);
	}

	/**
	 * Find the file path of given class file from a {@link Class} object.
	 * 
	 * @param c
	 *            a  non-null {@link Class} object
	 * @return the path leading to the class file from which this object was
	 *         instantiated, or <code><b>null</b></code> if <code>c</code> represents a type which 
	 *         was not loaded from a class file, i.e., <code>c</code> is a pritimitive type or 
	 *         an array type.
	 * @throws ClassNotFoundException
	 *             in case the class file was not found.
	 * @see #findURI(Class)
	 * @see #findURL(Class)
	 */
	public static String findFileName(Class<?> c) throws ClassNotFoundException {
		final URI uri = findURI(c);
		if (uri == null)
			return null;
		final String path = uri.getSchemeSpecificPart();
		return path.replaceFirst("^file:/", "");
	}

	/**
	 * Find the URI of a given class file from a {@link Class} object.
	 * 
	 * @param c
	 *            a  non-null {@link Class} object
	 * @return the URI of the class file from which this object was
	 *         instantiated, or <code><b>null</b></code> if <code>c</code> represents a type which 
	 *         was not loaded from a class file, i.e., <code>c</code> is a pritimitive type or 
	 *         an array type.
	 * @throws ClassNotFoundException
	 *             in case the class file was not found.
	 * @see #findURL(Class)
	 * @see #findFileName(Class)
	 */
	public static URI findURI(Class<?> c) throws ClassNotFoundException {
		final URL url = findURL(c);
		if (url == null)
			return null;
		try {
			return url.toURI();
		} catch (URISyntaxException e) {
			throw new ClassNotFoundException(e.getReason());
		}
	}

	/**
	 * Find the URL of a given class file from a {@link Class} object.
	 * 
	 * @param c
	 *            a non-null {@link Class} object
	 * @return the {@link URL} of the class file from which this object was
	 *         instantiated, or <code><b>null</b></code> if <code>c</code> represents a type which 
	 *         was not loaded from a class file, i.e., <code>c</code> is a pritimitive type or 
	 *         an array type.
	 * @throws ClassNotFoundException
	 *             in case the class file was not found.
	 * 
	 * @see #findURI(Class)
	 * @see #findFileName(Class)
	 */
	public static URL findURL(Class<?> c) throws ClassNotFoundException {
		final String path = relativePath(c);
		if (path == null)
			return null;
		return ClassLoader.getSystemResource(path);
	}

	public static String findCLASSPATH(Class<?> c) throws ClassNotFoundException {
		String $ = findFileName(c);
		if ($ == null)
			return null;
		$ = $.replaceFirst("\\Q" + relativePath(c) + "\\E$", "");
		$ = $.replaceFirst("/$", "");
		$ = $.replaceFirst("!$", "");
		return $;
	}

	/**
	 * Find the expected relative path of a {@link Class} object, e.g., for a
	 * </code>java.lang.Object.class</code>, the relative path is <code>java/lang/Object.class</code>
	 * 
	 * @param c
	 *            a non-null {@link Class} object
	 * @return the URI of the class file from which this object was
	 *         instantiated, or <code><b>null</b></code> if <code>c</code>
	 *         represents a type which was not loaded from a class file, i.e.,
	 *         <code>c</code> is a pritimitive type or an array type.
	 * @throws ClassNotFoundException
	 *             in case the class file was not found.
	 * 
	 * @see #findURI(Class)
	 * @see #findFileName(Class)
	 */
	public static String relativePath(Class<?> c) throws ClassNotFoundException {
		nonNull(c);
		if (c.isPrimitive())
			return null;
		final String name = c.getName();
		nonNull(name);
		return classNameToPath(name);
	}

	public static void nonNull(Object o) throws ClassNotFoundException {
		if (o == null)
			throw new ClassNotFoundException();
	}

	public static String classNameToPath(String className) {
		if (className.startsWith("["))
			return null;
		return className.replace('.', '/') + ".class";
	}

	public static ClassPathInfo makeCPI(Class<?> c) throws ClassNotFoundException {
		return new ClassPathInfo(findCLASSPATH(c));
	}

	public static void main(String args[]) throws ClassNotFoundException {
		Class<?> c = java.lang.Object.class;
		out("path", relativePath(c));
		out("findURL", findURL(c));
		out("findURI", findURI(c));
		out("findClassFile", findFileName(c));
		out("findCLASSPATH", findCLASSPATH(c));
		out("makeCPI", makeCPI(c));
	}
}
