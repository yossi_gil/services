package il.ac.technion.cs.ssdl.utils;

import java.util.Properties;

public enum SystemProperty {
	FILE_SEPARATOR, //
	JAVA_CLASS_PATH, //
	JAVA_CLASS_VERSION, //
	JAVA_HOME, //
	JAVA_VENDOR, //
	JAVA_VENDOR_URL, //
	JAVA_VERSION, //  
	LINE_SEPARATOR, //
	OS_ARCH, //
	OS_NAME, //
	OS_VERSION, //   
	PATH_SEPARATOR, //
	USER_DIR, //
	USER_HOME, //
	USER_NAME, //
	;
	public final String key;

	private SystemProperty() {
		key = name().toLowerCase().replace('_', '.');
	}

	public String value(Properties p) {
		return p.getProperty(key);
	}

	public String value() {
		return value(System.getProperties());
	}

	public static void main(String[] args) throws Exception {
		for (final SystemProperty sp : values()) {
			if (sp.value() == null)
				throw new Exception("property " + sp + " is probably misspelled");
			System.out.println(sp.key + "='" + sp.value() + "'");
		}
	}
}
