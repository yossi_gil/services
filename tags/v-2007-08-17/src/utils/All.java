package utils;

public class All {
	public static boolean notNull(Object a[]) {
		if (a == null)
			return false;
		for (final Object o : a)
			if (o == null)
				return false;
		return true;
	}

	public static boolean notNull(Iterable<? extends Object> os) {
		if (os == null)
			return false;
		for (final Object o : os)
			if (o == null)
				return false;
		return true;
	}
}
