/**
 * 
 */
package utils.csv;

import static utils.DBC.require;
import java.util.Map;
import java.util.TreeMap;
import utils.Separator;

public class CSVLine {
	Map<String, String> map = new TreeMap<String, String>();

	public void put(String key) {
		require(key != null);
		map.put(key, "");
	}

	public void put(String key, float value) {
		require(key != null);
		put(key, "" + value);
	}

	public void put(String key, double value) {
		require(key != null);
		put(key, "" + value);
	}

	public void put(String key, int value) {
		require(key != null);
		put(key, "" + value);
	}

	public void put(String key, Object value) {
		require(key != null);
		if (value == null)
			put(key);
		else
			map.put(key, value.toString());
	}

	public void put(String key, Object a[], int i) {
		require(key != null);
		put(key, a == null || i < 0 || i >= a.length ? null : a[i]);
	}

	public void put(String key, Object[] os) {
		require(key != null);
		put(key, os == null ? null : Separator.separateBy(os, ";"));
	}

	public String line() {
		String $ = "";
		Separator s = new Separator(delimiter);
		for (String k : map.keySet())
			$ += s + quote(map.get(k));
		return $;
	}

	public String header() {
		String $ = "";
		Separator s = new Separator(delimiter);
		for (String k : map.keySet())
			$ += s + quote(k);
		return $;
	}

	static final String quote = "\"";
	static final String delimiter = ",";

	public static String quote(String s) {
		return quote + s.replaceAll(quote, quote + quote) + quote;
	}
}