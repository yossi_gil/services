package utils.reflect;


public class ReflectionAnalyzer {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		class LocalClass {
			// Nothing here.
		}
		Explore.go(int[].class);
		Explore.go(void.class);
		Explore.go(java.lang.Object[].class);
		Explore.go(ReflectionAnalyzer.class);
		Explore.go(InnerClass.class);
		Explore.go(StaticInnerClass.class);
		Explore.go(LocalClass.class);
		Explore.go(new Object() {
			@Override public int hashCode() {
				return super.hashCode();
			}
		}.getClass());
	}

	static String toBinary(int value) {
		String $ = "";
		for (int pos = 0; pos <= 31; pos++)
			if (((1 << pos) & value) != 0)
				$ += "+" + (1 << pos);
		return $;
	}

	class InnerClass {
		// Nothing here.
	}

	class StaticInnerClass {
		// Nothing here.
	}
}

class A {
	class B {
		A f() {
			return A.this;
		}
	}
}
