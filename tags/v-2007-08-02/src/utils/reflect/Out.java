package utils.reflect;

import static utils.DBC.require;
import utils.Separator;

public class Out {
	static public void out(String s) {
		System.out.print(s);
	}

	static public void out(String name, Object a) {
		if (a == null)
			System.out.printf("No %s\n", name);
		else
			System.out.printf("%s = %s\n", name, a);
	}

	static public void out(String name, int a) {
		System.out.printf("%s = %d\n", name, new Integer(a));
	}

	static public void out(String name, boolean v) {
		System.out.printf("%s = %b\n", name, new Boolean(v));
	}

	static public void out(String name, Object[] a) {
		require(name != null);
		if (a == null || a.length <= 0)
			System.out.printf("No %s\n", name);
		else if (a.length == 1)
			System.out.printf("Only 1 %s: %s\n", name, a[0]);
		else
			System.out.printf("Total of %d %s:\n\t%s\n", new Integer(a.length), name, Separator.separateBy(a, "\n\t"));
	}
}
