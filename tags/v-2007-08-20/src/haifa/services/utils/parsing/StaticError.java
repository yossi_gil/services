/**
 * 
 */
package haifa.services.utils.parsing;

import haifa.services.utils.Position;

/**
 * An exception that indicates an error, in a JTL program, that is detected 
 * at compilation/linking time.
 *  
 * @author imaman
 */
public class StaticError extends RuntimeException {
	private static final long serialVersionUID = 2152061099759747608L;

   /**
    * Position where the error occured. May be <code>null</code>.
    */
   public final Position position;

   /**
    * Location where the error occured. May be <code>null</code>.
    */
   public final Location location;

   /**
    * Equivalent to <code>StaticError(message, null, null)</code>
    * @param message
    */
   public StaticError(String message) {
      this(message, null, null);
   }
   
   /**
    * Initialize a new instance by wrapping another exception.
    * @param e Exception to warp
    * @param loc Location where the error occured
    */
   protected StaticError(Exception e, Location loc)
   {
      super(e);
      position = null;
      location = loc;
   }
   /**
    * Initialzie a new instance with the given parameters. Typically
    * either <code>pos</code> or <code>loc</code> is <code>null</code>. 
    * @param message Error message 
    * @param pos Position where the error occured.
    * @param loc Location where the error occured. 
    */
	public StaticError(String message, Position pos, Location loc) {
		super(message);
      this.position = pos;
      this.location = loc;     
	}
}