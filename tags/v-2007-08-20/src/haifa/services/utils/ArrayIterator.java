package haifa.services.utils;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {
	private T[] ts;
	private int index;
	private int end;

	public static <E> Iterator<E> make(E[] es) {
		return make(es, 0);
	}

	public static <E> Iterator<E> make(E[] es, int begin) {
		return make(es, begin, es.length);
	}

	public static <E> Iterator<E> make(E[] es, int begin, int end) {
		return new ArrayIterator<E>(es, begin, end);
	}

	public ArrayIterator(T[] ts) {
		this(ts, 0);
	}

	public ArrayIterator(T[] ts, int begin) {
		this(ts, begin, ts.length);
	}

	public ArrayIterator(T[] ts, int begin, int end) {
		this.ts = ts;
		index = begin;
		this.end = end;
	}

	public boolean hasNext() {
		return index < end;
	}

	public T next() {
		return ts[index++];
	}

	public void remove() {
		throw new UnsupportedOperationException("ArrayIterator cannot remove " + "elements");
	}
}
