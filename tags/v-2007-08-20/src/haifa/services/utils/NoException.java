package haifa.services.utils;

import static haifa.services.utils.DBC.unreachable;

/**
 * A class representing an exception that can never be thrown. No uses are
 * known. To be erased.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il>
 * 
 */
@SuppressWarnings("serial") @Deprecated public final class NoException extends Exception {
	private NoException() throws Exception {
		unreachable("Tried to create a NoException object");
	}
}
