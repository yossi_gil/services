package haifa.services.utils.csv;

import static haifa.services.utils.testing.Assert.assertEquals;
import static junit.framework.Assert.*;
import java.util.Arrays;
import org.junit.Test;

public class CSV_Test {
	@Test public void test1() {
		String s = "abc,def\r\n\tg\\m";
		String t = CSV.escape(s);
		String u = CSV.unescape(t);
		assertEquals("abc\\.def\\r\\n\\tg\\\\m", t);
		assertEquals(s, u);
		assertFalse(s.equals(t));
	}

	@Test public void testNull() {
		String s = null;
		String t = CSV.escape(s);
		String u = CSV.unescape(t);
		assertNull(s);
		assertNotNull(t);
		assertNull(u);
	}

	@Test public void testCombineSplitShort() {
		String[] parts = { "abc", ",", "def" };
		String combo = CSV.combine(parts);
		String[] t = CSV.split(combo);
		assertEquals(parts.length, t.length);
		assertTrue(Arrays.deepEquals(parts, t));
	}

	@Test public void testCombineSplit() {
		String[] parts = { "abc", "", "def", "gh\n,", ",", "\r", "\ta", null, "a\t", "\rz", "o\np", "qwerty", ",,,,", ",1,2,3,4" };
		String combo = CSV.combine(parts);
		String[] t = CSV.split(combo);
		assertEquals(parts.length, t.length);
		assertTrue(Arrays.deepEquals(parts, t));
	}

	@Test public void testCombineSplitSingleNullElement() {
		String[] parts = { null, };
		String combo = CSV.combine(parts);
		String[] t = CSV.split(combo);
		assertEquals(parts.length, t.length);
		assertTrue(Arrays.deepEquals(parts, t));
	}

	public static enum Rgb {
		RED, GREEN, BLUE
	}

	@Test public void testCombineSplitEnum() {
		assertTrue(Arrays.deepEquals(Rgb.values(), CSV.split(Rgb.class, CSV.combine(Rgb.values()))));
		Rgb[] redNull = { Rgb.RED, null };
		assertTrue(Arrays.deepEquals(redNull, CSV.split(Rgb.class, CSV.combine(redNull))));
		Rgb[] justNull = { null };
		assertTrue(Arrays.deepEquals(justNull, CSV.split(Rgb.class, CSV.combine(justNull))));
	}

	@Test public void testSplitCombineEnum() {
		String s = "GREEN,RED,BLUE,\\0,RED";
		assertEquals(s, CSV.combine(CSV.split(Rgb.class, s)));
	}

	@Test public void testSplitCombineClasses() {
		Class<?>[] cs = { String.class, System.class, Object.class, null };
		assertTrue(Arrays.deepEquals(cs, CSV.splitToClasses(CSV.combine(cs))));
		String s = "java.lang.String,java.lang.System,java.lang.Object,\\0";
		assertEquals(s, CSV.combine(CSV.splitToClasses(s)));
		assertEquals(s, CSV.combine(cs));
	}

	public static void main(String[] args) {
		new CSV_Test().testCombineSplitShort();
	}
}
