package haifa.services.utils;

/**
 * An interface representing a typed procedure with a single argument.
 * 
 * @param <Argument>
 *        Type of argumet
 */
public interface Procedure<Argument> {
	/**
	 * Execute the procedure with the given input
	 * 
	 * @param a
	 *        Input argument
	 */
	public void eval(Argument a);
}
