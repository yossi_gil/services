package haifa.services.utils;

import java.io.*;

/**
 * A class through which all logging messages go.
 * 
 * @author Yossi Gil (11/01/2006)
 */
class Logger {
	final PrintWriter wr;
	private int line = 0;

	public int line() {
		return line;
	}

	public Logger(File f) throws FileNotFoundException {
		OutputStream os = new FileOutputStream(f);
		wr = new PrintWriter(os, true);
	}

	public Logger(PrintStream out) {
		wr = new PrintWriter(out, true);
	}

	public void println() {
		println(null);
	}

	public void println(String s) {
		wr.print("[" + ++line + "] ");
		wr.flush();
		if (s != null)
			wr.println(s);
	}

	public void flush() {
		wr.flush();
	}
}