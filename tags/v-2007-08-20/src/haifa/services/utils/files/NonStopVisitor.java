package haifa.services.utils.files;

import static haifa.services.utils.DBC.unreachable;
import haifa.services.utils.files.FileSystemVisitor.Action.StopTraversal;

import java.io.IOException;
import java.util.Collection;

/**
 * A class realizing the {@link FileSystemVisitor} functionality, except that it
 * does not allow throws of {@link StopTraversal} exceptions.
 * 
 * @author Yossi Gil
 * @date 13/07/2007
 */
public class NonStopVisitor extends FileSystemVisitor {
	public NonStopVisitor(String from, Action visitor, String[] extensions) {
		super(from, visitor, extensions);
	}

	public NonStopVisitor(Collection<String> from, NonStopAction action, String... extensions) {
		super(from, action, extensions);
	}

	public NonStopVisitor(String[] from, NonStopAction action, String... extensions) {
		super(from, action, extensions);
	}

	@Override public final void go() throws IOException {
		try {
			super.go();
		} catch (StopTraversal e) {
			unreachable();
			e.printStackTrace();
		}
	}
}
