package haifa.services.utils.files;

import haifa.services.utils.testing.TestedBy;

import java.io.File;

/**
 * A collection of functions representing the translation of class names to path
 * and vice versa.
 * 
 * @author Yossi Gil
 * @date 12/07/2007
 */
@TestedBy(Filename_Test.class)//
public enum Filename {,;
	/**
	 * Convert a file name as found in the file system, to a class name.
	 * 
	 * @param path
	 *        a file name to be converted
	 * @param root
	 *        the root of the packages directory
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2class(String path, String root) {
		return path2class(removeRoot(path, root));
	}

	/**
	 * Convert a relative file name into a class name.
	 * 
	 * @param path
	 *        a relative path name, with respect to JAVA packages directory
	 *        system
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2class(String path) {
		return path.replaceAll("\\.class$", "").replace('/', DOT).replace('\\', DOT);
	}

	/**
	 * Convert an absoulte directory name as found in the file system, to a
	 * class name.
	 * 
	 * @param path
	 *        a file name to be converted
	 * @param root
	 *        the root of the packages directory
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2package(String path, String root) {
		return path2package(removeRoot(path, root));
	}

	/**
	 * Convert a relative directory name into a pakcage name.
	 * 
	 * @param path
	 *        a relative path name, with respect to JAVA packages directory
	 *        system
	 * @return the fully qualified name of the class residing in this file.
	 */
	public static String path2package(String path) {
		return path.replaceAll("[\\/]", ".");
	}

	/**
	 * Trim the root portion of an absolute path
	 * 
	 * @param path
	 *        the absolute path to be trimmed
	 * @param root
	 *        the root portion to be trimmed from the path
	 * @return path, but without the root portion
	 */
	public static String removeRoot(String path, String root) {
		return path.replace(root + File.separator, "");
	}
	
	/**
	 * The '.' character, used in separation of fully qualified class names.
	 */
	public static final char DOT = '.';

	/**
	 * Obtain the leading portion of a fully qualified class name.
	 * 
	 * @param name
	 *        a class name to process
	 * @return the longest prefix of name that is followed by a {@link #DOT},
	 *         or the empty {@link String} if no such prefix exists.
	 * @see #tail_part(String)
	 */
	public static String head_part(String name) {
		final int index = name.lastIndexOf(DOT);
		return index < 0 ? "" : name.substring(0, index);
	}


	/**
	 * Obtain the last portion of a fully qualified class name.
	 * 
	 * @param name
	 *        a class name to process
	 * @return that portion of the name that follows the last occurence of the
	 *         {@link #DOT}, or the entire name, if name does not contain this
	 *         character.
	 * @see #tail_part(String)
	 */
	public static String tail_part(String name) {
		final int index = name.lastIndexOf(DOT);
		return index < 0 ? name : name.substring(index + 1);
	}

	/**
	 * determine whether a class is anonymous
	 * 
	 * @param name
	 *        a class name to process
	 * @return <code><b>true</b></code> iff the class is an anonymous one,
	 *         i.e., defined in the context of a <code><b>new</b></code>
	 *         expression.
	 */
	public static boolean isAnonymous(String name) {
		return trailer_part(name).matches("[0-9]+");
	}

	/**
	 * determine whether a class is local
	 * 
	 * @param name
	 *        a class name to process
	 * @return <code><b>true</b></code> iff the class is a local one, i.e.,
	 *         defined within a function.
	 */
	public static boolean isLocal(String name) {
		return trailer_part(name).matches("[0-9][A-Za-z_$].*");
	}

	/**
	 * determine whether a class is inner
	 * 
	 * @param name
	 *        a class name to process
	 * @return <code><b>true</b></code> iff the class is an inner class,
	 *         i.e., defined within another class.
	 */
	public static boolean isInner(String name) {
		return trailer_part(name).matches("[A-Za-z_$].*");
	}

	public static boolean isAllInner(String name) {
		return isInner(name) && !tail_part(name).matches(".*[$][0-9].*");
	}


	public static String name2Canonical(String name) {
		for (String before = name, after;; before = after) {
			after = before.replaceFirst("\\$([a-zA-Z_][a-zA-Z0-9_$]*)$", ".$1");
			if (after.equals(before))
				return after;
		}
	}

	public static String trailer_part(String name) {
		String tail = tail_part(name);
		int index = tail.lastIndexOf('$');
		return index < 1 ? "" : tail.substring(index + 1);
	}
}
