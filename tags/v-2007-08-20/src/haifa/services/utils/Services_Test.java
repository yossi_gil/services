package haifa.services.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	  haifa.services.utils.Pair_Test.class,
	  haifa.services.utils.Test_ClassPathInfo.class,
	  haifa.services.utils.Test_Stringer.class,
	  haifa.services.utils.csv.CSVLineTest.class,
	  haifa.services.utils.csv.CSV_Test.class,
	  haifa.services.utils.files.Filename_Test.class,
	  haifa.services.utils.files.JRETest.class,
})
public class Services_Test {
	// Do nothing
}
