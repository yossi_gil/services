/**
 * 
 */
package utils.testing;

import static org.junit.Assert.*;
import org.junit.Assert;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import utils.Iterables;

/**
 * This class consists exclusively of static methods that pertain to comparisons
 * of values in unit tests.
 * 
 * If the comparison yields a "not-equal" result, a JUnit assertion failure is
 * issued.
 * 
 * @author Itay Maman
 * @date Jul 9, 2007
 */
public class Assertions {
	public static <T> void equals(Set<T> set, T[] arr) {
		equals("", set, Arrays.asList(arr));
	}

	public static <T> void equals(String prefix, Set<T> set, Iterable<T> ts) {
		List<T> list = Iterables.toList(ts);
		Set<T> temp = new HashSet<T>();
		temp.addAll(set);
		temp.removeAll(list);
		assertTrue(temp.toString(), temp.isEmpty());
		temp = new HashSet<T>();
		temp.addAll(list);
		temp.removeAll(set);
		assertTrue(prefix + " - " + temp.toString(), temp.isEmpty());
	}

	public static void assertEquals(int a, int b) {
		Assert.assertEquals(new Integer(a), new Integer(b));
	}

	public static void assertEquals(Integer a, int b) {
		Assert.assertEquals(a, new Integer(b));
	}

	public static void assertEquals(int a, Integer b) {
		Assert.assertEquals(new Integer(a), b);
	}

	public static void assertEquals(String s, int a, int b) {
		Assert.assertEquals(s, new Integer(a), new Integer(b));
	}

	public static void assertEquals(String s, Integer a, int b) {
		Assert.assertEquals(s, a, new Integer(b));
	}

	public static void assertEquals(String s, int a, Integer b) {
		Assert.assertEquals(s, new Integer(a), b);
	}
}
