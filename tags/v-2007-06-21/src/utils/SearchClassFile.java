package utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import utils.FileSystemVisitor.Action;
import utils.FileSystemVisitor.Action.StopTraversal;

/**
 * 
 * 
 * @author Yossi Gil
 * @date Mar 29, 2007
 */
public class SearchClassFile {
	static final String name = "where";

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.printf("Usage: %s class [class*]\n", name);
			System.exit(1);
		}
		for (String arg : args) {
			if (processOption(arg))
				continue;
			try {
				new FileSystemVisitor(new String[] { "c:\\eclipse" }, new Searcher(arg + ".class"), ".class").go();
			} catch (IOException e) {
				System.err.println(e);
			} catch (StopTraversal e) {
				continue;
			}
		}
	}

	static boolean reportCounts;
	static boolean findFirstOnly;

	private static boolean processOption(String arg) {
		if (arg.equals("-n")) {
			reportCounts = true;
			return true;
		}
		if (arg.equals("-f")) {
			findFirstOnly = true;
			return true;
		}
		return false;
	}

	/**
	 * @author Yossi Gil
	 * @date 21/05/2007
	 */
	public static class Searcher implements Action {
		private int directories = 0;
		private int files = 0;
		private int zips = 0;
		private int entries = 0;
		private final String sought;

		public Searcher(final String sought) {
			this.sought = sought;
		}

		private void report(String s) {
			if (reportCounts)
				System.out.println(s + ". " + directories + " directories, " + files + " class files, " + zips + " ZIP archives, " + entries
				        + " entries.");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitDirectory(java.io.File)
		 */
		public void visitDirectory(File f) {
			directories++;
			report("Directory: " + f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitFile(java.io.File)
		 */
		public void visitFile(File f) throws StopTraversal {
			files++;
			report("File: " + f.getAbsolutePath());
			check(f.getName(), f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitZip(java.io.File)
		 */
		public void visitZip(File f) {
			zips++;
			report("Archive: " + f.getAbsolutePath());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see utils.FileVisitor.Action#visitZipEntry(java.lang.String,
		 *      java.lang.String, java.io.InputStream)
		 */
		public void visitZipEntry(String zipName, String entryName, @SuppressWarnings("unused") InputStream stream) throws StopTraversal {
			entries++;
			report("Archive entry: " + entryName);
			check(entryName, zipName);
		}

		private void check(String file, String directory) throws StopTraversal {
			if (!file.endsWith(sought))
				return;
			System.out.printf("%s: %s\n", file, directory);
			if (findFirstOnly)
				throw new StopTraversal();
		}

		public void visitZipDirectory(String zipName, String entryName, @SuppressWarnings("unused") InputStream stream)  {
			report("Archive directory: " + entryName + " in zip " + zipName);
		}
	}
}
