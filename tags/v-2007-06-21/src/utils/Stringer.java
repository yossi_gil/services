package utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An immutable class, whose various constructors concatenate the string value
 * of a list of items, optionally separated by a separator.
 * 
 * @author Yossi Gil (22/02/2006)
 */
public class Stringer {
	private final String value;

	/**
	 * Concat a prefix of a string with another string. The prefix is determined
	 * by the value of <code>pos</code> parameter:
	 * <ul>
	 * <li>If non-negative the prefix is the first <code>pos</code>
	 * characters of <code>lhs</code>
	 * <li>Otherwise, the prefix is all the characters of <code>lhs</code>
	 * but the last <code>pos</code> character.
	 * </ul>
	 * 
	 * @param lhs
	 *            Left hand side string
	 * @param pos
	 *            Position in the <code>lhs</code> string
	 * @param rhs
	 *            Right hand side string
	 * @return Concatenated String
	 */
	public static String concatAt(String lhs, int pos, String rhs) {
		return lhs.substring(0, pos >= 0 ? pos : lhs.length() + pos) + rhs;
	}

	/**
	 * Return the largest prefix of a String the does not conatin a certain
	 * character.
	 * 
	 * @param s
	 *            String whose prefix is to be taken
	 * @param c
	 *            The charcter that should not appear in the prefix
	 * @return Prefix of s. If s does not contain the character c then s is
	 *         returned.
	 */
	public static String prefixUntil(String s, char c) {
		int p = s.indexOf(c);
		return s.substring(0, p >= 0 ? p : s.length());
	}

	/**
	 * Return the largest suffix of a String the starts with a certain
	 * character.
	 * 
	 * @param s
	 *            String whose suffix is to be taken
	 * @param c
	 *            First character of the suffix.
	 * @return Suffix of s. If s does not contain the character c then the empty
	 *         string is returned.
	 */
	public static String suffixFrom(String s, char c) {
		int p = s.indexOf(c);
		return p >= 0 ? s.substring(p) : "";
	}

	public static String concatObjects(String sep, Iterable<?> os) {
		List<String> strings = new ArrayList<String>();
		for (Object o : os)
			strings.add(String.valueOf(o));
		return concat(sep, strings);
	}

	public static String concat(String sep, Object... os) {
		return concatObjects(sep, Arrays.asList(os));
	}

	public static String concat(String sep, Iterable<String> strings) {
		return sequence("", strings, sep, "");
	}

	public static String concat(String prefix, String sep, String s) {
		return (prefix == null ? "" : prefix + sep) + s;
	}

	@Override public String toString() {
		return value;
	}

	/**
	 * @param <T>
	 *            type of items in the list
	 * @param begin
	 *            the string starting the string representation.
	 * @param ts
	 *            the actual items in the list, method <code>toString()</code>
	 *            is used to compute obtain each item string represntation.
	 * @param sep
	 *            a string so separate these items
	 * @param end
	 *            a string terminating the string representation
	 * @return the string equivalent of the <code>ts</code> in the following
	 *         structure: <code> begin item1 sep item2 sep ... item2 end</code>
	 */
	public static final <T> String sequence(String begin, T[] ts, String sep, String end) {
		StringBuilder b = new StringBuilder(begin);
		Separator s = new Separator(sep);
		for (T t : ts)
			b.append(s).append(t);
		b.append(end);
		return b.toString();
	}

	/**
	 * @param <T>
	 *            type of items in the list
	 * @param begin
	 *            the string starting the string representation.
	 * @param ts
	 *            the actual items in the list, method <code>toString()</code>
	 *            is used to compute obtain each item string represntation.
	 * @param sep
	 *            a string so separate these items
	 * @param end
	 *            a string terminating the string representation
	 * @return the string equivalent of the <code>ts</code> in the following
	 *         structure: <code> begin item1 sep item2 sep ... item2 end</code>
	 */
	public static final <T> String sequence(String begin, Iterable<T> ts, String sep, String end) {
		StringBuilder b = new StringBuilder(begin);
		Separator s = new Separator(sep);
		for (T t : ts)
			b.append(s).append(t);
		b.append(end);
		return b.toString();
	}

	/**
	 * @author Oren Rubin
	 * @param <T>
	 *            type of items in the list
	 * @param begin
	 *            the string starting the string representation.
	 * @param ts
	 *            the actual items in the list, method <code>toString()</code>
	 *            is used to compute obtain each item string represntation.
	 * @param sep
	 *            a string so separate these items
	 * @param end
	 *            a string terminating the string representation
	 * @param converter class to customize conversions.
	 * @return the string equivalent of the <code>ts</code> in the following
	 *         structure: <code> begin item1 sep item2 sep ... item2 end</code>
	 */
	public static final <T> String sequence(String begin, T[] ts, String sep, String end, Converter<T> converter) {
		StringBuilder b = new StringBuilder(begin);
		Separator s = new Separator(sep);
		for (T t : ts)
			b.append(s).append(converter.convert(t));
		b.append(end);
		return b.toString();
	}
	
	public static interface Converter<T> {
		public String convert(T toBeConverted);
	}
	
	public <T> Stringer(String sep, Iterable<T> ts) {
		StringBuilder b = new StringBuilder();
		Separator s = new Separator(sep);
		for (T t : ts)
			b.append(s).append(t);
		value = b.toString();
	}

	public <T> Stringer(String sep, String nullStr, T... ts) {
		StringBuilder b = new StringBuilder();
		Separator s = new Separator(sep);
		for (T t : ts) {
			b.append(s);
			b.append(t != null ? t : nullStr);
		}
		value = b.toString();
	}

	public <T> Stringer(String sep, T... ts) {
		this(sep, "null", ts);
	}

	public <T> Stringer(String sep, int... ts) {
		StringBuilder b = new StringBuilder();
		Separator s = new Separator(sep);
		for (int i : ts)
			b.append(s).append(Integer.toString(i));
		value = b.toString();
	}

	/**
	 * Add leading zeros to a sequence of consecutive digits appearing at the
	 * suffix of a String. This allows sorting to follow the natrual order
	 * (i.e.: abc2 should come before abc21).
	 * 
	 * @param s
	 *            Input string
	 * @return Fixed string derived from s
	 */
	public static String fixNumericalSuffix(String s) {
		if (s == null || s.length() == 0)
			return s;
		int numDigits = 0;
		for (int i = 0, len = s.length(); i < len; ++i) {
			numDigits = i;
			if (!Character.isDigit(s.charAt(len - i - 1)))
				break;
		}
		if (numDigits == 0)
			return s;
		int firstDigitIndex = s.length() - numDigits;
		final String ZEROS = "0000000";
		if (numDigits >= ZEROS.length())
			return s;
		String z = ZEROS.substring(0, ZEROS.length() - numDigits);
		return s.substring(0, firstDigitIndex) + z + s.substring(firstDigitIndex);
	}

	/**
	 * Generate a string specifying the values of all declared fields of the
	 * given object.
	 * 
	 * @param o
	 *            Object to insepct
	 * @return String representation of o
	 */
	public static String toString(Object o) {
		List<String> lst = new ArrayList<String>();
		for (Field f : o.getClass().getDeclaredFields()) {
			f.setAccessible(true);
			try {
				lst.add(f.getName() + "=" + f.get(o));
			} catch (Exception e) {
				continue;
			}
		}
		return concat(", ", lst);
	}
}