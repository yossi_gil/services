package utils;

import java.io.Serializable;
import java.util.Iterator;

/**
 * An immutable collection of elements. Once construction is completed no new
 * elements can be added to the collections, nor can elements be removed from
 * the collection. The <code>indexOf()</code> and <code>has()</code> methods
 * assume that the collection does not contain null elements
 * 
 * @param <E>
 *            Type of elemenets in the sequence
 */
public class Sequence<E> implements Iterable<E>, Serializable {
	private final E[] es;
	private final int begin;
	private final int end;

	public static <T> Sequence<T> makeEmpty(Class<T> cls) {
		cls.hashCode(); // Avoid a "parameter never read" warning
		return make(null, 0, 0);
	}

	public static <T> Sequence<T> make(T[] ts, int begin, int end) {
		return new Sequence<T>(ts, begin, end);
	}

	public static <T> Sequence<T> make(T[] ts, int begin) {
		return make(ts, begin, ts.length);
	}

	public static <T> Sequence<T> make(T... ts) {
		return make(ts, 0);
	}

	public static <T> Sequence<T> make(Sequence<T> ts, int begin) {
		return make(ts.es, begin, ts.end);
	}

	public Sequence(E... es) {
		this(es, 0, es.length);
	}

	protected Sequence(E[] es, int begin, int end) {
		this.begin = begin;
		this.end = end;
		this.es = es;
	}

	public Iterator<E> iterator() {
		return ArrayIterator.make(es, begin, end);
	}

	public int size() {
		return end - begin;
	}

	// Lina bug fix
	public E get(int index) {
		// return es[index - begin];
		return es[index + begin];
	}

	public boolean has(E e) {
		return indexOf(e) >= 0;
	}

	public int indexOf(E e) {
		for (int i = begin; i < end; ++i)
			if (es[i].equals(e))
				return i - begin;
		return -1;
	}

	@Override public String toString() {
		return Stringer.sequence("[", this, ",", "]");
	}

	public static <T> Sequence<T> upcast(Class<T> cls, Sequence<? extends T> src) {
		cls.hashCode(); // Avoid warning
		return make((T[]) src.es, src.begin, src.end);
	}
}
