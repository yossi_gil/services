/**
 * 
 */
package il.ac.technion.cs.ssdl.parsing;

/**
 * Protocol of an object that provides the details about a single
 * compiler/parser error.
 * 
 */
public interface ErrorItem extends Comparable<ErrorItem> {
	/**
	 * Obtain the location where the error occured
	 * 
	 * @return A {@link Location} object
	 */
	public Location getLocation();

	/**
	 * Obtain the error message
	 * 
	 * @return A string explaining the error
	 */
	public String getMessage();

	/**
	 * Obtain the source code fragment which triggered the error
	 * 
	 * @return String
	 */
	public String getSourceCode();

	/**
	 * Obtain a string representation of the location where this error occurred
	 * 
	 * @return String
	 */
	public String where();
}