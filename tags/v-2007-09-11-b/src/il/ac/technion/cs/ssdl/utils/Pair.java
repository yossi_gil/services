package il.ac.technion.cs.ssdl.utils;

import il.ac.technion.cs.ssdl.testing.TestedBy;

@TestedBy(Pair_Test.class) public class Pair<First, Second> {
	public final First a;
	public final Second b;

	public static <A, B> Pair<A, B> newPair(A a, B b) {
		return new Pair<A, B>(a, b);
	}

	public Pair(First a, Second b) {
		this.a = a;
		this.b = b;
	}

	@Override public String toString() {
		return "<" + a + "," + b + ">";
	}

	@Override public int hashCode() {
		return a.hashCode() >>> 1 ^ b.hashCode();
	}

	@Override public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o == null || !getClass().equals(o.getClass()))
			return false;
		final Pair<?, ?> that = (Pair<?, ?>) o;
		return eq(a, that.a) && eq(b, that.b);
	}

	private static boolean eq(Object a, Object b) {
		if (a == null)
			return b == null;
		return a.equals(b);
	}
}
