package il.ac.technion.cs.ssdl.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Static methods for I/O related operations
 */
public class IO {
	/**
	 * Read the contents of the given reader and return it as a String
	 * 
	 * @param r
	 *        Reader
	 * @return the entire content of <code>r</code>
	 * @throws IOException
	 */
	public static String toString(Reader r) throws IOException {
		final StringBuilder sb = new StringBuilder();
		while (true) {
			final int c = r.read();
			if (c < 0)
				return sb.toString();
			sb.append((char) c);
		}
	}

	/**
	 * Read the contents of the given stream and return it as a String
	 * 
	 * @param is
	 *        Input stream
	 * @return the entire content of <code>is</code>
	 * @throws IOException
	 */
	public static String toString(InputStream is) throws IOException {
		return toString(new InputStreamReader(is));
	}

	/**
	 * Read the contents of the given class-path file.
	 * 
	 * @param clazz
	 *        Class - Specifies a location in the class-path tree
	 * @param path
	 *        Relative path to the file from the given class
	 * @return Contents of the file
	 * @throws IOException
	 */
	public static String toString(Class<?> clazz, String path) throws IOException {
		return toString(clazz.getResourceAsStream(path));
	}

	/**
	 * Write a string to a file
	 * 
	 * @param outputFile
	 *        File to be written
	 * @param ss
	 *        Strings to write
	 * @throws IOException
	 */
	public static void write(File outputFile, String... ss) throws IOException {
		final FileWriter fw = new FileWriter(outputFile);
		try {
			for (final String s: ss) {
				fw.append(s);
				fw.append("\n");
			}
		} finally {
			fw.close();
		}
	}

	public static List<String> lines(String str) throws IOException {
		final List<String> $ = new ArrayList<String>();
		final BufferedReader br = new BufferedReader(new StringReader(str));
		while (true) {
			final String line = br.readLine();
			if (line == null)
				return $;
			$.add(line);
		}
	}

	public static String concatLines(Iterable<String> ss) {
		StringBuffer sb = new StringBuffer(1000);
		final Separator nl = new Separator("\n");
		for (String s: ss)
			sb.append(nl).append(s);
		return sb.toString();
	}
	
//	public static String concatLines(Iterable<String> strings) {
//		final StringWriter sw = new StringWriter();
//		final PrintWriter pw = new PrintWriter(sw);
//		final Separator nl = new Separator("\n");
//		for (final String s : strings)
//			pw.print(nl + s);
//		pw.flush();
//		return sw.toString();
//	}
	


	public static String concatLines(String... ss) {
		StringBuffer sb = new StringBuffer(1000);
		final Separator nl = new Separator("\n");
		for (String s: ss)
			sb.append(nl).append(s);
		return sb.toString();
	}
}
