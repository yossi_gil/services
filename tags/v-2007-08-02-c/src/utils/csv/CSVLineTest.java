package utils.csv;

import static org.junit.Assert.*;
import org.junit.Test;
import utils.STRING;

public class CSVLineTest {
	@Test public void TwoFieldsTest() {
		CSVLine c = new CSVLine();
		c.put("Hello", "World");
		c.put("Bye", "Earth");
		assertEquals("\"Bye\",\"Hello\"", c.header());
		assertEquals("\"Earth\",\"World\"", c.line());
	}

	@Test public void emptyTest() {
		CSVLine c = new CSVLine();
		assertEquals("", c.header());
		assertEquals("", c.line());
	}

	@Test public void OneFieldTest() {
		CSVLine c = new CSVLine();
		c.put("Hello", "World");
		assertEquals("\"Hello\"", c.header());
		assertEquals("\"World\"", c.line());
	}

	@Test public void intTest() {
		CSVLine c = new CSVLine();
		c.put("Question", 42);
		assertEquals("\"Question\"", c.header());
		assertEquals("\"42\"", c.line());
	}

	@Test public void nullStringTest() {
		CSVLine c = new CSVLine();
		c.put("Null Header", (String) null);
		assertEquals("\"Null Header\"", c.header());
		assertEquals("\"\"", c.line());
	}

	@Test public void nullObjectTest() {
		CSVLine c = new CSVLine();
		c.put("Null Header", (Object) null);
		assertEquals("\"Null Header\"", c.header());
		assertEquals("\"\"", c.line());
	}

	@Test public void anObjectTest() {
		CSVLine c = new CSVLine();
		c.put("Some Object", new Object() {
			@Override public String toString() {
				return "some object";
			}
		});
		assertEquals("\"Some Object\"", c.header());
		assertEquals("\"some object\"", c.line());
	}

	@Test public void arrayTest() {
		String[] a = { "One", "two", "three", "four" };
		CSVLine c = new CSVLine();
		c.put("third", a, 3);
		assertEquals("\"third\"", c.header());
		assertEquals("\"four\"", c.line());
	}

	@Test public void arrayOutOfBoundsTest() {
		String[] a = { "One", "two", "three", "four" };
		CSVLine c = new CSVLine();
		c.put("OutOfBounds", a, 5);
		assertEquals("\"OutOfBounds\"", c.header());
		assertEquals("\"\"", c.line());
	}

	@Test public void simpleArrayTest() {
		String[] a = { "One", "two", "three", "four" };
		CSVLine c = new CSVLine();
		c.put("String Array", a);
		assertEquals("\"String Array\"", c.header());
		assertEquals("\"One;two;three;four\"", c.line());
	}

	static enum Week {
		Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday;
	}

	@Test public void enumArrayTest() {
		Week[] a = { Week.Sunday, Week.Saturday, Week.Friday };
		CSVLine c = new CSVLine();
		c.put("String Array", a);
		assertEquals("\"String Array\"", c.header());
		assertEquals("\"Sunday;Saturday;Friday\"", c.line());
	}

	@Test public void arrayNullTest() {
		String[] a = null;
		CSVLine c = new CSVLine();
		c.put("NullArray", a, 2);
		assertEquals("\"NullArray\"", c.header());
		assertEquals("\"\"", c.line());
	}

	@Test public void elementsSortTest() {
		String[] a = { "one", "two", "three", "four" };
		CSVLine c = new CSVLine();
		for (String s : a)
			c.put(s + "_key", s + "_value");
		assertEquals("\"four_key\",\"one_key\",\"three_key\",\"two_key\"", c.header());
		assertEquals("\"four_value\",\"one_value\",\"three_value\",\"two_value\"", c.line());
	}

	@Test public void quoteTest() {
		CSVLine c = new CSVLine();
		c.put("key", CSVLine.quote);
		assertEquals(STRING.fill(4, CSVLine.quote), c.line());
	}
}
