package utils;

/**
 * An interface representing a typed function with a single argument.
 * 
 * @param <ARGUMENT>
 *        Type of argumet
 * @param <RESULT>
 *        Type of result
 * @author Yossi Gil
 * @date 12/07/2007
 */
public interface Function<ARGUMENT, RESULT> {
	/**
	 * Evaluate the function for the given input
	 * 
	 * @param f
	 *        Input argument
	 * @return Value of the function for the given argument
	 */
	public RESULT eval(ARGUMENT f);
}
