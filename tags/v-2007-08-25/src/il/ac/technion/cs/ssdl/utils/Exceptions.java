package il.ac.technion.cs.ssdl.utils;

/**
 * This class provides exception-related services. It has one predefined
 * instance {@link #INSTANCE} but it is not a singleton, so customized behavior
 * can be achieved thru subclassing.
 * 
 * @author imaman <br>
 *         Written at: Aug 24, 2007
 * 
 */
public class Exceptions {
	/**
	 * A predefined instance of this class
	 */
	public static final Exceptions INSTANCE = new Exceptions();

	/**
	 * Translate any exception to an unchecked exception
	 * 
	 * @param t
	 *        Exception to translate
	 * @return An unchecked exception
	 */
	public RuntimeException makeUnchecked(Throwable t) {
		if (t instanceof RuntimeException)
			return (RuntimeException) t;
		RuntimeException e = new RuntimeException(t);
		e.setStackTrace(t.getStackTrace());
		return e;
	}
}
