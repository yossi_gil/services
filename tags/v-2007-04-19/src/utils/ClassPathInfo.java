/**
 * 
 */
package utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.zip.ZipFile;

/**
 * This class records all classes that are found on a certain class path
 * 
 * @author Itay Maman
 * @date Jul 6, 2006
 */
public class ClassPathInfo {
	private final ArrayList<String> elements = new ArrayList<String>();

	@Override public String toString() {
		return Separator.separateBy(elements, File.pathSeparator);
	}

	/**
	 * Initialize a new empty instance
	 */
	public ClassPathInfo() {
	}

	/**
	 * Add a new classpath element.
	 * 
	 * @param f
	 *            A File object specifying either a single directory or a single
	 *            jar file
	 */
	public ClassPathInfo add(File f) {
		elements.add(f.getAbsolutePath());
		return this;
	}

	public File[] asFiles() {
		File[] result = new File[elements.size()];
		for (int i = 0; i < elements.size(); ++i)
			result[i] = new File(elements.get(i));
		return result;
	}

	/**
	 * Find all classes on the classpath represented by the receiver
	 * 
	 * @return List of fully qualified names of all such classes
	 * @throws Exception
	 */
	public ArrayList<String> getClasses() throws Exception {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : elements)
			addFromDirectory(new File(s), s, result);
		return result;
	}

	/**
	 * Recursively adding all classes residing in specified directory into
	 * cache.
	 * 
	 * @param dirOrFile
	 *            file or directory
	 * @param root
	 *            the root directory
	 * @throws Exception
	 */
	private void addFromDirectory(File dirOrFile, String root, ArrayList<String> result) throws Exception {
		if (dirOrFile.isDirectory()) {
			String[] children = dirOrFile.list();
			for (String s : children)
				addFromDirectory(new File(dirOrFile, s), root, result);
			return;
		}
		String fileName = dirOrFile.getPath();
		// *** checking if file is .jar or .zip
		String suffix = getSuffix(fileName);
		// *** name without suffix
		if (suffix.length() > 0)
			fileName = fileName.substring(0, fileName.length() - suffix.length());
		if (suffix.equalsIgnoreCase(".jar") || suffix.equalsIgnoreCase(".zip")) {
			addFromArchive(dirOrFile.getPath(), result);
			return;
		}
		if (suffix.equalsIgnoreCase(".class")) {
			fileName = fileName.replace(root + File.separator, "");
			fileName = fileName.replace(File.separatorChar, '.');
			result.add(fileName);
		}
	}

	/**
	 * Getting a file extention i.e. .class
	 * 
	 * @param s
	 *            name of file
	 * @return the file extention
	 */
	private String getSuffix(String s) {
		return !s.contains(".") ? "" : s.substring(s.lastIndexOf("."), s.length());
	}

	/**
	 * Adding all classes residing in archive to cache
	 * 
	 * @param arc_name
	 *            name of archive ( .jar or .zip )
	 * @throws Exception
	 */
	private void addFromArchive(String jarFile, ArrayList<String> result) throws Exception {
		try {
			ZipFile f = new ZipFile(jarFile);
			Enumeration entries = f.entries();
			while (entries.hasMoreElements()) {
				String s = entries.nextElement().toString();
				if (!s.contains("."))
					continue;
				String suffix = getSuffix(s);
				if (!suffix.equalsIgnoreCase(".class"))
					continue;
				s = s.replace(".class", "");
				s = s.replace(File.separator, ".");
				s = s.replace("/", ".");
				result.add(s);
			}
		} catch (IOException e) {
			throw new Exception("File=" + jarFile, e);
		}
	}

	public static void main(String[] args) throws Exception {
		ClassPathInfo cpi = new ClassPathInfo();
		String s = "/usr/local/java/jdk1.5.0_05/jre/lib/rt.jar:samples-bin";
		for (StringTokenizer st = new StringTokenizer(s, File.pathSeparator); st.hasMoreTokens();) {
			File f = new File(st.nextToken());
			cpi.add(f);
		}
		System.out.println(cpi.getClasses().size());
	}
}
