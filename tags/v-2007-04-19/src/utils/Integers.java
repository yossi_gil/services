/**
 * 
 */
package utils;

import java.util.HashSet;

/**
 * A typedef for a set of integers.
 * 
 * @author Yossi Gil
 * @date 18/05/2006
 */
public class Integers extends HashSet<Integer> {
}
