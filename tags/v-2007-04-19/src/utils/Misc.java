package utils;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Misc {
	public static <T> Iterator<T> iteratorOf(T[] ts, int length) {
		return ArrayIterator.make(ts, 0, length);
	}

	public static <T> Iterator<T> iteratorOf(T[] ts) {
		return ArrayIterator.make(ts, 0);
	}

	public static <T> Iterable<T> iterableOf(final T[] ts) {
		return new Iterable<T>() {
			public Iterator<T> iterator() {
				return ArrayIterator.make(ts);
			}
		};
	}

	@SuppressWarnings("unchecked") public static <T> T[] duplicate(T[] src) {
		Class c = src.getClass().getComponentType();
		T[] result = (T[]) java.lang.reflect.Array.newInstance(c, src.length);
		System.arraycopy(src, 0, result, 0, src.length);
		return result;
	}

	public static boolean[] complement(boolean[] bs) {
		boolean[] result = new boolean[bs.length];
		for (int i = 0; i < bs.length; ++i)
			result[i] = !bs[i];
		return result;
	}

	public static boolean[] toArray(List<Boolean> bs) {
		boolean[] result = new boolean[bs.size()];
		for (int i = 0; i < bs.size(); ++i)
			result[i] = bs.get(i);
		return result;
	}

	public static <T, S extends T> void addAll(Collection<T> trg, Iterable<S> src) {
		for (S s : src)
			trg.add(s);
	}

	public static <E, T extends Collection<E>> T add(T trg, E[] src) {
		for (E e : src)
			trg.add(e);
		return trg;
	}

	// public static<T> T[] toArray(T... ts) { return ts; }
	@SuppressWarnings("unchecked") public static <T> T[] toArray(T t, T... ts) {
		DBC.require(t != null);
		T[] result = (T[]) Array.newInstance(t.getClass(), ts.length + 1);
		result[0] = t;
		for (int i = 0; i < ts.length; ++i)
			result[i + 1] = ts[i];
		return result;
	}

	public static <T> boolean allNonNull(Iterable<T> ts) {
		if (ts == null)
			return false;
		for (T t : ts)
			if (t == null)
				return false;
		return true;
	}

	public static String compaq(String s) {
		if (s == null)
			return null;
		char chars[] = s.toCharArray();
		String result = "";
		for (char c : chars)
			if (" \t\r\n".indexOf(c) < 0)
				result += c;
		return result;
	}

	public static boolean compareWithStream(String str, InputStream is) {
		Scanner actual = new Scanner(str);
		Scanner expected = new Scanner(is);
		while (true) {
			if (actual.hasNext() != expected.hasNext())
				return false;
			if (!actual.hasNext())
				return true;
			String a = actual.nextLine().trim();
			String b = expected.nextLine().trim();
			if (!a.equals(b)) {
				System.err.println("a=" + a);
				System.err.println("b=" + b);
				return false;
			}
		}
	}

	public static <T> Iterable<T> sortByString(Iterable<T> ts) {
		List<T> lst = new ArrayList<T>();
		for (T t : ts)
			lst.add(t);
		Collections.sort(lst, new Comparator<T>() {
			public int compare(T o1, T o2) {
				String s1 = o1 == null ? "" : o1.toString();
				String s2 = o2 == null ? "" : o2.toString();
				return s1.compareTo(s2);
			}
		});
		return lst;
	}
}
