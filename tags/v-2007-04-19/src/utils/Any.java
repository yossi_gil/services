/**
 * 
 */
package utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Any implements Iterable<Object[]> {
	public final Object[] head;
	public final Any tail;
	public static final Any EMPTY = new Any(null, null);

	public static Any make(Object... os) {
		Any a = EMPTY;
		for (Object o : os)
			a = a.add(o);
		return a;
	}

   @SuppressWarnings("unchecked")
	public static <T> Any make(Iterable<T> ts) {
		Any a = EMPTY;
		for (T t : ts)
			a = a.add(t);
		return a;
	}

	private Any(Object[] head, Any tail) {
		this.head = head;
		this.tail = tail;
	}

	public boolean headIs(String s) {
		return headAsString().equals(s);
	}

	public boolean headIs(int pos, String s) {
		return s.equals(head[pos].toString());
	}

	public String headAsString() {
		return new Stringer(",", head).toString();
	}

	public boolean headIs(Class<?> c) {
		return c.isAssignableFrom(head[0].getClass());
	}

	@Override public String toString() {
		if (isEmpty())
			return "";
		String s = "";
		s = head.length == 1 ? head[0].toString() : "<" + new Stringer(",", head) + ">";
		return s + (tail.isEmpty() ? "" : ":" + tail);
	}

	public int arity() {
return isEmpty()  ?  -1 
 :  head.length;
	}

	public boolean notEmpty() {
		return !isEmpty();
	}

	public boolean isEmpty() {
		return tail == null;
	}

	public <T> Any add(T... os) {
		// if(this.isEmpty() || os.length == this.arity())
		return new Any(os, this);
		// throw new IllegalArgumentException("Arity mismatch "
		// + os.length + " vs. " + this.arity());
	}

   @SuppressWarnings("unchecked")
	public <T> Any addAll(Iterable<T> ts) {
		Any a = this;
		for (T t : ts)
			a = a.add(t);
		return a;
	}

	public Any addAll(Any a) {
		return a.isEmpty() ? this : add(a.head).addAll(a.tail);
	}

	public Iterator<Object[]> iterator() {
		List<Object[]> os = new ArrayList<Object[]>();
		for (Any a = this; !a.isEmpty(); a = a.tail)
			os.add(a.head);
		return os.iterator();
	}
}