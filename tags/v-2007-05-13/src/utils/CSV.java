package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *	This class realize the CSV specification, by comprising methods for manipulating 
 *	CSV files.
 *	e.g. 
 *			1, 2, 3
 *			4, 5, 6
 *	
 *	The class supports string arrays as data.
 *	e.g. 
 *			1, {9, 10}, 3
 *			4, {8, 11}, 6
 *  
 *  This class also supports converting strings to enums instances.
 *  e.g. 
 *			1, EXTENDS, 3
 *			4, IMPLEMENTS, 6
 *  
 *	This is a simplified version of the CSV specification, each record must be a single line.    
 *  Within are some other useful auxilary functions for string manipulations. 
 * @author shex
 */
public class CSV {
	
	public static void saveToFile(String filename, String[][] fields) throws IOException{
		convertJavaStrings(fields);
		
		BufferedWriter bw = new BufferedWriter( new FileWriter(filename));
		for (int i = 0; i < fields.length; i++) {
			bw.write( Stringer.sequence("", fields[i], ", ", "\n") ); 
		}
		bw.flush();
		bw.close();
	}
	

	/**
	 * 
	 * @param filename 
	 * @param fieldCount number of fields in a record
	 * @return records as String[], where each string is a field 
	 * @throws IOException some problem with file 'filename'
	 * @throws CorruptedDataException when CSV record doesn't contains exacly 'fieldCount' fields 
	 */
	public static String[][] readFile(String filename, final int fieldCount) throws IOException, CorruptedDataException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		ArrayList<String[]> result = new ArrayList<String[]>(20);
		String line;
		while ((line = br.readLine()) != null) {
			line = line.trim(); // TODO ask itay about the trim
			if( line.startsWith("#") )
				continue;
			
			String[] values = line.split(",");
			if(values.length != fieldCount)
				throw new CorruptedDataException("Data corruption, occured in line: " + line);

			// trim each value
			for (int i = 0; i < values.length; i++)
				values[i] = convertToJavaString(values[i]);
			
			result.add(values);
		}
		
		String[][] resultAsArray = new String[result.size()][];
		return result.toArray(resultAsArray);
	}

	/**
	 * 
	 * @param filename
	 * @param fields records' field types
	 * @return records as Object[], where each object is a field
	 * @throws IOException
	 * @throws CorruptedDataException
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public static Object[][] readFile(String filename, Class<?>[] fields) throws IOException, CorruptedDataException, ClassNotFoundException {
		String[][] strings = readFile(filename, fields.length);
		Object[][] objects = new Object[strings.length][fields.length];
		for (int i = 0; i < fields.length; i++) {
			for (int j = 0; j < strings.length; j++) {
				String fieldName = null;
				try {
					// FIXME what about Categories[]
					fieldName = fields[i].getClass().getName();
					if( fieldName.equals("String") ){
						objects[j][i] = strings[j][i];
					}
					else if( fieldName.equals("String[]") ){
						objects[j][i] = splitString(strings[j][i]);
					}
					else if( fieldName.equals("Class<Enum>") ){
						objects[j][i] = convertToEnumInstance((Class<Enum>) fields[i], strings[j][i]);
					}
					else if( fieldName.equals("Class") ){
						objects[j][i] = convertToClass(fields[i], strings[j][i]);
					}
				} catch (RuntimeException e) {
					throw new CorruptedDataException("Record type does not match requested fields, string=" + 
														strings[j][i] +", type=" + fieldName);
				}
			}
			
		}
		
		return objects;
	}
	
	
	private static Object convertToClass(Class<?> class1, String className) throws ClassNotFoundException {
		return ClassLoader.getSystemClassLoader().loadClass(className);
	}

	/**
	 * @param string
	 * @return
	 */
	private static String convertToJavaString(String CSVString) {
		String result =  CSVString.trim();
		result =  result.replaceAll("\\n", "\n");
		result =  result.replaceAll("\\t", "\t");
		result =  result.replaceAll("\\r", "\r");
		result =  result.replaceAll("\\b", "\b");
		return result;
	}
	

	/** 
	 * @param string
	 * @return a string not containing any special characters. e.g. convert "\n" (newline) to "\\n" 
	 * 			
	 */
	private static String convertFromJavaString(String CSVString) {
		String result =  CSVString.trim();
		result =  result.replaceAll("\n", "\\n");
		result =  result.replaceAll("\t", "\\t");
		result =  result.replaceAll("\r", "\\r");
		result =  result.replaceAll("\b", "\\b");
		return result;
	}

	/**
	 * converts all strings using the convertFromJavaString() mathod
	 * @param strings
	 */
	private static void convertJavaStrings(String[][] strings) {
		for (int i = 0; i < strings.length; i++) {
			for (int j = 0; j < strings.length; j++) {
				strings[i][j] = convertFromJavaString(strings[i][j]); 
			}
		}
	}

	
	/**
	 * splits the string (which represents an array of strings) to a string array 
	 * e.g. {1, 2, 3}  as arg will return a string array of 
	 * size 3 containing the values 1, 2 and 3.   
	 * @param stringArray 
	 * @return splited string as array 
	 * @throws CorruptedDataException 
	 */
	private static String[] splitString(String stringArray) throws CorruptedDataException{
		String[] result;
		try {
			int first = stringArray.indexOf("{")+1;
			int last = stringArray.lastIndexOf("}");
			result = stringArray.substring(first, last).split(";");
		} catch (RuntimeException e) {
			throw new CorruptedDataException("couldn't split string " + stringArray); 
		}
		return result;
	}

	
	// TODO ask itay
	@SuppressWarnings("unchecked")
	private static Enum<?> convertToEnumInstance(Class<Enum> enumClass, String enumInstanceName){
		return Enum.valueOf(enumClass, enumInstanceName);
	}
	
    /**
     * Indicates that a file (e.g. a CSV file) is not well formed
     * @author shex
     * @date May 9, 2007
     */
    public static class CorruptedDataException extends Exception {
        public CorruptedDataException(String string) {
	        super(string);
        }

		private static final long serialVersionUID = 1L;
    }
}
