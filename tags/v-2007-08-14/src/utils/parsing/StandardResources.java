package utils.parsing;

import static java.lang.Math.max;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import utils.IO;

/**
 * A factory for standard {@link InputResource} objects.
 */
public class StandardResources 
{
   private StandardResources()
   {
      // Non instantiable
   }
   
   /**
    * Create a {@link InputResource} object that obtains its contents from 
    * the specified reader. 
    * @param r A reader object
    * @return A new InputResource
    */
   public static InputResource newReader(Reader r) 
   {
      return new StandardResourceImpl("/UNKNOWN READER/", r, true);
   }
   
   /**
    * Create a {@link InputResource} object that obtains its contents from 
    * the specified file. The file name is translated into an abolute path
    * so the resulting InputResource will be indifferent to changes 
    * in the current working directory.
    * 
    * @param fileName A file name  
    * @return A new InputResource
    */
   public static InputResource newFile(String fileName) 
   {
      return newFile(new File(fileName).getAbsoluteFile());
   }

   
   /**
    * Create a {@link InputResource} object that obtains its contents from 
    * the specified file. The file name is translated into an abolute path
    * so the resulting InputResource will be indifferent to changes 
    * in the current working directory.
    * 
    * @param f A file object
    * @return A new InputResource
    */
   public static InputResource newFile(File f)  {
      final File abs = f.getAbsoluteFile();
      return new StandardResourceImpl(abs.getPath(), null, false)
      {
         @Override public Reader reader() throws FileNotFoundException  
         {
            return new FileReader(abs);
         }         
      };
   }

   /**
    * Create a {@link InputResource} object that obtains its contents from 
    * the given string.
    * 
    * @param s String specifying the contents of the created InputResource.  
    * @return A new InputResource.
    */
   public static InputResource newString(String s) {
      return new StandardResourceImpl(makeName(s), new StringReader(s), true);
   }

   

   /**
    * A predefined {@link InputResource} object whose underlying physical 
    * resource is <code>System.in</code>
    */
   public static final InputResource stdin = new StandardResourceImpl("/STDIN/", null, true)
      {
         @Override public Reader reader() 
         {
            return new InputStreamReader(System.in);
         }
      };
   
   private static class StandardResourceImpl implements InputResource
   {
      private final String name;
   
      private String text = null;
      private final Reader reader;
   
      private static int counter = -1;
      private final int id;
      
      public StandardResourceImpl(String name, Reader r, boolean putIdInName)
      {
         this.id = ++counter;
         this.name = putIdInName ? id + ":" + name : name;
         this.reader = r;
      }
            
      @Override public String toString()
      {
         return name;
      }
      
      @Override public boolean equals(Object o)
      {
         if(o == null)
            return false;
         
         if(!this.getClass().equals(o.getClass()))
            return false;
         StandardResourceImpl that = (StandardResourceImpl) o;         
         return this.compareTo(that) == 0;         
      }
      
      public int compareTo(InputResource that)
      {
         return this.id() -  that.id();
      }          
            
      public int id()
      {
         return id;
      }
      
      public String name()
      {
         return name;
      }

      public Reader reader() throws IOException
      {
         if(text != null)
            return new StringReader(text);
         
         text = IO.toString(reader);
         return new StringReader(text);
      }
      
      public String text() throws IOException
      {
         if(text != null)
            return text;
         
         text = IO.toString(reader());
         return text;
      }      
   }      
   
   private final static int MAX_RECORD_STRING = 60;
   
   private static String makeName(String s) {
      return wrap(essense(s));
   }

   private static String essense(String s) {
      return s.length() < MAX_RECORD_STRING ? s : begin(s) + "..." + end(s);
   }

   private static String wrap(String s) {
      return "/STRING: " + s + "/";
   }

   private static String begin(String s) {
      return s.substring(0, MAX_RECORD_STRING / 3);
   }

   private static String end(String s) {
      final int len = s.length() - 1;
      return s.substring(max(len - MAX_RECORD_STRING, 0), len);
   }        
}
