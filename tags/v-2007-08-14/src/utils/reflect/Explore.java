package utils.reflect;

import static utils.reflect.Out.out;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;

/**
 * A class to print all properties of an aribtrary object which can be retrieved
 * by getters methods (i.e., getXXX()) methods and boolean inspection methods
 * (i.e., isXXX()), as can be determined by reflection information.
 * 
 * @author Yossi Gil
 * @date 24/07/2007
 */
public class Explore {
	public static void go(Class<?> c) {
		out("\n\n--IDENTIFICATION--\n");
		out("Simple Name", c.getSimpleName());
		out("Canonical Name", c.getCanonicalName());
		out("Name", c.getName());
		out("toString", c.toString());
		out("super class", c.getSuperclass());
		out("generic super class", c.getGenericSuperclass());
		out("class", c.getClass());
		out("component type", c.getComponentType());
		// out("protection domain", c.getProtectionDomain());
		out("class loader", c.getClassLoader());
		out("--MODIFIERS--\n");
		final int flags = c.getModifiers();
		out("Package", c.getPackage());
		out("Modifiers (decimal form)", flags);
		out("Modifiers (binary form)", ReflectionAnalyzer.toBinary(flags));
		out("IsSynthetic", c.isSynthetic());
		out("IsPrimitive", c.isPrimitive());
		out("IsFinal", Modifier.isFinal(flags));
		out("IsAbstract", Modifier.isAbstract(flags));
		out("--Visibility--\n");
		out("IsPublic", Modifier.isPublic(flags));
		out("IsPrivate", Modifier.isPrivate(flags));
		out("IsProtected", Modifier.isProtected(flags));
		out("--MEMBERS\n");
		out("fields", c.getFields());
		out("methods", c.getMethods());
		out("constructors", c.getConstructors());
		out("declared fields", c.getDeclaredFields());
		out("declared methods", c.getDeclaredMethods());
		out("declared constructors", c.getDeclaredConstructors());
		out("--CLASS SIGNATURE--\n");
		out("interfaces", c.getInterfaces());
		out("annotations", c.getAnnotations());
		out("type parameters", c.getTypeParameters());
		out("declared annotations", c.getDeclaredAnnotations());
		out("generic interfaces", c.getGenericInterfaces());
		out("--CONTAINERS--\n");
		out("declared classes", c.getDeclaredClasses());
		out("declaring class", c.getDeclaringClass());
		out("enclosing class", c.getEnclosingClass());
		out("enclosing constructor", c.getEnclosingConstructor());
		out("enclosing method", c.getEnclosingMethod());
		out("--CLASS MEMBERS--\n");
		out("public classes", c.getClasses());
		out("declared classes", c.getDeclaredClasses());
		out("declared annotations", c.getDeclaredAnnotations());
		out("---------------------------\n");
	}

	@SuppressWarnings("unchecked") public static void go(Object o) {
		final Class<?> c = o.getClass();
		out("\n\n--BEGIN " + c.getSimpleName() + " object: " + o.toString() + "\n");
		for (final Method m : c.getMethods()) {
			if (m.getParameterTypes().length != 0)
				continue;
			String name = m.getName();
			if (name.equals("getClass") || name.equals("toString"))
				continue;
			if (name.matches("^get[A-Z].*$"))
				name = name.replaceFirst("^get", "");
			else if (name.matches("^is[A-Z].*$"))
				name = name.replaceFirst("^is", "");
			else if (!name.matches("^to[A-Z].*$"))
				continue;
			try {
				final Object $ = m.invoke(o, new Object[0]);
				if ($ instanceof Object[])
					out(name, (Object[]) $);
				if ($ instanceof Collection)
					out(name, (Collection<Object>) $);
				else
					out(name, $);
			} catch (Throwable e) {
				// For some reason, a reflection call to method
				// getContent() in URL objects throws this exception.
				// We do not have much to do in this and other similar cases.
				out(name, "THROWS " + e);
			}
		}
		out("--END OBJECT\n\n");
		System.out.flush();
	}

	public static void main(String[] args) throws URISyntaxException, MalformedURLException {
		URL url = new URL("http://www.yahoo.com");
		go(url);
		go(url.toURI());
		go(byte.class);
		go(void.class);
		go(java.lang.annotation.Annotation.class);
		go(int[].class);
	}
}
