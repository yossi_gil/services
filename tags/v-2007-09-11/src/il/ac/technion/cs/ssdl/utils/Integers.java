package il.ac.technion.cs.ssdl.utils;

import java.util.HashSet;

/**
 * A typedef for a set of integers.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il> 18/05/2006
 */
public class Integers extends HashSet<Integer> {
	public boolean add(int e) {
		return super.add(new Integer(e));
	}

	public boolean contains(int e) {
		return super.add(new Integer(e));
	}

	public boolean remove(int e) {
		return super.remove(new Integer(e));
	}
}
