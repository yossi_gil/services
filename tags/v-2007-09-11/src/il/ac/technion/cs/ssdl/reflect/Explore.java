package il.ac.technion.cs.ssdl.reflect;

import static il.ac.technion.cs.ssdl.reflect.Out.out;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;

/**
 * A class to print all properties of an aribtrary object which can be retrieved
 * by getters methods (i.e., getXXX()) methods and boolean inspection methods
 * (i.e., isXXX()), as can be determined by reflection information.
 * 
 * @author Yossi Gil 24/07/2007
 */
public class Explore {
	public static void go(Class<?> c) {
		out("\n\n--IDENTIFICATION--\n");
		out("Simple Name", c.getSimpleName());
		out("Canonical Name", c.getCanonicalName());
		out("Name", c.getName());
		out("toString", c.toString());
		out("super class", c.getSuperclass());
		out("generic super class", c.getGenericSuperclass());
		out("class", c.getClass());
		out("component type", c.getComponentType());
		// out("protection domain", c.getProtectionDomain());
		out("class loader", c.getClassLoader());
		out("--MODIFIERS--\n");
		final int flags = c.getModifiers();
		out("Package", c.getPackage());
		out("Modifiers (decimal form)", flags);
		out("Modifiers (binary form)", ReflectionAnalyzer.toBinary(flags));
		out("IsSynthetic", c.isSynthetic());
		out("IsPrimitive", c.isPrimitive());
		out("IsFinal", Modifier.isFinal(flags));
		out("IsAbstract", Modifier.isAbstract(flags));
		out("IsStatic", Modifier.isStatic(flags));
		out("IsStrictfp", Modifier.isStrict(flags));
		out("--Visibility--\n");
		out("IsPublic", Modifier.isPublic(flags));
		out("IsPrivate", Modifier.isPrivate(flags));
		out("IsProtected", Modifier.isProtected(flags));
		out("--MEMBERS\n");
		out("fields", c.getFields());
		out("methods", c.getMethods());
		out("constructors", c.getConstructors());
		out("declared fields", c.getDeclaredFields());
		out("declared methods", c.getDeclaredMethods());
		out("declared constructors", c.getDeclaredConstructors());
		out("--CLASS SIGNATURE--\n");
		out("interfaces", c.getInterfaces());
		out("annotations", c.getAnnotations());
		out("type parameters", c.getTypeParameters());
		out("declared annotations", c.getDeclaredAnnotations());
		out("generic interfaces", c.getGenericInterfaces());
		out("--CONTAINERS--\n");
		out("declared classes", c.getDeclaredClasses());
		out("declaring class", c.getDeclaringClass());
		out("enclosing class", c.getEnclosingClass());
		out("enclosing constructor", c.getEnclosingConstructor());
		out("enclosing method", c.getEnclosingMethod());
		out("--CLASS MEMBERS--\n");
		out("public classes", c.getClasses());
		out("declared classes", c.getDeclaredClasses());
		out("declared annotations", c.getDeclaredAnnotations());
		out("---------------------------\n");
	}

	public static void go(Object o, String... ss) {
		for (String s : ss)
			out(s);
		final Class<?> c = o.getClass();
		out("\n\n--BEGIN " + c.getSimpleName() + " object: " + o.toString() + "\n");
		out("Class canonical name", c.getCanonicalName());
		out("Class name", c.getName());
		for (final Method m : c.getMethods()) {
			if (m.getParameterTypes().length != 0)
				continue;
			String name = m.getName();
			if (name.equals("getClass") || name.equals("toString"))
				continue;
			if (name.matches("^get[A-Z].*$"))
				name = name.replaceFirst("^get", "");
			else if (name.matches("^is[A-Z].*$"))
				name = name.replaceFirst("^is", "");
			else if (name.equals("size"))
				name = "size";
			else if (!name.matches("^to[A-Z].*$"))
				continue;
			try {
				final Object $ = m.invoke(o, new Object[0]);
				if ($ == null) {
					out(name, "null");
					continue;
				}
				if ($ instanceof Object[])
					out(name, (Object[]) $);
				if ($ instanceof Collection) {
					@SuppressWarnings("unchecked") final Collection<Object> os = (Collection<Object>) $;
					out(name, os);
				} else
					out(name, $);
			} catch (final Throwable e) {
				// For some reason, a reflection call to method
				// getContent() in URL objects throws this exception.
				// We do not have much to do in this and other similar cases.
				out(name, m.getName() + "THROWS " + e);
			}
		}
		out("--END OBJECT--\n\n");
		System.out.flush();
	}

	public static void main(String[] args) {
		Explore.go(Package.class);
	}
}
