/**
 * 
 */
package utils.parsing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import utils.Function;
import utils.IO;
import utils.Iterables;

/**
 * A default implementation of {@link MessageLog}
 * 
 * @author imaman
 */
public class DefaultMessageLog implements MessageLog
{
   private static final long serialVersionUID = 1000629616878797828L;
   
   private List<ErrorItem> errors = new ArrayList<ErrorItem>();
   
   @Override
   public String toString()
   {
      if(errors.isEmpty())
         return "no errors";
      return errors.size() + " errors";
   }
   
   /**
    * {@inheritDoc}
    */
   public Iterator<ErrorItem> iterator()
   {
      Collections.sort(errors);
      return errors.iterator();
   }

   /**
    * {@inheritDoc}
    */
   public void recordError(Location loc, String message, 
      String sourceCodeFragment)
   {
      errors.add(new DefaultErrorItem(loc, message, sourceCodeFragment));
      
   }

   /**
    * {@inheritDoc}
    */
   @SuppressWarnings("unused")
   public void recordWarning(Location loc, String message)
   {
      // Do nothing         
   }
   

   /**
    * {@inheritDoc}
    */
   public void throwIfErrors() throws ErrorBag
   {
      if(errors.size() == 0)
         return;
      
      Iterable<String> list = Iterables.map(this, new Function<ErrorItem, String>()
      {
         public String eval(ErrorItem item)
         {
            return item.getLocation().where() + ": " + item.getMessage();
         }            
      });
      
      throw new ErrorBag(Iterables.toArray(this, ErrorItem.class), IO.concatLines(list));
   }
   
}