package utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 *	This class realize the CSV specification, by comprising methods for manipulating 
 *	CSV files.
 *	e.g. 
 *			1, 2, 3
 *			4, 5, 6
 *	
 *	The class supports string arrays as data.
 *	e.g. 
 *			1, {9, 10}, 3
 *			4, {8, 11}, 6
 *  
 *  This class also supports converting strings to enums instances.
 *  e.g. 
 *			1, EXTENDS, 3
 *			4, IMPLEMENTS, 6
 *  
 *	This is a simplified version of the CSV specification, each record must be a single line.    
 *  Within are some other useful auxilary functions for string manipulations. 
 * @author Oren Rubin
 */
public class CSV {
	
	public static void save(File file, String[][] data) throws IOException {
		PrintWriter pw = new PrintWriter(new FileWriter(file));
		pw.print(toCsv(data));
		pw.close();
	}
	
	public static String toCsv(String[][] data)  {		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		for (String[] line : data) {
			
			boolean isFirst = true;
			for(String s : line) {
				pw.print(isFirst ? "" : ",");
				isFirst = false;
				pw.print(escape(s));				
			}
			
			pw.println();
		}
		
		pw.flush();
		return sw.toString();
	}
	

	/**
    * Read a CSV file.
    * 
	 * @param file Input file 
	 * @return A two dimensional array of strings
	 * @throws IOException some problem with file 'filename'
	 */
   public static String[][] load(File file) throws IOException {
      return load(new FileReader(file));
   }
   
   /**
    * Read a CSV file from the given Reader object.
    * 
    * @param r Input reader 
    * @return A two dimensional array of strings
    */
	public static String[][] load(Reader r) {
		ArrayList<String[]> result = new ArrayList<String[]>(20);
		for(Scanner s = new Scanner(r); s.hasNext(); ) 
			result.add(split(s.nextLine()));
		return result.toArray(new String[result.size()][]);
	}

	/**
	 * Escape the given input
	 * @param in Input string
	 * @return Escaped form of the input
	 */
	public static String escape(String in) {
		int len = in.length();
		StringBuilder out = new StringBuilder(len);
		
		for(int i = 0; i < len; ++i) {
			char c = in.charAt(i);
			if(c == '\\')
				out.append("\\\\");
			else if(c == '\n')
				out.append("\\n");
			else if(c == '\r')
				out.append("\\r");
			else if(c == '\t')
				out.append("\\t");
			else if(c == ',')
				out.append("\\.");
			else
				out.append(c);
		}
		
		return out.toString();
	}
	
	/**
	 * Unescape the given input
	 * @param in Input string
	 * @return Unescaped string
	 */
	public static String unescape(String in) {
		boolean esc = false;
		
		int len = in.length();
		StringBuilder out = new StringBuilder(len);
		for(int i = 0; i < len; ++i) {
			char c = in.charAt(i);
			
			if(c == 'n' && esc)  {
				out.append("\n");
				esc = false;
			}
			else if(c == 'r' && esc) {
				out.append("\r");
				esc = false;				
			}
			else if(c == 't' && esc) {
				out.append("\t");
				esc = false;				
			}
			else if(c == '.' && esc) {
				out.append(",");
				esc = false;				
			}
			else if(c == '\\' && esc) {
				out.append("\\");
				esc = false;
			} 
			else if(c == '\\' && !esc) {
				esc = true;
			}
			else {
				out.append(c);
				esc = false;
			}
		}
		
		return out.toString();
	}

	
	public static String[] split(String s) {
		if(s.trim().length() == 0)
			return new String[0];
		List<String> lst = new ArrayList<String>();
		int begin = 0;
		while(true) {
			int pos = s.indexOf(',', begin);
			if(pos < 0) {
				pos = s.length();
				lst.add(s.substring(begin, s.length()));
				break;
			}
			
			lst.add(unescape(s.substring(begin, pos)));
			begin = pos + 1;
		}
		return lst.toArray(new String[lst.size()]);
	}
}
