package utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Static methods for I/O related operations
 */
public class IO {
	/**
	 * Read the contents of the given reader and return it as a String
	 * 
	 * @param r
	 *            Reader
	 * @return String
	 * @throws IOException
	 */
	public static String toString(Reader r) throws IOException {
		final StringBuilder sb = new StringBuilder();
		while (true) {
			final int c = r.read();
			if (c < 0)
				return sb.toString();
			sb.append((char) c);
		}
	}
   
   /**
    * Read the contents of the given stream and return it as a String
    * 
    * @param is
    *            Input stream
    * @return String
    * @throws IOException
    */
   public static String toString(InputStream is) throws IOException {
      return toString(new InputStreamReader(is));
   }
}
