package utils;

/**
 * An interface representing a typed function with a single argument.
 * 
 * @param <F>
 *            Type of argumet
 * @param <T>
 *            Type of result
 */
public interface Func<F, T> {
	/**
	 * Evaluate the function for the given input
	 * 
	 * @param f
	 *            Input argument
	 * @return Value of the function for the given argument
	 */
	public T eval(F f);
}
