package utils;

import junit.framework.TestCase;

public class CSV_Test extends TestCase {

	public void test1() {
		String s = "abc,def\r\n\tg\\m";
		String t = CSV.escape(s);
		String u = CSV.unescape(t);
		
		assertEquals("abc\\,def\\r\\n\\tg\\\\m", t);
		assertEquals(s, u);
		assertFalse(s.equals(t));		
	}
}
