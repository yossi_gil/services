package utils;

public enum STRING {
	// No elements in this name space
	;
	public static String strip(String s) {
		return s.substring(1, s.length() - 1);
	}

	public static String cat(String... ss) {
		StringBuilder b = new StringBuilder("");
		for (String s : ss)
			b.append(s);
		return b.toString();
	}

	public static String fill(int n, char c) {
		String s = Character.toString(c);
		String result = "";
		for (int i = 0; i < n; ++i)
			result += s;
		return result;
	}

	/**
	 * Convert a string in the form [+-][0-9]* into an integer. The conversion
	 * assumes that the string indeed matches the pattern. An assertion occur
	 * will occur if the input does not match this pattern. No guarantee is made
	 * of the returned value if the string does not match this pattern. Also, no
	 * check for overflow is made.
	 * 
	 * @param s
	 *            The string to convert.
	 * @return An integer representation of the input string.
	 */
	public static int atoi(String s) {
		int i = 0;
		int sign = 1;
		switch (s.charAt(i)) {
			case '-':
				sign = -1;
				i++;
				break;
			case '+':
				i++;
		}
		int result;
		for (result = 0; i < s.length(); i++) {
			assert s.charAt(i) >= '0';
			assert s.charAt(i) <= '9';
			result = result * 10 + s.charAt(i) - '0';
		}
		return sign * result;
	}
}
