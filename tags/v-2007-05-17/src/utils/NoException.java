package utils;

/**
 * A class representing an exception that can never be thrown. No uses are
 * known. To be erased.
 * 
 * @author Yossi Gil
 * 
 */
@SuppressWarnings("serial") @Deprecated public final class NoException extends Exception {
	private NoException() throws Exception {
		DBC.unreachable("Tried to create a NoException object");
	}
}
