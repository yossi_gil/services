package utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * A collection class that allows addition, counting, iteration, but noting
 * else.
 * 
 * @author Yossi Gil <yogi@cs.technion.ac.il>
 * @param <T>
 *        Type of elements in the collection.
 * @date 01/05/2007
 */
public abstract class ImmutableList<T> implements Iterable<T>, Serializable {
	protected List<T> data = new ArrayList<T>();

	/**
	 * Adds another collection to this one.
	 * 
	 * @param other
	 *        The element to be added.t
	 */
	public void addAll(ImmutableList<T> other) {
		data.addAll(other.data);
	}

	/**
	 * Adds another collection to this one.
	 * 
	 * @param other
	 *        The element to be added.t
	 */
	public void addAll(Set<T> other) {
		data.addAll(other);
	}

	/**
	 * Add an element to the collection.
	 * 
	 * @param element
	 *        The element to be added.
	 * @return The added element
	 */
	public T add(T element) {
		if (element != null)
			data.add(element);
		return element;
	}

	/**
	 * @return The number of elements in the collection.
	 */
	public int size() {
		return data.size();
	}

	public Iterator<T> iterator() {
		return data.iterator();
	}

	/**
	 * convert this collection into an array
	 * 
	 * @return an arrary of the elements stored in this collection
	 */
	abstract public T[] toArrary();

	public boolean contains(Object o) {
		return data.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return data.containsAll(c);
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		return data.equals(((ImmutableList) o).data);
	}

	@Override public int hashCode() {
		final int PRIME = 31;
		int $ = super.hashCode();
		$ = PRIME * $ + (data == null ? 0 : data.hashCode());
		return $;
	}

	public boolean isEmpty() {
		return data.isEmpty();
	}

	public Object[] toArray() {
		return data.toArray();
	}

	public T[] toArray(T[] a) {
		return data.toArray(a);
	}
}
