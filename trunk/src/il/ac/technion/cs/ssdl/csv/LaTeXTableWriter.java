/**
 *
 */
package il.ac.technion.cs.ssdl.csv;

import static il.ac.technion.cs.ssdl.utils.Box.box;
import il.ac.technion.cs.ssdl.csv.Aggregator.Aggregation;
import il.ac.technion.cs.ssdl.iteration.Iterables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Yossi Gil
 * @since Apr 5, 2012
 */
public class LaTeXTableWriter extends CSVLineWriter {
	/**
	 * Instantiate {@link LaTeXTableWriter}.
	 */
	public LaTeXTableWriter() {
		super(Renderer.LaTeX);
	}

	/**
	 * Instantiate {@link LaTeXTableWriter}.
	 *
	 * @param fileName
	 */
	public LaTeXTableWriter(final String fileName) {
		super(fileName, Renderer.LaTeX);
	}

	@Override
	protected String extension() {
		return ".tex";
	}

	public CSVLine in(final Object innerTableName) {
		return in(innerTableName.toString());
	}

	public CSVLine in(final String innerTableName) {
		ensure(inner, innerTableName, new CSVLine.Ordered());
		return inner.get(innerTableName);
	}

	private static <K, V> void ensure(final Map<K, V> m, final K k, final V v) {
		if (!m.containsKey(k)) {
			m.put(k, v);
			return;
		}
	}

	@Override
	public String header() {
		return renderer.allTop() + wrappingHeader() + makeLine(keys())
				+ renderer.headerEnd();
	}

	private String wrappingHeader() {
		if (inner.size() == 0)
			return "";
		final List<String> $ = new ArrayList<String>();
		final Formatter f = new Formatter();
		int column = size();
		$.add(String.format("\\multicolumn{%d}{c}{\\mbox{}}", box(column)));
		for (final String nestedTableName : inner.keySet()) {
			f.format("\\cmidrule(lr){%d-", box(column + 1));
			final int size = inner.get(nestedTableName).size();
			$.add(String.format("\\multicolumn{%d}{c}{%s}", box(size),
					nestedTableName));
			f.format("%d} ", box(column += size));
		}
		return makeLine($) + "\n" + f + "\n";
	}

	@Override
	public Collection<String> keys() {
		final List<String> $ = new ArrayList<String>(super.keys());
		for (final AbstractStringProperties nested : inner.values())
			Iterables.addAll($, nested.keys());
		return $;
	}

	@Override
	public Collection<String> values() {
		final List<String> $ = new ArrayList<String>(super.values());
		for (final AbstractStringProperties nested : inner.values())
			Iterables.addAll($, nested.values());
		return $;
	}

	private final Map<String, CSVLine> inner = new LinkedHashMap<String, CSVLine>();

	@Override
	public String close() {
		if (aggregating()) {
			writer.writeln(super.renderer.headerEnd());
			for (final Aggregation a : aggregations())
        writer.writeln(makeLine(collect(a).values()));
		}
		return super.close();
	}

  private AbstractStringProperties collect(final Aggregation a) {
    final AbstractStringProperties $ = new ListProperties();
    addAggregates($, a);
		for (final CSVLine nested : inner.values())
      nested.addAggregates($, a);
    return $;
	}


	@Override
	public boolean aggregating() {
		boolean $ = super.aggregating();
		for (final CSVLine nested : inner.values())
			$ |= nested.aggregating();
		return $;
	}

	@Override
	public final Iterable<Aggregation> aggregations() {
		final Set<Aggregation> $ = new LinkedHashSet<Aggregation>();
		Iterables.addAll($, super.aggregations());
		for (final CSVLine nested : inner.values())
			Iterables.addAll($, nested.aggregations());
		return $;
	}
}
