/**
 *
 */
package il.ac.technion.cs.ssdl.csv;

import static il.ac.technion.cs.ssdl.utils.DBC.require;
import il.ac.technion.cs.ssdl.csv.Aggregator.Aggregation.FormatSpecifier;
import il.ac.technion.cs.ssdl.statistics.RealStatistics;
import il.ac.technion.cs.ssdl.utils.Box;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yossi Gil
 * @since Apr 8, 2012
 */
public class Aggregator {
  private final List<Aggregation> allAggregations = new ArrayList<Aggregation>();

  private final Map<String, Map<Aggregation, String>> columnSpecificAggregation = new HashMap<String, Map<Aggregation, String>>();

  private final Map<String, RealStatistics> realStatistics = new LinkedHashMap<String, RealStatistics>();

  private String markColumn;

  public void record(final String key, final double value, final FormatSpecifier... as) {
    record(key, value, toMap(as));
  }

  public void markColumn(final String key) {
    markColumn = key;
  }

  public final Iterable<Aggregation> aggregations() {
    return allAggregations;
  }

  public int size() {
    return allAggregations.size();
  }

  public void addAggregates(final Iterable<String> keys, final AbstractStringProperties to, final Aggregation a) {
    for (final String key : keys)
      addAggregate(key, to, a);
  }

  private void addAggregate(final String key, final AbstractStringProperties to, final Aggregation a) {
    if (key.equals(markColumn)) to.put(key, a.toString());
    else
      to.put(key, missing(key, a) ? "" : get(key, a));
  }

  private static Map<Aggregation, String> toMap(final FormatSpecifier[] as) {
    final Map<Aggregation, String> $ = new LinkedHashMap<Aggregation, String>();
    for (final FormatSpecifier a : as)
      $.put(a.getKey(), a.format());
    return $;
  }

  public void record(final String key, final double value, final Map<Aggregation, String> as) {
    ensure(realStatistics, key, new RealStatistics());
    force(columnSpecificAggregation, key, as);
    merge(as);
    realStatistics.get(key).record(value);
  }

  private String get(final String key, final Aggregation a) {
    return a.retreive(realStatistics.get(key), columnSpecificAggregation.get(key).get(a));
  }

  private boolean missing(final String key, final Aggregation a) {
    return !columnSpecificAggregation.containsKey(key) || !columnSpecificAggregation.get(key).containsKey(a);
  }

  private static <K, V> void ensure(final Map<K, V> m, final K k, final V v) {
    if (!m.containsKey(k)) {
      m.put(k, v);
      return;
    }
  }

  private static <K, V> void force(final Map<K, V> m, final K k, final V v) {
    ensure(m, k, v);
    final V u = m.get(k);
    require(v == u || v.equals(u) || v.getClass().isArray() && Arrays.equals((Object[]) u, (Object[]) v));
  }

  protected void merge(final Map<Aggregation, String> as) {
    int lastFound = -1;
    for (final Aggregation a : as.keySet()) {
      final int j = allAggregations.indexOf(a);
      if (j < 0) {
        allAggregations.add(a);
        continue;
      }
      require(j > lastFound);
      lastFound = j;
    }
  }

  public enum Aggregation {
    COUNT {
      @Override public double retreive(final RealStatistics r) {
        return r.n();
      }
    },
    MIN {
      @Override public double retreive(final RealStatistics r) {
        return r.min();
      }

      @Override public String toString() {
        return "\\textbf{\\emph{Min}}";
      }
    },
    MAX {
      @Override public double retreive(final RealStatistics r) {
        return r.max();
      }

      @Override public String toString() {
        return "\\textbf{\\emph{Max}}";
      }
    },
    MEAN {
      @Override public double retreive(final RealStatistics r) {
        return r.mean();
      }

      @Override public String toString() {
        return "\\textbf{\\emph{Mean}}";
      }
    },
    MEDIAN {
      @Override public double retreive(final RealStatistics r) {
        return r.median();
      }

      @Override public String toString() {
        return "\\textbf{\\emph{Median}}";
      }
    },
    SD {
      @Override public double retreive(final RealStatistics r) {
        return r.sd();
      }

      @Override public String toString() {
        return "$\\sigma$";
      }
    },
    TOTAL {
      @Override public double retreive(final RealStatistics r) {
        return r.sum();
      }

      @Override public String toString() {
        return "\\textbf{\\emph{Total}}";
      }
    },
    MAD {
      @Override public double retreive(final RealStatistics r) {
        return r.mad();
      }

      @Override public String toString() {
        return "\\textbf{\\emph{M.A.D}}";
      }
    };
    public String retreive(final RealStatistics r, final String format) {
      try {
        return String.format(format, Box.it(retreive(r)));
      } catch (final ArithmeticException e) {
        return ""; //
      }
    }

    public abstract double retreive(RealStatistics r);

    public static abstract class FormatSpecifier {
      public abstract Aggregation getKey();

      public abstract String format();
    }

    public static FormatSpecifier COUNT() {
      return COUNT.format("%d");
    }

    public static FormatSpecifier MIN() {
      return MIN.format("%g");
    }

    public static FormatSpecifier MAX() {
      return MAX.format("%g");
    }

    public static FormatSpecifier MEAN() {
      return MEAN.format("%g");
    }

    public static FormatSpecifier MEDIAN() {
      return MEDIAN.format("%g");
    }

    public static FormatSpecifier SD() {
      return SD.format("%g");
    }

    public static FormatSpecifier TOTAL() {
      return TOTAL.format("%g");
    }

    public static FormatSpecifier MAD() {
      return MAD.format("%g");
    }

    @SuppressWarnings("static-method")//
    public FormatSpecifier format(final String format) {
      return new FormatSpecifier() {
        @Override public Aggregation getKey() {
          return Aggregation.this;
        }

        @Override public String format() {
          return format;
        }
      };
    }
  }
}
