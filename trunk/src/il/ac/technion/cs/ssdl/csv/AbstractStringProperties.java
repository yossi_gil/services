/**
 *
 */
package il.ac.technion.cs.ssdl.csv;

import il.ac.technion.cs.ssdl.utils.Separate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yossi Gil
 * @since Apr 8, 2012
 *
 */
public abstract class AbstractStringProperties {
  public AbstractStringProperties() {
    this(Renderer.CSV);
  }
  public AbstractStringProperties(final Renderer renderer) {
    this.renderer = renderer;
  }
  public abstract int size();
  public abstract Iterable<String> keys();
  public abstract Iterable<String> values();
  public abstract AbstractStringProperties put(final String key, final String value);
  public abstract String get(final String key);

  /**
   * A total inspector
   *
   * @return the content of the CSV line as per all recorded values.
   */
  public final String line() {
    return makeLine(values());
  }

  final Renderer renderer;

  protected String makeLine(final Iterable<String> ss) {
    return renderer.makeLine(ss);
  }
  /**
   * A total inspector
   *
   * @return the header of the CSV line
   */
  public String header() {
    return renderer.allTop() + makeLine(keys()) + renderer.headerEnd();
  }
  @Override public AbstractStringProperties clone() {
    try {
      return (AbstractStringProperties) super.clone();
    } catch (final CloneNotSupportedException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static class ListProperties extends AbstractStringProperties {
    /*
     * (non-Javadoc)
     * @see il.ac.technion.cs.ssdl.csv.AbstractStringProperties#size()
     */
    @Override public int size() {
      return keys.size();
    }
    /*
     * (non-Javadoc)
     * @see il.ac.technion.cs.ssdl.csv.AbstractStringProperties#headers()
     */
    @Override public Iterable<String> keys() {
      return keys;
    }
    /*
     * (non-Javadoc)
     * @see il.ac.technion.cs.ssdl.csv.AbstractStringProperties#values()
     */
    @Override public Iterable<String> values() {
      return values;
    }
    /*
     * (non-Javadoc)
     * @see
     * il.ac.technion.cs.ssdl.csv.AbstractStringProperties#put(java.lang.String,
     * java.lang.String)
     */
    @Override public ListProperties put(final String key, final String value) {
      keys.add(key);
      values.add(value);
      return this;
    }
    /*
     * (non-Javadoc)
     * @see
     * il.ac.technion.cs.ssdl.csv.AbstractStringProperties#get(java.lang.String)
     */
    @Override public String get(final String key) {
      final int $ = keys.lastIndexOf(key);
      return $ < 0 ? null : values.get($);
    }

    private final List<String> keys = new ArrayList<String>();
    private final List<String> values = new ArrayList<String>();
  }

  public enum Renderer {
    CSV {
      /**
       * Wraps values in a CSV line. Occurrences of this character in field
       * content are escaped by typing it twice.
       */
      public static final String QUOTE = "" + '"';
      public static final String DELIMETER = ",";

      public @Override String makeField(final String s) {
        if (s == null)
          return "";
        if (!s.contains(QUOTE) && !s.contains(delimiter()))
          return s;
        return QUOTE + s.replaceAll(QUOTE, QUOTE + QUOTE) + QUOTE;
      }
      @Override String delimiter() {
        return DELIMETER;
      }
      @Override String lineBegin() {
        return "";
      }
      @Override String lineEnd() {
        return "";
      }
      @Override public String headerEnd() {
        return "";
      }
      @Override String allBottom() {
        return "";
      }
      @Override String allTop() {
        return "";
      }
    },
		MATRIX {
			public static final String DELIMETER = " ";
			public static final int WIDTH = 3;
			@Override
			String allTop() {
				return "";
			}

			@Override
			String headerEnd() {
				return "";
			}

			@Override
			String allBottom() {
				return "";
			}

			@Override
			String delimiter() {
				return DELIMETER;
			}

			@Override
			String lineBegin() {
				return "";
			}

			@Override
			String lineEnd() {
				return "";
			}

			@Override
			String makeField(final String s) {
        return String.format("%" + WIDTH + "s", s);
			}
		},
    LaTeX() {
      @Override String allTop() {
        return "\\toprule\n";
      }
      @Override String allBottom() {
        return "\\bottomrule\n";
      }
      @Override String headerEnd() {
        return "\n\\midrule";
      }
      @Override String delimiter() {
        return " &\t\t";
      }
      @Override String lineBegin() {
        return "";
      }
      @Override String lineEnd() {
        return "\\\\";
      }
      @Override String makeField(final String s) {
        if (s == null)
          return "";
        if (!s.contains(delimiter()))
          return s;
        return s.replaceAll(delimiter(), "\\" + delimiter());
      }
    };
    abstract String allTop();
    abstract String headerEnd();
    abstract String allBottom();
    abstract String delimiter();
    abstract String lineBegin();
    abstract String lineEnd();
    abstract String makeField(String s);
    public String makeLine(final Iterable<String> ss) {
      return lineBegin() + separate(ss) + lineEnd();
    }
    public String separate(final Iterable<String> ss) {
      return Separate.by(new Separate.F<String>() {
        @Override public final String _(final String o) {
          return makeField(o);
        }
      }, ss, delimiter());
    }

  }
}
