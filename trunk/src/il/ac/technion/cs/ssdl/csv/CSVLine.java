// <a href=http://ssdl-linux.cs.technion.ac.il/wiki/index.php>SSDLPedia</a>
package il.ac.technion.cs.ssdl.csv;

import static il.ac.technion.cs.ssdl.utils.Box.box;
import il.ac.technion.cs.ssdl.csv.Aggregator.Aggregation;
import il.ac.technion.cs.ssdl.csv.Aggregator.Aggregation.FormatSpecifier;
import il.ac.technion.cs.ssdl.external.External;
import il.ac.technion.cs.ssdl.stereotypes.Canopy;
import il.ac.technion.cs.ssdl.stereotypes.Instantiable;
import il.ac.technion.cs.ssdl.utils.Accumulator;
import il.ac.technion.cs.ssdl.utils.Separate;
import il.ac.technion.cs.ssdl.utils.____;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Create a line in an "Comma Separated Values" format from a sequence of named
 * values.
 *
 * @author Yossi Gil
 */
@Instantiable
public abstract class CSVLine extends AbstractStringProperties implements Cloneable {
  final Map<String, String> map;
  protected final Aggregator aggregator = new Aggregator();

  public Iterable<Aggregation> aggregations() {
    return aggregator.aggregations();
  }

  protected void addAggregates(final AbstractStringProperties to, final Aggregation a) {
    aggregator.addAggregates(map.keySet(), to, a);
  }

  public boolean aggregating() {
    return aggregator.size() != 0;
  }

  @Override public int size() {
    return map.size();
  }

  @Override public Collection<String> keys() {
    return map.keySet();
  }

  @Override public Collection<String> values() {
    return map.values();
  }

  public final Iterable<? extends Map.Entry<String, String>> entries() {
    return map.entrySet();
  }

  /**
   * Separator of multi-values, i.e., array elements stored in a single field
   */
  public static final String ARRAY_SEPARATOR = ";";

  public final String asKeyValuePairs() {
    return Separate.by(new Separate.F<Map.Entry<String, String>>() {
      @Override public String _(final Entry<String, String> e) {
        return e.getKey() + "=" + e.getValue();
      }
    }, entries(), ", ");
  }

  /**
   * Instantiate {@link CSVLine}.
   *
   * @param renderer
   * @param map
   *          which implementation should we use for storing values
   */
  CSVLine(final Map<String, String> map) {
    this.map = map;
  }

  /**
   * Instantiate {@link CSVLine}.
   *
   * @param renderer
   * @param map
   *          which implementation should we use for storing values
   */
  CSVLine(final Renderer renderer, final Map<String, String> map) {
    super(renderer);
    this.map = map;
  }

  /**
   * A mutator to add a key and a general {@link String} value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  @Override public final CSVLine put(final String key, final String value) {
    map.put(key, value);
    return this;
  }

  /**
   * A mutator to add a key and a general {@link String} value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public final CSVLine putAggregatorColumn(final String key, final String value) {
    aggregator.markColumn(key);
    return put(key, value);
  }

  /**
   * Add a key without a value to this instance.
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @return this
   */
  public final CSVLine put(final String key) {
    return put(key, "");
  }

  /**
   * Add a key and a general {@link Object} value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>; must
   *          not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public final CSVLine put(final String key, final Object value) {
    return value == null ? put(key) : put(key, value.toString());
  }

  /**
   * Add a key and a general {@link Object} value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>; must
   *          not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public final CSVLine put(final String key, final Integer value) {
    return value == null ? put(key) : put(key, value.toString());
  }

  /**
   * Add a key and a general <code><b>float</b><code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public final CSVLine put(final String key, final float value) {
    return put(key, "" + value);
  }

  /**
   * Add a key and a <code><b>double</b><code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public CSVLine put(final String key, final double value) {
    return put(key, value, new FormatSpecifier[0]);
  }

  /**
   * Add a key and a <code><b>double</b><code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @param as
   *          Which (if any) aggregate statistics should be produced for this
   *          column
   * @return this
   */
  public CSVLine put(final String key, final double value, final FormatSpecifier... as) {
    aggregator.record(key, value, as);
    return put(key, "" + value);
  }

  /**
   * Add a key and a <code><b>double</b><code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @param format
   *          How should the value be formatted
   * @param as
   *          Which (if any) aggregate statistics should be produced for this
   *          column
   * @return this
   */
  public CSVLine put(final String key, final double value, final String format, final FormatSpecifier... as) {
    aggregator.record(key, value, as);
    ____.sure(as.length == 0 || aggregating());
    return put(key, String.format(format, box(value)));
  }

  /**
   * Add a key and an <code><b>int</b></code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @param format
   *          How should this value be formatted?
   * @param as
   *          List of aggregations to collect on this column and their
   *          respective formatting
   * @return this
   */
  public CSVLine put(final String key, final int value, final String format, final FormatSpecifier... as) {
    aggregator.record(key, value, as);
    ____.sure(as.length == 0 || aggregating());
    return put(key, String.format(format, box(value)));
  }

  /**
   * Add a key and an <code><b>int</b></code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public CSVLine put(final String key, final int value) {
    return put(key, "" + value);
  }

  public CSVLine put(final Enum<?> key, final int value) {
    return put(key.toString(), "" + value);
  }

  public CSVLine put(final Enum<?> key, final String value) {
    return put(key.toString(), value);
  }

  public CSVLine put(final Accumulator c) {
    return put(c.name(), c.value());
  }

  public CSVLine put(final Accumulator... cs) {
    for (final Accumulator c : cs)
      put(c);
    return this;
  }

  /**
   * Add a key and a <code><b>long</b></code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public CSVLine put(final String key, final long value) {
    return put(key, "" + value);
  }

  /**
   * Add a key and a <code><b>short</b></code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public CSVLine put(final String key, final short value) {
    return put(key, "" + value);
  }

  /**
   * Add a key and a <code><b>char</b></code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public final CSVLine put(final String key, final char value) {
    return put(key, "" + value);
  }

  /**
   * Add a key and a <code><b>boolean</b></code> value to this instance
   *
   * @param key
   *          The key to be added; must not be <code><b>null</b></code>
   * @param value
   *          The value associated with the key
   * @return this
   */
  public final CSVLine put(final String key, final boolean value) {
    return put(key, "" + value);
  }

  public final CSVLine put(final String key, final Object a[], final int i) {
    return put(key, a == null || i < 0 || i >= a.length ? null : a[i]);
  }

  public final CSVLine put(final String key, final Object[] os) {
    return put(key, os == null ? null : Separate.by(os, ARRAY_SEPARATOR));
  }

  /**
   * Adds all {@link External} properties in a given object.
   *
   * @param t
   *          an arbitrary object, usually with some of its fields and methods
   *          marked {@link External}
   * @return the parameter
   */
  public <T> T extract(final T t) {
    for (final Entry<String, String> e : External.Introspector.toOrderedMap(t).entrySet())
      put(e.getKey(), e.getValue());
    return t;
  }



  @Canopy
  public static final class Sorterd extends CSVLine {
    public Sorterd() {
      super(new TreeMap<String, String>());
    }
  }

  @Canopy
  public static class Ordered extends CSVLine {
    public class Separated extends CSVLine.Ordered {
      final String separator;

      public Separated(final String separator) {
        this.separator = separator;
      }
    }

    public Ordered() {
      super(new LinkedHashMap<String, String>());
    }

    public Ordered(final Renderer renderer) {
      super(renderer, new LinkedHashMap<String, String>());
    }
  }

  @Override public String get(final String key) {
    return map.get(key);
  }
}
