package il.ac.technion.cs.ssdl.csv;

import java.io.File;

/**
 * Combines {@link CSVWriter} and {@link CSVLine}, to make it easy to write to a
 * CSV file.
 *
 * @author Yossi Gil
 * @since Dec 25, 2009
 */
public class CSVLineWriter extends CSVLine.Ordered {
  @SuppressWarnings("static-method") protected String extension() {
    return ".csv";
  }
  /**
   * Instantiate this class, setting CSV file to the standard output stream.
   */
  public CSVLineWriter() {
    this(new CSVWriter());
  }
  public CSVLineWriter(final CSVWriter writer) {
    this.writer = writer;
  }
  public CSVLineWriter(final Renderer renderer) {
    this(new CSVWriter(), renderer);
  }
  public CSVLineWriter(final String fileName) {
    writer = new CSVWriter(fileName + extension());
  }
  public CSVLineWriter(final String fileName, final Renderer renderer) {
    super(renderer);
    writer = new CSVWriter(fileName + extension());
  }
  public CSVLineWriter(final CSVWriter writer, final Renderer renderer) {
    super(renderer);
    this.writer = writer;
  }
  public String fileName() {
    return writer.fileName();
  }
  public String absolutePath() {
    return writer.file() == null ? null : writer.file().getAbsolutePath();
  }
  public File file() {
    return writer.file();
  }
  public int count() {
    return count;
  }
  public String close() {
    writer.writeln(renderer.allBottom());
    return writer.close();
  }
  public void nl() {
    writer.writeFlush(this);
    count++;
  }

  protected final CSVWriter writer;
  private int count = 0;
}
