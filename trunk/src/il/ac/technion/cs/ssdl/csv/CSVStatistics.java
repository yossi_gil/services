package il.ac.technion.cs.ssdl.csv;

import static il.ac.technion.cs.ssdl.utils.DBC.nonnull;
import il.ac.technion.cs.ssdl.csv.Aggregator.Aggregation.FormatSpecifier;
import il.ac.technion.cs.ssdl.statistics.ImmutableStatistics;
import il.ac.technion.cs.ssdl.statistics.RealStatistics;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Similar to {@link CSVWriter}, except that in addition to the production of
 * output to the main CSV file, this class generates a secondary CSV file,
 * recording the essential statistics (min, max, count, etc.) of each numerical
 * column in the main CSV file.
 *
 * @author Yossi Gil
 * @since Dec 25, 2009
 */
public class CSVStatistics extends CSVLine.Ordered {
  private final String keysHeader;

  /**
   * Instantiate this class, setting the names of the main and secondary CSV
   * files.
   *
   * @param baseName
   *          the name of the files into which statistics should be written; if
   *          this name ends with ".csv", this extension is removed.
   * @param keysHeader
   *          the name of the column in which the names of the numerical columns
   *          in the principal file
   * @throws IOException
   */
  public CSVStatistics(final String baseName, final String keysHeader) throws IOException {
    nonnull(baseName);
    nonnull(keysHeader);
    inner = new CSVWriter(removeExtension(baseName));
    summarizer = new CSVWriter(removeExtension(baseName) + SUMMARY_EXTENSION);
    this.keysHeader = keysHeader;
  }
  /**
   * @param baseName
   * @return
   */
  private static String removeExtension(final String baseName) {
    return baseName.replaceFirst("\\.csv$", "");
  }
  public String mainFileName() {
    return inner.fileName();
  }
  public String summaryFileName() {
    return summarizer.fileName();
  }
  public String close() {
    inner.close();
    for (final String key : stats.keySet()) {
      final CSVLine l = new CSVLine.Ordered.Separated("%");
      l.put(keysHeader, key);
      final ImmutableStatistics s = stats.get(key);
      l//
      .put("$N$", s.n()) //
          .put("\\emph{n/a}", s.missing())//
          .put("Mean", s.n() <= 0 ? Double.NaN : s.mean()) //
          .put("Median", s.n() <= 0 ? Double.NaN : s.median())//
          .put("$\\sigma$", s.n() <= 0 ? Double.NaN : s.sd()) //
          .put("m.a.d", s.n() <= 0 ? Double.NaN : s.mad()) //
          .put("$\\min$", s.n() <= 0 ? Double.NaN : s.min()) //
          .put("$\\max$", s.n() <= 0 ? Double.NaN : s.max()) //
          .put("Range", s.n() <= 0 ? Double.NaN : s.max() - s.min())//
          .put("Total", s.n() <= 0 ? Double.NaN : s.sum())//
      ;
      summarizer.writeFlush(l);
    }
    return summarizer.close();
  }
  @Override public CSVLine put(final String key, final int value) {
    getStatistics(key).record(value);
    return super.put(key, value);
  }
  @Override public CSVLine put(final String key, final long value) {
    getStatistics(key).record(value);
    return super.put(key, value);
  }
  @Override public CSVLine put(final String key, final double value, final FormatSpecifier... as) {
    getStatistics(key).record(value);
    return super.put(key, value, as);
  }
  public void nl() {
    inner.writeFlush(this);
  }

  private static final String SUMMARY_EXTENSION = ".summary";
  final Map<String, RealStatistics> stats = new LinkedHashMap<String, RealStatistics>();
  final CSVWriter inner;
  final CSVWriter summarizer;

  RealStatistics getStatistics(final String key) {
    if (stats.get(key) == null)
      stats.put(key, new RealStatistics());
    return stats.get(key);
  }

  public class Line extends CSVLine.Ordered {
    public void close() {
      inner.writeFlush(this);
    }
    @Override public CSVLine put(final String key, final double value, final FormatSpecifier... as) {
      getStatistics(key).record(value);
      return super.put(key, value, as);
    }
    @Override public CSVLine put(final String key, final long value) {
      getStatistics(key).record(value);
      return super.put(key, value);
    }
  }
}
