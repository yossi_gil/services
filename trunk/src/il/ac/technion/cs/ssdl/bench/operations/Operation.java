package il.ac.technion.cs.ssdl.bench.operations;

import il.ac.technion.cs.ssdl.bench.StopWatch;

import java.util.concurrent.Callable;

/**
 * A typed procedure with no arguments, which is also suitable for time
 * measurement, i.e., its execution time is sufficiently greater than the time
 * granularity of loop execution, function calls, and the other instructions
 * required for timing.
 */
public abstract class Operation implements Callable<Object> {
  /**
   * The body of this operation; to be filled in by sub-classes.
   *
   * @return whatever
   */
  @Override public abstract Object call();
  public StopWatch netTime(final StopWatch netTime, final int runs) {
    netTime.start();
    for (int i = 0; i < runs; i++)
      call();
    return netTime.stop();
  }
  public final StopWatch netTime(final int runs) {
    return netTime(makeStopWatch(), runs);
  }
  public final StopWatch netTime() {
    return netTime(makeStopWatch());
  }
  public StopWatch netTime(final StopWatch netTime) {
    netTime.start();
    call();
    return netTime.stop();
  }
  @SuppressWarnings("static-method")//
  public StopWatch makeStopWatch() {
    return new StopWatch();
  }
}
