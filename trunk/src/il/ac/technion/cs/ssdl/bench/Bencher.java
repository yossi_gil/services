package il.ac.technion.cs.ssdl.bench;

import il.ac.technion.cs.ssdl.bench.operations.Bencheon;
import il.ac.technion.cs.ssdl.bench.operations.NamedOperation;
import il.ac.technion.cs.ssdl.bench.operations.Operation;

public class Bencher extends LogBook.Mutable {
  public Bencher(final Object initiator) {
    super(initiator);
  }
  
  private transient Operation after;
  
  public void go(final String name, final long n, final Operation o) {
    BenchingPolicy.go(this, name, n, o);
    BenchingPolicy.after(after);
  }
  public void go(final long size, final NamedOperation o) {
    BenchingPolicy.go(this, size, o);
    BenchingPolicy.after(after);
  }
  public void go(final Bencheon b) {
    BenchingPolicy.go(this, b);
    BenchingPolicy.after(after);
  }
  public void afterEachGo(final Operation o) {
    after = o;
  }
  @Override public LogBook clear() {
    super.clear();
    current.clear();
    dotter.clear();
    return this;
  }
  
  private static final long serialVersionUID = 1L;
}