/**
 * 
 */
package il.ac.technion.cs.ssdl.bench;

import il.ac.technion.cs.ssdl.utils.DBC;

/**
 * @author Yossi Gil
 * @since 01/05/2011
 */
public final class StopWatch {
  private long time = 0;
  private int runs = 0;
  private long begin;
  private final String name;
  private boolean started = false;
  
  /**
   * Instantiate {@link StopWatch}.
   * 
   * @param name
   */
  public StopWatch(final String name) {
    this.name = name;
  }
  public StopWatch() {
    this(null);
  }
  public long time() {
    return time;
  }
  public String name() {
    return name;
  }
  public int runs() {
    return runs;
  }
  /**
   * @return <code><b>this</b></code>
   */
  public StopWatch stop() {
    DBC.require(started);
    time += System.nanoTime() - begin;
    runs++;
    started = false;
    return this;
  }
  /**
   * @return <code><b>this</b></code>
   */
  public StopWatch start() {
    DBC.require(!started);
    begin = System.nanoTime();
    started = true;
    return this;
  }
  /**
   * @return <code><b>this</b></code>
   */
  public StopWatch reset() {
    time = runs = 0;
    return this;
  }
  /**
   * @param time
   * @return <code><b>this</b></code>
   */
  public StopWatch setTime(final long time) {
    this.time = time;
    return this;
  }
  /**
   * @param runs
   * @return <code><b>this</b></code>
   */
  public StopWatch setRuns(final int runs) {
    this.runs = runs;
    return this;
  }
  @Override public String toString() {
    return Unit.format(this);
  }
}