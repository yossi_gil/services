/**
 * 
 */
package il.ac.technion.cs.ssdl.bench.operations;

import il.ac.technion.cs.ssdl.bench.StopWatch;

/**
 * @author Yossi Gil
 * @since 30/05/2011
 */
public abstract class NamedOperation extends Operation {
  @Override public final StopWatch makeStopWatch() {
    return new StopWatch(name);
  }
  /**
   * Instantiate {@link NamedOperation}.
   * 
   * @param name
   *          name of this object
   */
  public NamedOperation(final String name) {
    this.name = name;
  }
  
  public final String name;
}