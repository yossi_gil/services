package il.ac.technion.cs.ssdl.bench.trials;

import il.ac.technion.cs.ssdl.bench.BenchingPolicy;
import il.ac.technion.cs.ssdl.bench.Log;
import il.ac.technion.cs.ssdl.bench.LogBook;
import il.ac.technion.cs.ssdl.bench.operations.Operation;

/**
 * @author Yossi Gil
 * @since 30/05/2011
 */
public class BenchEmptyOperation {
  private static int trials = 100;
  
  public static void main(final String args[]) throws Exception {
    final LogBook.Mutable l = new LogBook.Mutable(BenchEmptyOperation.class);
    final Operation o = new Operation() {
      @Override public Object call() {
        return null;
      }
    };
    Log.deactivate();
    for (int i = 0; i < trials; i++)
      BenchingPolicy.go(l, "empty", 1, o);
    System.err.println(l.currentEntry().format("A D I X"));
  }
}
