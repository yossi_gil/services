package il.ac.technion.cs.ssdl.bench.trials;

import il.ac.technion.cs.ssdl.bench.BenchingPolicy;
import il.ac.technion.cs.ssdl.bench.Log;
import il.ac.technion.cs.ssdl.bench.LogBook;
import il.ac.technion.cs.ssdl.bench.operations.Bencheon;

/**
 * @author Yossi Gil
 * @since 30/05/2011
 */
public class BenchEmptyBencheon {
  private static int trials = 100;
  
  public static void main(final String args[]) throws Exception {
    final LogBook.Mutable l = new LogBook.Mutable(BenchEmptyBencheon.class);
    final Bencheon b = new Bencheon("empty", 1) {
      @Override public Object call() {
        return null;
      }
    };
    Log.deactivate();
    for (int i = 0; i < trials; i++)
      BenchingPolicy.go(l, b);
    System.err.println(l.currentEntry().format("A D I X"));
  }
}
