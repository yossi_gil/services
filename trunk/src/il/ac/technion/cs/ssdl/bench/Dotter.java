/**
 *
 */
package il.ac.technion.cs.ssdl.bench;

import static il.ac.technion.cs.ssdl.utils.Box.box;

/**
 * @author Yossi Gil
 * @since 01/05/2011
 */
public class Dotter implements java.io.Serializable {
  private final long initTime;
  
  public Dotter() {
    initTime = System.nanoTime();
    clear();
  }
  public int n() {
    return n;
  }
  public int line() {
    return line;
  }
  public void clear() {
    if (cleared)
      return;
    cleared = true;
    n = line = 0;
    lineStart();
  }
  public void click() {
    cleared = false;
    System.err.print(++n % 10 == 0 ? '*' : '.');
    if (n % DOTS_IN_LINE == 0) {
      nl();
      lineStart();
    }
  }
  public void end() {
    nl();
  }
  
  public static final int DOTS_IN_LINE = 60;
  private int n = 0;
  private int line = 0;
  private boolean cleared = false;
  private long lineStartTime = -1;
  
  private void nl() {
    final long now = System.nanoTime();
    System.err.println(" " + Unit.formatNanoseconds(now - lineStartTime) + " Total: " + Unit.formatNanoseconds(now - initTime));
  }
  private void lineStart() {
    lineStartTime = System.nanoTime();
    System.err.printf("%3d", box(++line));
  }
  
  private static final long serialVersionUID = 1L;
}
