/**
 *
 */
package il.ac.technion.cs.ssdl.bench;

import il.ac.technion.cs.ssdl.strings.StringUtils;
import il.ac.technion.cs.ssdl.utils.Separate;
import il.ac.technion.cs.ssdl.utils.Tab;
import il.ac.technion.cs.ssdl.utils.____;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Stack;

/**
 * @author Yossi Gil
 * @since 04/06/2011
 */
public enum Log {
  ;
  private static int level = 0;
  private static boolean active = true;
  private static PrintStream out = System.out;
  private static Tab tabber = new Tab("  ");

  public static void setLogStream(final PrintStream log) {
    out = log;
  }
  public static void increaseLevel() {
    tabber.more();
    ++level;
  }
  public static void decreaseLevel() {
    ____.require(level > 0);
    --level;
    tabber.less();
    ____.sure(level >= 0);
  }

  private static int maxLevel = 100;

  public static int getMaxLevel() {
    return maxLevel;
  }
  public static void setMaxLevel(final int maxLevel) {
    Log.maxLevel = maxLevel;
  }
  public static void print(final Object o) {
    if (!logging())
      return;
    out.print(o);
    flush();
  }
  public static void ln(final Object... os) {
    print(prefix() + Separate.by(os, " ") + "\n");
  }
  public static void f(final String format, final Object... os) {
    ln(String.format(format, os));
  }
  public static boolean logging() {
    return active && level <= maxLevel;
  }
  public static void flush() {
    out.flush();
  }
  public static void deactivate() {
    active = false;
  }
  public static void activate() {
    active = true;
  }
  public static String now() {
    return new SimpleDateFormat("HH:mm:ss ").format(Calendar.getInstance().getTime());
  }

  private static String prefix() {
    return now() + tabber.toString();
  }

  private final static Stack<StopWatch> stack = new Stack<StopWatch>();

  public static void beginStage(final Object... os) {
    beginStage(Separate.by(os, " "));
  }
  public static void beginStage(final String stage) {
    Log.ln("Begin:", stage);
    stack.push(new StopWatch(stage).start());
  }
  public static void endStage(final Object... ss) {
    final StopWatch s = stack.pop().stop();
    Log.ln("End:", s.name(), StringUtils.paren(s) + ";", Separate.by(ss, " "));
  }
  public static void beginCompoundStage(final Object... os) {
    beginStage(os);
    increaseLevel();
  }
  public static void endCompoundStage(final Object... ss) {
    decreaseLevel();
    endStage(ss);
  }
}
