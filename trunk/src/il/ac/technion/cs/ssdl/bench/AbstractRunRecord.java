/**
 * 
 */
package il.ac.technion.cs.ssdl.bench;

import il.ac.technion.cs.ssdl.utils.Box;

public class AbstractRunRecord {
  public int runs() {
    return runs;
  }
  public long grossTime() {
    return grossTime;
  }
  public long netTime() {
    return netTime;
  }
  
  protected int runs;
  protected long grossTime;
  protected long netTime;
  
  @Override public String toString() {
    return String.format("runs=%d, netTime=%s, grossTime=%s, efficacy=%s", //
        Box.it(runs), //
        Unit.formatNanoseconds(netTime), //
        Unit.formatNanoseconds(grossTime), //
        Unit.formatRelative(netTime, grossTime));
  }
}
