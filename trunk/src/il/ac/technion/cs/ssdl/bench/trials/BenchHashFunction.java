package il.ac.technion.cs.ssdl.bench.trials;

import il.ac.technion.cs.ssdl.bench.BenchingPolicy;
import il.ac.technion.cs.ssdl.bench.Log;
import il.ac.technion.cs.ssdl.bench.LogBook;
import il.ac.technion.cs.ssdl.bench.operations.Operation;

/**
 * @author Yossi Gil
 * @since 30/05/2011
 */
public class BenchHashFunction {
  private static int trials = 100;
  public static int size = 9;
  
  public static void main(final String args[]) throws Exception {
    final LogBook.Mutable l = new LogBook.Mutable(BenchHashFunction.class);
    final Hash h = new Hash();
    Log.deactivate();
    for (int i = 0; i < trials; i++)
      BenchingPolicy.go(l, "hash", 1, h);
    System.err.println(l.currentEntry().format("A D I X") + h.a);
  }
  
  public static class Hash extends Operation {
    int a = 0;
    
    private static int hash(final int h) {
      int $ = h;
      $ ^= $ >>> 20 ^ $ >>> 12;
      return $ ^ $ >>> 7 ^ $ >>> 4;
    }
    @Override public Void call() {
      a = hash(++a);
      return null;
    }
  }
}
