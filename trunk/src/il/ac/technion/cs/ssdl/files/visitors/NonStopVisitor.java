// <a href=http://ssdl-linux.cs.technion.ac.il/wiki/index.php>SSDLPedia</a>
package il.ac.technion.cs.ssdl.files.visitors;

import static il.ac.technion.cs.ssdl.utils.DBC.unreachable;
import il.ac.technion.cs.ssdl.files.visitors.FileSystemVisitor.Action.StopTraversal;

import java.io.File;
import java.io.IOException;

/**
 * A class realizing the {@link FileSystemVisitor} functionality, except that it
 * does not allow throws of {@link StopTraversal} exceptions.
 * 
 * @author Yossi Gil
 * @since 13/07/2007
 */
public class NonStopVisitor extends FileSystemVisitor {
  public NonStopVisitor(final String from, final NonStopAction action, final String[] extensions) {
    super(from, action, extensions);
  }
  public NonStopVisitor(final Iterable<String> from, final NonStopAction action, final String... extensions) {
    super(from, action, extensions);
  }
  public NonStopVisitor(final File from, final NonStopAction action, final String... extensions) {
    super(from, action, extensions);
  }
  public NonStopVisitor(final String[] from, final NonStopAction action, final String... extensions) {
    super(from, action, extensions);
  }
  public NonStopVisitor(final File[] from, final NonStopAction action, final String... extensions) {
    super(from, action, extensions);
  }
  @Override public final void go() throws IOException {
    try {
      super.go();
    } catch (final StopTraversal e) {
      unreachable();
    }
  }
}
