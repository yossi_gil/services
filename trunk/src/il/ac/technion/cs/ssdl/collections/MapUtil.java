package il.ac.technion.cs.ssdl.collections;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MapUtil {
  public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(final Map<K, V> map) {
    final List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
    Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
      @Override public int compare(final Map.Entry<K, V> o1, final Map.Entry<K, V> o2) {
        return o1.getValue().compareTo(o2.getValue());
      }
    });
    final Map<K, V> $ = new LinkedHashMap<K, V>();
    for (final Map.Entry<K, V> entry : list)
      $.put(entry.getKey(), entry.getValue());
    return $;
  }
  public static <K, V extends Comparable<? super V>> Map<K, V> sortByValueReverse(final Map<K, V> map) {
    final List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
    Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
      @Override public int compare(final Map.Entry<K, V> o1, final Map.Entry<K, V> o2) {
        return o2.getValue().compareTo(o1.getValue());
      }
    });
    final Map<K, V> $ = new LinkedHashMap<K, V>();
    for (final Map.Entry<K, V> entry : list)
      $.put(entry.getKey(), entry.getValue());
    return $;
  }
  @SuppressWarnings("boxing")//
  public static <K> void addToValue(final Map<K, Integer> map, final K key, final int val) {
    Integer i = map.get(key);
    if (i == null)
      i = new Integer(0);
    i += val;
    map.put(key, i);
  }
  
  
  public static <K, V> Iterator<K> keysIterator(final Map<K, V> map) {
    return new Iterator<K>() {
      Iterator<Map.Entry<K, V>> inner = map.entrySet().iterator();

      @Override public boolean hasNext() {
        return inner.hasNext();
      }
      @Override public K next() {
        return inner.next().getKey();
      }
      @Override public void remove() {
        inner.remove();
      }
    };
  }
}
