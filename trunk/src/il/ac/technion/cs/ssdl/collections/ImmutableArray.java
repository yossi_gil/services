// <a href=http://ssdl-linux.cs.technion.ac.il/wiki/index.php>SSDLPedia</a>
package il.ac.technion.cs.ssdl.collections;

import il.ac.technion.cs.ssdl.stereotypes.Canopy;
import il.ac.technion.cs.ssdl.stereotypes.Immutable;
import il.ac.technion.cs.ssdl.stereotypes.Instantiable;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An immutable array of any reference type.
 * 
 * @param <T>
 *          Type of elements stored in the array.
 * @author Yossi Gil, the Technion.
 * @since 23/08/2008
 */
@Immutable @Canopy @Instantiable public class ImmutableArray<T> implements Iterable<T> {
  /**
   * @param ts
   *          an array that this class will protect from modifications.
   */
  public ImmutableArray(final T[] ts) {
    implementation = ts;
  }
  /**
   * Retrieve a value from the encapsulated array.
   * 
   * @param i
   *          from which array index should the value be retrieved?
   * @return the value at the location specified by the parameter.
   */
  public T get(final int i) {
    return implementation[i];
  }
  /**
   * Determine how many elements are there in the encapsulated array.
   * 
   * @return the number of elements in this array.
   */
  public int length() {
    return implementation.length;
  }
  @Override public Iterator<T> iterator() {
    return new Iterator<T>() {
      private int i = 0;
      
      @Override public boolean hasNext() {
        return i < length();
      }
      @Override public T next() {
        if (i >= length())
          throw new NoSuchElementException();
        return get(i++);
      }
      @Override public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  
  /**
   * The encapsulated array.
   */
  private final T[] implementation;
  
  /**
   * An immutable array of <code>boolean</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Booleans {
    /**
     * @param bs
     *          an array that this class will protect from modifications.
     */
    public Booleans(final boolean[] bs) {
      implementation = bs;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public boolean get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final boolean[] implementation;
  }
  
  /**
   * An immutable array of <code>byte</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Bytes {
    /**
     * @param bs
     *          an array that this class will protect from modifications.
     */
    public Bytes(final byte[] bs) {
      implementation = bs;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public byte get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final byte[] implementation;
  }
  
  /**
   * An immutable array of <code>char</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Chars {
    /**
     * @param cs
     *          an array that this class will protect from modifications.
     */
    public Chars(final char[] cs) {
      implementation = cs;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public char get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final char[] implementation;
  }
  
  /**
   * An immutable array of <code>double</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Doubles {
    /**
     * @param ds
     *          an array that this class will protect from modifications.
     */
    public Doubles(final double[] ds) {
      implementation = ds;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public double get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final double[] implementation;
  }
  
  /**
   * An immutable array of <code>float</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Floats {
    /**
     * @param fs
     *          an array that this class will protect from modifications.
     */
    public Floats(final float[] fs) {
      implementation = fs;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public float get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final float[] implementation;
  }
  
  /**
   * An immutable array of <code>int</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Ints {
    /**
     * @param is
     *          an array that this class will protect from modifications.
     */
    public Ints(final int[] is) {
      implementation = is;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public int get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final int[] implementation;
  }
  
  /**
   * An immutable array of <code>long</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Longs {
    /**
     * @param ls
     *          an array that this class will protect from modifications.
     */
    public Longs(final long[] ls) {
      implementation = ls;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public long get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final long[] implementation;
  }
  
  /**
   * An immutable array of <code>short</code>s.
   * 
   * @author Yossi Gil, the Technion.
   * @since 23/08/2008
   */
  @Immutable @Canopy public static class Shorts {
    /**
     * @param ss
     *          an array that this class will protect from modifications.
     */
    public Shorts(final short[] ss) {
      implementation = ss;
    }
    /**
     * Retrieve a value from the encapsulated array.
     * 
     * @param i
     *          from which array index should the value be retrieved?
     * @return the value at the location specified by the parameter.
     */
    public short get(final int i) {
      return implementation[i];
    }
    /**
     * Determine how many elements are there in the encapsulated array.
     * 
     * @return the number of elements in this array.
     */
    public int length() {
      return implementation.length;
    }
    
    /**
     * The encapsulated array.
     */
    private final short[] implementation;
  }
}
