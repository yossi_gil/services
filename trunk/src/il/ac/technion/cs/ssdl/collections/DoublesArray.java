/**
 *
 */
package il.ac.technion.cs.ssdl.collections;

import java.util.Arrays;

/**
 * A <href a=http://en.wikipedia.org/wiki/Dynamic_array>growable array</a> of
 * <code><b>double</b></code>s.
 *
 * @author Yossi Gil
 * @since February 20, 2012
 */
public class DoublesArray {
  /**
   * What is this array's length?
   *
   * @return current array length
   */
  public int length() {
    return length;
  }
  /**
   * A representation as a Java array.
   *
   * @return the underlying array; likely to be invalid if the array grows
   */
  public double[] toArray() {
    return Arrays.copyOf(inner, length);
  }
  /**
   * Retrieve the content of a specified array cell. If the contents of the
   * specified cell was not previously set, the returned value is zero, and the
   * array grow.
   *
   * @param i
   *          a non-negative array index
   * @return the contents at the specified location
   */
  public double get(final int i) {
    return access(i).inner[i];
  }
  /**
   * Set the contents of a specified array location
   *
   * @param i
   *          a non-negative array index
   * @param value
   *          the new contents of the specified array cell
   * @return the new contents of the specified array cell
   */
  public double set(final int i, final double value) {
    return access(i).inner[i] = value;
  }
  /**
   * @param i
   *          a non-negative array index
   * @return the new contents of the specified array cell
   */
  public double inc(final int i) {
    return ++access(i).inner[i];
  }
  /**
   * @param i
   *          a non-negative array index
   * @return the new contents of the specified array cell
   */
  public double dec(final int i) {
    return --access(i).inner[i];
  }
  /**
   * @param i
   *          a non-negative array index
   * @param value
   * @return the new contents of the specified array cell
   */
  public double addTo(final int i, final double value) {
    return access(i).inner[i] += value;
  }
  /**
   * Adds a value at the end, extending this array
   *
   * @param value
   *          an arbitrary value to be added
   */
  public void push(final double value) {
    set(length, value);
  }
  /**
   * Removes the last cell
   *
   * @return the removed value
   */
  public double pop() {
    return inner[--length];
  }
  /**
   * Makes sure the specified array index exists, extending {@link #length()} if
   * necessary
   *
   * @param i
   *          a non-negative array index
   * @return <code><b>this</b></code>
   */
  public DoublesArray access(final int i) {
    if (i >= length) {
      final int n = inner.length;
      inner = i < n ? inner : Arrays.copyOf(inner, 1 + Math.max(i, n + (n >> 1)));
      length = i + 1;
    }
    return this;
  }

  private int length = 0;
  private double[] inner = new double[0];
}
