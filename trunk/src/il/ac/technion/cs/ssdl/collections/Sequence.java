package il.ac.technion.cs.ssdl.collections;

import il.ac.technion.cs.ssdl.iteration.ArrayIterator;
import il.ac.technion.cs.ssdl.utils.Stringify;

import java.io.Serializable;
import java.util.Iterator;

/**
 * An immutable collection of elements. Once construction is completed no new
 * elements can be added to the collections, nor can elements be removed from
 * the collection. The {@link #indexOf(Object)} and {@link #has(Object)} methods
 * assume that the collection does not contain null elements
 * 
 * @param <E>
 *          Type of elements in the sequence
 */
public class Sequence<E> implements Iterable<E>, Serializable {
  private static final long serialVersionUID = 8259437798546027307L;
  private final E[] es;
  private final int begin;
  private final int end;
  
  public Sequence() {
    this(null, 0, 0);
  }
  public static <T> Sequence<T> make(final T[] ts, final int begin, final int end) {
    return new Sequence<T>(ts, begin, end);
  }
  public static <T> Sequence<T> make(final Sequence<T> ts, final int begin) {
    return make(ts.es, begin, ts.end);
  }
  public Sequence(final E... es) {
    this(es, 0, es.length);
  }
  protected Sequence(final E[] es, final int begin, final int end) {
    this.begin = begin;
    this.end = end;
    this.es = es;
  }
  @Override public Iterator<E> iterator() {
    return ArrayIterator.make(es, begin, end);
  }
  public int size() {
    return end - begin;
  }
  public E get(final int index) {
    return es[index + begin];
  }
  public boolean has(final E e) {
    return indexOf(e) >= 0;
  }
  public int indexOf(final E e) {
    for (int i = begin; i < end; ++i)
      if (es[i].equals(e))
        return i - begin;
    return -1;
  }
  @Override public String toString() {
    return Stringify.it(this);
  }
  public static <T> Sequence<T> upcast(@SuppressWarnings("unused") final Class<T> _, final Sequence<? extends T> src) {
    return make((T[]) src.es, src.begin, src.end);
  }
}
