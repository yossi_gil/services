/**
 * 
 */
package il.ac.technion.cs.ssdl.utils;

public class Triple<First, Second, Third> extends Pair<First, Second> {
  public final Third third;
  
  public Triple(final First first, final Second second, final Third third) {
    super(first, second);
    this.third = third;
  }
  public static <First, Second, Third> Triple<First, Second, Third> make(final First first, final Second second, final Third third) {
    return new Triple<First, Second, Third>(first, second, third);
  }
  public static <First, Second, Third> //
  Triple<First, Second, Third>[] //
  makeTriples(final int n, final int m, final int k) {
    return Triple.makeTriples(n * m * k);
  }
  @SuppressWarnings("unchecked")//
  public static <First, Second, Third> //
  Triple<First, Second, Third>[] //
  makeTriples(final int n) {
    return new Triple[n];
  }
}
