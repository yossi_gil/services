/**
 *
 */
package il.ac.technion.cs.ssdl.utils;

/**
 * @author Yossi Gil
 * @since Mar 6, 2012
 * @param <T>
 *          an arbitrary type
 */
public class Minimizer<T> {
  private double min = Double.NaN;
  private T value = null;
  private int index = 0;
  private int maxIndex = -1;

  public double min() {
    return min;
  }
  public T value() {
    ____.nonnull(value);
    return value;
  }
  public Minimizer<T> next(final T t, final double next) {
    ____.nonnull(t);
    if (Double.isNaN(min) || next < min) {
      min = next;
      value = t;
      maxIndex = index;
    }
    index++;
    return this;
  }
  public int index() {
    return maxIndex;
  }
}
