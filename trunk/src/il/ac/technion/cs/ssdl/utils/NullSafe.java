/**
 *
 */
package il.ac.technion.cs.ssdl.utils;

/**
 * @author Yossi Gil
 * @since Apr 20, 2012
 */
public class NullSafe {
  /**
   * Return the hashCode of
   *
   * @param <T>
   *          type of parameter to receive a default value
   * @param t
   *          a possibly <code><b>null</b></code> value
   * @return <code>t.hashCode()</code> if <code>t</code> is not
   *         <code><b>null</b></code>, otherwise <code>0</code>
   */
  public static <T> int hashCode(final T t) {
    return t == null ? 0 : t.hashCode();
  }
  public static <T> boolean equals(final T t1, final T t2) {
    return t1 == t2 || t1 != null && t1.equals(t2);
  }
}
