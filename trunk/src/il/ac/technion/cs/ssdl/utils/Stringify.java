// <a href=http://ssdl-linux.cs.technion.ac.il/wiki/index.php>SSDLPedia</a>
package il.ac.technion.cs.ssdl.utils;

import static il.ac.technion.cs.ssdl.utils.DBC.nonnull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import il.ac.technion.cs.ssdl.iteration.Prune;
import il.ac.technion.cs.ssdl.stereotypes.Utility;
import il.ac.technion.cs.ssdl.utils.Separate.F;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

/**
 * A <b>Utility Class</b> providing services of converting a collection or an
 * array into a {@link String}, with means for omitting <code><b>null</b></code>
 * elements, replacing these with a designated filler, adding a separator string
 * between elements, and a beginning and ending texts which are printed only if
 * the collection/array is not empty. For example, the following function
 * 
 * <pre>
 *  * void g(String... ss) {
 * 	System.out.println(//
 * 	        Stringify.it(ss, begin(&quot;&lt;&lt;&quot;).end(&quot;&gt;&gt;&quot;).separator(&quot;|&quot;).omitNulls()) //
 * 	        );
 * }
 * </pre>
 * 
 * prints a list of its non-<code><b>null</b></code> arguments, wrapped with
 * double angular brackets and separated by a vertical bar. Nothing is printed
 * though if no non-<code><b>null</b></code> are passed to the program.
 * <p>
 * Observe that the formatting style in the above example is specified with an
 * <b>Options Object</b> of type {@link Stringify.Option}, created by the
 * expression
 * 
 * <pre>
 * begin(&quot;&lt;&lt;&quot;).end(&quot;&gt;&gt;&quot;).separator(&quot;|&quot;).omitNulls()
 * </pre>
 * 
 * In this expression, <code>begin(&quot;&lt;&lt;&quot;)</code> is a
 * <code><b>static</b></code> function defined in {@link Stringify} which
 * creates this object. The remaining functions in this call chain are defined
 * in {@link Stringify.Option} and modify the options object.
 * 
 * @author Yossi Gil, the Technion.
 * @since 28/08/2008
 */
@Utility public enum Stringify {
  ; // No values in this namespace.
  /**
   * Convert an {@link Iterable} collection of objects to a {@link String} using
   * the default formatting style.
   * 
   * @param <T>
   *          type of arguments to be converted.
   * @param ts
   *          a collection of objects of type <code>T</code>.
   * @return a {@link String} representation of the parameter, prepared with the
   *         default list formatting style as represented by
   *         {@link Option#defaultStyle}.
   */
  public static <T> String it(final Iterable<T> ts) {
    return it(ts, Option.defaultStyle);
  }
  /**
   * Create a textual representation of an {@link Iterable} collection of
   * objects using a supplied formatting style.
   * 
   * @param <T>
   *          type of arguments to be converted.
   * @param ts
   *          an array of objects of type <code>T</code>.
   * @param o
   *          formatting style for this list.
   * @return a {@link String} representation of the parameter, prepared with the
   *         supplied formatting style.
   */
  public static <T> String it(final Iterable<T> ts, final Option o) {
    return makeList(asStringArray(prune(ts, o), o.filler()), o);
  }
  /**
   * Create a textual representation of an {@link Iterable} collection of
   * objects using a supplied formatting style, after applying a user supplied
   * conversion to each object.
   * 
   * @param ts
   *          an array of objects of type <code>T</code>.
   * @param f
   *          a function to apply on each object prior to conversion.
   * @param o
   *          formatting style for this list.
   * @param <T>
   *          type of arguments to be converted.
   * @return a {@link String} representation of the parameter, prepared with the
   *         supplied formatting style.
   */
  public static <T> String it(final Iterable<? extends T> ts, final F<T> f, final Option o) {
    return it(apply(f, ts), o);
  }
  public static <T> ArrayList<String> apply(final F<T> f, final Iterable<? extends T> ts) {
    final ArrayList<String> $ = new ArrayList<String>();
    for (final T t : ts)
      $.add(f._(t));
    return $;
  }
  /**
   * Convert an array of objects to a {@link String} using the default
   * formatting style.
   * 
   * @param <T>
   *          type of arguments to be converted.
   * @param ts
   *          an array of objects of type <code>T</code>.
   * @return a {@link String} representation of the parameter, prepared with the
   *         default list formatting style as represented by
   *         {@link Option#defaultStyle}.
   */
  public static <T> String it(final T[] ts) {
    return it(ts, Option.defaultStyle);
  }
  /**
   * Convert an {@link Iterable} collection of objects to a {@link String} using
   * a supplied style.
   * 
   * @param <T>
   *          type of arguments to be converted.
   * @param ts
   *          an array of objects of type <code>T</code>.
   * @param o
   *          formatting style for this list.
   * @return a {@link String} representation of the parameter, prepared with the
   *         supplied formatting style.
   */
  public static <T> String it(final T[] ts, final Option o) {
    return makeList(asStringArray(prune(ts, o), o.filler()), o);
  }
  /**
   * Create an option object with default setting, except for a specified
   * beginning string.
   * 
   * @param begin
   *          a non-<code><b>null</b></code> specifying the text to place at the
   *          beginning of a non-empty list.
   * @return a newly created options object with the default settings, except
   *         for the specified beginning string.
   */
  public static Option begin(final String begin) {
    nonnull(begin);
    return new Option().begin(begin);
  }
  /**
   * Create an option object with default setting, except for a specified ending
   * string.
   * 
   * @param end
   *          a non-<code><b>null</b></code> specifying the text to add at the
   *          end of a non-empty list.
   * @return a newly created options object with the default settings, except
   *         for the specified ending string.
   */
  public static Option end(final String end) {
    nonnull(end);
    return new Option().end(end);
  }
  /**
   * Create an option object with default setting, except for a specified string
   * to replace <code><b>null</b></code> values.
   * 
   * @param filler
   *          string to replace <code><b>null</b></code> values found in the
   *          list. If <code><b>null</b></code>, then empty elements are not .
   * @return a newly created options object with the default settings, except
   *         for the specified filler string.
   */
  public static Option filler(final String filler) {
    return new Option().filler(filler);
  }
  /**
   * Create an option object with default setting, except for the specification
   * <code><b>null</b></code> are to be omitted.
   * 
   * @return a newly created options object with the default settings, except
   *         for except for the specification <code><b>null</b></code> are to be
   *         omitted.
   */
  public static Option omitNulls() {
    return new Option().omitNulls();
  }
  /**
   * Create an option object with default setting, except for a specified
   * separator string.
   * 
   * @param separator
   *          a non-<code><b>null</b></code> specifying the text used to
   *          separate the list items.
   * @return a newly created options object with the default settings, except
   *         for the specified filler string.
   */
  public static Option separator(final String separator) {
    nonnull(separator);
    return new Option().separator(separator);
  }
  private static <T> Iterable<T> prune(final Iterable<T> ts, final Option o) {
    return o.isOmittingNulls() ? Prune.nulls(ts) : ts;
  }
  private static <T> String[] asStringArray(final Iterable<T> ts, final String nullFiller) {
    return asStringCollection(ts, nullFiller).toArray(new String[0]);
  }
  private static <T> String[] asStringArray(final T[] ts, final String nullFiller) {
    final String[] $ = new String[ts.length];
    int i = 0;
    for (final T t : ts)
      $[i++] = t == null ? nullFiller : t.toString();
    return $;
  }
  private static <T> T[] prune(final T[] ts, final Option o) {
    return o.isOmittingNulls() ? Prune.nulls(ts) : ts;
  }
  private static String makeList(final String[] ss, final Option o) {
    return ss.length == 0 ? "" : o.begin() + Separate.by(ss, o.separator()) + o.end();
  }
  private static <T> Collection<String> asStringCollection(final Iterable<T> ts, final String nullFiller) {
    final ArrayList<String> $ = new ArrayList<String>();
    for (final T t : ts)
      if (t != null)
        $.add(t.toString());
      else if (nullFiller != null)
        $.add(nullFiller);
    return $;
  }
  
  /**
   * An options object for the containing class.
   * 
   * @author Yossi Gil, the Technion.
   * @since 28/08/2008
   */
  public static class Option {
    public static final String COMMA = ", ";
    public static final String NL = "\n";
    public static final String NULL_TEXT = "(null)";
    public static final Option functionStyle = new Option("(", ")", "_", COMMA);
    public static final Option statementStyle = new Option("{", "}", "_", NL);
    public static final Option defaultStyle = new Option("[", "]", NULL_TEXT, ",");
    
    /**
     * Create a new instance, initialized with the default formatting style.
     */
    public Option() {
      this(defaultStyle);
    }
    public String begin() {
      return _begin;
    }
    public String end() {
      return _end;
    }
    public String filler() {
      return _filler;
    }
    public boolean isOmittingNulls() {
      return _filler == null;
    }
    public String separator() {
      return _separator;
    }
    public Option begin(final String begin) {
      _begin = begin;
      return this;
    }
    public Option end(final String end) {
      _end = end;
      return this;
    }
    public Option filler(final String filler) {
      _filler = filler;
      return this;
    }
    public Option omitNulls() {
      _filler = null;
      return this;
    }
    public Option separator(final String separator) {
      _separator = separator;
      return this;
    }
    
    private String _begin;
    private String _end;
    private String _filler;
    private String _separator;
    
    private Option(final String _begin, final String _end, final String _filler, final String _separator) {
      this._begin = _begin;
      this._end = _end;
      this._filler = _filler;
      this._separator = _separator;
    }
    private Option(final Option o) {
      _begin = o._begin;
      _end = o._end;
      _filler = o._filler;
      _separator = o._separator;
    }
  }
  
  @SuppressWarnings("static-method") public static class TEST {
    @Test public void testMakeBeginOption() {
      assertNotNull(begin("("));
    }
    @Test public void testMakeEndOption() {
      assertNotNull(end(")"));
    }
    @Test public void testMakeFillerOption() {
      assertNotNull(filler(")"));
    }
    @Test public void testMakeSeparatorOption() {
      assertNotNull(separator(")"));
    }
    public Collection<String> makeCollectionABC() {
      return makeCollection("A", "B", "C");
    }
    private static Collection<String> makeCollection(final String... ss) {
      final ArrayList<String> $ = new ArrayList<String>();
      for (final String s : ss)
        $.add(s);
      return $;
    }
    private static String[] makeArray(final String... ss) {
      return ss;
    }
    @Test public void testMakeIgnoreNulls() {
      assertNotNull(filler(")"));
    }
    @Test public void testSimpleList() {
      assertEquals("[A,B,C]", it(makeCollection("A", "B", "C")));
    }
    @Test public void testNulledList() {
      assertEquals("[(null),A,(null),B,(null),C,(null)]", it(makeCollection(null, "A", null, "B", null, "C", null)));
    }
    @Test public void testNulledListPruned() {
      assertEquals("[A,B,C]", it(makeCollection(null, "A", null, "B", null, "C", null), filler(null)));
    }
    @Test public void testNulledListPrunedWithOmitNulls() {
      assertEquals("[A,B,C]", it(makeCollection(null, "A", null, "B", null, "C", null), omitNulls()));
    }
    @Test public void testPrunedNulledListCommas() {
      assertEquals("[A,B,C]", it(makeCollection(null, "A", null, "B", null, "C", null), omitNulls().separator(",")));
    }
    @Test public void testPrunedNulledListSemiColons() {
      assertEquals("[A;B;C]", it(makeCollection(null, "A", null, "B", null, "C", null), omitNulls().separator(";")));
    }
    @Test public void testPrunedNulledListCommasCallsBeginEnd() {
      assertEquals("(A,B,C)", it(//
          makeCollection(null, "A", null, "B", null, "C", null),//
          omitNulls().separator(",").begin("(").end(")")));
    }
    @Test public void testPrunedEmptyCollectionBeginEnd() {
      assertEquals("", it(//
          makeCollection(),//
          omitNulls().separator(",").begin("(").end(")")));
    }
    @Test public void testPrunedEmptyCollectionOmittingNullsBeginEnd() {
      assertEquals("", it(//
          makeCollection(null, null, null),//
          omitNulls().separator(",").begin("(").end(")")));
    }
    @Test public void testDefaultsArray() {
      assertEquals("[A,B,C]", it(makeArray("A", "B", "C")));
    }
    @Test public void testArrayNoBegin() {
      assertEquals("A,B,C]", it(makeArray("A", "B", "C"), begin("")));
    }
    @Test public void testArrayNoEnd() {
      assertEquals("[A,B,C", it(makeArray("A", "B", "C"), end("")));
    }
    @Test public void testArrayFillerSeparatorBeginEnd() {
      assertEquals("[[_:A:_:B:C:_]]", //
          it(//
          makeArray(null, "A", null, "B", "C", null), //
              filler("_").begin("[[").end("]]").separator(":")//
          )//
      );
    }
  }
}
