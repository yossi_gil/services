package il.ac.technion.cs.ssdl.sequence;

public abstract class Sequence implements Cloneable {
  protected final int threshold;
  protected int current;
  protected static final int MAX_VALUE = Integer.MAX_VALUE / 3;
  
  public Sequence() {
    this(MAX_VALUE);
  }
  public Sequence(final int threshold) {
    this.threshold = threshold;
  }
  public final Sequence advanceTo(final int n) {
    while (current() < n && more())
      advance();
    return this;
  }
  public final int current() {
    return current;
  }
  public final boolean more() {
    return current() <= threshold;
  }
  public abstract Sequence advance();
  public abstract Sequence reset();
  @Override public final Sequence clone() {
    try {
      return (Sequence) super.clone();
    } catch (final CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
  }
  public final int[] toArray() {
    final int[] $ = new int[count()];
    return clone().fill($);
  }
  /**
   * @param $
   * @return
   */
  private int[] fill(final int[] $) {
    for (int i = 0; more(); i++, advance())
      $[i] = current();
    return $;
  }
  public final int count(final int from) {
    return clone().startAt(from)._count();
  }
  public final int count() {
    return clone()._count();
  }
  private final int _count() {
    for (int $ = 0;; $++)
      if (more())
        advance();
      else
        return $;
  }
  public Sequence startAt(final int n) {
    return reset().advanceTo(n);
  }
  
  public static class Binary extends Sequence {
    public Binary(final int threshold) {
      super(threshold);
      current = 1;
    }
    @Override public Sequence advance() {
      current += current;
      return this;
    }
    @Override public Sequence reset() {
      current = 1;
      return this;
    }
  }
  
  public static Sequence merge(final Sequence s1, final Sequence s2) {
    return new Merged(s1, s2);
  }
  
  private static class Merged extends Sequence {
    public Merged(final Sequence _s1, final Sequence _s2) {
      s1 = _s1;
      s2 = _s2;
      current = Math.min(s1.current(), s2.current());
    }
    
    final Sequence s1;
    final Sequence s2;
    
    @Override public Sequence advance() {
      if (current < s1.current())
        s2.advance();
      else if (current < s2.current())
        s1.advance();
      else {
        s1.advance();
        s2.advance();
      }
      current = Math.min(s1.current(), s2.current());
      return this;
    }
    @Override public Sequence reset() {
      s1.reset();
      s2.reset();
      return this;
    }
  }
}