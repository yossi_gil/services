/**
 *
 */
package il.ac.technion.cs.ssdl.sequence;

import il.ac.technion.cs.ssdl.utils.____;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yossi Gil
 * @param <T>
 * @since 8 באוק 2011
 */
public class History<T> {
  private final int n;
  
  public History(final int n) {
    ____.positive(n);
    this.n = n;
  }
  
  private final List<T> inner = new ArrayList<T>();
  
  void add(final T t) {
    inner.add(t);
    while (inner.size() > n)
      inner.remove(0);
  }
}
