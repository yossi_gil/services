package il.ac.technion.cs.ssdl.sequence;

public class Fibonacci extends Sequence {
  private int previous;
  
  public Fibonacci() {
    super();
  }
  public Fibonacci(final int threshold) {
    super(threshold);
    reset();
  }
  @Override public final Fibonacci reset() {
    current = previous = 1;
    return this;
  }
  @Override public Fibonacci advance() {
    final int temp = previous;
    previous = current;
    current += temp;
    return this;
  }
}