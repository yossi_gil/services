/**
 *
 */
package il.ac.technion.cs.ssdl.graph;


/**
 * @author Yossi Gil
 * @since Apr 19, 2012
 */
public abstract class AbstractSmallIntegersGraph {
  public abstract boolean has(final int n);

  public final boolean connected(final int n1, final int n2) {
    return has(n1) && has(n2) && component((short) n1) == component((short) n2);
  }
  public abstract short components();
  public abstract short component(final int n);

  protected static short makeShort(final int n) {
    if (n < 0 || n > Short.MAX_VALUE)
      throw new IllegalArgumentException();
    return (short) n;
  }

}
