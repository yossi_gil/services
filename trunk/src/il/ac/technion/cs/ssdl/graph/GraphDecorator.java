package il.ac.technion.cs.ssdl.graph;

import il.ac.technion.cs.ssdl.collections.ImmutableArrayList;

/**
 * A basic graph decorator, which does nothing except for delegating all its
 * missions to an encapsulated {@link AbstractGraph}. To be used as base class
 * for all other graph decorators.
 * 
 * @param <E>
 *          type of elements stored in this graph
 * @author Yossi Gil
 * @since 2011-11-11
 */
public class GraphDecorator<E> extends AbstractGraph<E> {
  /**
   * Instantiated with a given {@link AbstractGraph} object.
   * 
   * @param inner
   *          an arbitrary {@link AbstractGraph} to be encapsulated.
   */
  public GraphDecorator(final AbstractGraph<E> inner) {
    this.inner = inner;
  }
  
  /**
   * The encapsulated decorated graph.
   */
  public final AbstractGraph<E> inner;
  
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.iteration.NamedEntity#name()
   */
  @Override public String name() {
    return inner.name();
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.iteration.NamedEntity#name()
   */
  @Override public String description() {
    return inner.description();
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#contains(java.lang.Object)
   */
  @Override public boolean contains(final E e) {
    return inner.contains(e);
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#vertices()
   */
  @Override public ImmutableArrayList<Vertex<E>> vertices() {
    return inner.vertices();
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#sources()
   */
  @Override public ImmutableArrayList<Vertex<E>> sources() {
    return inner.sources();
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#sinks()
   */
  @Override public ImmutableArrayList<Vertex<E>> sinks() {
    return inner.sinks();
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#sourcesCount()
   */
  @Override public int sourcesCount() {
    return inner.sourcesCount();
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#sinksCount()
   */
  @Override public int sinksCount() {
    return inner.sinksCount();
  }
  /*
   * (non-Javadoc)
   * @see
   * il.ac.technion.cs.ssdl.graph.AbstractGraph#incoming(il.ac.technion.cs.ssdl
   * .graph.Vertex)
   */
  @Override public ImmutableArrayList<Vertex<E>> incoming(final Vertex<E> v) {
    return inner.incoming(v);
  }
  /*
   * (non-Javadoc)
   * @see
   * il.ac.technion.cs.ssdl.graph.AbstractGraph#outgoing(il.ac.technion.cs.ssdl
   * .graph.Vertex)
   */
  @Override public ImmutableArrayList<Vertex<E>> outgoing(final Vertex<E> v) {
    return inner.outgoing(v);
  }
  /*
   * (non-Javadoc)
   * @see il.ac.technion.cs.ssdl.graph.AbstractGraph#vertex(java.lang.Object)
   */
  @Override public Vertex<E> vertex(final E e) {
    return inner.vertex(e);
  }
}
