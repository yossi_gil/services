/**
 *
 */
package il.ac.technion.cs.ssdl.statistics;

import static il.ac.technion.cs.ssdl.statistics.Kurtosis.kurtosisNormalizedVector;
import static il.ac.technion.cs.ssdl.statistics.MomentUtils.sqr;
import static il.ac.technion.cs.ssdl.statistics.Skewness.skewnessNormalizedVector;
import static il.ac.technion.cs.ssdl.statistics.StandardDeviation.normalize;
import il.ac.technion.cs.ssdl.stereotypes.Utility;

/**
 * @author Yossi Gil
 * @since 30/04/2011
 */
@Utility public enum JarqueBera {
  ;
  public static double jarqueBera(final double... vs) {
    return jarqueBeraNormalizedVector(normalize(vs.clone()));
  }
  public static double jarqueBeraNormalizedVector(final double... ds) {
    return ds.length * (sqr(skewnessNormalizedVector(ds)) + sqr(kurtosisNormalizedVector(ds) / 2)) / 6;
  }
}
