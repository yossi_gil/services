package il.ac.technion.cs.ssdl.statistics;

import static il.ac.technion.cs.ssdl.statistics.MomentUtils.sqr;
import umontreal.iro.lecuyer.probdist.Distribution;
import umontreal.iro.lecuyer.probdist.StudentDist;
/**
 * @author Yossi Gil
 * @since 30/04/2011
 */
public class WelchT extends Statistics {
  public final double t;
  public final double ν;
  public final double p;
  public final Statistics s1;
  public final Statistics s2;

  public WelchT(final double[] s1, final double[] s2) {
    this(new RealStatistics().record(s1), new RealStatistics().record(s2));
  }
  /*
   * See <a href=http://en.wikipedia.org/wiki/Welch%27s_t_test>Welch's
   * T-Test</a>
   */
  public WelchT(final Statistics s1, final Statistics s2) {
    this.s1 = s1;
    this.s2 = s2;
    final double v_sum = s1.v() / s1.n() + s2.v() / s2.n();
    t = Math.abs((s1.mean() - s2.mean()) / Math.sqrt(v_sum));
    ν = sqr(v_sum) / (sqr(s1.v() / s1.n()) / s1.ν() + sqr(s2.v() / s2.n()) / s2.ν());
    if ((int) Math.round(ν) <= 0)
      p = 1;
    else {
      final Distribution d = new StudentDist((int) Math.round(ν));
      p = 2 * (1 - d.cdf(t));
    }
  }

  public static class Pruned extends WelchT {
    public Pruned(final double[] s1, final double[] s2) {
      super(prune(s1), prune(s2));
    }
    public Pruned(final ImmutableStatistics s1, final ImmutableStatistics s2) {
      this(s1.all(), s2.all());
    }
  }
}
