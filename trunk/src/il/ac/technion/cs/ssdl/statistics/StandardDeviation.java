/**
 *
 */
package il.ac.technion.cs.ssdl.statistics;

import static il.ac.technion.cs.ssdl.statistics.Mean.destructiveMoment;
import static il.ac.technion.cs.ssdl.statistics.Mean.shift;
import static il.ac.technion.cs.ssdl.statistics.Sum.sum;
import static il.ac.technion.cs.ssdl.statistics.Sum.sum2;
import il.ac.technion.cs.ssdl.stereotypes.Utility;

/**
 * @author Yossi Gil
 * @since 2011-08-1
 */
@Utility public enum StandardDeviation {
  ;
  public static double sd(final double... vs) {
    return destructiveSd(vs);
  }
  public static double destructiveSd(final double[] vs) {
    return Math.sqrt(destructiveVariance(vs.clone()));
  }
  public static double variance(final double... vs) {
    return destructiveVariance(vs.clone());
  }
  public static double destructiveVariance(final double[] vs) {
    return destructiveMoment(2, vs);
  }
  public static double correctedSd(final double... vs) {
    return sd(vs) * sdCorrection(vs);
  }
  public static double sdCorrection(final double... vs) {
    return sdCorrection(vs.length);
  }
  public static double sdCorrection(final int n) {
    return Math.sqrt((double) n / (n - 1));
  }
  public static double[] scale(final double[] vs) {
    final double sd = sd(vs);
    for (int i = 0; i < vs.length; i++)
      vs[i] /= sd;
    return vs;
  }
  public static double[] normalize(final double[] vs) {
    return scale(shift(vs));
  }
  /**
   * Compute a <a href=
   * "http://en.wikipedia.org/wiki/Variance#Population_variance_and_sample_variance"
   * >sample variance</a>
   * 
   * @param ds
   *          the sample
   * @return the sample variance of the parameter
   */
  public static double sampleVariance(final double... ds) {
    final double sum = sum(ds);
    final double sum2 = sum2(ds);
    final int n = ds.length;
    return sum2 / (n - 1) - sum * sum / (n * n - n);
  }
}
