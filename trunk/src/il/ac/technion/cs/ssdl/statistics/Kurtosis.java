/**
 *
 */
package il.ac.technion.cs.ssdl.statistics;

import static il.ac.technion.cs.ssdl.statistics.Mean.moment;
import static il.ac.technion.cs.ssdl.statistics.StandardDeviation.normalize;
import il.ac.technion.cs.ssdl.stereotypes.Utility;

/**
 * @author Yossi Gil
 * @since 2011-08-1
 */
@Utility public enum Kurtosis {
  ;
  public static double kurotsis(final double... ds) {
    return kurtosisNormalizedVector(normalize(ds.clone()));
  }
  public static double kurtosisNormalizedVector(final double... ds) {
    return moment(4, ds) - 3;
  }
}
