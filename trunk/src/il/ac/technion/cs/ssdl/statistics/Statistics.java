/**
 *
 */
package il.ac.technion.cs.ssdl.statistics;

import static il.ac.technion.cs.ssdl.statistics.MomentUtils.sqr;
import static il.ac.technion.cs.ssdl.utils.Box.box;
import il.ac.technion.cs.ssdl.iteration.Iterables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Yossi Gil
 * @since 30/04/2011
 */
public abstract class Statistics {
  /**
   * Provides the number of elements in this instance.
   *
   * @return a non-negative integer, giving the number of elements in the
   *         sequence
   */
  public final int n() {
    return n;
  }
  /**
   * Provides the degrees of freedom offered by this instance.
   *
   * @return a non-negative integer, giving the degree of freedom in the
   *         sequence, i.e., the number of elements - 1.
   * @see #n()
   */
  public final int ν() {
    return n() - 1;
  }
  /**
   * @return a non-negative integer, giving the number of ''missing'' elements
   *         in the sequence
   */
  public final int missing() {
    return missing;
  }
  /**
   * @return the sum of all values in the sequence
   */
  public final double sum() {
    return moments[1];
  }
  /**
   * @return the average value of the elements in the sequence
   * @throws ArithmeticException
   *           in case this function was called prior to recording any elements
   *           in the sequence
   */
  public final double mean() throws ArithmeticException {
    checkEmpty();
    return sum() / n();
  }
  /**
   * @return the sum of squares of all values in the sequence
   */
  public double sum2() {
    return moments[2];
  }
  /**
   * @return the standard deviation of the elements in the sequence
   * @throws ArithmeticException
   *           in case this function was called prior to recording any elements
   *           in the sequence
   */
  public final double sd() {
    return Math.sqrt(sum2() / n() - sqr(mean()));
  }
  /**
   * @return the <a href=
   *         "http://en.wikipedia.org/wiki/Variance#Population_variance_and_sample_variance"
   *         >sample variance</a> f the elements in the sequence
   * @throws ArithmeticException
   *           in case this function was called prior to recording any elements
   *           in the sequence
   */
  public final double v() {
    return sum2() / (n - 1) - sqr(sum()) / (sqr(n) - n);
  }
  public double variance() {
    return sum2() / n() - sqr(mean());
  }
  /**
   * @return the relative error of the elements in the sequence, defined as the
   *         standard deviation divided by the mean.
   * @throws ArithmeticException
   *           in case this function was called prior to recording any elements
   *           in the sequence
   */
  public final double relativeError() {
    return mean() == 0 ? sd() : sd() / Math.abs(mean());
  }
  /**
   * @return the smallest value of the elements in the sequence
   * @throws ArithmeticException
   *           in case this function was called prior to recording any elements
   *           in the sequence
   */
  public final double min() throws ArithmeticException {
    checkEmpty();
    return min;
  }
  /**
   * @return the largest value of the elements in the sequence
   * @throws ArithmeticException
   *           in case this function was called prior to recording any elements
   *           in the sequence
   */
  public final double max() throws ArithmeticException {
    checkEmpty();
    return max;
  }
  /**
   * @return <code><b>true</b></code> <i>if, and only if</i> one ore more
   *         non-missing values were recorded in the sequence.
   */
  public final boolean isEmpty() {
    return n() == 0;
  }
  protected void checkEmpty() throws ArithmeticException {
    if (isEmpty())
      throw new ArithmeticException(EMPTY_SEQUENCE);
  }

  protected static final int MOMENTS = 4;
  protected static final String EMPTY_SEQUENCE = "No elements yet in sequene.";

  public static double sampleMean(final double[] ds) {
    double sum = 0;
    for (final double d : ds)
      sum += d;
    return sum / ds.length;
  }
  /**
   * Compute a <a href=
   * "http://en.wikipedia.org/wiki/Variance#Population_variance_and_sample_variance"
   * >sample variance</a>
   *
   * @param ds
   *          the sample
   * @return the sample variance of the parameter
   */
  public static double sampleVariance(final double[] ds) {
    double sum = 0;
    double sum2 = 0;
    for (final double d : ds) {
      sum += d;
      sum2 += d * d;
    }
    final int n = ds.length;
    return sum2 / (n - 1) - sum * sum / (n * n - n);
  }
  public static double[] prune(final double[] ds) {
    final List<Double> $ = new ArrayList<Double>();
    final double median = median(ds);
    final double mad = mad(ds);
    for (final double d : ds)
      if (median - 2 * mad <= d && d <= median + 2 * mad)
        $.add(box(d));
    return Iterables.toArray($);
  }
  public static double mad(final double[] ds) {
    final int n = ds.length;
    final double median = median(ds);
    final double $[] = new double[n];
    for (int i = 0; i < n; i++)
      $[i] = Math.abs(ds[i] - median);
    return median($);
  }
  public static double median(final double[] ds) {
    Arrays.sort(ds);
    final int n = ds.length;
    return (ds[n / 2] + ds[(n - 1) / 2]) / 2;
  }


  protected int n;
  protected int missing;
  protected double min;
  protected double max;
  protected final double[] moments = new double[MOMENTS];
}
