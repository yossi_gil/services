// <a href=http://ssdl-linux.cs.technion.ac.il/wiki/index.php>SSDLPedia</a>
package il.ac.technion.cs.ssdl.stereotypes;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A <b>Designator</b> used for marking classes serving as JUnit test cases.
 * 
 * @author Yossi Gil, the Technion.
 * @since 23/08/2008
 */
@Documented//
@Retention(RetentionPolicy.SOURCE)//
@Target({ ElementType.TYPE })//
@Designator//
public @interface TestCase {
  // No members in a <b>Designator</b>.
}
