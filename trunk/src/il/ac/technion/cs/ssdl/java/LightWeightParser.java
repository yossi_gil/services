package il.ac.technion.cs.ssdl.java;

import static il.ac.technion.cs.ssdl.java.Token.AT_INTERFACE;
import static il.ac.technion.cs.ssdl.java.Token.IDENTIFIER;
import static il.ac.technion.cs.ssdl.java.Token.LBRACE;
import static il.ac.technion.cs.ssdl.java.Token._class;
import static il.ac.technion.cs.ssdl.java.Token._enum;
import static il.ac.technion.cs.ssdl.java.Token._interface;

public class LightWeightParser {
  public LightWeightParser(final Tokenizer tokenizer) {
    this.tokenizer = tokenizer;
  }

  private final Tokenizer tokenizer;

  public void file() {
  }

  public void clazz() {
    final Token t = skipUntil(_class, _enum, _interface, AT_INTERFACE);
    if (t == null)
      return;
    final String s = getIdentifier();
    skipUntil(LBRACE);
  }

  private String getIdentifier() {
    skipUntil(IDENTIFIER);
    return tokenizer.text();
  }

  public void method() {
  }

  public void statement() {
  }

  public void variable() {
  }

  public void field() {
  }

  private Token skipUntil(final Token... ts) {
    for (Token t = tokenizer.next(); t != null; t = tokenizer.next())
      if (in(t, ts))
        return t;
    return null;
  }

  private static boolean in(final Token t, final Token[] ts) {
    for (final Token tʹ : ts)
      if (t == tʹ)
        return true;
    return false;
  }
}
