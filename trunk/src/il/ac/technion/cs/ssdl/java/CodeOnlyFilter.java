/**
 *
 */
package il.ac.technion.cs.ssdl.java;

import static il.ac.technion.cs.ssdl.java.Token.Category.IGNORE;

/**
 * @author Yossi Gil
 * @since 2011-11-19
 */
public class CodeOnlyFilter extends TokenFilter {
  /*
   * (non-Javadoc)
   * 
   * @see
   * il.ac.technion.cs.ssdl.java.TokenFilter#ok(il.ac.technion.cs.ssdl.java.
   * Token)
   */
  @Override protected boolean ok(final Token t) {
    if (t.isError() || t.isNL())
      return false;
    return t.kind.category != IGNORE;
  }
  
  @Override protected void _process(final Token t, final String text) {
    $.append(text);
  }
  
  private final StringBuilder $ = new StringBuilder();
  
  @Override public String toString() {
    return $.toString();
  }
}
