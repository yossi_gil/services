package il.ac.technion.cs.ssdl.java;

public class RichToken {
  public static class Factory {
    private final RawTokenizer tokenizer;
    
    public Factory(final RawTokenizer tokenizer) {
      this.tokenizer = tokenizer;
    }
    
    @SuppressWarnings("synthetic-access") public RichToken make(final Token t) {
      return new RichToken(tokenizer, t);
    }
  }
  
  private RichToken(final RawTokenizer tokenizer, final Token token) {
    this.token = token;
    this.tokenizer = tokenizer;
  }
  
  private final Token token;
  private final RawTokenizer tokenizer;
  
  public String text() {
    return tokenizer.text();
  }
  
  public String token() {
    return tokenizer.token();
  }
  
  public int column() {
    return tokenizer.column();
  }
  
  public int chars() {
    return tokenizer.chars();
  }
  
  public String location() {
    return tokenizer.location();
  }
  
  public int line() {
    return tokenizer.line();
  }
  
  public boolean isNL() {
    return token.isNL();
  }
  
  public final boolean isError() {
    return token.isError();
  }
  
  public final String name() {
    return token.name();
  }
  
  public final int ordinal() {
    return token.ordinal();
  }
  
  @Override public String toString() {
    return token.toString();
  }
}
