package il.ac.technion.cs.ssdl.java;

import static il.ac.technion.cs.ssdl.utils.DBC.nothing;
import il.ac.technion.cs.ssdl.iteration.Iterables;

public abstract class TokenProcessor {
  protected abstract void process(final Token t, final String text);
  
  @SuppressWarnings("static-method") protected void after() {
    nothing();
  }
  
  @SuppressWarnings("static-method") protected void before() {
    nothing();
  }
  
  public static class Multiplexor extends TokenProcessor {
    private final Iterable<TokenProcessor> inners;
    
    public Multiplexor(final Iterable<TokenProcessor> inners) {
      this.inners = inners;
    }
    
    public Multiplexor(final TokenProcessor... inners) {
      this(Iterables.make(inners));
    }
    
    @Override protected void before() {
      for (final TokenProcessor inner : inners)
        inner.before();
    }
    
    @Override protected void process(final Token t, final String text) {
      for (final TokenProcessor inner : inners)
        inner.process(t, text);
    }
    
    @Override protected void after() {
      for (final TokenProcessor inner : inners)
        inner.after();
    }
  }
}
