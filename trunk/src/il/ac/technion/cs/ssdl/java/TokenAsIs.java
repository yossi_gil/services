/**
 *
 */
package il.ac.technion.cs.ssdl.java;

import static il.ac.technion.cs.ssdl.iteration.Iterables.first;
import static il.ac.technion.cs.ssdl.utils.DBC.unused;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

/**
 * @author Yossi Gil
 * @since 2011-11-19
 */
public class TokenAsIs extends TokenProcessor {
  @Override public void process(final Token t, final String text) {
    unused(t);
    $.append(text);
  }
  
  @Override public String toString() {
    return $.toString();
  }
  
  private final StringBuilder $ = new StringBuilder();
  
  public static void main(final String argv[]) throws IOException {
    final String s = fileToString(first(argv));
    System.out.println(s);
  }
  
  public static String fileToString(final String fileName) throws IOException, FileNotFoundException {
    return new TokenFeeder(new Tokenizer(fileName), new TokenAsIs()).go().processor.toString();
  }
  
  public static String fileToString(final File f) throws IOException, FileNotFoundException {
    return new TokenFeeder(new Tokenizer(f), new TokenAsIs()).go().processor.toString();
  }
  
  public static String stringToString(final String text) throws IOException, FileNotFoundException {
    return new TokenFeeder(new StringReader(text), new TokenAsIs()).go().processor.toString();
  }
}
