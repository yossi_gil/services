/**
 *
 */
package il.ac.technion.cs.ssdl.java;

import il.ac.technion.cs.ssdl.utils.____;

import java.io.StringReader;

/**
 * @author Yossi Gil
 * @since 19 November 2011
 */
public class SignatureAnalyzer {
  public static SignatureAnalyzer ofString(final String s) {
    ____.___unused(s);
    return new SignatureAnalyzer();
  }
  
  public static SignatureAnalyzer ofFile(final String fileName) {
    ____.___unused(fileName);
    return new SignatureAnalyzer();
  }
  
  public static SignatureAnalyzer ofReader(final StringReader stringReader) {
    ____.___unused(stringReader);
    return new SignatureAnalyzer();
  }
}
