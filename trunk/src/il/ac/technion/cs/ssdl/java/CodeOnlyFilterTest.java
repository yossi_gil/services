/**
 *
 */
package il.ac.technion.cs.ssdl.java;

import static org.junit.Assert.assertNotNull;

import java.io.StringReader;

import org.junit.Test;

/**
 * @author Yossi Gil
 * @since 19 November 2011
 * 
 */
@SuppressWarnings("static-method")
public class CodeOnlyFilterTest {
  static TokenFeeder makeFilter(final String s) {
    return new TokenFeeder(new Tokenizer(new StringReader(s)), new CodeOnlyFilter());
  }
  
  @Test public void creater() {
    assertNotNull(makeFilter("Hello, World!\n"));
  }
  
  @Test public void content() {
    assertNotNull("Hello,World!", makeFilter("Hello, World!\n").processor.toString());
  }
}
