package il.ac.technion.cs.ssdl.java;

import static il.ac.technion.cs.ssdl.strings.StringUtils.esc;
import il.ac.technion.cs.ssdl.utils.____;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class Tokenizer {
  private final RawTokenizer inner;
  private final String streamName;
  private final Reader reader;
  
  public String streamName() {
    return streamName;
  }
  
  public Token next() {
    try {
      return inner.next();
    } catch (final IOException e) {
      e.printStackTrace();
      ____.unreachable();
      return null;
    }
  }
  
  public Tokenizer(final String streamName, final Reader reader) {
    inner = new RawTokenizer(this.reader = reader);
    this.streamName = streamName;
  }
  
  public Tokenizer(final Reader in) {
    this("", in);
  }
  
  /**
   * Instantiate {@link Tokenizer}.
   * 
   * @param streamName
   *          read input from this file
   * @throws FileNotFoundException
   */
  public Tokenizer(final String streamName) throws FileNotFoundException {
    this(streamName, reader(streamName));
  }
  
  /**
   * Instantiate {@link Tokenizer}.
   * 
   * @param f
   *          read input from this file
   * @throws FileNotFoundException
   */
  public Tokenizer(final File f) throws FileNotFoundException {
    this(f.getPath(), reader(f));
  }
  
  public static Reader reader(final String fileName) throws FileNotFoundException {
    return fileName != null ? reader(new File(fileName)) : new InputStreamReader(System.in);
  }
  
  public static Reader reader(final File f) throws FileNotFoundException {
    return new FileReader(f);
  }
  
  public String description(final Token t) {
    return location() + t + " / " + t.kind + "<" + esc(text()) + "> S=" + state();
  }
  
  protected String state() {
    return "" + inner.yystate();
  }
  
  public void closeReader() {
    try {
      reader.close();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
  
  public String text() {
    return inner.text();
  }
  
  public String location() {
    return inner.location();
  }
  
  public int line() {
    return inner.line();
  }
  
  public int column() {
    return inner.column();
  }
}
