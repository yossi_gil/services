/**
 *
 */
package il.ac.technion.cs.ssdl.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import il.ac.technion.cs.ssdl.files.visitors.FileSystemVisitor.PlainFileOnlyAction;
import il.ac.technion.cs.ssdl.files.visitors.JavaFilesVisitor;
import il.ac.technion.cs.ssdl.iteration.Iterables;
import il.ac.technion.cs.ssdl.utils.Separate;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

/**
 * @author Yossi Gil
 * @since 16/05/2011
 */
@RunWith(Theories.class)
@SuppressWarnings("static-method")
//
public class TestNoOther {
  @Test public void brace_brace_newline() throws IOException {
    assertEquals("{}\n", TokenAsIs.stringToString("{}\n"));
  }
  
  @Test public void some_method() throws IOException {
    final String s = Separate.nl(
    //
        "private static int circularSum(final int[] a, final int[] b, final int offset) {", //
        "  int $ = 0;",//
        " for (int i = 0; i < a.length; i++)", //
        "    $ += a[i] * b[(i + offset) % b.length];", //
        "  return $;", //
        "}",//
        " ",//
        " ",//
        "  ");
    assertEquals(s, TokenAsIs.stringToString(s));
  }
  
  @Test public void unicode() throws IOException {
    final String s = "יוסי";
    assertEquals(s, TokenAsIs.stringToString(s).toString());
  }
  
  private final File fin = new File("test/data/UnicodeFile");
  
  @Test public void unicodeFileLenth() throws IOException {
    assertTrue(fin.length() > TokenAsIs.fileToString(fin).length());
  }
  
  @Test public void unicodeFileAgainstString() throws IOException {
    assertEquals(read(fin), TokenAsIs.fileToString(fin));
  }
  
  @Test public void unicodeFileAgainstFileOutput() throws IOException {
    final String s = TokenAsIs.fileToString(fin);
    final File fout = new File(fin.getPath() + ".out");
    write(fout, s);
    assertEquals(s, read(fout));
    assertEquals(read(fin), read(fout));
  }
  
  @DataPoints public static File[] javaFiles() throws IOException {
    final Set<File> $ = new TreeSet<File>();
    new JavaFilesVisitor(".", new PlainFileOnlyAction() {
      @Override public void visitFile(final File f) {
        $.add(f);
      }
    }).go();
    return Iterables.toArray($, File.class);
  }
  
  @Theory public void fullTokenization(final File f) throws IOException {
    System.err.println("Testing " + f);
    assertEquals(read(f), TokenAsIs.fileToString(f));
  }
  
  public static String read(final File f) throws IOException {
    final char[] $ = new char[(int) f.length()];
    final FileReader x = new FileReader(f);
    final int n = x.read($);
    x.close();
    return new String(Arrays.copyOf($, n));
  }
  
  public static void write(final File f, final String text) throws IOException {
    final Writer w = new FileWriter(f);
    w.write(text);
    w.close();
  }
}
