package il.ac.technion.cs.ssdl.java;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;

import org.junit.Test;

@SuppressWarnings("static-method")
public class StringLiteralsTest {
  static Token toToken(final String s) {
    try {
      final RawTokenizer J = new RawTokenizer(new StringReader(s));
      final Token t = J.next();
      assertEquals(Token.EOF, J.next());
      return t;
    } catch (final IOException E) {
      return Token.EOF;
    }
  }
  
  @Test public void test_simple_literal() {
    final String s = "\"abcd\"";
    final Token t = toToken(s);
    assertEquals(Token.STRING_LITERAL, t);
  }
}
