/**
 *
 */
package il.ac.technion.cs.ssdl.xy;

/**
 * @author Yossi Gil
 * @since February 22, 2012
 */
public class WeightedDisribution {
  public WeightedDisribution record(final double x, final double w) {
    sumw += w;
    sum += w * x;
    sum2 += w * x * x;
    sumwlogw += w * Math.log(w);
    return this;
  }
  public double mean() {
    return sum / sumw;
  }
  public double mean2() {
    return sum2 / sumw;
  }
  public double var() {
    return mean2() - mean() * mean();
  }
  public double entropy() {
    return sumw * Math.log(sumw) - sumwlogw;
  }
  public void clear() {
    sum = sum2 = sumwlogw = 0;
  }

  private double sum = 0;
  private double sum2 = 0;
  private double sumw = 0;
  private double sumwlogw;
}
