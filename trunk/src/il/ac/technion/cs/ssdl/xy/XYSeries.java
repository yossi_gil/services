/**
 *
 */
package il.ac.technion.cs.ssdl.xy;

import static il.ac.technion.cs.ssdl.iteration.Iterables.seq;
import static il.ac.technion.cs.ssdl.misc.LinearAlgebra.add;
import static il.ac.technion.cs.ssdl.misc.LinearAlgebra.max;
import static il.ac.technion.cs.ssdl.misc.LinearAlgebra.product;
import il.ac.technion.cs.ssdl.misc.LinearAlgebra;
import il.ac.technion.cs.ssdl.xy.XYProcessor.Gatherer;
/**
 * @author Yossi Gil
 * @since Mar 1, 2012
 */
public class XYSeries {
  public final double[] x;
  public final double[] y;
  public final double[] dy;

  public XYSeries(final double[] x, final double[] y, final double[] dy) {
    this.x = x;
    this.y = y;
    this.dy = dy;
  }
  public XYSeries(final double[] x, final double[] y) {
    this(x, y, product(0, y));
  }
  public XYSeries(final Gatherer g) {
    this(g.xs(), g.ys(), g.dys());
  }
  public int n() {
    return y.length;
  }
  public XYSeries xshift(final double c) {
    return new XYSeries(add(c, x), y, dy);
  }
  public XYSeries yshift(final double c) {
    return new XYSeries(x, add(c, y), dy);
  }
  public XYSeries log() {
    final XYProcessor.RealsOnly p = new XYProcessor.RealsOnly();
    p.feed(LinearAlgebra.log(x), LinearAlgebra.log(y), dLogY());
    return new XYSeries(p);
  }

  private double[] dLogY() {
    final double $[] = new double[y.length];
    for (int i = 0; i < y.length; i++)
      $[i] = (Math.log(y[i]+dy[i]) - Math.log(y[i]-dy[i]))/2;
    return $;
  }
  public static XYSeries histogram(final double[] y, final double[] dy) {
    return new XYSeries(seq(y.length), y, dy);
  }
  public static XYSeries histogram(final int[] y) {
    return histogram(LinearAlgebra.promote(y));
  }
  public static XYSeries histogram(final double[] y) {
    return new XYSeries(seq(y.length), y);
  }

  public XYSeries scale(final double newMaxY) {
    return scale(max(y), newMaxY);
  }
  private XYSeries scale(final double oldMaxY, final double newMaxY) {
    for (int i = 0; i < n(); i++) {
      y[i] = y[i] * newMaxY / oldMaxY;
      dy[i] = dy[i] * newMaxY / oldMaxY;
    }
    return this;
  }



}
