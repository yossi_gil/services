/**
 *
 */
package il.ac.technion.cs.ssdl.xy;

import il.ac.technion.cs.ssdl.bench.Log;

/**
 * @author Yossi Gil
 * @since February 21, 2012
 */
public final class XYLogger extends XYProcessor.Vacuous {
  @Override public void p(final double x, final double y, final double dy) {
    Log.ln(x + "\t" + y + "\t" + dy);
  }
  @Override public void p(final double x, final double y) {
    Log.ln(x + "\t" + y);
  }
  @Override public void p(final int x, final int y) {
    Log.ln(x + "\t" + y);
  }
  @Override public void done() {
    Log.flush();
  }
}