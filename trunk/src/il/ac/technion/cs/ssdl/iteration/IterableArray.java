/**
 * Copyright
 */
package il.ac.technion.cs.ssdl.iteration;

import il.ac.technion.cs.ssdl.iteration.Iterables.ReadonlyIterator;

/**
 * An array {@linkplain "http://en.wikipedia.org/wiki/Adapter_pattern Adapter"}
 * adjusting it to the {@link Iterable} interface.
 *
 * @author Yossi Gil
 * @since Oct 19, 2009
 * @param <T>
 *          type of objects in the array
 */
public class IterableArray<T> implements Iterable<T> {
  /**
   * Instantiate the adapter with an array
   *
   * @param ts
   *          the array of T objects over which we iterate
   */
  public IterableArray(final T[] ts) {
    this.ts = ts;
  }
  public int count() {
    return ts.length;
  }

  protected final T[] ts;

  @Override public ArrayIterator<T> iterator() {
    return new ArrayIterator<T>(ts);
  }

  public static class ArrayIterator<T> extends ReadonlyIterator<T> {
    public ArrayIterator(final T[] ts) {
      this.ts = ts;
    }
    @Override public boolean hasNext() {
      return current < ts.length;
    }
    @Override public T next() {
      return ts[current++];
    }

    private int current = 0;
    private final T[] ts;
  }
}
