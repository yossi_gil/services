/**
 *
 */
package il.ac.technion.cs.ssdl.iteration;

import static il.ac.technion.cs.ssdl.utils.DBC.unused;

/**
 * @param <T>
 *          Type over which we shall iterate * @author Yossi Gil
 * @since 01/05/2011
 */
public abstract class Iteration<T> {
  public void prolog(final T t) {
    unused(t);
  }
  public void prev(final T t, final T previous) {
    unused(previous);
    unused(t);
  }
  public void at(final T t) {
    unused(t);
  }
  public void next(final T t, final T next) {
    unused(t);
    unused(next);
  }
  public void epilog(final T t) {
    unused(t);
  }
}
