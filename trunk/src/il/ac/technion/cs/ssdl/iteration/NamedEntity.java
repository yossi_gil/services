package il.ac.technion.cs.ssdl.iteration;

public interface NamedEntity {
  /**
   * Which name is associated with this entity?
   * 
   * @return the name, if known, of this entity, or the empty string.
   */
  String name();
  
  static final String INVERTED = "'";
}
