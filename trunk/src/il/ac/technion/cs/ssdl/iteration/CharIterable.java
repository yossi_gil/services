/**
 *
 */
package il.ac.technion.cs.ssdl.iteration;

/**
 * @author Yossi Gil
 * @since 21 November 2011
 */
public interface CharIterable {
  public CharIterator iterator();
}
