package il.ac.technion.cs.ssdl.iteration;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {
  private T[] ts;
  private int index;
  private int end;
  
  public static <E> Iterator<E> make(final E[] es) {
    return make(es, 0);
  }
  public static <E> Iterator<E> make(final E[] es, final int begin) {
    return make(es, begin, es.length);
  }
  public static <E> Iterator<E> make(final E[] es, final int begin, final int end) {
    return new ArrayIterator<E>(es, begin, end);
  }
  public ArrayIterator(final T[] ts) {
    this(ts, 0);
  }
  public ArrayIterator(final T[] ts, final int begin) {
    this(ts, begin, ts.length);
  }
  public ArrayIterator(final T[] ts, final int begin, final int end) {
    this.ts = ts;
    index = begin;
    this.end = end;
  }
  @Override public boolean hasNext() {
    return index < end;
  }
  @Override public T next() {
    return ts[index++];
  }
  @Override public void remove() {
    throw new UnsupportedOperationException("ArrayIterator cannot remove " + "elements");
  }
}
