package il.ac.technion.cs.ssdl.iteration.closures;

/**
 * An interface representing a no parameters procedure
 */
public interface Procedure0 {
  /**
   * Execute the procedure
   * 
   * @return nothing
   */
  public Void eval();
}
