package il.ac.technion.cs.ssdl.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class Reflection {
  public static Class<?> typeOf(final Member m) {
    if (m instanceof Method)
      return ((Method) m).getReturnType();
    if (m instanceof Field)
      return ((Field) m).getType();
    return null;
  }
}
