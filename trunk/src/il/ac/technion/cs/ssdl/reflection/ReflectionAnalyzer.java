package il.ac.technion.cs.ssdl.reflection;

public enum ReflectionAnalyzer {
  ;
  /**
   * @param args
   *          command line arguments
   */
  public static void main(final String[] args) {
    class LocalClass {
      // Nothing here.
    }
    Explore.go(int[].class);
    Explore.go(void.class);
    Explore.go(java.lang.Object[].class);
    Explore.go(ReflectionAnalyzer.class);
    Explore.go(InnerClass.class);
    Explore.go(StaticInnerClass.class);
    Explore.go(LocalClass.class);
    Explore.go(new Object() {
      @Override public int hashCode() {
        return super.hashCode();
      }
      @Override public boolean equals(final Object other) {
        return super.equals(other);
      }
    }.getClass());
  }
  static String toBinary(final int value) {
    String $ = "";
    for (int mask = 1; mask != 0; mask <<= 1)
      $ += (mask & value) == 0 ? "" : "+" + mask;
    return $;
  }
  
  class InnerClass {
    // Nothing here.
  }
  
  class StaticInnerClass {
    // Nothing here.
  }
}

class A {
  class B {
    A f() {
      return A.this;
    }
  }
}
