package il.ac.technion.cs.ssdl.reflection;

import il.ac.technion.cs.ssdl.utils.As;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ClassPredicates {
  public static boolean isImmutable(final Class<?> c) {
    return !c.isEnum() && nFields(c) > 0 && !hasMutableFields(c);
  }
  public static int nFields(final Class<?> c) {
    int $ = 0;
    for (final Field f : c.getDeclaredFields())
      $ += As.binary(!f.isSynthetic() && !Modifier.isStatic(f.getModifiers()));
    final Class<?> parent = c.getSuperclass();
    return $ + (parent == null ? 0 : nFields(parent));
  }
  public static boolean hasMutableFields(final Class<?> c) {
    for (final Field f : c.getDeclaredFields())
      if (!Modifier.isFinal(f.getModifiers()))
        return true;
    final Class<?> parent = c.getSuperclass();
    return parent == null ? false : hasMutableFields(parent);
  }
}
