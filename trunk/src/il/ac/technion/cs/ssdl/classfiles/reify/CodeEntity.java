/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

/**
 * @author Yossi Gil
 * @since 26 November 2011
 */
public class CodeEntity {
  public CodeEntity(final int maxStack, final int maxLocals, final byte[] codes) {
    this.maxStack = maxStack;
    this.maxLocals = maxLocals;
    this.codes = codes;
    simplifiedCode = new SimplifiedCode(codes);
  }

  public int maxStack;
  public int maxLocals;
  public final byte[] codes;
  public final SimplifiedCode simplifiedCode;

  public int instructionsCount() {
    return simplifiedCode.instructionsCount();
  }

	public int throwCount() {
		return simplifiedCode.throwCount();
	}

  public int cyclomaticComplexity() {
    return simplifiedCode.cyclomaticComplexity();
  }
}
