/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

/**
 * @author Yossi Gil
 * @since 25 November 2011
 */
public class AttributedEntity extends NamedEntity {
  public AttributedEntity(final AttributeInfo[] attributes, final String name) {
    super(name);
    this.attributes = attributes;
  }
  public final boolean containsAttribute(final String attributeName) {
    return findAttribute(attributeName) != null;
  }
  public final AttributeInfo findAttribute(final String attributeName) {
    for (final AttributeInfo $ : attributes)
      if (attributeName.equals($.name))
        return $;
    return null;
  }
  
  protected final AttributeInfo[] attributes;
}
