/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import il.ac.technion.cs.ssdl.classfiles.RobustReader;
import il.ac.technion.cs.ssdl.classfiles.reify.ConstantPool.ClassConstant;

import java.io.DataInputStream;

/**
 * @author Yossi Gil
 * @since 26 November 2011
 */
public class ConstantPoolReader extends RobustReader {
  private final ConstantPool constantPool;

  public ConstantPoolReader(final DataInputStream inner, final ConstantPool constantPool) {
    super(inner);
    this.constantPool = constantPool;
  }
  public String readClassName() {
    return readClassName(readUnsignedShort());
  }
  public String readClassName(final int classIndex) {
    return classIndex == 0 ? null : constantPool.getClassName(classIndex);
  }

	public String classShortName(final int classIndex) {
		return classIndex == 0 ? null : constantPool
				.getShortClassName(classIndex);
	}

	public String classPackage(final int classIndex) {
		return classIndex == 0 ? null : constantPool
				.getPackage(classIndex);
	}
  public TypedEntity[] readMembers() {
    final TypedEntity[] $ = new TypedEntity[readUnsignedShort()];
    for (int i = 0; i < $.length; i++) {
      final int flags = readUnsignedShort(); // access flag
      final String memberName = readStringConstant(); // u2 name;
      final String descriptor = readStringConstant(); // u2 descriptionIndex;
      $[i] = new TypedEntity(constantPool, flags, memberName, descriptor, readAttributes());
    }
    return $;
  }
  public AttributeInfo[] readAttributes() {
    final AttributeInfo[] $ = new AttributeInfo[readUnsignedShort()];
    for (int i = 0; i < $.length; i++)
      $[i] = readAttribute();
    return $;
  }
  private AttributeInfo readAttribute() {
    final String attributeName = readStringConstant();
    return new AttributeInfo(attributeName, readBytesArrray());
  }
  public byte[] readBytesArrray() {
    return readBytes(new byte[readInt()]);
  }
  public String readStringConstant() {
    return constantPool.getUTF8(readUnsignedShort());
  }
  public ClassConstant[] readClasses() {
    final ClassConstant[] $ = new ClassConstant[readUnsignedShort()];
    for (int i = 0; i < $.length; i++)
      $[i] = readClassConstant();
    return $;
  }
  public ClassConstant readClassConstant() {
    return constantPool.getClassConstant(readUnsignedShort());
  }
}
