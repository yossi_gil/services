// <a href=http://ssdl-linux.cs.technion.ac.il/wiki/index.php>SSDLPedia</a>
package il.ac.technion.cs.ssdl.classfiles;

import il.ac.technion.cs.ssdl.stereotypes.Utility;
import il.ac.technion.cs.ssdl.utils.Separate;

import java.io.File;

@Utility public enum EXTENSIONPATH {
  ;
  private static final String JAVA_EXTENSION_DIRECTORIES = "java.ext.dirs";
  
  public static String[] asArray() {
    return System.getProperty(JAVA_EXTENSION_DIRECTORIES).split(File.pathSeparator);
  }
  /**
   * Exercise this class, by printing the result of its principal function.
   * 
   * @param _
   *          unused
   */
  public static void main(final String[] _) {
    System.out.println(Separate.by(asArray(), "\n"));
  }
}
