package il.ac.technion.cs.ssdl.classfiles.reify;

import il.ac.technion.cs.ssdl.utils.Separate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class TypeInfo {
  public static TypeInfo makePrimitiveType(final String name) {
    return new AtomicType(true, name);
  }
  public static TypeInfo makeReferenceType(final String name) {
    return new AtomicType(false, name);
  }
  public static TypeInfo makeArrayOf(final TypeInfo t) {
    return new ArrayType(t);
  }
  public static TypeInfo makeInitializer(@SuppressWarnings("unused") final TypeInfo t) {
    return new InitializerType();
  }
  public static TypeInfo makeConstructor(final TypeInfo t) {
    return new ConstructorType(((MethodType) t).arguments);
  }
  public static TypeInfo makeMethodType(final TypeInfo returnValue, final TypeInfo... arguments) {
    return new MethodType(returnValue, arguments);
  }
  public abstract Collection<TypeInfo> components();
  public abstract boolean isPrimitive();
  @Override public abstract String toString();
  
  public final static class AtomicType extends TypeInfo {
    AtomicType(final String name) {
      this(true, name);
    }
    AtomicType(final boolean isPrimitive, final String name) {
      this.name = name;
      this.isPrimitive = isPrimitive;
    }
    
    final String name;
    final boolean isPrimitive;
    
    @Override public Collection<TypeInfo> components() {
      final ArrayList<TypeInfo> $ = new ArrayList<TypeInfo>(1);
      $.add(this);
      return $;
    }
    @Override public String toString() {
      return name;
    }
    @Override public boolean isPrimitive() {
      return isPrimitive;
    }
  }
  
  public static class ArrayType extends TypeInfo {
    public ArrayType(final TypeInfo inner) {
      this.inner = inner;
    }
    
    private final TypeInfo inner;
    
    @Override public Collection<TypeInfo> components() {
      return inner.components();
    }
    @Override public String toString() {
      return inner.toString() + "[]";
    }
    @Override public boolean isPrimitive() {
      return false;
    }
  }
  
  public static class InitializerType extends TypeInfo {
    @Override public Collection<TypeInfo> components() {
      return new ArrayList<TypeInfo>();
    }
    @Override public final boolean isPrimitive() {
      return false;
    }
    @Override public String toString() {
      return "()";
    }
  }
  
  public static class ConstructorType extends InitializerType {
    public ConstructorType(final TypeInfo[] arguments) {
      this.arguments = arguments;
    }
    
    public final TypeInfo[] arguments;
    
    @Override public Collection<TypeInfo> components() {
      final List<TypeInfo> $ = new ArrayList<TypeInfo>();
      for (final TypeInfo a : arguments)
        $.addAll(a.components());
      return $;
    }
  }
  
  public static class MethodType extends ConstructorType {
    public final TypeInfo returnValue;
    
    public MethodType(final TypeInfo returnValue, final TypeInfo[] arguments) {
      super(arguments);
      this.returnValue = returnValue;
    }
    @Override public Collection<TypeInfo> components() {
      final List<TypeInfo> $ = new ArrayList<TypeInfo>(returnValue.components());
      $.addAll(super.components());
      return $;
    }
    @Override public String toString() {
      return returnValue.toString() + " (" + Separate.by(arguments, ", ") + ")";
    }
  }
}