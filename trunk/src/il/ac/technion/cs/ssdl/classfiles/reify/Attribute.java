/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yossi Gil
 * @since 28 November 2011
 */
@Retention(RUNTIME) public @interface Attribute {
  public class Content {
    /**
     * Instantiate {@link Content}.
     *
     * @param value
     * @param name
     */
    public Content(final String name, final String value) {
      this.name = name;
      this.value = value;
    }

    public final String name;
    public final String value;
  }

  public class Extractor {
    public static List<Content> attributes(final Object target) {
      final List<Content> $ = new ArrayList<Content>();
      for (final Method m : target.getClass().getMethods())
        if (isAttribute(m))
          $.add(new Content(m.getName(), value(target, m)));
      return $;
    }
    private static String value(final Object target, final Method m) {
      try {
        return m.invoke(target) + "";
      } catch (final IllegalArgumentException e) {
        return "IllegalArgument: " + e.getMessage();
      } catch (final IllegalAccessException e) {
        return "IllegalAccess: " + e.getMessage();
      } catch (final InvocationTargetException e) {
        return "Exception in call: " + e.getMessage();
      }
    }
    private static boolean isAttribute(final Method m) {
      return null != m.getAnnotation(Attribute.class);
    }
  }
}
