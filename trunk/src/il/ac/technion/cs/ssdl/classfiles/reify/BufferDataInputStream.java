package il.ac.technion.cs.ssdl.classfiles.reify;

import java.io.DataInputStream;

public final class BufferDataInputStream extends DataInputStream {
  public BufferDataInputStream(final byte[] bytes) {
    this(new BufferInputStream(bytes));
  }
  public BufferDataInputStream(final BufferInputStream inner) {
    super(inner);
    this.inner = inner;
  }
  @Override public long skip(final long n) {
    final long $ = inner.skip(n);
    if ($ != n)
      throw new RuntimeException();
    return $;
  }
  public final int position() {
    return inner.position();
  }
  public boolean eof() {
    return inner.eof();
  }
  @Override public int read() {
    return inner.read();
  }
  
  private final BufferInputStream inner;
  
  public void align4() {
    if (position() % 4 == 0)
      return;
    skip(4 - position() % 4);
  }
}