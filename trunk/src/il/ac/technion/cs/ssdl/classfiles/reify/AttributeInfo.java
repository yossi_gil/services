/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public class AttributeInfo {
  public AttributeInfo(final String name, final byte[] data) {
    this.name = name;
    this.data = data;
  }
  
  public final String name;
  final byte[] data;
  
  final ConstantPoolReader reader(final ConstantPool constantPool) {
    return new ConstantPoolReader(new DataInputStream(new ByteArrayInputStream(data)), constantPool);
  }
}