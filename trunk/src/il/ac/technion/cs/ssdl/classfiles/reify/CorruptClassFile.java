/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

/**
 * @author Yossi Gil
 * @since 25 November 2011
 */
public class CorruptClassFile extends RuntimeException {
  public CorruptClassFile(final Exception e) {
    super(e);
    fillInStackTrace();
  }
  
  private static final long serialVersionUID = 1L;
}
