package il.ac.technion.cs.ssdl.classfiles;

import static il.ac.technion.cs.ssdl.utils.DBC.unused;
import il.ac.technion.cs.ssdl.files.visitors.ClassFilesVisitor;
import il.ac.technion.cs.ssdl.files.visitors.FileSystemVisitor;
import il.ac.technion.cs.ssdl.files.visitors.FileSystemVisitor.Action.StopTraversal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class AllClasses {
  public static void main(final String[] args) throws IOException, StopTraversal {
    for (final String root : CLASSPATH.asArray())
      new ClassFilesVisitor(root, new FileSystemVisitor.FileOnlyAction() {
        @Override public void visitFile(final File f) {
          System.out.println(Filename.path2class(f.getAbsolutePath(), root));
        }
        @Override public void visitZipEntry(final String entryName, final InputStream _) {
          unused(_);
          System.out.println(Filename.path2class(entryName, root));
        }
      }).go();
  }
}
