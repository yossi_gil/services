package il.ac.technion.cs.ssdl.classfiles;

import static il.ac.technion.cs.ssdl.strings.StringUtils.esc;

import java.util.TreeSet;

/**
 * A representation of the system global CLASSPATH.
 * 
 * @author Yossi Gil
 * @since 12/07/2007
 */
public enum Properties {
  ;
  public static void main(final String[] args) {
    final TreeSet<String> t = new TreeSet<String>();
    t.addAll(System.getProperties().stringPropertyNames());
    for (final String key : t)
      System.out.println(key + ": " + esc(System.getProperty(key)));
  }
}
