/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import il.ac.technion.cs.ssdl.classfiles.reify.ClassInfo.LinkComponents;
import il.ac.technion.cs.ssdl.classfiles.reify.ConstantPool.ClassConstant;
import il.ac.technion.cs.ssdl.classfiles.reify.ConstantPool.FieldReference;
import il.ac.technion.cs.ssdl.classfiles.reify.ConstantPool.MemberReference;
import il.ac.technion.cs.ssdl.classfiles.reify.OpCode.Instruction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Yossi Gil
 * @since 21 November 2011
 */
public class ExecutableEntity extends TypedEntity {
  public ExecutableEntity(final ConstantPool constantPool, final int accessFlags, final String name, final String descriptor,
      final AttributeInfo[] attributes) {
    super(constantPool, accessFlags, name, descriptor, attributes);
    exceptions = readExceptions();
    code = readCodeAttribute();
  }
  public ExecutableEntity(final TypedEntity t) {
    super(t.constantPool, t.flags, t.name, t.descriptor, t.attributes);
    exceptions = readExceptions();
    code = readCodeAttribute();
  }
  private ClassConstant[] readExceptions() {
    final AttributeInfo $ = findAttribute("Exceptions");
    return $ == null ? new ClassConstant[0] : $.reader(constantPool).readClasses();
  }
  public ExecutableEntity(final ConstantPool constantPool, final int flags, final String name, final TypeInfo t,
      final String descriptor, final AttributeInfo[] attributes) {
    super(constantPool, flags, name, t, descriptor, attributes);
    exceptions = readExceptions();
  }

  public final ClassConstant[] exceptions;

  private CodeEntity readCodeAttribute() {
    final AttributeInfo $ = findAttribute("Code");
    return $ == null ? null : readCodeAttribute($);
  }
  private CodeEntity readCodeAttribute(final AttributeInfo a) {
    final ConstantPoolReader r = a.reader(constantPool);
    final int maxStack = r.readUnsignedShort();
    final int maxLocals = r.readUnsignedShort();
		return new CodeEntity(maxStack, maxLocals, r.readBytesArrray());
  }
  public int codeSize() {
    return code == null ? 0 : code.codes.length;
  }

  CodeEntity code;

  public CodeEntity getCode() {
    return code;
  }
  public int instructionCount() {
    return code == null ? 0 : code.instructionsCount();
  }

	public int throwCount() {
		return code == null ? 0 : code.throwCount();
	}

  public Set<String> instanceVariables(){
    final Set<String> $ = new HashSet<String>();
    if (code == null)
      return $;
    for (int index = 0; index < code.simplifiedCode.instructions().size(); index++) {
      final Instruction i = code.simplifiedCode.instructions().get(index);
      if(i.opCode == OpCode.GETFIELD || i.opCode == OpCode.PUTFIELD) {
        final int cpIndex = i.args()[0] << 8 | i.args()[1];
        final FieldReference fr = constantPool.getFieldReference(cpIndex);
        $.add(fr.getClassConstant().getClassName() + ":" + fr.getNameAndType());
      }
    }
    return $;
  }

  public int cyclomaticComplexity() {
    return code == null ? 0 : code.cyclomaticComplexity();
  }

	@Attribute
	public int referencedFieldsCount() {
		return instanceVariables().size();
	}

  Map<String, int[]> class2refsByComponents = null;
  Map<String, int[]> class2staticRefsByComponents = null;

  public int[] referencesToClass(final String className) {
    if (class2refsByComponents == null)
      referencesToClasses();
    return class2refsByComponents.get(className);
  }



  public Map<String, int[]> referencesToAllClasses() {
    if (class2refsByComponents == null)
      referencesToClasses();
    return class2refsByComponents;
  }

  private void referencesToClasses() {
    class2refsByComponents = new HashMap<String, int[]>();
    for (final TypeInfo t : type.components()){
      final int[] refsByComponents = getClassRefsByComponents(t.toString());
      refsByComponents[LinkComponents.MethodDeclaration.ordinal()]++;
    }
    if (code == null)
      return;
    for (int index = 0; index < code.simplifiedCode.instructions().size(); index++) {
      final Instruction i = code.simplifiedCode.instructions().get(index);
      final int cpIndex = i.args()[0] << 8 | i.args()[1];
      int component = -1;
	  switch (i.opCode) {
	  case NEW:
        final int[] refsByComponents = getClassRefsByComponents(constantPool.getClassName(cpIndex));
        refsByComponents[LinkComponents.Instantiation.ordinal()]++;
        continue;
      case GETSTATIC:
      case PUTSTATIC:
        component = LinkComponents.StaticFieldAccess.ordinal();
        break;
      case GETFIELD:
      case PUTFIELD:
        component = LinkComponents.FieldAccess.ordinal();
        break;
      case INVOKEVIRTUAL:
      case INVOKESPECIAL:
      case INVOKEINTERFACE:
      case INVOKEDYNAMIC:
        component = LinkComponents.MethodInvocation.ordinal();
        break;
      case INVOKESTATIC:
        component = LinkComponents.StaticMethodInvocation.ordinal();
        break;
      default:
        continue;

      }
      assert component != -1;
      final MemberReference mr = constantPool.getMemberReference(cpIndex);
      if (!mr.getNameAndType().getName().equals("<init>")) {
        final int[] refsByComponents = getClassRefsByComponents(mr.getClassConstant().getClassName());
        refsByComponents[component]++;
      }
      for (final TypeInfo t : decode(mr.getNameAndType().getDescriptor()).components()) {
        final int[] refsByComponents = getClassRefsByComponents(t.toString());
        refsByComponents[component]++;
      }
    }
  }

  private int[] getClassRefsByComponents(final String className) {
    int[] $ = class2refsByComponents.get(className);
    if ($ == null) {
      $ = new int[LinkComponents.values().length];
      class2refsByComponents.put(className, $);
    }
    return $;
  }


  private static String signature(final String n, final String d) {
    return n + ":" + decode(d);
  }
  public static String signature(final String className, final String n, final String d) {
    return className + "." + signature(n, d);
  }
  public String signature() {
    return signature(name, descriptor);
  }

  public Set<String> getReferencedMethods() {
	final Set<String> $ = new HashSet<String>();
		if (code == null)
			return $;
    for (int index = 0; index < code.simplifiedCode.instructions().size(); index++) {
      final Instruction i = code.simplifiedCode.instructions().get(index);
      int cpIndex;
      if(!i.isInvokeInstruction())
    	 continue;
      cpIndex = i.args()[0] << 8 | i.args()[1];
      final MemberReference mr = constantPool.getMemberReference(cpIndex);
      $.add(signature(mr.getClassConstant().getClassName(), mr.getNameAndType().getName(), mr.getNameAndType().getDescriptor()));
    }
    return $;
  }

	public boolean isAccessed(final TypedEntity t, final String thisClassName) {
		if (code == null)
			return false;
	  for (int index = 0; index < code.simplifiedCode.instructions().size(); index++) {
	      final Instruction i = code.simplifiedCode.instructions().get(index);
			if (!i.isFieldAccessInstruction() || i.isInvokeInstruction())
	    	 continue;
			if (isAccessed(t, thisClassName, i))
				return true;
	    }
		return false;
  }

	private boolean isAccessed(final TypedEntity t, final String thisClassName,	final Instruction i) {
		final int cpIndex = i.args()[0] << 8 | i.args()[1];
		final MemberReference mr = constantPool.getMemberReference(cpIndex);
		if (mr.getNameAndType().getName().equals(t.name)
				&& mr.getNameAndType().getDescriptor().equals(t.descriptor)
				&& mr.getClassConstant().getClassName().endsWith(thisClassName))
			return true;
		return false;

	}

}
