/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import static il.ac.technion.cs.ssdl.utils.Box.box;
import static il.ac.technion.cs.ssdl.utils.Unbox.unbox;
import il.ac.technion.cs.ssdl.classfiles.RobustReader;
import il.ac.technion.cs.ssdl.collections.IntsArray;

import java.util.ArrayList;

/**
 * A representation of an entry in the constant pool array.
 *
 * @author Yossi Gil
 */
public final class ConstantPool {
  /**
   * Which other classes does this class refer to?
   *
   * @return an array with names of all classes that this class uses
   */
  public String[] getReferencedClasses() {
    final ArrayList<String> $ = new ArrayList<String>();
    for (final Constant c : pool)
      if (c instanceof ClassConstant && c.toString() != null)
        $.add(c.toString());
    return $.toArray(new String[$.size()]);
  }

  public int[] getReferencedClassesIndices() {
    final IntsArray $ = new IntsArray();
		for (int i = 0; i < pool.length; i++)
			if (pool[i] instanceof ClassConstant && pool[i].toString() != null)
        $.push(i);
    return $.toArray();
	}

  public String[] getReferencedMethods() {
    final ArrayList<String> $ = new ArrayList<String>();
    for (final Constant c : pool)
      if (c instanceof MethodReference &&
          !((MethodReference) c).getNameAndType().getName().equals("<init>") && c.toString() != null)
        $.add(((MethodReference) c).getClassConstant().getClassName() + ":" + ((MethodReference) c).getNameAndType().getName());
    return $.toArray(new String[$.size()]);
  }
  /**
   * Which {@link String}s does this class refer to?
   *
   * @return an array with all {@link String}s that this class uses
   */
  public String[] getReferencedStrings() {
    final ArrayList<String> $ = new ArrayList<String>();
    for (final Constant c : pool)
      if (c instanceof StringConstant)
        $.add(c.toString());
    return $.toArray(new String[$.size()]);
  }
  /**
   * Which <code><b>int</b></code>s are found in this class's constants' pool?
   *
   * @return an array with <code><b>int</b></code>s that this class uses.
   */
  public int[] getReferencedInts() {
    final ArrayList<Integer> $ = new ArrayList<Integer>();
    for (final Constant c : pool)
      if (c instanceof IntLiteral)
        $.add(box(((IntLiteral) c).value));
    return unbox($.toArray(new Integer[$.size()]));
  }
  /**
   * Which <code><b>long</b></code>s are found in this class's constants' pool?
   *
   * @return an array with <code><b>long</b></code>s that this class uses.
   */
  public long[] getReferencedLongs() {
    final ArrayList<Long> $ = new ArrayList<Long>();
    for (final Constant c : pool)
      if (c instanceof LongLiteral)
        $.add(box(((LongLiteral) c).value));
    return unbox($.toArray(new Long[$.size()]));
  }

  private final Constant[] pool;

  public ConstantPool(final RobustReader reader) {
    pool = new Constant[reader.readUnsignedShort()];
    for (int i = 1; i < pool.length; i++)
      if ((pool[i] = readConstant(reader)).isDoubleLength())
        pool[++i] = Empty.$$;
  }
  /**
   * Which <code><b>float</b></code>s are found in this class's constants' pool?
   *
   * @return an array with <code><b>float</b></code>s that this class uses.
   */
  public float[] getReferencedFloats() {
    final ArrayList<Float> $ = new ArrayList<Float>();
    for (final Constant c : pool)
      if (c instanceof FloatLiteral)
        $.add(box(((FloatLiteral) c).value));
    return unbox($.toArray(new Float[$.size()]));
  }
  /**
   * Which <code><b>double</b></code>s are found in this class's constants'
   * pool?
   *
   * @return an array with <code><b>double</b></code>s that this class uses.
   */
  public double[] getReferencedDoubles() {
    final ArrayList<Double> $ = new ArrayList<Double>();
    for (final Constant c : pool)
      if (c instanceof DoubleLiteral)
        $.add(box(((DoubleLiteral) c).value));
    return unbox($.toArray(new Double[$.size()]));
  }
  /**
   * Which <code><b>UTF8</b></code>s are found in this class's constants' pool?
   *
   * @return an array with <code><b>UTF8</b></code>s that this class uses.
   */
  public String[] getReferencedUTF8() {
    final ArrayList<String> $ = new ArrayList<String>();
    for (final Constant c : pool)
      if (c instanceof UTF8)
        $.add(((UTF8) c).value);
    return $.toArray(new String[$.size()]);
  }
  /**
   * @param classIndex
   * @return name of class stored in this location
   */
  public String getClassName(final int classIndex) {
    return ((ClassConstant) pool[classIndex]).getClassName();
  }

	public String getShortClassName(final int classIndex) {
		return ((ClassConstant) pool[classIndex]).getShortClassName();
	}

	public String getPackage(final int classIndex) {
		return ((ClassConstant) pool[classIndex]).getPackage();
	}

  public MemberReference getMemberReference(final int classIndex) {
    return (MemberReference) pool[classIndex];
  }

  public FieldReference getFieldReference(final int classIndex) {
    return (FieldReference) pool[classIndex];
  }

  public String getUTF8(final int a) {
    return asUTF8(pool[a]).value;
  }
  private static UTF8 asUTF8(final Constant c) {
    try {
      return (UTF8) c;
    } catch (final ClassCastException e) {
      throw new CorruptClassFile(e);
    }
  }
  ClassConstant getClassConstant(final int classIndex) {
    return (ClassConstant) pool[classIndex];
  }
  /**
   * Read the next constant pool entry from a given stream
   *
   * @param reader
   * @return the next constant pool entry found in the given stream
   */
  private Constant readConstant(final RobustReader reader) {
    final int//
    CONSTANT_UTF8 = 1, //
    CONSTANT_INTEGER = 3, //
    CONSTANT_FLOAT = 4, //
    CONSTANT_LONG = 5, //
    CONSTANT_DOUBLE = 6, //
    CONSTANT_CLASS = 7, //
    CONSTANT_STRING = 8, //
    CONSTANT_FIELDREF = 9, //
    CONSTANT_METHODREF = 10, //
    CONSTANT_INTERFACE_METHODREF = 11, //
    CONSTANT_NAME_AND_TYPE = 12; //
    int b;
    switch (b = reader.readUnsignedByte()) {
      case CONSTANT_CLASS:
        return new ClassConstant(reader.readUnsignedShort());
      case CONSTANT_FIELDREF:
        return new FieldReference(reader.readUnsignedShort(), reader.readUnsignedShort());
      case CONSTANT_METHODREF:
        return new MethodReference(reader.readUnsignedShort(), reader.readUnsignedShort());
      case CONSTANT_INTERFACE_METHODREF:
        return new InterfaceMethodReference(reader.readUnsignedShort(), reader.readUnsignedShort());
      case CONSTANT_STRING:
        return new StringConstant(reader.readUnsignedShort());
      case CONSTANT_INTEGER:
        return new IntLiteral(reader.readInt());
      case CONSTANT_FLOAT:
        return new FloatLiteral(reader.readFloat());
      case CONSTANT_LONG:
        return new LongLiteral(reader.readLong());
      case CONSTANT_DOUBLE:
        return new DoubleLiteral(reader.readDouble());
      case CONSTANT_NAME_AND_TYPE:
        return new NameAndTypeConstant(reader.readUnsignedShort(), reader.readUnsignedShort());
      case CONSTANT_UTF8:
        return new UTF8(reader.readUTF());
      default:
        System.out.print("Unfamiliar field identifier = " + b);
        return null;
    }
  }

  public static abstract class Constant {
    /**
     * Does this entry occupy double space?
     *
     * @return <code><b>true</b></code> <i>iff</i> this instance occupies two
     *         32-bit words
     */
    @SuppressWarnings("static-method")//
    public boolean isDoubleLength() {
      return false;
    }
    /**
     * What is the type of this constant?
     *
     * @return a textual representation of the type of this instance
     */
    public abstract String typeName();
  }

  /**
   * A representation of a literal in the constants' pool
   *
   * @author Yossi Gil
   */
  public abstract static class Literal extends Constant {
    // Empty
  }

  /**
   * A representation of a UTF8 literal in the constants' pool
   *
   * @author Yossi Gil
   */
  public class UTF8 extends Literal {
    /**
     * Actual content of this instance
     */
    public final String value;

    /**
     * Creates an instance for the specified string.
     *
     * @param string
     *          wrapped string.
     */
    public UTF8(final String string) {
      value = string;
    }
    @Override public String toString() {
      return value;
    }
    @Override public String typeName() {
      return "UTF8";
    }
  }

  /**
   * A representation of an <code><b>int</b></code> literal in the constants'
   * pool
   *
   * @author Yossi Gil
   */
  public static final class IntLiteral extends Literal {
    /**
     * Actual content of this instance
     */
    public final int value;

    /**
     * Creates an instance for the specified <code><b>int</b></code> value.
     *
     * @param value
     *          the value to be stored in this constants pool item
     */
    public IntLiteral(final int value) {
      this.value = value;
    }
    @Override public String toString() {
      return "" + value;
    }
    @Override public String typeName() {
      return "INTEGER";
    }
  }

  /**
   * A representation of a <code><b>long</b></code> literal in the constants'
   * pool
   *
   * @author Yossi Gil
   */
  public final class LongLiteral extends Literal {
    /**
     * Actual content of this instance
     */
    public final long value;

    /**
     * Creates an instance for the specified <code><b>int</b></code> value.
     *
     * @param value
     *          the value to be stored in this constants pool item
     */
    public LongLiteral(final long value) {
      this.value = value;
    }
    @Override public String toString() {
      return "" + value;
    }
    @Override public String typeName() {
      return "DOUBLE";
    }
    @Override public boolean isDoubleLength() {
      return true;
    }
  }

  /**
   * A representation of a <code><b>float</b></code> literal in the constants'
   * pool
   *
   * @author Yossi Gil
   */
  public final class FloatLiteral extends Literal {
    /**
     * Actual content of this instance
     */
    public final float value;

    /**
     * Creates an instance for the specified <code><b>float</b></code> value.
     *
     * @param value
     *          the value to be stored in this constants pool item
     */
    public FloatLiteral(final float value) {
      this.value = value;
    }
    @Override public String toString() {
      return "CONSTANT_Float: " + value;
    }
    @Override public String typeName() {
      return "DOUBLE";
    }
  }

  /**
   * A representation of an unoccupied entry in the in memory representation of
   * the constants' pool table. Such an entry occurs whenever the previous entry
   * uses a double space on disk. The in-memory representation, which relies on
   * polymorphism, does not require allocation of multiple entries for one
   * constant, hence we have empty locations.
   * <p>
   * Implementation is a singleton realizing of the <b>Null Object</b> design
   * pattern
   *
   * @author Yossi Gil
   */
  public static final class Empty extends Literal {
    /**
     * A default, inaccessible constructor, forcing the singleton restriction
     */
    private Empty() {
      // Forbid instantiation by clients
    }

    /**
     * Singleton instance
     */
    public final static Empty $$ = new Empty();

    @Override public String typeName() {
      return "EMPTY";
    }
  }

  /**
   * A representation of a <code><b>long</b></code> literal in the constants'
   * pool
   *
   * @author Yossi Gil
   */
  public static final class DoubleLiteral extends Literal {
    /**
     * Actual content of this instance
     */
    public final double value;

    /**
     * Creates an instance for the specified <code><b>double</b></code> value.
     *
     * @param value
     *          the value to be stored in this constants pool item
     */
    public DoubleLiteral(final double value) {
      this.value = value;
    }
    @Override public String toString() {
      return "" + value;
    }
    @Override public String typeName() {
      return "DOUBLE";
    }
    @Override public boolean isDoubleLength() {
      return true;
    }
  }

  /**
   * An entry in the constant pool which is represented as a reference to
   * another such entry.
   *
   * @author Yossi Gil
   */
  public abstract class Reference extends Constant {
    /**
     * Where is the content of this entry stored.
     */
    public final int contentIndex;

    /**
     * Instantiate this class with a given constant pool reference
     *
     * @param contentIndex
     *          where is the content of this entry stored in the constants'
     *          pool?
     */
    public Reference(final int contentIndex) {
      this.contentIndex = contentIndex;
    }
    /**
     * Return a UTF8 representation of a specific entry in the constants' pool
     *
     * @param i
     *          where to look for this entry?
     * @return a UTF8 representation of the content of this entry
     */
    @SuppressWarnings("synthetic-access")//
    final String getUTF8(final int i) {
      return pool[i] instanceof UTF8 ? ((UTF8) pool[i]).value : null;
    }
    @Override public String toString() {
      return getUTF8(contentIndex);
    }
  }

  /**
   * A representation of a class constant in the constants' pool
   *
   * @author Yossi Gil
   */
  public class ClassConstant extends Reference {
    /**
     * Creates an instance for the specified index referring an {@link UTF8}
     *
     * @param nameIndex
     *          Index into constant pool
     */
    public ClassConstant(final int nameIndex) {
      super(nameIndex);
    }
    /**
     * What is the fully-qualified class name. In the case of an object array
     * only the class name of the object is returned.
     *
     * @return fully-qualified class name in standard notation with '.'.
     */
    public String getClassName() {
      String $ = getUTF8(contentIndex);
      if ($ == null)
        return null;
      $ = $.replaceFirst("^[\\[]+L", "");
      $ = $.replaceAll(";$", "");
      if ($.startsWith("["))
        return null;
      return $.replace('/', '.').replace('$', '.');
    }

		public String getShortClassName() {
			String $ = getUTF8(contentIndex);
			if ($ == null)
				return null;
			$ = $.replaceFirst("^[\\[]+L", "");
			$ = $.replaceAll(";$", "");
			if ($.startsWith("["))
				return null;
			return $.substring($.lastIndexOf('/') + 1).replace('$', '.');
		}

		public String getPackage() {
			String $ = getUTF8(contentIndex);
			if ($ == null)
				return null;
			$ = $.replaceFirst("^[\\[]+L", "");
			$ = $.replaceAll(";$", "");
			if ($.startsWith("["))
				return null;
			final int i = $.lastIndexOf('/');
			if (i == -1)
				return "";
			return $.substring(0, i).replace('/', '.');
		}

    @Override public String toString() {
      return getClassName();
    }
    @Override public String typeName() {
      return "CLASS";
    }

  }

  /**
   * A representation of a String constant in the constants' pool
   *
   * @author Yossi Gil
   */
  public class StringConstant extends Reference {
    /**
     * Creates an instance for the specified index. Constant pool. Needed for
     * resolving the reference.
     *
     * @param stringIndex
     *          index of an {@link UTF8} constant in the constants' pool
     */
    public StringConstant(final int stringIndex) {
      super(stringIndex);
    }
    @Override public String typeName() {
      return "STRING";
    }
  }

  /**
   * A representation of a name&type constant in the constants' pool
   *
   * @author Yossi Gil
   */
  public class NameAndTypeConstant extends Reference {
    /**
     * Where is the type descriptor of this entry stored
     */
    public final int descriptorIndex;

    /**
     * Creates an instance for the specified name and type or method descriptor.
     *
     * @param nameIndex
     *          Index of the name in the pool.
     * @param descriptorIndex
     *          Index of the type or method descriptor in the pool.
     */
    public NameAndTypeConstant(final int nameIndex, final int descriptorIndex) {
      super(nameIndex);
      this.descriptorIndex = descriptorIndex;
    }
    /**
     * @return the name.
     */
    public String getName() {
      return getUTF8(contentIndex);
    }
    /**
     * @return the type or method descriptor.
     */
    public String getDescriptor() {
      return getUTF8(descriptorIndex);
    }
    @Override public String toString() {
      return getName() + ", " + getDescriptor();
    }
    @Override public String typeName() {
      return "NAME&TYPE";
    }
  }

  /**
   * An representation of a reference to a class member in the constants' pool
   *
   * @author Yossi Gil
   */
  public abstract class MemberReference extends Constant {
    /**
     * Where is the class of this member stored?
     */
    public final int classIndex;
    /**
     * Where are the name and type descriptor of this member stored?
     */
    public final int nameAndTypeIndex;

    /**
     * Creates an instance for the specified class, name, and type. Constant
     * pool. Needed for resolving references.
     *
     * @param classIndex
     *          index of {@link ClassConstant}.
     * @param nameAndTypeIndex
     *          index of {@link NameAndTypeConstant}.
     */
    public MemberReference(final int classIndex, final int nameAndTypeIndex) {
      this.classIndex = classIndex;
      this.nameAndTypeIndex = nameAndTypeIndex;
    }
    /**
     * @return the class constant.
     */
    @SuppressWarnings("synthetic-access")//
    public ClassConstant getClassConstant() {
      return (ClassConstant) pool[classIndex];
    }
    /**
     * @return the name-and-type constant.
     */
    @SuppressWarnings("synthetic-access")//
    public NameAndTypeConstant getNameAndType() {
      return (NameAndTypeConstant) pool[nameAndTypeIndex];
    }
    @Override public final String toString() {
      return "Class = " + getClassConstant().getClassName() + //
          ", Name = " + getNameAndType().getName() + //
          ", Descriptor = " + getNameAndType().getDescriptor();
    }
  }

  /**
   * A representation of a method reference constant in the constants' pool
   *
   * @author Yossi Gil
   */
  public final class MethodReference extends MemberReference {
    /**
     * Instantiate with a given class name and name,type pair
     *
     * @param classIndex
     *          in which class is this method defined?
     * @param nameAndTypeIndex
     *          what is the name and type of this method?
     */
    public MethodReference(final int classIndex, final int nameAndTypeIndex) {
      super(classIndex, nameAndTypeIndex);
    }
    @Override public String typeName() {
      return "METHOD";
    }
  }

  /**
   * A representation of a field reference constant in the constants' pool
   *
   * @author Yossi Gil
   */
  public final class FieldReference extends MemberReference {
    /**
     * Instantiate with a given class name and name,type pair
     *
     * @param classIndex
     *          in which class is this field defined?
     * @param nameAndTypeIndex
     *          what is the name and type of this field?
     */
    public FieldReference(final int classIndex, final int nameAndTypeIndex) {
      super(classIndex, nameAndTypeIndex);
    }
    @Override public String typeName() {
      return "FIELD";
    }
  }

  /**
   * A representation of an interface method reference constant in the
   * constants' pool
   *
   * @author Yossi Gil
   */
  public final class InterfaceMethodReference extends MemberReference {
    /**
     * Instantiate with a given class name and name,type pair
     *
     * @param classIndex
     *          in which class is this interface method defined?
     * @param nameAndTypeIndex
     *          what is the name and type of this interface method?
     */
    public InterfaceMethodReference(final int classIndex, final int nameAndTypeIndex) {
      super(classIndex, nameAndTypeIndex);
    }
    @Override public String typeName() {
      return "INTERFACE METHOD";
    }
  }
}
