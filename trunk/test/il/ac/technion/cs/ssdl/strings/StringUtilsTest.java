package il.ac.technion.cs.ssdl.strings;

import static org.junit.Assert.assertEquals;
import il.ac.technion.cs.ssdl.utils.DBC;

import org.junit.Test;

@SuppressWarnings("static-method")//
public class StringUtilsTest {
  @Test(expected = DBC.Bug.Assertion.Value.NonNull.class) public final void testStripNull() {
    StringUtils.strip(null);
  }
  @Test(expected = DBC.Bug.Contract.Precondition.class) public final void testStripLength0() {
    StringUtils.strip(".");
  }
  @Test(expected = DBC.Bug.Contract.Precondition.class) public final void testStripLength1() {
    StringUtils.strip(".");
  }
  @Test(expected = DBC.Bug.Contract.Precondition.class) public final void testStripLength1Alternate() {
    StringUtils.strip("X");
  }
  @Test public final void testValidStrip() {
    assertEquals("ab", StringUtils.strip("xaby"));
    assertEquals("", StringUtils.strip("ab"));
    assertEquals("b", StringUtils.strip("Abc"));
  }
  @Test public void testToLowerSmallNumbers() {
    assertEquals("", StringUtils.lowCounter(-1));
    assertEquals("a", StringUtils.lowCounter(0));
    assertEquals("b", StringUtils.lowCounter(1));
  }
  @Test public void testToLowerLargeNumbers() {
    assertEquals("z", StringUtils.lowCounter(25));
    assertEquals("ba", StringUtils.lowCounter(26));
    assertEquals("bb", StringUtils.lowCounter(27));
  }
}
