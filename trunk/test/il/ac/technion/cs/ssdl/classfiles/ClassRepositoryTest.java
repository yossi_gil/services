package il.ac.technion.cs.ssdl.classfiles;

import static il.ac.technion.cs.ssdl.testing.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

/**
 * A class to test {@link ClassRepository} including path names with funny
 * UNICODE characters. If some of the text here is unreadable, you should change
 * encoding to UTF-8. In Eclipse this is done through the following menu chain:
 * Project/Properties/Info/Text File Encoding. In other versions, your may try:
 * Window -> Preferences -> General -> Workspace // - > text file encoding. Then
 * choose "Other", "UTF-8"
 *
 * @author Itay Maman Jul 11, 2006
 */
@SuppressWarnings("static-method")//
public class ClassRepositoryTest {
  // @Test public void testGetJRE() {
  // int total = 0;
  // int failures = 0;
  // final ClassRepository cpi = new ClassRepository.JRE();
  // final ClassLoader loader = new URLClassLoader(new URL[0],
  // getClass().getClassLoader());
  // for (final String s : cpi.getClasses()) {
  // total++;
  // try {
  // Class.forName(s, false,loader);
  // } catch (Throwable _) {
  // System.out.println(s);
  // failures++;
  // //
  // }
  // }
  // assertTrue("I failed to load " + failures + //
  // " classes. This indicates that something is " + //
  // "wrong with the environment", failures < total * 0.01);
  // }
  //
  // @Test public void testGetDEFAULT() {
  // int total = 0;
  // int failures = 0;
  // final ClassRepository cpi = new ClassRepository.DEFAULT();
  // final ClassLoader loader = new URLClassLoader(new URL[0],
  // getClass().getClassLoader());
  // for (final String s : cpi.getClasses())
  // {
  // total++;
  // try {
  // Class.forName(s, false, loader);
  // continue;
  // }
  // catch(Exception e)
  // {
  // System.err.println("Failed to load class '" + s + "'");
  // failures++;
  // }
  // }
  // System.out.println("DEFAULT size is " + total);
  // assertTrue("I failed to load " + failures + //
  // " classes ( out of " + total + " loadedThis indicates that something is "
  // + //
  // "wrong with the environment", failures < total * 0.01);
  // }
  /**
   * Compare the reflection-based information with the one obtained from the
   * Filename class.
   *
   * @param className
   *          Class name
   * @return true if the class was successfully loaded
   */
  private static boolean check(final String className) {
    try {
      final Class<?> c = Class.forName(className, false, ClassRepositoryTest.class.getClassLoader());
      assertEquals(className, c.isAnonymousClass(), Filename.isAnonymous(className));
      assertEquals(className, c.isLocalClass(), Filename.isLocal(className));
      assertEquals(className, c.getEnclosingClass() != null, Filename.isInner(className));
      assertEquals(className, c.getCanonicalName() == null, Filename.isAllInner(className));
      if (c.isMemberClass())
        assertEquals(c.getCanonicalName(), Filename.name2Canonical(className));
      else
        assertEquals(c.getCanonicalName(), className);
      return true;
    } catch (final ClassNotFoundException e) {
      return false;
    }
  }

  //
  // Sample classes (serve as input values)
  //
  public static class MyClass {
    // No body
  }

  @Test public void noDuplications() {
    final ClassRepository cr = new ClassRepository(ClassRepositoryTest.class);
    final Set<String> set = new HashSet<String>();
    for (final File f : cr.getRoots()) {
      final String abs = f.getAbsolutePath();
      assertFalse(abs, set.contains(abs));
      set.add(abs);
    }
  }
  public static void main(final String[] args) {
    final ClassRepository cr = new ClassRepository(ClassRepositoryTest.class);
    System.out.println(cr);
  }
}
