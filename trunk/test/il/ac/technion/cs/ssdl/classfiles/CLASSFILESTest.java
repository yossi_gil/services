package il.ac.technion.cs.ssdl.classfiles;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import il.ac.technion.cs.ssdl.utils.Separate;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("static-method") public class CLASSFILESTest {
  @Test public void testFindObject() {
    assertNotNull(CLASSFILES.open(Object.class));
  }
  @Test public void testFindString() {
    assertNotNull(CLASSFILES.open(String.class));
  }
  @Test public void testFindArray() {
    assertNull(CLASSFILES.open(String[].class));
  }
  @Test public void testFindPrimitive() {
    assertNull(CLASSFILES.open(int.class));
  }
  @Test public void testFindPrimitiveArray() {
    assertNull(CLASSFILES.open(int[].class));
  }
  @Test public void testFindVoid() {
    assertNull(CLASSFILES.open(void.class));
  }
  @Test public void testFindMe() {
    assertNotNull(CLASSFILES.open(this.getClass()));
  }
  @Test public void testFindAnonymous() {
    assertNotNull(CLASSFILES.open(new Object() {
      // Empty
    }.getClass()));
  }
  @Test public void testFindLocal() {
    class Local {
      // Empty
    }
    assertNotNull(CLASSFILES.open(new Local().getClass()));
  }
  
  class InnerClass {
    // Empty
  }
  
  @Test public void testFindInner() {
    assertNotNull(CLASSFILES.open(new InnerClass().getClass()));
  }
  
  class StaticInnerClass {
    // Empty
  }
  
  @Test public void testFindStaticInner() {
    assertNotNull(CLASSFILES.open(new StaticInnerClass().getClass()));
  }
  
  interface InnerInterface {
    // Empty
  }
  
  @Test public void testFindInnerInterface() {
    assertNotNull(CLASSFILES.open(InnerInterface.class));
  }
  
  enum InnerEnum {
    // Empty
  }
  
  @Test public void testFindInnerEnum() {
    assertNotNull(CLASSFILES.open(InnerEnum.class));
  }
  
  enum InnerEnumValues {
    A() {
      @Override public String toString() {
        return "My value";
      }
    },
    B() {
      @Override public String toString() {
        return "My value";
      }
    };
  }
  
  @Test public void testFindInnerEnumValue() {
    assertNotNull(CLASSFILES.open(InnerEnumValues.A.getClass()));
    assertNotNull(CLASSFILES.open(InnerEnumValues.B.getClass()));
    assertNotNull(CLASSFILES.open(InnerEnumValues.class));
  }
  
  static @interface Annotation {
    // Empty
  }
  
  @Test public void testFindInnerAnnotation() {
    assertNotNull(CLASSFILES.open(Annotation.class));
  }
  @Test public void testFindJunitTest() {
    assertNotNull(CLASSFILES.open(Test.class));
  }
  @Test public void testFindJunitAssert() {
    assertNotNull(CLASSFILES.open(Assert.class));
  }
  @Test public void testFindStringBuilder() {
    assertNotNull(CLASSFILES.open(StringBuilder.class));
  }
  @Test public void testIOException() {
    assertNotNull(CLASSFILES.open(IOException.class));
  }
  @Test public void testSeparate() {
    assertNotNull(CLASSFILES.open(Separate.class));
  }
  @Test public void testMeArray() {
    assertNull(CLASSFILES.open(CLASSFILESTest[].class));
  }
}
