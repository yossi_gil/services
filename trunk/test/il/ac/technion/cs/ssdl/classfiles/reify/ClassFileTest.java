package il.ac.technion.cs.ssdl.classfiles.reify;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import il.ac.technion.cs.ssdl.bench.Stopper;
import il.ac.technion.cs.ssdl.bench.Unit;
import il.ac.technion.cs.ssdl.classfiles.CLASSFILES;
import il.ac.technion.cs.ssdl.classfiles.CLASSPATH;
import il.ac.technion.cs.ssdl.classfiles.RobustReader;
import il.ac.technion.cs.ssdl.classfiles.reify.ClassInfo.Abstraction;
import il.ac.technion.cs.ssdl.classfiles.reify.ClassInfo.Builder;
import il.ac.technion.cs.ssdl.classfiles.reify.ClassInfo.Kind;
import il.ac.technion.cs.ssdl.external.External;
import il.ac.technion.cs.ssdl.files.visitors.ClassFilesVisitor;
import il.ac.technion.cs.ssdl.files.visitors.FileSystemVisitor.Action.StopTraversal;
import il.ac.technion.cs.ssdl.files.visitors.FileSystemVisitor.FileOnlyAction;
import il.ac.technion.cs.ssdl.utils.Box;
import il.ac.technion.cs.ssdl.utils.____;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.Random;

import org.junit.Test;

@SuppressWarnings("static-method") public class ClassFileTest {
  private static final String GOOD_FILE_PATH = "bin/il/ac/technion/cs/ssdl/classfiles/reify/ClassFileTest.class";
  private static final String BAD_FILE_PATH = "asdfasdfaafasdfas";

  @Test public void badFile() {
    final File f = new File(BAD_FILE_PATH);
    assertNotNull(f);
  }
  @Test(expected = FileNotFoundException.class) public void badFileInputStream() throws FileNotFoundException {
    final File f = new File(BAD_FILE_PATH);
    assertNotNull(new FileInputStream(f));
  }
  @Test public void classInfoFromBadPath() {
    assertNull(ClassInfo.make(BAD_FILE_PATH));
  }
  @Test public void classInfoFromBadPathCLASSFILE() {
    assertNull(ClassInfo.make(BAD_FILE_PATH));
  }
  @Test public void badInputStream() {
    assertNull(ClassInfo.make(BAD_FILE_PATH));
  }
  @Test public void openBadPath() {
    assertNull(ClassInfo.make(BAD_FILE_PATH));
  }
  @Test public void fileFromPath() {
    assertNotNull(new File(GOOD_FILE_PATH));
  }
  @Test public void inputStreamFromPath() throws FileNotFoundException {
    assertNotNull(new FileInputStream(new File(GOOD_FILE_PATH)));
  }
  @Test public void dataInputStreamFromPath() throws FileNotFoundException {
    assertNotNull(new DataInputStream(new FileInputStream(new File(GOOD_FILE_PATH))));
  }
  @Test public void readFromDataInputStreamFromPath() throws IOException {
    final DataInputStream f = new DataInputStream(new FileInputStream(new File(GOOD_FILE_PATH)));
    assertEquals(ClassInfo.Builder.MAGIC, f.readInt());
    f.close();
  }
  @Test public void readFromNoExceptionsReaderFromGoodPath() {
    assertEquals(ClassInfo.Builder.MAGIC, new RobustReader(GOOD_FILE_PATH).readInt());
  }
  @Test public void readFromNoExceptionsReaderFromBadPath() {
    assertEquals(0, new RobustReader(BAD_FILE_PATH).readInt());
  }
  @Test public void openPath() {
    assertNotNull(ClassInfo.make(GOOD_FILE_PATH));
  }

  public static class EmptyClass {
    // Empty
  }

  @Test public void openEmptyClass() {
    assertNotNull(ClassInfo.make(EmptyClass.class));
  }
  @Test public void openPathCLASSFILES() {
    assertNotNull(ClassInfo.make(this.getClass()));
  }
  @Test public void openPathCLASSFILESBadPath() {
    assertNull(ClassInfo.make(BAD_FILE_PATH));
  }
  @Test public void findSelf() {
    assertNotNull(this.getClass());
  }
  @Test public void openSelf() {
    assertNotNull(CLASSFILES.open(this.getClass()));
  }
  @Test public void openSelfNoErrors() {
    final Builder b = new ClassInfo.Builder(this.getClass());
    b.go();
    assertFalse(b.hasErrors());
  }
  @Test public void selfMakeNotNull() {
    assertNotNull(ClassInfo.make(this.getClass()));
  }
  @Test public void selfSourceNotNul() {
    assertNotNull(ClassInfo.make(this.getClass()).source);
  }
  @Test public void selfSource() {
    assertEquals(this.getClass().getSimpleName() + ".java", ClassInfo.make(this.getClass()).source);
  }
  @Test public void meFlags() {
    assertEquals(Modifier.PUBLIC | Modifier.SYNCHRONIZED, ClassInfo.make(this.getClass()).flags);
  }
  @Test public void meName() {
    assertEquals(this.getClass().getCanonicalName(), ClassInfo.make(this.getClass()).name);
  }
  @Test public void meSuperClassName() {
    assertEquals(Object.class.getCanonicalName(), ClassInfo.make(this.getClass()).superClass);
  }
  @Test public void objecClassCreate() {
    assertNotNull(ClassInfo.make(Object.class));
  }
  @Test public void objectSuperClassName() {
    assertNull(ClassInfo.make(Object.class).superClass);
  }
  @Test public void interfaceSuperClassName() {
    assertEquals(Object.class.getCanonicalName(), ClassInfo.make(Serializable.class).superClass);
  }
  @Test public void meHasOneInteface() {
    assertEquals(1, ClassInfo.make(this.getClass()).interfaces.length);
  }
  @Test public void meHasCorrectInteface() {
    final ClassInfo _ = ClassInfo.make(this.getClass());
    assertEquals(Serializable.class.getCanonicalName(), _.interfaces[0].toString());
  }
  @Test public void emptyClassFlags() {
    assertEquals(Modifier.SYNCHRONIZED | Modifier.PUBLIC, ClassInfo.make(EmptyClass.class).flags);
  }
  @Test public void emptyClassNFields() {
    assertEquals(0, ClassInfo.make(EmptyClass.class).fields.length);
  }
  @Test public void emptyClassNMethods() {
    assertEquals(0, ClassInfo.make(EmptyClass.class).methods.length);
  }
  @Test public void emptyClassDefaultConstructorName() {
    assertEquals("", ClassInfo.make(EmptyClass.class).constructors[0].name);
  }
  @Test public void emptyClassDefaultConstructorType() {
    assertEquals("()", ClassInfo.make(EmptyClass.class).constructors[0].type.toString());
  }
  @Test public void emptyClassDGetConstructors() {
    assertNotNull("()", ClassInfo.make(EmptyClass.class).constructors);
  }
  @Test public void emptyClassDefaultConstructorgFlags() {
    assertEquals(Modifier.PUBLIC, ClassInfo.make(EmptyClass.class).constructors[0].flags);
  }
  @Test public void emptyClassDefaultConstructorAttributesLength() {
    assertEquals(1, ClassInfo.make(EmptyClass.class).constructors[0].attributes.length);
  }
  @Test public void emptyClassDefaultConstructorAttributesName() {
    assertEquals("Code", ClassInfo.make(EmptyClass.class).constructors[0].attributes[0].name);
  }
  @Test public void emptyClassDefaultConstructorIsSynthetic() {
    assertFalse(ClassInfo.make(EmptyClass.class).constructors[0].isSynthetic());
  }
  @Test public void emptyClassDefaultConstructorFlags() {
    assertEquals(Modifier.PUBLIC, ClassInfo.make(EmptyClass.class).constructors[0].flags);
  }
  @Test public void emptyClassNInterfaces() {
    assertEquals(0, ClassInfo.make(EmptyClass.class).interfaces.length);
  }

  public static class EmptyClassOneMethod {
    void empty() throws IOException {
      throw new IOException();
    }
  }

  @Test public void emptyClassOneMethodNFields() {
    assertEquals(0, ClassInfo.make(EmptyClassOneMethod.class).fields.length);
  }
  @Test public void emptyClassOneMethodNMethods() {
    assertEquals(1, ClassInfo.make(EmptyClassOneMethod.class).methods.length);
  }

  public static class OneField {
    @Deprecated public final String fieldName = null;
  }

  @Test public void oneFieldFieldCount() {
    assertEquals(1, ClassInfo.make(OneField.class).fields.length);
  }
  @Test public void oneFieldFlags() {
    final int flags = ClassInfo.make(OneField.class).fields[0].flags;
    assertEquals(Modifier.FINAL | Modifier.PUBLIC | FlaggedEntity.DEPRECATED, flags);
  }
  @Test public void nameOneField() {
    final ClassInfo _ = ClassInfo.make(OneField.class);
    assertEquals("fieldName", _.fields[0].name);
  }

  public static class TwoAnnotatedFields {
    @External @Deprecated static private final String firstField = null;
    @Deprecated final String secondField = null;
  }

  @Test public void namesTwoAnnotatedFields() {
    final NamedEntity[] fields = ClassInfo.make(TwoAnnotatedFields.class).fields;
    assertEquals("firstField", fields[0].name);
    assertEquals("secondField", fields[1].name);
  }

  public static class TwoFields {
    static final String firstField = null;
    private final String secondField = null;

    public String getSecondField() {
      return secondField;
    }
  }

  @Test public void twoFieldsAreNotPrimtive() {
    for (final TypedEntity f : ClassInfo.make(TwoFields.class).fields)
      assertFalse(f.type.isPrimitive());
  }
  @Test public void twoFieldsFlags() {
    final FlaggedEntity[] fields = ClassInfo.make(TwoFields.class).fields;
    assertEquals(Modifier.FINAL | Modifier.STATIC, fields[0].flags);
    assertEquals(Modifier.FINAL | Modifier.PRIVATE, fields[1].flags);
  }
  @Test public void namesTwoFields() {
    final NamedEntity[] fields = ClassInfo.make(TwoFields.class).fields;
    assertEquals("firstField", fields[0].name);
    assertEquals("secondField", fields[1].name);
  }

  public static class AllPrimitiveFields {
    public final byte firstField = (byte) hashCode();
    public final short secondField = (short) hashCode();
    public final long thirdField = (byte) hashCode();
    public final float fourthField = hashCode();
    public final double fifthField = hashCode();
    public final char sixthField = toString().charAt(0);
    public final boolean seventhField = hashCode() > 100;
  }

  @Test public void allPrimitiveFieldsCount() {
    final NamedEntity[] fields = ClassInfo.make(AllPrimitiveFields.class).fields;
    assertEquals(7, fields.length);
  }
  @Test public void allPrimitiveFieldsArePrimitive() {
    for (final TypedEntity f : ClassInfo.make(AllPrimitiveFields.class).fields)
      assertTrue(f.type.isPrimitive());
  }
  @Test public void allPrimitiveFieldsNames() {
    final NamedEntity[] fields = ClassInfo.make(AllPrimitiveFields.class).fields;
    assertEquals("firstField", fields[0].name);
    assertEquals("secondField", fields[1].name);
    assertEquals("thirdField", fields[2].name);
    assertEquals("fourthField", fields[3].name);
    assertEquals("fifthField", fields[4].name);
    assertEquals("sixthField", fields[5].name);
  }
  @Test public void allPrimitiveFieldsModifiers() {
    final FlaggedEntity[] fields = ClassInfo.make(AllPrimitiveFields.class).fields;
    for (final FlaggedEntity f : fields)
      assertEquals(Modifier.FINAL | Modifier.PUBLIC, f.flags);
  }
  @Test public void allPrimitiveFieldsAttributes() {
    final FlaggedEntity[] fields = ClassInfo.make(AllPrimitiveFields.class).fields;
    for (final FlaggedEntity f : fields)
      assertEquals(0, f.attributes.length);
  }
  @Test public void allPrimitiveFieldsTypes() {
    final TypedEntity[] fields = ClassInfo.make(AllPrimitiveFields.class).fields;
    assertEquals("byte", fields[0].type.toString());
    assertEquals("short", fields[1].type.toString());
    assertEquals("long", fields[2].type.toString());
    assertEquals("float", fields[3].type.toString());
    assertEquals("double", fields[4].type.toString());
    assertEquals("char", fields[5].type.toString());
    assertEquals("boolean", fields[6].type.toString());
  }

  public static abstract class AllAccessLevels {
    private void f1() {
      f1();
    }
    void f2() {
      f1();
    }
    protected abstract void f3();
    public abstract void f4();
  }

  @Test public void allAccessLevels() {
    final TypedEntity[] methods = ClassInfo.make(AllAccessLevels.class).methods;
    assertTrue(methods[0].isPrivate());
    assertTrue(methods[1].isDefault());
    assertTrue(methods[2].isProtected());
    assertTrue(methods[3].isPublic());
  }
  @Test public void allAccessLevelsNegative() {
    final TypedEntity[] methods = ClassInfo.make(AllAccessLevels.class).methods;
    assertFalse(methods[1].isPrivate());
    assertFalse(methods[2].isPrivate());
    assertFalse(methods[3].isPrivate());
    assertFalse(methods[0].isDefault());
    assertFalse(methods[2].isDefault());
    assertFalse(methods[3].isDefault());
    assertFalse(methods[0].isProtected());
    assertFalse(methods[1].isProtected());
    assertFalse(methods[3].isProtected());
    assertFalse(methods[0].isPublic());
    assertFalse(methods[1].isPublic());
    assertFalse(methods[2].isPublic());
  }

  public static class OneStaticMethod {
    public final static String methodName() {
      return null;
    }
  }

  @Test public void oneStaticMethodSize() {
    assertEquals(1, ClassInfo.make(OneMethod.class).methods.length);
  }

  public static class OneMethod {
    public final Object methodName() {
      return this;
    }
  }

  @Test public void oneMethodSize() {
    assertEquals(1, ClassInfo.make(OneMethod.class).methods.length);
  }
  @Test public void oneMethodFlags() {
    final int flags = ClassInfo.make(OneMethod.class).methods[0].flags;
    assertEquals(Modifier.FINAL | Modifier.PUBLIC, flags);
  }
  @Test public void oneMethodLengths() {
    final ClassInfo _ = ClassInfo.make(OneMethod.class);
    assertEquals(1, _.constructors.length);
    assertEquals(1, _.methods.length);
    assertEquals(0, _.fields.length);
    assertEquals(0, _.initializers.length);
  }
  @Test public void oneMethodName() {
    final ClassInfo _ = ClassInfo.make(OneMethod.class);
    assertEquals("", _.constructors[0].name);
    assertEquals("methodName", _.methods[0].name);
  }

  public static abstract class TwoMethods {
    abstract String firstMethod(int a, int b);
    public static final String secondMethod() {
      return null;
    }
  }

  @Test public void twoMethodLengths() {
    final ClassInfo _ = ClassInfo.make(OneMethod.class);
    assertEquals(1, _.constructors.length);
    assertEquals(1, _.methods.length);
    assertEquals(0, _.fields.length);
    assertEquals(0, _.initializers.length);
  }
  @Test public void twoMethodsCount() {
    assertEquals(2, ClassInfo.make(TwoMethods.class).methods.length);
  }
  @Test public void twoMethodsFlags() {
    final FlaggedEntity[] methods = ClassInfo.make(TwoMethods.class).methods;
    assertEquals(Modifier.ABSTRACT, methods[0].flags);
    assertEquals(Modifier.FINAL | Modifier.PUBLIC | Modifier.STATIC, methods[1].flags);
  }
  @Test public void nameTwoMethods() {
    final ClassInfo _ = ClassInfo.make(TwoMethods.class);
    assertEquals("", _.constructors[0].name);
    assertEquals("firstMethod", _.methods[0].name);
    assertEquals("secondMethod", _.methods[1].name);
  }

  public static abstract class Initializer {
    {
      System.getenv();
      System.getenv("PATH");
      for (final String key : System.getenv().keySet())
        if (key != null)
          System.getenv(key);
    }

    Initializer() {
      // Empty constructor
    }
    Initializer(final String s) {
      System.getenv(s);
    }
  }

  @Test public void InitializerConstructors() {
    final NamedEntity[] ctros = ClassInfo.make(Initializer.class).constructors;
    assertEquals(2, ctros.length);
    assertEquals("", ctros[0].name);
    assertEquals("", ctros[1].name);
  }

  public static abstract class SingleStaticInitializer {
    static {
      System.getenv();
      System.getenv("PATH");
      for (final String key : System.getenv().keySet())
        if (key != null)
          System.getenv(key);
    }
  }

  @Test public void singleStaticInitilizerLengths() {
    final ClassInfo _ = ClassInfo.make(SingleStaticInitializer.class);
    assertEquals(1, _.constructors.length);
    assertEquals(0, _.methods.length);
    assertEquals(0, _.fields.length);
    assertEquals(1, _.initializers.length);
  }

  public static abstract class StaticInitializer {
    static {
      System.getenv();
      System.getenv("PATH");
      for (final String key : System.getenv().keySet())
        if (key != null)
          System.getenv(key);
    }

    StaticInitializer() {
    }
    StaticInitializer(final String s) {
      System.getenv(s);
    }
  }

  @Test public void staticInitializerType() {
    final InitializerInfo[] initializers = ClassInfo.make(StaticInitializer.class).initializers;
    assertEquals("()", initializers[0].type.toString());
  }
  @Test public void staticInitializerIsStatic() {
    final InitializerInfo[] initializers = ClassInfo.make(StaticInitializer.class).initializers;
    assertTrue(initializers[0].isStatic());
  }
  @Test public void staticInitializerIsPrivate() {
    final InitializerInfo[] initializers = ClassInfo.make(StaticInitializer.class).initializers;
    assertTrue(initializers[0].isDefault());
  }
  @Test public void staticInitializerNames() {
    final ClassInfo _ = ClassInfo.make(StaticInitializer.class);
    assertEquals(0, _.methods.length);
    assertEquals("", _.initializers[0].name);
    assertEquals("", _.constructors[0].name);
    assertEquals("", _.constructors[1].name);
  }

  public static abstract class MethodWithParametrs {
    public MethodWithParametrs() {
      super();
    }
    public final Integer method(final int a, final int b, final int c) {
      return new Integer(a + b + c + hashCode());
    }
  }

  @Test public void methodWithParameters() {
    final TypedEntity method = ClassInfo.make(MethodWithParametrs.class).methods[0];
    assertEquals("method", method.name);
    assertEquals("(III)Ljava/lang/Integer;", method.descriptor);
  }
  @Test public void emptyClassDescriptor() {
    final TypedEntity constructor = ClassInfo.make(EmptyClass.class).constructors[0];
    assertEquals("", constructor.name);
    assertEquals("()V", constructor.descriptor);
  }

  public static class IntField {
    int fieldName = 5;
  }

  @Test public void intFieldName() {
    final TypedEntity field = ClassInfo.make(IntField.class).fields[0];
    assertEquals("fieldName", field.name);
    assertEquals("I", field.descriptor);
    assertEquals("int", field.type.toString());
  }

  public static class ObjectField {
    Object fieldName = new Object();
  }

  @Test public void objectFieldName() {
    final TypedEntity field = ClassInfo.make(ObjectField.class).fields[0];
    assertEquals("fieldName", field.name);
    assertEquals("Ljava/lang/Object;", field.descriptor);
    assertEquals("java.lang.Object", field.type.toString());
  }

  public static class ClassWithCapitalL {
    ClassWithCapitalL fieldName = new ClassWithCapitalL();
  }

  @Test public void classWithCapitalL() {
    final TypedEntity field = ClassInfo.make(ClassWithCapitalL.class).fields[0];
    assertEquals("fieldName", field.name);
    assertEquals("Lil/ac/technion/cs/ssdl/classfiles/reify/ClassFileTest$ClassWithCapitalL;", field.descriptor);
    assertEquals("il.ac.technion.cs.ssdl.classfiles.reify.ClassFileTest$ClassWithCapitalL", field.type.toString());
  }

  public static class ArrayField {
    int[][][][] fieldName = null;
  }

  @Test public void arrayField() {
    final TypedEntity field = ClassInfo.make(ArrayField.class).fields[0];
    assertEquals("fieldName", field.name);
    assertEquals("[[[[I", field.descriptor);
    assertEquals("int[][][][]", field.type.toString());
  }

  public static class VoidMethod {
    public final void voidMethod() {
      voidMethod();
    }
  }

  @Test public void voidMethod() {
    assertEquals("()V", ClassInfo.make(VoidMethod.class).methods[0].descriptor);
    assertEquals("void ()", ClassInfo.make(VoidMethod.class).methods[0].type.toString());
  }

  public static class ComplexMethod {
    public final Object[][][] m(final Void v, final Object a, final int b, final float c, final double[][] d) {
      ____.unused(v, a, Box.it(b), Box.it(c));
      return new Object[d.length][][];
    }
  }

  @Test public void complexMethod() {
    assertEquals("java.lang.Object[][][] (java.lang.Void, java.lang.Object, int, float, double[][])",
        ClassInfo.make(ComplexMethod.class).methods[0].type.toString());
  }

  interface Interface {
    public void method0();
  }

  @Test public void interfaceMethod() {
    final ClassInfo _ = ClassInfo.make(Interface.class);
    assertEquals(1, _.methods.length);
    assertEquals(0, _.constructors.length);
    assertEquals(0, _.initializers.length);
    assertTrue(_.methods[0].isAbstract());
    assertTrue(_.methods[0].isPublic());
  }
  @Test public void interfaceClassFile() {
    final ClassInfo _ = ClassInfo.make(Interface.class);
    assertTrue(_.isAbstract());
    assertTrue(_.isInterface());
  }
  @Test public void emptlyClassInteface() {
    final ClassInfo _ = ClassInfo.make(EmptyClass.class);
    assertFalse(_.isAbstract());
    assertFalse(_.isInterface());
    assertTrue(_.isClass());
  }

  enum Enum {
    ;
    public void myMethod() {
      myMethod();
    }
  }

  @Test public void enumConstructorsCounts() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertEquals(1, _.constructors.length);
  }
  @Test public void enumMethodCounts() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertEquals(2, _.methods.length);
    assertEquals(0, _.initializers.length);
  }
  @Test public void enumMethodName() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertEquals("myMethod", _.methods[0].name);
    assertEquals("values", _.methods[1].name);
    assertTrue(_.methods[1].isSynthetic());
  }
  @Test public void enumMethod() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertEquals(1, _.methods.length);
    assertEquals(0, _.constructors.length);
    assertEquals(0, _.initializers.length);
    assertTrue(_.methods[0].isAbstract());
    assertTrue(_.methods[0].isPublic());
  }
  @Test public void enumClassFile() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertFalse(_.isAbstract());
    assertFalse(_.isInterface());
    assertFalse(_.isStatic());
  }
  @Test public void enumClassFileIsEnum() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertTrue(_.isEnum());
  }
  @Test public void enumClassFileIsAnnotation() {
    final ClassInfo _ = ClassInfo.make(Enum.class);
    assertFalse(_.isAnnotation());
  }

  @interface Annotation {
    int f();
  }

  @Test public void annotationFlags() {
    assertEquals(0, ClassInfo.make(Annotation.class).flags & ~(Modifier.INTERFACE | Modifier.ABSTRACT | FlaggedEntity.ANNOTATION));
  }
  @Test public void annotationIsEnum() {
    assertFalse(ClassInfo.make(Annotation.class).isEnum());
  }
  @Test public void annotationCounts() {
    final ClassInfo _ = ClassInfo.make(Annotation.class);
    assertEquals(0, _.constructors.length);
    assertEquals(1, _.methods.length);
    assertEquals(0, _.fields.length);
    assertEquals(0, _.initializers.length);
  }
  @Test public void annotationIsAnnotation() {
    final ClassInfo _ = ClassInfo.make(Annotation.class);
    assertTrue(_.isAnnotation());
  }
  @Test public void isClass() {
    assertFalse(ClassInfo.make(Annotation.class).isClass());
    assertFalse(ClassInfo.make(Enum.class).isClass());
    assertFalse(ClassInfo.make(Interface.class).isClass());
    assertTrue(ClassInfo.make(EmptyClass.class).isClass());
  }
  @Test public void emptyClassIsDeprecated() {
    assertFalse(ClassInfo.make(EmptyClass.class).isDeprecated());
    assertFalse(ClassInfo.make(EmptyClass.class).constructors[0].isDeprecated());
  }

  @Deprecated public static class DeprecatedClass {
    @Deprecated DeprecatedClass() {
      super();
    }
  }

  @Test public void deprecatedClassIsDeprecated() {
    assertTrue(ClassInfo.make(DeprecatedClass.class).isDeprecated());
    assertTrue(ClassInfo.make(DeprecatedClass.class).constructors[0].isDeprecated());
  }
  @Test public void oneFieldIsDeperecated() {
    assertFalse(ClassInfo.make(OneField.class).isDeprecated());
  }
  @Test public void oneFieldFieldIsDeperecated() {
    assertTrue(ClassInfo.make(OneField.class).fields[0].isDeprecated());
  }
  @Test public void enumIsDeprecated() {
    assertFalse(ClassInfo.make(Enum.class).isDeprecated());
    assertFalse(ClassInfo.make(Enum.class).constructors[0].isDeprecated());
  }
  @Test public void interfaceIsDeprecated() {
    assertFalse(ClassInfo.make(Interface.class).isDeprecated());
  }
  @Test public void annotationIsDeprecated() {
    assertFalse(ClassInfo.make(Annotation.class).isDeprecated());
  }
  @Test public void deprecatedClassIsSynthetic() {
    assertFalse(ClassInfo.make(DeprecatedClass.class).isSynthetic());
    assertFalse(ClassInfo.make(DeprecatedClass.class).constructors[0].isSynthetic());
  }
  @Test public void oneFieldIsSynthetic() {
    assertFalse(ClassInfo.make(OneField.class).isSynthetic());
  }
  @Test public void oneFieldFieldIsSynthetic() {
    assertFalse(ClassInfo.make(OneField.class).fields[0].isSynthetic());
  }
  @Test public void enumIsSynthetic() {
    assertFalse(ClassInfo.make(Enum.class).isSynthetic());
  }
  @Test public void enumConstructorIsSynthetic() {
    assertTrue(ClassInfo.make(Enum.class).constructors[0].isSynthetic());
  }
  @Test public void interfaceIsSynthetic() {
    assertFalse(ClassInfo.make(Interface.class).isSynthetic());
  }
  @Test public void annotationIsSynthetic() {
    assertFalse(ClassInfo.make(Annotation.class).isSynthetic());
  }
  @Test public void omeMethoExceptions() {
    assertFalse(ClassInfo.make(Annotation.class).isSynthetic());
  }
  @Test public void emptyClassConstructorExceptions() {
    assertEquals(0, ClassInfo.make(EmptyClass.class).constructors[0].exceptions.length);
  }
  @Test public void noExceptionsCount() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        throw new IOException();
      }
    }
    assertEquals(2, ClassInfo.make(_.class).methods[0].exceptions.length);
  }
  @Test public void methodExceptionsLength() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        throw new IOException();
      }
    }
    assertEquals(2, ClassInfo.make(_.class).methods[0].exceptions.length);
  }
  @Test public void methodExceptionsName() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        throw new IOException();
      }
    }
    assertEquals(IOException.class.getName(), ClassInfo.make(_.class).methods[0].exceptions[0].toString());
    assertEquals(ArrayIndexOutOfBoundsException.class.getName(), ClassInfo.make(_.class).methods[0].exceptions[1].toString());
  }
  @Test public void codeAttributeExists() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        //
      }
    }
    assertNotNull(ClassInfo.make(_.class).methods[0].getCode());
  }
  @Test public void codeAttributeMaxStack() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        //
      }
    }
    assertEquals(0, ClassInfo.make(_.class).methods[0].getCode().maxStack);
  }
  @Test public void codeAttributeMaxLocals() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        //
      }
    }
    assertEquals(1, ClassInfo.make(_.class).methods[0].getCode().maxLocals);
  }
  @Test public void byteCodeCount() {
    class _ {
      @SuppressWarnings("unused")//
      void $() throws IOException, ArrayIndexOutOfBoundsException {
        //
      }
    }
    assertTrue(0 < ClassInfo.make(_.class).methods[0].getCode().codes.length);
  }
  @Test public void noByteCodeEmptyConstructor() {
    class _ {
      @SuppressWarnings("unused") _() {
      }
    }
    assertNull(ClassInfo.make(_.class).constructors[0].code);
  }
  @Test public void noByteCodeEmptyConstructorCallingSuper() {
    class _ {
      @SuppressWarnings("unused") _() {
        super();
      }
    }
    assertNull(ClassInfo.make(_.class).constructors[0].code);
  }
  @Test public void noByteCodeEmptyClassConstructor() {
    assertNull(ClassInfo.make(EmptyClass.class).constructors[0].code);
  }
  @Test public void parseByteCodesSelfFirstMethod() {
    final CodeEntity c = ClassInfo.make(this.getClass()).methods[0].getCode();
    assertTrue(0 < c.instructionsCount());
  }
  @Test public void parseSelfAllMethods() {
    for (final MethodInfo m : ClassInfo.make(this.getClass()).methods)
      m.getCode().instructionsCount();
  }
  @Test public void parseComplexMethod() {
    class ClassWithComplexMethod {
      @SuppressWarnings("unused") boolean __(final int _) {
        return _ % 1 == 0 ? __(_ * _) : __(_ % 7) != __(_ * _ % 3);
      }
    }
    ClassInfo.make(ClassWithComplexMethod.class).methods[0].getCode().instructionsCount();
  }

  static class LargeClass implements Serializable, Cloneable, Interface {
    private static final long serialVersionUID = 1L;

    @Override public void method0() {
      method1();
    }
    int method1() {
      return method2(11);
    }
    private int method2(final int i) {
      return i * i + field3;
    }
    static int method3() {
      return field5.nextInt();
    }
    static int method4() {
      return field5.nextInt();
    }
    static int method5() {
      return field5.nextInt();
    }
    static int method6() {
      return field5.nextInt();
    }

    private static int field1;
    private final int field2;
    private final int field3;
    static Random field4 = new Random();
    static {
      field4 = new Random(field4.nextLong());
      for (int i = 1; i < 10; i++)
        field4.nextDouble();
    }
    static Random field5 = new Random(field4.nextLong() + method3());
    static {
      for (int i = 1; i < 20; i++)
        field5.nextDouble();
    }
    static Random field6 = new Random(field5.nextLong());

    LargeClass(final int a) {
      field1 = field4.nextInt() + a;
      field2 = field5.nextInt() + field1;
      field3 = field6.nextInt() + field2;
    }
    public LargeClass(final int field1, final int field2, final int field3) {
      super();
      LargeClass.field1 = field1;
      this.field2 = field2;
      this.field3 = field3;
    }
    public LargeClass(final double d) {
      super();
      field1 = 1;
      field2 = (int) d;
      field3 = field2 + 3;
    }
    public LargeClass() {
      this(12);
    }
    public LargeClass(final byte a) {
      this(12 + a);
    }
    static int method7() {
      return field6.nextInt();
    }

    static {
      for (int i = 1; i < 20; i++)
        field4 = new Random(field6.nextLong());
    }
  }

  @Test public void complexClassCounts() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(1, c.initializersCount());
  }
  @Test public void complexClassConstructorCounts() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(5, c.constructorsCount());
  }
  @Test public void complexClassMethodNames() {
    final MethodInfo[] ms = ClassInfo.make(LargeClass.class).methods;
    int i = 0;
    assertEquals("method0", ms[i++].name);
    assertEquals("method1", ms[i++].name);
    assertEquals("method2", ms[i++].name);
    assertEquals("method3", ms[i++].name);
    assertEquals("method4", ms[i++].name);
    assertEquals("method5", ms[i++].name);
    assertEquals("method6", ms[i++].name);
    assertEquals("method7", ms[i++].name);
  }
  @Test public void complexClassMethodCounts() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(8, c.methodCount());
  }
  @Test public void complexClassFieldsCounts() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(6, c.fieldsCount());
  }
  @Test public void complexClassFieldsInterfaceCounts() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(3, c.interfacesCount());
  }
  @Test public void complexClassFileKind() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(Kind.CLASS, c.kind());
  }
  @Test public void complexClassFileAbstraction() {
    final ClassInfo c = ClassInfo.make(LargeClass.class);
    assertEquals(Abstraction.PLAIN, c.abstraction());
  }
  @Test public void parseTableSwitch() {
    class _ {
      @SuppressWarnings("unused") int tableSwitch(final int _) {
        int i = _;
        // i *= _ + 19 * _ - 41;
        for (int j = 0; j < 3; j++)
          switch (_) {
            case 49:
              return 19;
            case 50:
              return 23;
            case 51:
              i %= 131;
              return i * 37;
            case 52:
              return 29;
            case 53:
              i++;
              return i - 17;
            case 54:
              i *= i + 7;
              return i - 23;
            case 55:
              i <<= _;
              i += 2;
              return i * 41 << 3;
            case 56:
              return 7;
            case 57:
              i++;
              i++;
              i <<= _;
              i += 2;
              i %= 12;
              return i * 17 << 3;
            case 58:
              i++;
              i++;
              i++;
              i <<= _;
              i += 2;
              i %= 12;
              return i * 17 << 3;
            case 59:
              return i * 17 << 3;
            case 60:
              return 3;
            case 61:
              return 5;
            case 62:
              return 7;
            case 63:
              return 11;
            case 64:
              return 13;
            case 65:
              return 17;
            default:
              return 712;
          }
        return tableSwitch(_ * _ * 41 - 19);
      }
    }
    assertEquals("tableSwitch", ClassInfo.make(_.class).methods[0].name);
    assertTrue(20 < ClassInfo.make(_.class).methods[0].getCode().instructionsCount());
  }
  @Test public void parseByteCodesTwoMethods() {
    class _ {
      boolean __(final int _) {
        return _ % 1 == 0 ? __(_ * _) : __(_ % 7) != ___(_ * _ % 3);
      }
      boolean ___(final int _) {
        return _ == 1 == __(_);
      }
    }
    final CodeEntity c0 = ClassInfo.make(_.class).methods[0].getCode();
    final CodeEntity c1 = ClassInfo.make(_.class).methods[1].getCode();
    assertTrue(c0.instructionsCount() > c1.instructionsCount());
  }

  public static class Parser {
    int nFiles = 0;
    int nMethods = 0;
    int nCodes = 0;
    int nInstructions = 0;
    Stopper s = new Stopper();

    void parse(final String... path) {
      try {
        new ClassFilesVisitor(path, new FileOnlyAction() {
          @Override public void visitZip(final File zip) {
            zipFile = zip;
          }

          File zipFile = null;

          @Override public void visitZipEntry(final String entryName, final InputStream stream) {
            try {
              parse(ClassInfo.make(stream));
            } catch (final RuntimeException e) {
              System.out.println(zipFile + ":" + entryName);
            }
          }
          @Override public void visitFile(final File f) {
            try {
              parse(ClassInfo.make(f));
            } catch (final RuntimeException e) {
              System.out.println("\n** " + f.toString() + ": " + e);
            }
          }
        }).go();
      } catch (final IOException e) {
        e.printStackTrace();
      } catch (final StopTraversal e) {
        e.printStackTrace();
      }
    }
    void parse(final ClassInfo c) {
      parse(c.methods);
    }
    void parse(final MethodInfo[] methods) {
      nFiles++;
      for (final MethodInfo m : methods)
        parseMethod(m);
    }
    private void parseMethod(final MethodInfo m) {
      nMethods++;
      if (m.getCode() != null)
        parse(m.getCode());
    }
    private void parse(final CodeEntity c) {
      nCodes++;
      nInstructions += c.instructionsCount();
    }
    @Override public String toString() {
      final long stop = s.time();
      return String
          .format(
          //
              "Processed %s files, consisting of %s methods with %s code blocks\n" + //
                  "comprising a total of %s bytecode instructions in %s.", //
              Unit.INTEGER.format(nFiles), //
              Unit.INTEGER.format(nMethods), //
              Unit.INTEGER.format(nCodes), //
              Unit.INTEGER.format(nInstructions),//
              Unit.NANOSECONDS.format(stop)) //
          .toString();
    }
  }

  @Test public void parseAllProjectMethods() {
    final Parser p = new Parser();
    p.parse(".");
    System.out.println(p);
  }
  @Test public void parseAllPathMethods() {
    final Parser p = new Parser();
    p.parse(CLASSPATH.asArray());
    System.out.println(p);
  }
  @Test public void parseAllWorspace() {
    final Parser p = new Parser();
    p.parse("..");
    System.out.println(p);
  }
}
