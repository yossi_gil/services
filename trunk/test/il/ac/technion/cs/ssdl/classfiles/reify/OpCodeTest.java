/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

/**
 * @author Yossi Gil
 * @since 27 November 2011
 */
@SuppressWarnings("static-method") public class OpCodeTest {
  @Test public void tableSwitch() throws IOException {
    final BufferDataInputStream b = new BufferDataInputStream(new byte[] {//
        (byte) OpCode.TABLESWITCH.ordinal(), 0, 0, 0, //
            0, 0, 0, 0,//
            0, 0, 0, 55, //
            0, 0, 0, 56, //
            0, 0, 0, 1, //
            0, 0, 0, 1, //
        });
    OpCode.read(b);
    assertTrue(b.eof());
    b.close();
  }
  
  @Test public void tableSwitch1() throws IOException {
    final BufferDataInputStream b = new BufferDataInputStream(new byte[] {
        //
        0, (byte) OpCode.TABLESWITCH.ordinal(), 0, 0, //
        0, 0, 0, 0,//
        0, 0, 0, 55, //
        0, 0, 0, 56, //
        0, 0, 0, 1, //
        0, 0, 0, 1, //
    });
    assertEquals(OpCode.NOP, OpCode.read(b).opCode);
    assertEquals(OpCode.TABLESWITCH, OpCode.read(b).opCode);
    OpCode.read(b);
    assertTrue(b.eof());
    b.close();
  }

  @Test public void tableSwitch2() throws IOException {
    final BufferDataInputStream b = new BufferDataInputStream(new byte[] {
        //
        0, 0, (byte) OpCode.TABLESWITCH.ordinal(), 0, //
        0, 0, 0, 0,//
        0, 0, 0, 55, //
        0, 0, 0, 56, //
        0, 0, 0, 1, //
        0, 0, 0, 1, //
    });
    assertEquals(OpCode.NOP, OpCode.read(b).opCode);
    assertEquals(OpCode.NOP, OpCode.read(b).opCode);
    assertEquals(OpCode.TABLESWITCH, OpCode.read(b).opCode);
    assertTrue(b.eof());
    b.close();
  }

  @Test public void tableSwitch3() throws IOException {
    final BufferDataInputStream b = new BufferDataInputStream(new byte[] {
        //
        0, 0, 0, (byte) OpCode.TABLESWITCH.ordinal(), //
        0, 0, 0, 0,//
        0, 0, 0, 55, //
        0, 0, 0, 56, //
        0, 0, 0, 1, //
        0, 0, 0, 1, //
    });
    assertEquals(OpCode.NOP, OpCode.read(b).opCode);
    assertEquals(OpCode.NOP, OpCode.read(b).opCode);
    assertEquals(OpCode.NOP, OpCode.read(b).opCode);
    assertEquals(OpCode.TABLESWITCH, OpCode.read(b).opCode);
    assertTrue(b.eof());
    b.close();
  }
}
