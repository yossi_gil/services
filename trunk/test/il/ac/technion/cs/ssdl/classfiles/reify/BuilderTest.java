/**
 *
 */
package il.ac.technion.cs.ssdl.classfiles.reify;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import il.ac.technion.cs.ssdl.classfiles.CLASSFILES;
import il.ac.technion.cs.ssdl.classfiles.reify.ClassInfo.Builder;

import org.junit.Test;

/**
 * @author Yossi Gil
 * @since 25 November 2011
 */
@SuppressWarnings("static-method") public class BuilderTest {
  @Test public void self() {
    assertNotNull(new ClassInfo.Builder(this.getClass()));
  }
  @Test public void selfGo() {
    assertNotNull(new ClassInfo.Builder(this.getClass()).go());
  }
  @Test public void other() {
    assertNotNull(new ClassInfo.Builder(ClassFileTest.class));
  }
  @Test public void selfMakeNotNull() {
    assertNotNull(ClassInfo.make(this.getClass()));
  }
  @Test public void otherBuilderNoErrors() {
    assertFalse(new Builder(CLASSFILES.open(ClassFileTest.class)).hasErrors());
  }
  @Test public void constantPoolEntityBuilder() {
    final Builder b = new Builder(CLASSFILES.open(ClassFileTest.class));
    assertNotNull(new ConstantPoolEntity(b));
  }
  @Test public void constantPoolEntityStrings() {
    final Builder b = new Builder(CLASSFILES.open(ClassFileTest.class));
    final ConstantPoolEntity c = new ConstantPoolEntity(b);
    System.out.println(c.getReferencedStrings());
  }
  @Test public void constantPoolEntityUTF8() {
    final Builder b = new Builder(CLASSFILES.open(ClassFileTest.class));
    assertNotNull(new ConstantPoolEntity(b));
  }
  @Test public void classInfoFromBuilder() {
    final Builder b = new Builder(CLASSFILES.open(ClassFileTest.class));
    assertNotNull(new ClassInfo(b));
  }
  @Test public void otherBuilderNotNull() {
    final Builder b = new Builder(CLASSFILES.open(ClassFileTest.class));
    assertNotNull(b.go());
  }
  @Test public void otherMakeNotNull() {
    assertNotNull(ClassInfo.make(ClassFileTest.class));
  }
}
