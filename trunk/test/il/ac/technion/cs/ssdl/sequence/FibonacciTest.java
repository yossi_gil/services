package il.ac.technion.cs.ssdl.sequence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

@SuppressWarnings("static-method") public class FibonacciTest {
  @Test public void testCreate() {
    assertTrue(new Fibonacci().more());
  }
  @Test public void testCreateThreshold() {
    assertTrue(new Fibonacci(1000).more());
  }
  @Test public void testFirst() {
    assertEquals(1, new Fibonacci(1000).current());
  }
  @Test public void testSecond() {
    assertEquals(2, new Fibonacci(1000).advance().current());
  }
  @Test public void testThird() {
    assertEquals(3, new Fibonacci(1000).advance().advance().current());
  }
  @Test public void testFourth() {
    assertEquals(5, new Fibonacci(1000).advance().advance().advance().current());
  }
  @Test public void testFifth() {
    assertEquals(8, new Fibonacci(1000).advance().advance().advance().advance().current());
  }
}
