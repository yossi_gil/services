package il.ac.technion.cs.ssdl.utils;

import static il.ac.technion.cs.ssdl.testing.Assert.assertEquals;

import org.junit.Test;

@SuppressWarnings("static-method") public class PairTest {
  @SuppressWarnings("all") public static class NamedPair extends Pair<Integer, Integer> {
    public final String name;
    
    public NamedPair(final String name, final Integer a, final Integer b) {
      super(a, b);
      this.name = name;
    }
    public String toString() {
      return name + super.toString();
    }
    public boolean equals(final Pair<Integer, Integer> o) {
      if (o == this)
        return true;
      if (o == null)
        return false;
      if (!(o instanceof NamedPair))
        return false;
      final NamedPair that = (NamedPair) o;
      return first.equals(that.first) && second.equals(that.second) && name.equals(that.name);
    }
  }
  
  @Test public void testSymmetry() {
    final Pair<Integer, Integer> p1 = new NamedPair("a", Integer.valueOf(1001), Integer.valueOf(-5017));
    final Pair<Integer, Integer> p2 = new Pair<Integer, Integer>(Integer.valueOf(1001), Integer.valueOf(-5017));
    assertEquals(p1.equals(p2), p2.equals(p1));
  }
}
