package il.ac.technion.cs.ssdl.reflection;

import static il.ac.technion.cs.ssdl.reflection.ClassPredicates.isImmutable;
import static il.ac.technion.cs.ssdl.reflection.ClassPredicates.nFields;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import il.ac.technion.cs.ssdl.utils.Box;

import org.junit.Test;

@SuppressWarnings("static-method") public class ClassPredicatesTest {
  @Test public void testIsImmutableTrue() {
    class A {
      final int a = 3;
      final Object o = null;
      
      int b() {
        return a + (o == null ? 1 : 2);
      }
    }
    new A().b();
    assertTrue(isImmutable(new A().getClass()));
  }
  @Test public void testIsImmutableInheritingTrue() {
    class A {
      final int a = 3;
      final Object o = null;
      
      int b() {
        return a + (o == null ? 1 : 2);
      }
    }
    class B extends A {
      // Empty local class
    }
    new A().b(); // Use the class so that it does not eliminated by
    // Eclipse's "Save Actions".
    assertTrue(isImmutable(new B().getClass()));
  }
  @Test public void testIsImmutableFalse() {
    class A {
      int a = 3;
      final Object o = null;
      
      int b() {
        a = 2 * a;
        return a + (o == null ? 1 : 2);
      }
    }
    new A().b();
    assertFalse(isImmutable(new A().getClass()));
  }
  
  public static class ImmutableStatic {
    final int a = 3;
    final Object o = null;
  }
  
  @Test public void testImmutableStatic() {
    assertTrue(isImmutable(ImmutableStatic.class));
  }
  
  public static class MutableStatic {
    int a = 3;
    Object o = null;
  }
  
  @Test public void testMutableStatic() {
    assertFalse(isImmutable(MutableStatic.class));
  }
  
  public static class MutableInhertingFromImmutable extends ImmutableStatic {
    int c = 3;
    Object object = null;
  }
  
  @Test public void testMutableInhertingFromImmutable() {
    assertFalse(isImmutable(MutableInhertingFromImmutable.class));
  }
  
  public static class ImmmutableInhertingFromMmutable extends MutableStatic {
    final int b = 3;
    final Object object = null;
  }
  
  @Test public void testImmmutableInhertingFromMmutable() {
    assertFalse(isImmutable(ImmmutableInhertingFromMmutable.class));
  }
  @Test public void testObject() {
    assertFalse(isImmutable(Object.class));
  }
  
  static class EmptyStaticClass {
    // Empty
  }
  
  @Test public void testEmptyStaticClass() {
    assertFalse(isImmutable(EmptyStaticClass.class));
  }
  
  class EmptyNonStaticClass {
    // Empty
  }
  
  @Test public void testEmptyNonStaticClassFields() {
    assertEquals(0, nFields(EmptyNonStaticClass.class));
  }
  @Test public void testEmptyNonStaticClass() {
    assertFalse(isImmutable(EmptyNonStaticClass.class));
  }
  
  static class StaticUtilityClass {
    static final int a = 19;
  }
  
  @Test public void testStaticUtilityClassFields() {
    assertEquals(0, nFields(StaticUtilityClass.class));
  }
  @Test public void testStaticUtilityClassImmutable() {
    assertFalse(isImmutable(StaticUtilityClass.class));
  }
  
  static enum EmptyEnum {
    ;
    int f() {
      return 2;
    }
    
    public int x;
  }
  
  @Test public void testEmptyEnumFields() {
    assertEquals(3, nFields(EmptyEnum.class));
  }
  @Test public void testEmptyEnumImmutable() {
    assertFalse(isImmutable(EmptyEnum.class));
  }
  @Test public void testBoxFields() {
    Explore.go(Box.class.getSuperclass());
    assertEquals(2, nFields(Box.class));
  }
  @Test public void testBoxImmutable() {
    assertFalse(isImmutable(Box.class));
  }
  
  static enum NonEmptyEnum {
    A, B, C;
    int f() {
      return ordinal();
    }
    
    int a;
  }
}
